<?php
//for main.js modal files
define("_MAINJS_ATTENTION", "Attention");
define("_MAINJS_ENTER_BELOW_INFO", "Please enter the following details");
define("_MAINJS_ENTER_USERNAME", "Please enter your username");
define("_MAINJS_ENTER_EMAIL", "Please enter your email address");
define("_MAINJS_ENTER_ICNO", "Please enter your ID number");
define("_MAINJS_SELECT_COUNTRY", "Please choose your country");
define("_MAINJS_ENTER_PHONENO", "Please enter your phone number");
define("_MAINJS_ENTER_PASSWORD", "Please enter your password");
define("_MAINJS_ENTER_CONFIRM_PASSWORD", "Please re-enter your password");
define("_MAINJS_ACCEPT_TERMS", "Please accept the terms and conditions");
define("_MAINJS_ICNO_WRONG_", "The ID number format is wrong");
define("_MAINJS_ENTER_CURRENT_PASSWORD", "Please enter your current password");
define("_MAINJS_ENTER_NEW_PASSWORD", "Please enter your new password");
define("_MAINJS_REENTER_NEW_PASSWORD", "Please re-enter your new password");
define("_MAINJS_VALID_EMAIL_ADDRESS", "Please enter valid email address");
define("_MAINJS_VALID_PASSWORD_LENGTH", "password length must be more than 5");
define("_MAINJS_COUNTRY_NULL_", "must insert country");