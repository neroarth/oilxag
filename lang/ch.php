<?php
//for main.js modal files
define("_MAINJS_ATTENTION", "注意");
define("_MAINJS_ENTER_BELOW_INFO", "请输入以下详细信息");
define("_MAINJS_ENTER_USERNAME", "请输入您的用户名");
define("_MAINJS_ENTER_EMAIL", "请输入您的电子邮件地址");
define("_MAINJS_ENTER_ICNO", "请输入您的身份证号码");
define("_MAINJS_SELECT_COUNTRY", "请选择你的国家");
define("_MAINJS_ENTER_PHONENO", "请输入您的电话号码");
define("_MAINJS_ENTER_PASSWORD", "请输入您的密码");
define("_MAINJS_ENTER_CONFIRM_PASSWORD", "请重新输入您的密码");
define("_MAINJS_ACCEPT_TERMS", "请接受条款和条件");
define("_MAINJS_ICNO_WRONG_", "身份证号码不正确");
define("_MAINJS_ENTER_CURRENT_PASSWORD", "请输入您当前的密码");
define("_MAINJS_ENTER_NEW_PASSWORD", "请输入您的新密码");
define("_MAINJS_REENTER_NEW_PASSWORD", "请重新输入您的新密码");
// need translation
define("_MAINJS_VALID_EMAIL_ADDRESS", "Please enter valid email address");
define("_MAINJS_VALID_PASSWORD_LENGTH", "password length must be more than 5");
define("_MAINJS_COUNTRY_NULL_", "must insert country");