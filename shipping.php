<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/Shipping.php';

require_once dirname(__FILE__) . '/classes/GroupCommission.php';
require_once dirname(__FILE__) . '/classes/TransactionHistory.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $subtotal = rewrite($_POST["insert_subtotal"]);
    $total = rewrite($_POST["insert_total"]);
    $payment_status = rewrite($_POST["insert_paymentstatus"]);
}

$userOrder = getOrders($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$orderDetails = $userOrder[0];

if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
    $productListHtml = getShoppingCart($conn,2);
}else
{}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://dcksupreme.asia/information.php" />
<meta property="og:title" content="Information | DCK Supreme" />
<title>Information | DCK Supreme</title>
<meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
<meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
<meta name="keywords" content="DCK®,dck, dck supreme, supreme, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
noisiness and temperature, dry cold start,etc">
<link rel="canonical" href="https://dcksupreme.asia/information.php" />
<?php include 'css.php'; ?>
<?php require_once dirname(__FILE__) . '/header.php'; ?>
</head>

<body class="body">

<!-- Start Menu -->
<?php include 'header-sherry.php'; ?>

<form method="POST"  action="utilities/shippingDetail.php">
<div class="flex-container">          
    <div class="left-status-div">
    <!-- Start of Check Out Status Div-->
        <div class="check-out-status">
                <div class="status-container">
                    <a href="viewCart.php" class="status-a">
                        <div class="black-round-div"><img src="img/tick2.png" class="yellow-tick" alt="Completed" title="Completed"></div>
                        CART
                    </a>
                </div>

            <div class="status-div">></div>

                <div class="status-container">
                    <a href="checkout.php" class="status-a">
                        <div class="black-round-div"><img src="img/tick2.png" class="yellow-tick" alt="Completed" title="Completed"></div>
                        INFORMATION
                    </a>
                </div>

            <div class="status-div">></div>

                <div class="status-container">        
                    <div class="white-round-div">3</div>
                    SHIPPING
                </div>        

            <div class="status-div">></div>

                <div class="status-container">        
                    <div class="white-round-div">4</div>
                    PAYMENT
                </div>   
        </div>
    <!-- End of Check Out Status Div-->

        <div class="clear"></div>
        <p class="info-title spacing2"><b>Ship To</b></p> 
        <table class="profile-table">
        	<tr class="profile-tr">
            	<td class="profile-td1">Name</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><?php echo $orderDetails->getName();?></td>
            </tr>
        	<tr class="profile-tr">
            	<td class="profile-td1">Contact</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><?php echo $orderDetails->getContactNo();?></td>
            </tr>            
        	<tr class="profile-tr">
            	<td class="profile-td1">Address</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3">
                    <?php echo $orderDetails->getAddressLine1();?>
                </td>
            </tr>      
            <tr class="profile-tr">
            	<td class="profile-td1"></td>
                <td class="profile-td2"></td>
                <td class="profile-td3">
                    <?php echo $orderDetails->getAddressLine2();?>
                </td>
            </tr>
            <tr class="profile-tr">
            	<td class="profile-td1"></td>
                <td class="profile-td2"></td>
                <td class="profile-td3">
                    <?php echo $orderDetails->getCity();?>
                    <?php echo $orderDetails->getZipcode();?>
                    <?php echo $orderDetails->getState();?>
                    <?php echo $orderDetails->getCountry();?>
                </td>
            </tr>       
                   
        </table>

        <input class="clean white-input two-box-input" type="hidden" 
                id="insert_paymentstatus" name="insert_paymentstatus" value="pending">

        <!-- <input class="clean white-input two-box-input" type="hidden" 
                id="insert_shippingstatus" name="insert_shippingstatus" value="1"> -->

        <!-- <div class="white-input-div">
        	<p class="grey-p">Contact</p>
            <input class="clean white-input middle-input" type="email" placeholder="Email">
        </div> -->


        <!-- <div class="white-input-div">
            <p class="grey-p">Ship to</p>
            <input class="clean white-input middle-input" type="text" placeholder="Address">
        </div>  -->

        <!-- <p class="info-title spacing2"><b>SHIPPING METHOD</b></p>  
        <div class="white-input-div select-div-set">  
            <select class="clean white-select">
                <option>POSLAJU MALAYSIA (Semenanjung) - RM 7.00</option>
            </select>  
        </div> -->

        <div class="clear"></div>

        <div class="cart-bottom-div spacing2">
            <div class="left-cart-bottom-div">
                <p class="continue-shopping pointer continue2"><a href="checkout.php" class="black-white-link"><img src="img/back.png" class="back-btn" alt="back" title="back" > Return</a></p>
            </div>
            <div class="right-cart-div">
                <button class="clean black-button add-to-cart-btn checkout-btn continue2 add-to-cart-btn2">NEXT</button>
            </div>
        </div>

    </div>	

    <div class="right-status-div">      
        <?php echo $productListHtml; ?>
    </div>	

</div>    
</form>

<?php include 'js.php'; ?>

</body>
</html>