<?php
  

require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php'; 
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    //todo create table for transaction_history with at least a column for quantity(in order table),product_id(in order table), order_id, status (others can refer to btcw's), target_uid, trigger_transaction_id
    //todo create table for order and product_order
    $totalProductCount = count($_POST['product-list-id-input']);
    for($i = 0; $i < $totalProductCount; $i++){
        $productId = $_POST['product-list-id-input'][$i];
        $quantity = $_POST['product-list-quantity-input'][$i];

        echo " this: $productId total: $quantity";
    }
}

$products = getProduct($conn);

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/adminPayout.php" />
    <meta property="og:title" content="Payout | DCK Supreme" />
    <title>Payout | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/adminPayout.php" />
    <?php include 'css.php'; ?>    
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<!--<form method="POST">-->
    <?php
//    if(isset($_POST['product-list-quantity-input'])){
//        createProductList($products,$_POST['product-list-quantity-input']);
//    }else{
//        createProductList($products);
//    }

    ?>
<!--    <button type="submit" name="addToCartButton" id="addToCartButton" >Add to cart</button>-->
<!--</form>-->
<div class="yellow-body padding-from-menu same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Total Payout</h1>
    <div class="clear"></div>
	<div class="search-container0 payout-search">

            <div class="shipping-input clean smaller-text2">
                <p>Start Date</p>
                <input class="shipping-input2 clean normal-input" type="date">
            </div>
            <div class="shipping-input clean smaller-text2 middle-shipping-div second-shipping">
                <p>End Date</p>
                <input class="shipping-input2 clean normal-input" type="date">
            </div>
            <div class="shipping-input clean smaller-text2">
                <p>Username</p>
                <input class="shipping-input2 clean normal-input same-height-with-date" type="text" placeholder="Username">
            </div>

     
            <button class="clean black-button shipping-search-btn second-shipping same-height-with-date2">Search</button>

    </div>    

    <div class="clear"></div>

    <div class="width100 shipping-div2">
    	<div class="overflow-scroll-div">
            <table class="shipping-table payout-table">
                <thead>
                    <tr class="payout-th-tr">
                        <th>NO.</th>
                        <th>PAYOUT</th>
                        <th>PRODUCT</th>
                        <th>TOTAL (RM)</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1.</td>
                        <td>Referral Payout</td>
                        <td>-</td>
                        <td>200,000.00</td>
                    </tr>
                    <tr>
                        <td>2.</td>
                        <td>Commission</td>
                        <td>Synthetic Plus Oil - SAE 5W - 30</td>
                        <td>200,000.00</td>
                    </tr>   
                    <tr>
                        <td></td>
                        <td></td>
                        <td>Synthetic Plus Oil - SAE 10W - 40</td>
                        <td>300,000.00</td>
                    </tr>                 
                    <tr>
                        <td></td>
                        <td></td>
                        <td>DCK Fuel Booster</td>
                        <td>300,000.00</td>
                    </tr> 
                    <tr>
                        <td></td>
                        <td></td>
                        <td>DCK Engine Oil Booster</td>
                        <td>1,000,000.00</td>
                    </tr> 
                    <tr class="total-tr">
                        <td></td>
                        <td></td>
                        <td><b>Total</b></td>
                        <td><b>2,000,000.00</b></td>
                    </tr>
                  </tbody>                                                       
            </table>
        </div>
    </div>
    <div class="clear"></div> 
</div>


<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

</body>
</html>