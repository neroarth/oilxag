<?php
// if (session_id() == ""){
//     session_start();
// }
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/Shipping.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $subtotal = rewrite($_POST["insert_subtotal"]);
    $total = rewrite($_POST["insert_total"]);
    //todo validation on server side
    //TODO change login with email to use username instead
    //TODO add username field to register's backend
}

$userOrder = getOrders($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$orderDetails = $userOrder[0];

if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
    $productListHtml = getShoppingCart($conn,2);
}else
{}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://dcksupreme.asia/payment.php" />
<meta property="og:title" content="Payment | DCK Supreme" />
<title>Payment | DCK Supreme</title>
<meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
<meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
<meta name="keywords" content="DCK®,dck, dck supreme, supreme, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
noisiness and temperature, dry cold start,etc">
<link rel="canonical" href="https://dcksupreme.asia/payment.php" />
<?php include 'css.php'; ?>
<?php require_once dirname(__FILE__) . '/header.php'; ?>
</head>

<body class="body">
<!--
<div id="overlay">
 <div class="center-food"><img src="img/loading-gif.gif" class="food-gif"></div>
 <div id="progstat"></div>
 <div id="progress"></div>
</div>-->

<!-- Start Menu -->
	<?php include 'header-sherry.php'; ?>
<div class="flex-container">    
    <div class="left-status-div">
    	<!-- Start of Check Out Status Div-->
        <div class="check-out-status">
            <div class="status-container">
                <a href="viewCart.php" class="status-a">
                    <div class="black-round-div"><img src="img/tick2.png" class="yellow-tick" alt="Completed" title="Completed"></div>
                    CART
                </a>
            </div>

            <div class="status-div">></div>

            <div class="status-container">
                <a href="checkout.php" class="status-a">
                    <div class="black-round-div"><img src="img/tick2.png" class="yellow-tick" alt="Completed" title="Completed"></div>
                    INFORMATION
                </a>
            </div>

            <div class="status-div">></div>

            <!-- <div class="status-container">        
                <a href="shipping.php" class="status-a">
                    <div class="black-round-div"><img src="img/tick2.png" class="yellow-tick" alt="Completed" title="Completed"></div>
                    SHIPPING
                </a>   
            </div>     -->
            
            <div class="status-container">
                <a href="shipping.php" class="status-a">
                    <div class="black-round-div"><img src="img/tick2.png" class="yellow-tick" alt="Completed" title="Completed"></div>
                    SHIPPING
                </a>
            </div>

            <div class="status-div">></div>

            <div class="status-container">        
                <div class="white-round-div">4</div>
                PAYMENT
            </div>                
        </div>
        <!-- End of Check Out Status Div-->
        <div class="clear"></div>
        <p class="info-title spacing2"><b>PAYMENT</b></p>  
        <p class="smaller-text">All transactions are secure and encrypted.</p>
        <div class="white-input-div payment-white-div">
        	<p><b>Bank In Details</b></p>
            <p>For Malaysian only, you may walk in to your nearest bank or cash deposit machine to made a payment. Below is the details for your reference.</p>
        	<p>Balance after payment will be included in your parcel.</p>
            <p>Please screen-shot before "Complete Order".</p>
            <p>Bank: Maybank</p>
            <p>Acc. No: XXXXXXXXXXXXXX</p>
            <p>Bank Acc. Holder: DCK Supreme</p>
        </div>  
        <div class="white-input-div payment-white-div">
        	<input type="radio" value="Online Banking"> Online Banking
        </div>
        <div class="white-input-div payment-white-div">
        	<input type="radio" value="CDM"> CDM
        </div>     
        <div class="white-input-div payment-white-div">
        	<p class="payment-input-p">Amount: <input type="text" placeholder="0.00" class="clean edit-profile-input payment-input"></p>
        </div>     
        <div class="white-input-div payment-white-div">
        	<p class="payment-input-p">Bank In Date: <input type="date" class="clean edit-profile-input payment-input"></p>
        </div>        
        <div class="white-input-div payment-white-div">
        	<p class="payment-input-p">Bank In Time: <input type="time" class="clean edit-profile-input payment-input"></p>
        </div>                  
		<!--p class="info-title spacing2"><b>UPLOAD RECEIPT</b></p> 
        <input type="file"  class="clean upload-file-input">-->
        
        <div class="clear"></div>
        <div class="cart-bottom-div spacing2">
            <div class="left-cart-bottom-div">
                <p class="continue-shopping pointer continue2"><a href="shipping.php" class="black-white-link"><img src="img/back.png" class="back-btn" alt="back" title="back" > Return</a></p>
            </div>

            <div class="right-cart-div">
    
                <button class="clean black-button add-to-cart-btn checkout-btn continue2 add-to-cart-btn2">COMPLETE ORDER</button>
            </div>
        </div>
    </div>

    <div class="right-status-div">      
        <?php echo $productListHtml; ?>
    </div>	

</div>    	
        
        


<script>
function goBack() {
  window.history.back();
}
</script>
<script>
function incrementValue(e) {
  e.preventDefault();
  var fieldName = $(e.target).data('field');
  var parent = $(e.target).closest('div');
  var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

  if (!isNaN(currentVal)) {
    parent.find('input[name=' + fieldName + ']').val(currentVal + 1);
  } else {
    parent.find('input[name=' + fieldName + ']').val(0);
  }
}

function decrementValue(e) {
  e.preventDefault();
  var fieldName = $(e.target).data('field');
  var parent = $(e.target).closest('div');
  var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

  if (!isNaN(currentVal) && currentVal > 0) {
    parent.find('input[name=' + fieldName + ']').val(currentVal - 1);
  } else {
    parent.find('input[name=' + fieldName + ']').val(0);
  }
}

$('.input-group').on('click', '.button-plus', function(e) {
  incrementValue(e);
});

$('.input-group').on('click', '.button-minus', function(e) {
  decrementValue(e);
});

</script>

<?php include 'js.php'; ?>
<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully register your profile! Please confirm your registration inside of your email.";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "There are no referrer with this email ! Please register again";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Successfully register your profile! Please confirm your registration inside of your email.";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "User password must be more than 5 !";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "User password does not match";
        }
        else if($_GET['type'] == 6)
        {
            $messageType = "Wrong email format.";
        }
        else if($_GET['type'] == 7)
        {
            $messageType = "There are no user with this email ! Please try again.";
        }
        else if($_GET['type'] == 8)
        {
            $messageType = "Successfully reset your password! Please check your email.";
        }
        else if($_GET['type'] == 9)
        {
            $messageType = "Successfully reset your password! ";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
if(isset($_GET['promptError']))
{
    $messageType = null;

    if($_GET['promptError'] == 1)
    {
        $messageType = "Error registering new account.The account already exist";
    }
    else if($_GET['promptError'] == 2)
    {
        $messageType = "Error assigning referral relationship. Please register again.";
    }
    echo '
    <script>
        putNoticeJavascript("Notice !! ","'.$messageType.'");
    </script>
    ';   
}
?>
</body>
</html>