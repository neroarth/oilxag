<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    //todo create table for transaction_history with at least a column for quantity(in order table),product_id(in order table), order_id, status (others can refer to btcw's), target_uid, trigger_transaction_id
    //todo create table for order and product_order
    $totalProductCount = count($_POST['product-list-id-input']);
    for($i = 0; $i < $totalProductCount; $i++){
        $productId = $_POST['product-list-id-input'][$i];
        $quantity = $_POST['product-list-quantity-input'][$i];

        echo " this: $productId total: $quantity";
    }
}

$products = getProduct($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/rewards.php/" />
    <meta property="og:title" content="My Rewards | DCK Supreme" />
    <title>My Rewards | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/rewards.php/" />
    <?php include 'css.php'; ?>    
</head>
<body class="body">
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<!--<form method="POST">-->
    <?php
//    if(isset($_POST['product-list-quantity-input'])){
//        createProductList($products,$_POST['product-list-quantity-input']);
//    }else{
//        createProductList($products);
//    }

    ?>
<!--    <button type="submit" name="addToCartButton" id="addToCartButton" >Add to cart</button>-->
<!--</form>-->
<div class="yellow-body padding-from-menu same-padding">
	<div class="left-profile-div">
    	<!-- help link the profile picture in css style.css -->
    	<div class="profile-div">
        	<button class="edit-profile-pic-btn text-center white-text clean"><img src="img/camera.png" class="camera-icon" alt="Update" title="Update"> UPDATE</button>
        </div>
        <p class="edit-profile-p text-center width100">
            <a href="editProfile.php" class="profile-a"><img src="img/edit-profile.png" class="edit-profile-icon" alt="Edit Profile" title="Edit Profile"> Edit Profile</a>
        </p>        
    </div>
    <div class="right-profile-div">
    	<div class="profile-tab width100">
        	<a href="profile.php" class="profile-tab-a">ABOUT</a>
            <a href="#" class="profile-tab-a active-tab-a">MY REWARDS</a>
            <a href="referee.php" class="profile-tab-a">MY REFEREE</a>
        </div>
        <div class="black-reward-div">
        	<h3 class="title-h3"><span class="star-span"><img src="img/white-star.png" class="white-star" alt="MY POINTS" title="MY POINTS"></span> MY POINTS</h3>
            <div class="black-left-div">
            	<h1 class="point-h1">320 Points</h1>
            </div>
            <div class="black-right-div">    
            	<div class="three-white-div open-withdraw">WITHDRAW POINTS</div>
                <div class="three-white-div">BUY PRODUCTS</div>
                <div class="three-white-div">TRANSFER</div>
            </div>        
        </div>
        <div class="black-reward-div white-reward-div">
        	<h3 class="title-h3 ow-black-text"><span class="star-span"><img src="img/discount-voucher.png" class="white-star"  alt="MY VOUCHERS" title="MY VOUCHERS"></span> MY VOUCHERS</h3>
            <div class="black-left-div">
            	<h1 class="point-h1 ow-black-text">3 Vouchers</h1>
                <img src="img/10off.png" class="discount-png"  alt="MY VOUCHERS" title="MY VOUCHERS">
                <img src="img/15off.png" class="discount-png"  alt="MY VOUCHERS" title="MY VOUCHERS">
                <img src="img/50off.png" class="discount-png"  alt="MY VOUCHERS" title="MY VOUCHERS">
            </div>
            <div class="black-right-div">    
            	<div class="three-white-div black-div-btn">BUY PRODUCTS</div>
                <div class="three-white-div black-div-btn">TRANSFER</div>
            </div>         
        </div>
        
    </div>



</div>


<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>

</body>
</html>