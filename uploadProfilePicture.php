<?php
    require_once dirname(__FILE__) . '/sessionLoginChecker.php';
    require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
    require_once dirname(__FILE__) . '/classes/User.php';
    require_once dirname(__FILE__) . '/classes/Product.php';
    require_once dirname(__FILE__) . '/classes/Images.php';
    require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
    require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
    require_once dirname(__FILE__) . '/utilities/generalFunction.php';
    require_once dirname(__FILE__) . '/utilities/languageFunction.php';
    require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
    $conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $picture = rewrite($_POST["update_picture"]);
}

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$userPic = getImages($conn," WHERE pid = ? ",array("pid"),array($userDetails->getPicture()),"s");
$userProPic = $userPic[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

$folder = 'upload/';
$or_w = 500;
if(isset($_POST['upload']))
{
    $image = $_FILES['image']['tmp_name'];
    $filename = basename($_FILES['image']['name']);
    list($width, $height) = getimagesize($image);
    $or_h = ($height/$width)* $or_w;
    $src = imagecreatefromjpeg($image);
    $tmp = imagecreatetruecolor($or_w, $or_h);
    imagecopyresampled($tmp, $src, 0, 0, 0, 0, $or_w, $or_h, $width, $height);
    imagejpeg($tmp, $folder.$filename);
    imagedestroy($tmp);
    imagedestroy($src);
    $filename = urlencode($filename);
    //header('location:crop.php?file='.$filename);
    header('location:uploadProfilePicCrop.php?file='.$filename);
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/uploadProfilePicture.php" />
    <meta property="og:title" content="Upload Profile Picture | DCK Supreme" />
    <title>Upload Profile Picture | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/uploadProfilePicture.php" />
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <?php include 'css.php'; ?>    
</head>
<body class="body">
<?php include 'header-sherry.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<div class="yellow-body padding-from-menu same-padding partb-div">

        <div class="right-profile-div paddingleft0">
        	<h2 class="profile-title update-h2"><b>Update Profile Picture</b></h2>
            <form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data">
                <input type="file" name="image" id="image" required><br>
                <input type="submit" value="Upload" name="upload" class="btn btn-primary upload-blue-btn">
            </form>
        </div>
</div>
<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>

</body>
</html>