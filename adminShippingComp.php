<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php'; 
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();

$productsOrders =  getProductOrders($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/adminShippingComp.php" />
    <meta property="og:title" content="Shipping Completed | DCK Supreme" />
    <title>Shipping Completed | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/adminShippingComp.php" />
    <?php include 'css.php'; ?>    
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Completed | <a href="adminShipping.php" class="white-text title-tab-a">Waiting for Shipping Out</a></h1>
    <!-- This is a filter for the table result -->
    <select class="filter-select clean">
    	<option class="filter-option">Latest Shipping</option>
        <option class="filter-option">Oldest Shipping</option>
    </select>
    
    <!-- End of Filter -->
    <div class="clear"></div>
	<div class="search-container0">

            <div class="shipping-input clean smaller-text2">
                <p>Order Number</p>
                <input class="shipping-input2 clean normal-input" type="number" placeholder="Order Number">
            </div>
            <div class="shipping-input clean smaller-text2 middle-shipping-div second-shipping">
                <p>Username</p>
                <input class="shipping-input2 clean normal-input" type="text" placeholder="Username">
            </div>
            <div class="shipping-input clean smaller-text2">
                <p>Contact</p>
                <input class="shipping-input2 clean normal-input" type="number" placeholder="Contact">
            </div>  
                     
            <div class="shipping-input clean smaller-text2 second-shipping">
                <p>Start Date</p>
                <input class="shipping-input2 clean" type="date" placeholder="Start Date">
            </div>

     
            <div class="shipping-input clean smaller-text2 middle-shipping-div">
                <p>End Date</p>
                <input class="shipping-input2 clean" type="date" placeholder="End Date">
            </div>
            <div class="shipping-input clean smaller-text2 second-shipping">
                <p>Tracking No.</p>
                <input class="shipping-input2 clean normal-input" type="Text" placeholder="Tracking No.">
            </div> 
            <button class="clean black-button shipping-search-btn comp-btn">Search</button>

    </div>    

    <div class="clear"></div>

    <div class="width100 shipping-div2">
    <?php $conn = connDB();?>
    <table class="shipping-table">
        <thead>
            <tr>
                <th>NO.</th>
                <th>ORDER NUMBER</th>
                <th>USERNAME</th>
                <th>CONTACT</th>
                <th>STATUS</th>
                <!-- <th>SHIP OUT DATE</th> -->
                <th>ISSUE DATE</th>
                <th>METHOD</th>
                <th>TRACKING NO.</th>
                <th>DETAILS</th>
            </tr>
        </thead>
        <tbody>
        <?php
        for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
        {   
            $orderDetails = getOrders($conn,"WHERE id = ? AND shipping_status != 'PENDING' ",array("id"),array($productsOrders[$cnt]->getOrderId()),"s");
            if($orderDetails != null)
            {
                for($cntAA = 0;$cntAA < count($orderDetails) ;$cntAA++)
                {?>
                <tr>
                    <td><?php echo ($cnt+1)?></td>
                    <td>#<?php echo $productsOrders[$cnt]->getOrderId();?></td>
                    <td><?php echo $orderDetails[$cntAA]->getUsername();?></td>
                    <td><?php echo $orderDetails[$cntAA]->getContactNo();?></td>
                    <td><?php echo $orderDetails[$cntAA]->getShippingStatus();?></td>
                    <!-- <td><b><?php //echo $orderDetails[$cntAA]->getShippingStatus();?></b></td> -->
                    <!-- <td><?php //echo $orderDetails[$cntAA]->getShippingDate();?></td> -->
                    <td><?php $dateCreated = date("Y-m-d",strtotime($orderDetails[$cntAA]->getDateCreated()));
                            echo $dateCreated;?></td>
                    <td><?php echo $orderDetails[$cntAA]->getShippingMethod();?></td>
                    <td><?php echo $orderDetails[$cntAA]->getTrackingNumber();?></td>

                    <td>
                        <!-- <form method="POST"> -->
                        <form action="reviewShipping.php" method="POST">
                            <button class="clean edit-anc-btn hover1" type="submit" name="order_id" value="<?php echo $productsOrders[$cnt]->getOrderId();?>">
                                <img src="img/details.png" class="edit-announcement-img hover1a" alt="Details" title="Details">
                                <img src="img/details2.png" class="edit-announcement-img hover1b" alt="Details" title="Details">
                            </button>
                        </form>
                    </td>

                </tr>
                <?php
                }
            } 
           
        }
        ?>
        </tbody>
    </table>
    <?php $conn->close();?>
    </div>  

    </div>
  
</div>


<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>
<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})

</script>
</body>
</html>