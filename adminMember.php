<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php'; 
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();


$memberList = getUser($conn," WHERE user_type = ? ",array("user_type"),array(1),"i");


$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/adminShipping.php" />
    <meta property="og:title" content="Shipping | DCK Supreme" />
    <title>Shipping | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/adminShipping.php" />
    <?php include 'css.php'; ?>    
</head>
<body class="body">

<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">
<h1 class="h1-title h1-before-border shipping-h1">Member</h1>

    <!-- This is a filter for the table result -->
    <!-- <select class="filter-select clean">
    	<option class="filter-option">Latest</option>
    	<option class="filter-option">Oldest</option>    
    </select> -->
    <!-- End of Filter -->

    <!-- <div class="clear"></div> -->

	<!-- <div class="search-container0">

            <div class="shipping-input clean smaller-text2">
                <p>Name</p>
                <input class="shipping-input2 clean normal-input" type="text" placeholder="Name">
            </div>
            <div class="shipping-input clean smaller-text2 middle-shipping-div second-shipping">
                <p>Contact</p>
                <input class="shipping-input2 clean normal-input" type="number" placeholder="Contact">
            </div>
            <div class="shipping-input clean smaller-text2">
                <p>Email</p>
                <input class="shipping-input2 clean normal-input" type="email" placeholder="Email">
            </div>            
            <div class="shipping-input clean smaller-text2 second-shipping">
                <p>Start Date</p>
                <input class="shipping-input2 clean" type="date" placeholder="Start Date">
            </div>

     
            <div class="shipping-input clean middle-shipping-div smaller-text2">
                <p>End Date</p>
                <input class="shipping-input2 clean" type="date" placeholder="End Date">
            </div>

            <button class="clean black-button shipping-search-btn second-shipping">Search</button>

    </div>     -->

    <div class="clear"></div>

    <div >
    	<div class="overflow-scroll-div">
            <table class="shipping-table">
                <thead>
                    <tr>
                        <th>NO.</th>
                        <th>USERNAME</th>
                        <th>FULLNAME</th>
                        <th>JOINED DATE</th>
                        <th>REGISTERED DOWNLINE NUMBER</th>
                        <th>TOTAL BONUS</th>
                    </tr>
                </thead>
                <tbody>

                <?php
                if($memberList)
                {
                    for($cnt = 0;$cnt < count($memberList) ;$cnt++)
                    {?>
                        <tr class="link-to-details">
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $memberList[$cnt]->getUsername();?></td>
                            <td><?php echo $memberList[$cnt]->getFullname();?></td>
                            <td>
                                <?php $dateCreated = date("Y-m-d",strtotime($memberList[$cnt]->getDateCreated()));echo $dateCreated;?>
                            </td>
                            <td><?php echo $memberList[$cnt]->getRegisterDownlineNo();?></td>
                            <td><?php echo $memberList[$cnt]->getBonus();?></td>
                        </tr>
                        <?php
                    }
                }
                ?>
                </tbody>           
            </table>
        </div>
    </div>

</div>


<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<script>
function goBack() {
  window.history.back();
}

$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})
</script>

</body>
</html>

