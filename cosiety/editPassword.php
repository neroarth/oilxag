<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Edit Password | Cosiety" />
<title>Edit Password | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow edit-password-big-div">
	<h1 class="backend-title-h1">Edit Password</h1>
	<div class="edit-half-div">
    	<p class="grey-text input-top-p">Current Password</p>
        <input class="three-select clean"type="password">
        <span class="view-password-span"><img src="img/visible.png" class="view-password-img hover-effect" alt="View Password" title="View Password"></span>
	</div>
    <div class="clear"></div>
	<div class="edit-half-div">
    	<p class="grey-text input-top-p">New Password</p>
        <input class="three-select clean"type="password">
        <span class="view-password-span"><img src="img/visible.png" class="view-password-img hover-effect" alt="View Password" title="View Password"></span>
	</div>  
	<div class="clear"></div>    
 	<div class="edit-half-div">
    	<p class="grey-text input-top-p">Retype New Password</p>
        <input class="three-select clean"type="password">
        <span class="view-password-span"><img src="img/visible.png" class="view-password-img hover-effect" alt="View Password" title="View Password"></span>
	</div>            
	<div class="clear"></div>

 
	<div class="divider"></div>
    <div class="clear"></div> 
    <div class="fillup-extra-space"></div><a href="profile.php"><button class="blue-btn payment-button clean next-btn">Confirm</button></a>
    <div class="clear"></div>
    <div class="fillup-extra-space2"></div><a  onclick="goBack()" class="cancel-a hover-effect">Cancel</a>
</div>


<?php include 'js.php'; ?>
</body>
</html>