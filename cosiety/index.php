<!doctype html>
<html>
<head>
    <?php include 'meta.php'; ?>
    <!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
    <meta property="og:title" content="Co-working Space at Straits Quay, Penang | Cosiety" />
    <title>Co-working Space at Straits Quay, Penang | Cosiety</title>
    <meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
    <meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
    <meta name="keywords" content="cosiety,co-working space, coworking space, penang, malaysia, pulau pinang, etc">
    <!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
    <?php include 'css.php'; ?>
</head>

<body class="body">
<!--
<div id="overlay">
 <div class="center-food"><img src="img/loading-gif.gif" class="food-gif"></div>
 <div id="progstat"></div>
 <div id="progress"></div>
</div>
-->
<div class="landing-first-div">
    <!-- Start Menu -->
    <header id="header" class="header header--fixed same-padding header1 menu-white" role="banner">
        <div class="big-container-size hidden-padding">
            <div class="left-logo-div float-left hidden-logo-padding">
                <img src="img/cosiety-logo.png" class="logo-img" alt="Cosiety" title="Cosiety">
            </div>

            <div class="right-menu-div float-right" id="top-menu">

                <a href="#about" class="menu-padding">About</a>
                <a href="#plan" class="menu-padding">Plan & Price</a>
                <a href="#booking" class="menu-padding">Booking</a>
                <a class="menu-padding open-signup hover-effect">Sign Up</a>
                <a class="menu-padding open-login hover-effect">Login</a>
                <a class="menu-icon openmenu"><img src="img/menu.png" class="menu-img" alt="Menu" title="menu"></a>
                <!-- Mobile View-->
                <!--    <a href="#whatyouneed" class="white-text menu-padding red-hover2">
                        <img src="img/thousand-media/menu-icon-14.png" class="menu-img" alt="Your Need" title="Your Need">
                    </a>
                    <a href="#promotion" class="white-text menu-padding red-hover2">
                        <img src="img/thousand-media/menu-icon-13.png" class="menu-img" alt="Promotion" title="Promotion">
                    </a>

                    <a href="#services" class="white-text menu-padding red-hover2">
                        <img src="img/thousand-media/menu-icon-11.png" class="menu-img" alt="Services" title="Services">
                    </a>
                    <a href="#contact" class="white-text menu-padding red-hover2">
                        <img src="img/thousand-media/menu-icon-10.png" class="menu-img" alt="Contact" title="Contact">
                    </a>
                    <a class="white-text menu-padding red-hover2 open-register pointer">
                        <img src="img/thousand-media/menu-icon-15.png" class="menu-img" alt="Register" title="Register">
                    </a>
                    <a class="white-text red-hover2 open-login pointer">
                        <img src="img/thousand-media/menu-icon-16.png" class="menu-img" alt="Login" title="Login">
                    </a>        -->
            </div>
        </div>

    </header>

</div>
<div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:1300px;height:500px;overflow:hidden;visibility:hidden;">
    <!-- Loading Screen -->
    <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
        <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="svg/loading/static-svg/spin.svg" />
    </div>
    <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1300px;height:500px;overflow:hidden;">
        <div>
            <img data-u="image" src="img/banner1.jpg" />
            <div class="white-div text-center ow-white-div">
                <h2 class="black-text">Your Zen!</h2>
                <p class="grey-text">
                    Cosiety, a co-working space that knows your importance in making the place your Zen! Book a tour around the place and select which is your Zen!
                </p>
                <a href="#booking"><div class="black-btn">Book Now</div></a>
            </div>

        </div>
        <div>
            <img data-u="image" src="img/banner2.jpg" />
        </div>
        <div>
            <img data-u="image" src="img/banner3.jpg" />
        </div>
        <div>
            <img data-u="image" src="img/banner4.jpg" />
        </div>
        <div>
            <img data-u="image" src="img/banner5.jpg" />
        </div>
        <div>
            <img data-u="image" src="img/banner6.jpg" />
        </div>

    </div>
    <!-- Bullet Navigator -->
    <div data-u="navigator" class="jssorb032" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
        <div data-u="prototype" class="i" style="width:16px;height:16px;">
            <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <circle class="b" cx="8000" cy="8000" r="5800"></circle>
            </svg>
        </div>
    </div>
    <!-- Arrow Navigator -->
    <div data-u="arrowleft" class="jssora051" style="width:35px;height:35px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
        <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
            <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
        </svg>
    </div>
    <div data-u="arrowright" class="jssora051" style="width:35px;height:35px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
        <svg viewBox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
            <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
        </svg>
    </div>
</div>
<div class="clear"></div>
<!--<div class="home-banner-div menu-distance same-padding">
	<!--<img src="img/banner1.jpg" class="home-banner width100" alt="Coworking Space" title="Coworking Space">-->
<!--    <div class="white-div text-center">
    	<h2 class="black-text">Your Zen!</h2>
        <p class="grey-text">
        	Cosiety, a coworking space that knows your importance in making the place your Zen! Book a tour around the place and select which is your Zen!
        </p>
        <a ><div class="black-btn">Book Now</div></a>
    </div>
</div>-->


<div id="about" class="overflow about-div same-padding">
    <div class="about-left">
        <!--<img src="img/coworking-space.jpg" class="about-img" alt="About Cosiety Coworking Space" title="About Cosiety Coworking Space">-->
    </div>
    <div class="about-right">
        <h1 class="black-text style-h1">About Cosiety</h1>
        <div class="style-border"></div>
        <p class="subtitle-p black-text">Bringing Companies and Societies Together</p>
        <p class="content-p">
            Cosiety is a brainchild of the esteemed founders of Adashared with the mindset of creating a harmonious co-working space for all walks of societies. Hence, the name Cosiety. Over at Cosiety, what you can expect is a vibrant co-working space with plenty of collaborations and opportunities through networking and large-scaled workshops.
        </p>

    </div>

</div>
<div class="clear"></div>
<div id="plan" class="same-padding overflow plan-div">
    <h1 class="black-text style-h1 text-center">Plans and Pricing</h1>
    <div class="style-border center-style-border"></div>
    <a href="addBooking.php"><div class="plan-small-div service-bg">
        <p class="black-img-text">Lounge</p>
    </div></a>
    <a href="addBooking.php"><div class="plan-small-div middle-plan-div hot-bg">
        <p class="black-img-text">Work Desk</p>
    </div></a>
    <a href="addBooking.php"><div class="plan-small-div meeting-bg">
        <p class="black-img-text">Private Suit</p>
    </div></a>
</div>
<div class="clear"></div>
<div id="booking" class="same-padding overflow booking-div">
    <h1 class="black-text style-h1 text-center">Make a Booking</h1>
    <div class="style-border center-style-border"></div>
</div>
<div class="width100 iframe-div">
	<iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d834.9526145127651!2d100.31286810414129!3d5.458434922077122!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1s1st%20Floor%2C%20Straits%20Quay%2CJalan%20Seri%20Tanjung%20Pinang%2C%20Tanjung%20Tokong%2C10470%2C%20Pulau%20Pinang.!5e0!3m2!1sen!2smy!4v1572417837955!5m2!1sen!2smy" width="100%" height="450" class="map-iframe" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
</div>
<div class="clear"></div>
<div class="same-padding contact-details overflow">
    <div class="left-contact-page">
        <p class="subtitle-p black-text">Company</p>
        <p class="content-p company-p">
            Cosiety – is registered in Penang and serves as a melting pot of multi-diverse cultures and societies around the world. Making this co-working space a must have location for your business establishment!
        </p>
        <p class="content-p address-p">
            1st Floor, Straits Quay,<br>Jalan Seri Tanjung Pinang, Tanjung Tokong,<br>10470, Pulau Pinang.
        </p>
        <p class="content-p address-p">
            Tel: +604-123 4567<br>
            Email: support@cosiety.com
        </p>
        <p class="content-p address-p social-p">
            <img src="img/facebook.png" class="social-img" alt="Cosiety Facebook" title="Cosiety Facebook">
            <img src="img/linkedin.png" class="social-img" alt="Cosiety Linkedin" title="Cosiety Linkedin">
        </p>
    </div>
    <div class="right-contact-page">
        <form id="contactform" method="post" action="index.php" class="form-class extra-margin">

            <input type="text" name="name" placeholder="Name" class="input-name clean" required >

            <input type="text" name="telephone" placeholder="Phone number" class="input-name clean" required >

            <input type="email" name="email" placeholder="Email" class="input-name clean" required >

            <textarea placeholder="Message" class="input-name input-message clean" name="comments"  ></textarea>

            <div class="clear"></div>
            <table class="option-table">
                <tbody>
                <tr>
                    <td>
                        <input type="radio" name="contact-option" value="contact-more-info" class="radio1" required>
                    </td>
                    <td>
                        <p class="opt-msg">I want to be contacted with more information about your company's offering marketing services and consulting</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="radio" name="contact-option" value="contact-on-request" class="radio1"  required>
                    </td>
                    <td>
                        <p class="opt-msg">I just want to be contacted based on my request/ inquiry</p>
                    </td>
                </tr>
                </tbody>
            </table>
            <div class="res-div"><input type="submit" name="submit" value="Submit" class="input-submit white-text clean pointer black-btn" ></div>
        </form>
    </div>
</div>
<div class="clear"></div>

<div class="home-banner-div menu-distance same-padding footer-bg">
    <!--<img src="img/banner1.jpg" class="home-banner width100" alt="Coworking Space" title="Coworking Space">-->
    <div class="white-div text-center">
        <h2 class="black-text">Cosiety</h2>
        <p class="grey-text">
            Are you ready to be a part of Cosiety?
        </p>
        <div class="left-black-btn pointer open-signup">Sign Up</div>
        <div class="right-black-btn pointer open-login">Login</div>
    </div>
</div>


<style>
    .jssorb032{
        display:none !important;}
</style>
<!-- CSS -->
<style>
    .food-gif{
        width:100px;
        position:absolute;
        top:calc(50% - 150px);
        text-align:center;
    }
    .center-food{
        width:100%;
        text-align:center;
        margin-left:-50px;}
    #container{
        margin-top:-20px;}
    #overlay{
        position:fixed;
        z-index:99999;
        top:0;
        left:0;
        bottom:0;
        right:0;
        background:#7cd1d1;
        /*background: -moz-linear-gradient(left, #a9151c 0%, #d60d26 100%);
        background: -webkit-linear-gradient(left, #a9151c 0%,#d60d26 100%);
        background: linear-gradient(to right, #a9151c 0%,#d60d26 100%);*/
        transition: 1s 0.4s;
    }
    #progress{
        height:1px;
        background:#fff;
        position:absolute;
        width:0;
        top:50%;
    }
    #progstat{
        font-size:0.7em;
        letter-spacing: 3px;
        position:absolute;
        top:50%;
        margin-top:-40px;
        width:100%;
        text-align:center;
        color:#fff;
    }
    @media all and (max-width: 500px){
        .food-gif{
            width:60px;
            top:calc(50% - 120px);
            text-align:center;
        }
        .center-food{
            margin-left:-30px;}

    }
</style>
<?php include 'js.php'; ?>
<?php

if( $_SERVER['REQUEST_METHOD'] == 'POST') {

    $error_message = "";

    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "sherry2.vidatech@gmail.com,michaelwong.vidatech@gmail.com,kevinyam.vidatech@gmail.com";
    $email_subject = "Contact Form via Cosiety website";

    function died($error)
    {
        // your error code can go here
        echo '<script>alert("We are very sorry, but there were error(s) found with the form you submitted.\n\nThese errors appear below.\n\n';
        echo $error;
        echo '\n\nPlease go back and fix these errors.\n\n")</script>';
        die();
    }


    // validation expected data exists
    if(!isset($_POST['name']) ||
        !isset($_POST['email']) ||
        !isset($_POST['comments']) ||
        !isset($_POST['telephone'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');
    }



    $first_name = $_POST['name']; // required
    $telephone = $_POST['telephone']; //required
    $email = $_POST['email']; //required
    $comments = $_POST['comments']; //required
    $contactOption = $_POST['contact-option']; // required
    $contactMethod = null;

    if($contactOption == null || $contactOption == ""){
        $contactMethod = "don\'t bother me";
    }else if($contactOption == "contact-more-info"){
        $contactMethod = "I want to be contacted with more information about your company's offering marketing services and consulting";
    }else if($contactOption == "contact-on-request"){
        $contactMethod = "I just want to be contacted based on my request/ inquiry";
    }else{
        $contactMethod = "error getting contact options";
    }



    $string_exp = "/^[A-Za-z .'-]+$/";

    if(!preg_match($string_exp,$first_name)) {
        $error_message .= 'The Name you entered does not appear to be valid.<br />';
    }



    if(strlen($comments) < 2) {
        $error_message .= 'The message you entered do not appear to be valid.<br />';
    }

    if(strlen($error_message) > 0) {
        died($error_message);
    }

    $email_message = "Form details below.\n\n";


    function clean_string($string) {
        $bad = array("content-type","bcc:","to:","cc:","href");
        return str_replace($bad,"",$string);
    }



    $email_message .= "Name: ".clean_string($first_name)."\n";
    $email_message .= "Email: ".clean_string($email)."\n";
    $email_message .= "Telephone: ".clean_string($telephone)."\n";
    $email_message .= "Message : ".clean_string($comments)."\n";
    $email_message .= "Contact Option : ".clean_string($contactMethod)."\n";

    $email_from = "vidatech@gmail.com";

// create email headers
    $headers = 'From: '.$email_from."\r\n".
        'Reply-To: '.$email_from."\r\n" .
        'X-Mailer: PHP/' . phpversion();
    @mail($email_to, $email_subject, $email_message, $headers);


    echo '<script>alert("Thank you! We will be in contact shortly!")</script>';

    ?>
    <!-- include your own success html here -->

    <!--Thank you for contacting us. We will be in touch with you very soon.-->
    <?php

}
?>

</body>
</html>