<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Payment Method | Cosiety" />
<title>Payment Method | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1">Payment</h1>
            <p class="thick-payment-p">Total: RM1378.00</p>    
    <!--
    <div class="half-div-radio">
        <label class="container2">
          <div class="payment1-div">
          	<p class="thin-payment-p">One Time Payment (Discount 10%)</p>
            <p class="thick-payment-p">Total: RM1378.00</p>
          </div>
          <input type="radio" checked="checked" name="radio">
          <span class="checkmark2"></span>
        </label>
    </div>
    <div class="half-div-radio">    
        <label class="container2">
          <div class="payment1-div">
          	<p class="thin-payment-p">Monthly Payment (No Extra Discount)</p>
            <p class="thick-payment-p">Total: RM760.00/month</p>
          </div>       
          <input type="radio" name="radio">
          <span class="checkmark2"></span>
        </label>
    </div>-->
    <h2 class="backend-title-h2">Choose Your Payment Method</h2>  
    <div class="three-div-radio">
        <label class="container2"><img src="img/ipay88.png" class="payment-img" alt="ipay88" title="ipay88">      
          <input type="radio" name="radio1">
          <span class="checkmark2"></span>
        </label>    
    </div>
    <div class="three-div-radio">
        <label class="container2"><img src="img/visa-mastercard.png" class="payment-img" alt="Visa/Master Card" title="Visa/Master Card">      
          <input type="radio" name="radio1">
          <span class="checkmark2"></span>
        </label>    
    </div>    
    <div class="three-div-radio no-margin-right">
        <label class="container2">Online Banking      
          <input type="radio" name="radio1">
          <span class="checkmark2"></span>
        </label>    
    </div>      
    <div class="clear"></div>
    <div class="fillup-extra-space"></div><a href="reserveSpace.php"><button class="blue-btn payment-button clean next-btn">Next</button></a>
    <div class="clear"></div>
    <div class="fillup-extra-space2"></div><a  onclick="goBack()" class="cancel-a hover-effect">Cancel</a>
</div>


<?php include 'js.php'; ?>
</body>
</html>