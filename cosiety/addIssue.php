<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Add New Issue | Cosiety" />
<title>Add New Issue | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<div class="width100 overflow">
    	<h1 class="backend-title">Add New Issue</h1>
    </div>

    <div class="width100 overflow">
			<textarea class="clean width100 project-textarea project-textarea2" placeholder="Type the details here"></textarea>   
    </div>
    <div class="width100 overflow">
            <div class="upload-btn-wrapper">
              <button class="upload-btn">Upload Image</button>
              <input class="hidden-input" type="file" name="myfile" />
            </div>
            <!-- Crop the image 16:9 -->
            <p class="img-preview">Image Preview</p> 
            <div class="left-img-preview"><img src="img/promotion.jpg" class="uploaded-img"></div><span class="right-remove-span hover-effect">Remove</span>           
    </div>
    <div class="divider"></div>
    <div class="fillup-extra-space"></div><button class="blue-btn payment-button clean">Submit</button>
    <div class="clear"></div>
    <div class="fillup-extra-space2"></div><a  onclick="goBack()" class="cancel-a hover-effect">Cancel</a>
    <div class="divider"></div>            
</div>


<?php include 'js.php'; ?>
</body>
</html>