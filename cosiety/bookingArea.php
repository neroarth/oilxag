<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Booking Area | Cosiety" />
<title>Booking Area | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1">Booking Area  <a href="addBookingArea.php" class="hover1"><img src="img/add.png" class="add-icon hover1a" alt="Add Booking Area" title="Add Booking Area"><img src="img/add2.png" class="add-icon hover1b" alt="Add Booking Area" title="Add Booking Area"></a></h1>
    <div class="clear"></div>
 	<div class="small-divider width100"></div>
    <div class="clear"></div>
	<a href="editBookingArea.php">
        <div class="four-white-div whitebox-hover">
            <p class="black-text white-div-title">Ground Floor</p>
            <p class="white-div-number">24</p>
        </div>
    </a>
	<a href="editBookingArea.php">
        <div class="four-white-div second-white-div third-two four-two whitebox-hover">
            <p class="black-text white-div-title">First Floor</p>
            <p class="white-div-number">34</p>
        </div>
    </a>   
	<a href="editBookingArea.php">
        <div class="four-white-div third-white-div whitebox-hover">
            <p class="black-text white-div-title">Second Floor</p>
            <p class="white-div-number">34</p>
        </div>
    </a>   
	<a href="editBookingArea.php">
        <div class="four-white-div four-two whitebox-hover">
            <p class="black-text white-div-title">Garden</p>
            <p class="white-div-number">14</p>
        </div>
    </a>             
</div>


<?php include 'js.php'; ?>
</body>
</html>