<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Add Booking Area | Cosiety" />
<title>Add Booking Area | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1">Add Booking Area</h1>
    <div class="width100 overflow">
    	<input type="text" class="three-select clean" placeholder="Area Name">
    </div>
    <div class="small-divider"></div>
	<div class="three-div">
    	<p class="grey-text input-top-p">No. of Seat</p>
		<input type="number" class="three-select clean" placeholder="0">
    </div>
	<div class="three-div middle-three-div second-three-div">
    	<p class="grey-text input-top-p">No. of Small Meeting Room</p>
        <input type="number" class="three-select clean" placeholder="0">
    </div>
	<div class="three-div">
    	<p class="grey-text input-top-p">No. of Big Meeting Room</p>
		<input type="number" class="three-select clean" placeholder="0">
    </div>
    <div class="clear"></div>
    <div class="divider"></div> 
 
    <div class="fillup-extra-space"></div><a href="addBookingAreaTwo.php"><button class="blue-btn payment-button clean">Next</button></a>
    <div class="clear"></div>
    <div class="fillup-extra-space2"></div><a  onclick="goBack()" class="cancel-a hover-effect">Cancel</a>
</div>


<?php include 'js.php'; ?>
</body>
</html>