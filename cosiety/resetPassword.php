<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Reset Password | Cosiety" />
<title>Reset Password | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
    <!-- Start Menu -->
    <header id="header" class="header header--fixed same-padding header1 menu-white" role="banner">
        <div class="big-container-size hidden-padding">
            <div class="left-logo-div float-left hidden-logo-padding">
                <a href="index.php" class="hover-effect"><img src="img/cosiety-logo.png" class="logo-img" alt="Cosiety" title="Cosiety"></a>
            </div>

            <div class="right-menu-div float-right" id="top-menu">

                <a href="index.php#about" class="menu-padding">About</a>
                <a href="index.php#plan" class="menu-padding">Plan & Price</a>
                <a href="index.php#booking" class="menu-padding">Booking</a>
                <a class="menu-padding open-signup hover-effect">Sign Up</a>
                <a class="menu-padding open-login hover-effect">Login</a>
                <a class="menu-icon openmenu"><img src="img/menu.png" class="menu-img" alt="Menu" title="menu"></a>
                <!-- Mobile View-->
                <!--    <a href="#whatyouneed" class="white-text menu-padding red-hover2">
                        <img src="img/thousand-media/menu-icon-14.png" class="menu-img" alt="Your Need" title="Your Need">
                    </a>
                    <a href="#promotion" class="white-text menu-padding red-hover2">
                        <img src="img/thousand-media/menu-icon-13.png" class="menu-img" alt="Promotion" title="Promotion">
                    </a>

                    <a href="#services" class="white-text menu-padding red-hover2">
                        <img src="img/thousand-media/menu-icon-11.png" class="menu-img" alt="Services" title="Services">
                    </a>
                    <a href="#contact" class="white-text menu-padding red-hover2">
                        <img src="img/thousand-media/menu-icon-10.png" class="menu-img" alt="Contact" title="Contact">
                    </a>
                    <a class="white-text menu-padding red-hover2 open-register pointer">
                        <img src="img/thousand-media/menu-icon-15.png" class="menu-img" alt="Register" title="Register">
                    </a>
                    <a class="white-text red-hover2 open-login pointer">
                        <img src="img/thousand-media/menu-icon-16.png" class="menu-img" alt="Login" title="Login">
                    </a>        -->
            </div>
        </div>

    </header>


<div class="grey-bg menu-distance2 same-padding overflow edit-password-big-div">
	<h1 class="backend-title-h1">Reset Password</h1>
	<div class="edit-half-div">
    	<p class="grey-text input-top-p">New Password</p>
        <input class="three-select clean"type="password">
        <span class="view-password-span"><img src="img/visible.png" class="view-password-img hover-effect" alt="View Password" title="View Password"></span>
	</div>  
	<div class="clear"></div>    
 	<div class="edit-half-div">
    	<p class="grey-text input-top-p">Retype New Password</p>
        <input class="three-select clean"type="password">
        <span class="view-password-span"><img src="img/visible.png" class="view-password-img hover-effect" alt="View Password" title="View Password"></span>
	</div>            
	<div class="clear"></div>

 
	<div class="divider"></div>
    <div class="clear"></div> 
    <div class="fillup-extra-space"></div><a href="profile.php"><button class="blue-btn payment-button clean next-btn">Confirm</button></a>
    <div class="clear"></div>
    <div class="fillup-extra-space2"></div><a  onclick="goBack()" class="cancel-a hover-effect">Cancel</a>
</div>


<?php include 'js.php'; ?>
</body>
</html>