<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Cancel Plan | Cosiety" />
<title>Cancel Plan | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1 hover1 align-select-h1 issue-h1">Cancel Plan - Paid Plan | <a href="unpaidPlan.php" class="lightblue-text hover-effect">Unpaid Plan</a> | <a href="planRecord.php" class="lightblue-text hover-effect">Record</a></h1>
	<select class="clean align-h1-select issue-select">
    	<option>Latest</option>
        <option>Oldest</option>
        <option>Highest</option>
        <option>Lowest</option>
    </select>
	<div class="clear"></div>
    <div class="width100 search-div overflow">
    	<div class="three-search-div">
        	<p class="upper-search-p">Receipt No.</p>
            <input class="search-input" type="text" placeholder="Receipt No.">
        </div>
    	<div class="three-search-div middle-three-search second-three-search">
        	<p class="upper-search-p">Plan</p>
            <select class="search-input">
            	<option>Lounge</option>
                <option>Dedicated Work Desk</option>
                <option>Co-Working Space (Hot Seat)</option>
                <option>Private Suit</option>                
            </select>
        </div>
        <div class="three-search-div">
        	<p class="upper-search-p">Member</p>
            <input class="search-input" type="text" placeholder="Member">
        </div>
    	<div class="three-search-div second-three-search">
        	<p class="upper-search-p">Start Date</p>
            <input class="search-input" type="date" >
        </div>
    	<div class="three-search-div middle-three-search">
        	<p class="upper-search-p">End Date</p>
            <input class="search-input" type="date" >
        </div>
        <div class="three-search-div second-three-search"><button class="three-search blue-btn clean search-blue-btn">Search</button></div>                
    </div>
    <div class="clear"></div>
    <div class="small-divider"></div>
    <div class="width100">
    	<div class="overflow-scroll-div">    
            <table class="issue-table">
            	<tr>
                	<thead>
                    	<th>No.</th>
                        <th>Receipt No.</th>
                        <th>Plan</th>
                        <th>Amount (RM)</th>
                        <th>Member</th>
                        <th>Issue Date</th>                       
                    </thead>
                </tr>
                <tr data-url="cancelPlanDetails.php" class="link-to-details hover-effect">
                	<td>1.</td>
                    <td>102920</td>
                    <td>Co-Working Space (Hot Seat) X5</td>
                    <td>1995.00</td>
                    <td>Janice Lim</td>
                    <td>12/8/2019</td>                    
                </tr>
                <tr data-url="cancelPlanDetails.php" class="link-to-details hover-effect">
                	<td>2.</td>
                    <td>101920</td>
                    <td>Co-Working Space (Hot Seat) X5</td>
                    <td>1995.00</td>
                    <td>Jenny Lim</td>
                    <td>12/6/2019</td>  
                </tr>                
            </table>
		</div>
    </div>
  		<!--
    
        <div class="clear"></div>
        <div class="fillup-leftspace"></div><a href="addBooking.php"><div class="blue-btn add-new-btn">Add New Booking</div></a>-->
  
     
</div>


<?php include 'js.php'; ?>
</body>
</html>