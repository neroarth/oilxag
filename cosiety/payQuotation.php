<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Quotation | Cosiety" />
<title>Quotation | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
            <h1 class="receipt-title-h1">Quotation</h1> <span class="logo-span"><img src="img/cosiety-logo.png" class="logo-img2" alt="Cosiety" title="Cosiety"></span>
            <div class="clear"></div>
            <div class="address-div">
            	<p class="receipt-address-p"><b class="black-text">Cosiety Sdn. Bhd.</b><br>No, Street,<br>Town, Postcode,<br>City, State,<br>Country</p>
            </div>
            <div class="clear"></div>
            <div class="receipt-half-div">
            	<h2 class="receipt-subtitle-h2 black-text">ISSUED TO</h2>
            </div>
            <div class="receipt-half-div second-receipt-half-div">
            	<h2 class="receipt-subtitle-h2 align-p-h2 black-text">Quotation No.</h2>
                <p class="align-h2-p">112003</p>
            </div>            
            <div class="clear"></div>
            <div class="receipt-half-div">
            	<p class="receipt-address-p no-margin-top"><b class="black-text">Company Name</b><br>No, Street,<br>Town, Postcode,<br>City, State,<br>Country</p>
            </div>            
            <div class="receipt-half-div second-receipt-half-div">
            	<h2 class="receipt-subtitle-h2 align-p-h2 no-margin-top black-text">Issue Date</h2>
                <p class="align-h2-p no-margin-top">12/8/2019</p>
                <div class="clear"></div>
            	<h2 class="receipt-subtitle-h2 align-p-h2 no-margin-top black-text">Expired On</h2>
                <p class="align-h2-p no-margin-top">1/8/2019</p>
                <div class="clear"></div>
            	<h2 class="receipt-subtitle-h2 align-p-h2 no-margin-top black-text">Renew Quo. No.</h2>
                <p class="align-h2-p no-margin-top">-</p>                                
            </div>  
            <div class="clear"></div>            
            <div class="width100 receipt-border"></div>             
            <div class="clear"></div>
            <div class="receipt-half-div">
            	<p class="receipt-upper-p">Plan<br>
                <b class="receipt-lower-p">Co-Working Space (Hot Seat)</b></p>
            </div>            
            <div class="receipt-half-div second-receipt-half-div">
            	<p class="receipt-upper-p">Duration<br>
                <b class="receipt-lower-p">1 Month</b></p>
            </div> 
            <div class="clear"></div>  
            <div class="receipt-half-div">
            	<p class="receipt-upper-p">Start Date<br>
                <b class="receipt-lower-p">1/9/2019</b></p>
            </div>            
            <div class="receipt-half-div second-receipt-half-div">
            	<p class="receipt-upper-p">End Date<br>
                <b class="receipt-lower-p">1/10/2019</b></p>
            </div> 
            <div class="clear"></div> 
            <div class="receipt-half-div">
            	<p class="receipt-upper-p">Reserved Seat<br>
                <b class="receipt-lower-p">1,2,3</b></p>
            </div>            
            <div class="clear"></div>
            <div class="receipt-half-div">
            	<p class="receipt-upper-p">Company Involved<br>
                <b class="receipt-lower-p">XXX Company</b></p>
            </div>            
            <div class="receipt-half-div second-receipt-half-div">
            	<p class="receipt-upper-p">&nbsp;<br>
                <b class="receipt-lower-p">ABC Company</b></p>
            </div> 
            <div class="clear"></div> 
            <div class="receipt-half-div">
            	<p class="receipt-upper-p">Payment<br>
                <b class="receipt-lower-p">Total: RM957.60</b></p>
            </div> 
            <div class="clear"></div>               
            <div class="width100 receipt-border"></div>  
            <div class="clear"></div>                
            <!--<h2 class="backend-title-h2 payment-h2">Payment</h2>
            <div class="half-div-radio">
                <label class="container2">
                  <div class="payment1-div">
                    <p class="thin-payment-p">Total: RM957.60</p>
                    <p class="thick-payment-p">Total: RM957.60</p>
                  </div>
                  <input type="radio" checked="checked" name="radio">
                  <span class="checkmark2"></span>
                </label>
            </div>
            <div class="half-div-radio">    
                <label class="container2">
                  <div class="payment1-div">
                    <p class="thin-payment-p">Monthly Payment (No Extra Discount)</p>
                    <p class="thick-payment-p">Total: RM957.60/month</p>
                  </div>       
                  <input type="radio" name="radio">
                  <span class="checkmark2"></span>
                </label>
            </div>-->
            <div class="clear"></div>
            <h2 class="backend-title-h2">Choose Your Payment Method</h2>  
            <div class="three-div-radio">
                <label class="container2"><img src="img/ipay88.png" class="payment-img" alt="ipay88" title="ipay88">      
                  <input type="radio" name="radio1">
                  <span class="checkmark2"></span>
                </label>    
            </div>
            <div class="three-div-radio">
                <label class="container2"><img src="img/visa-mastercard.png" class="payment-img" alt="Visa/Master Card" title="Visa/Master Card">      
                  <input type="radio" name="radio1">
                  <span class="checkmark2"></span>
                </label>    
            </div>    
            <div class="three-div-radio no-margin-right">
                <label class="container2">Online Banking      
                  <input type="radio" name="radio1">
                  <span class="checkmark2"></span>
                </label>    
            </div>  
            <div class="clear"></div>
            <div class="divider"></div>  
            <div class="width100 overflow">
            	<div class="fillup-2-btn-space"></div>
                <button class="clean print-btn">Send Quotation</button>
            	<!--Maybe will go to a page with QR code can scan and pay? ---><div class="blue-btn payment-button clean next-btn view-plan-btn">Pay Now</div></a>
                <div class="fillup-2-btn-space"></div>
            </div>
            <div class="clear"></div>
            <div class="small-divider"></div>
    		<div class="fillup-extra-space2"></div><a  onclick="goBack()" class="cancel-a hover-effect">Cancel</a>

</div>


<?php include 'js.php'; ?>
</body>
</html>