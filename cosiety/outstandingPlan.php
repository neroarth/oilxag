<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Outstanding Plan | Cosiety" />
<title>Outstanding Plan | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1 hover1 align-select-h1 issue-h1">Outstanding Plan | <a href="paidPlan.php" class="lightblue-text hover-effect">Paid Plan</a></h1>
	<select class="clean align-h1-select issue-select">
    	<option>Latest</option>
        <option>Oldest</option>
        <option>Highest Amount</option>
        <option>Lowest Amount</option>
    </select>
	<div class="clear"></div>
   
    <div class="width100 search-div overflow">
    	<div class="three-search-div">
        	<p class="upper-search-p">Quotation No.</p>
            <input class="search-input" type="text" placeholder="Quotation No.">
        </div>
    	<div class="three-search-div middle-three-search second-three-search">
        	<p class="upper-search-p">Plan</p>
            <select class="search-input">
            	<option>Lounge</option>
                <option>Dedicated Work Desk</option>
                <option>Co-Working Space (Hot Seat)</option>
                <option>Private Suit</option>                
            </select>
        </div>
        <div class="three-search-div">
        	<p class="upper-search-p">Member</p>
            <input class="search-input" type="text" placeholder="Member">
        </div>
    	<div class="three-search-div second-three-search">
        	<p class="upper-search-p">Start Date</p>
            <input class="search-input" type="date" >
        </div>
    	<div class="three-search-div middle-three-search">
        	<p class="upper-search-p">End Date</p>
            <input class="search-input" type="date" >
        </div>
        <div class="three-search-div second-three-search"><button class="three-search blue-btn clean search-blue-btn">Search</button></div>                
    </div>
    <div class="clear"></div>
    <h2 class="backend-title-h2 review-title">Total: RM100.00</h2>      
    <div class="small-divider"></div>
    <div class="width100">
    	<div class="overflow-scroll-div">    
            <table class="issue-table">
            	<tr>
                	<thead>
                    	<th>No.</th>
                        <th>Quotation No.</th>
                        <th>Plan</th>
                        <th>Duration</th>
                        <th>Amount (RM)</th>
                        <th>Member</th>
                        <th>Should Pay On</th>
                    </thead>
                </tr>
                <tr data-url="quotation.php" class="link-to-details hover-effect">
                	<td>1.</td>
                    <td>102920</td>
                    <td>Private Suit</td>
                    <td>1 Month</td>
                    <td>1000.00</td>
                    <td>Alicia Tang</td>
                    <td>1/8/2019</td>
                </tr>
                <tr data-url="quotation.php" class="link-to-details hover-effect">
                	<td>2.</td>
                    <td>102921</td>
                    <td>Private Suit</td>
                    <td>1 Month</td>
                    <td>1000.00</td>
                    <td>Jenny Lim</td>
                    <td>1/8/2019</td>
                </tr>                
            </table>
		</div>
    </div>
  		<!--
    
        <div class="clear"></div>
        <div class="fillup-leftspace"></div><a href="addBooking.php"><div class="blue-btn add-new-btn">Add New Booking</div></a>-->
  
     
</div>


<?php include 'js.php'; ?>
</body>
</html>