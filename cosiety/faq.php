<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="FAQ | Cosiety" />
<title>FAQ | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1">FAQ</h1>
    <div class="width100 overflow">
    	<p class="grey-text input-top-p float-left">Question 1</p><span class="right-remove-span hover-effect faq-remove">Remove</span> 
        <div class="clear"></div>
    	<input type="text" class="three-select clean" placeholder="FAQ">
    </div>
    <div class="width100 overflow">
    	<p class="grey-text input-top-p">Question 1 Answer</p>
    	<input type="text" class="three-select clean" placeholder="FAQ Answer">
    </div>
    <div class="clear"></div>
    <div class="divider"></div> 
    <div class="width100 overflow">
    	<p class="grey-text input-top-p float-left">Question 2</p><span class="right-remove-span hover-effect faq-remove">Remove</span>
        <div class="clear"></div>
    	<input type="text" class="three-select clean" placeholder="FAQ">
    </div>
    <div class="width100 overflow">
    	<p class="grey-text input-top-p">Question 2 Answer</p>
    	<input type="text" class="three-select clean" placeholder="FAQ Answer">
    </div> 
    <div class="clear"></div>
    <div class="divider"></div>     
    <div class="fillup-extra-space"></div><a href="adminCustomise.php"><button class="blue-btn payment-button clean">Confirm</button></a>
    <div class="clear"></div>
    <div class="fillup-extra-space2"></div><a  onclick="goBack()" class="cancel-a hover-effect">Cancel</a>
</div>


<?php include 'js.php'; ?>
</body>
</html>