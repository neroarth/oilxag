<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Add Slot | Cosiety" />
<title>Add Slot | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1">Add Slot</h1>
	<div class="edit-half-div">
    	<p class="grey-text input-top-p">Slot Name</p>
        <input class="three-select clean" type="text" placeholder="Seat Name">
	</div>           
	<div class="clear"></div>
	<div class="edit-half-div">
    	<p class="grey-text input-top-p">Min. People</p>
		<input class="three-select clean" type="number" placeholder="1">
	</div>
	<div class="edit-half-div second-edit-half-div">
    	<p class="grey-text input-top-p">Max. People</p>
        <input class="three-select clean" type="number" placeholder="1">
	</div>                    
    <div class="clear"></div>
    <div class="width100 overflow">
            <div class="upload-btn-wrapper">
              <button class="upload-btn">Upload Icon</button>
              <input class="hidden-input" type="file" name="myfile" />
            </div>
            <!-- Crop the image 16:9 -->
            <p class="img-preview">Image Preview</p> 
            <div class="left-img-preview"><img src="img/seat.png" class="uploaded-img"></div><span class="right-remove-span hover-effect">Remove</span>           
    </div> 
    <div class="clear"></div> 
    <div class="clear"></div> 
	<div class="divider"></div>
    <div class="fillup-extra-space"></div><a href="typeofSlot.php"><button class="blue-btn payment-button clean next-btn">Save</button></a>
    <div class="clear"></div>
    <div class="fillup-extra-space2"></div><a  onclick="goBack()" class="cancel-a hover-effect">Cancel</a>
</div>


<?php include 'js.php'; ?>
</body>
</html>