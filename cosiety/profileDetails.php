<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Profile | Cosiety" />
<title>Profile | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<div class="width100 overflow">
    	<h1 class="backend-title-h1">Profile</h1>
    </div>
	<div class="clear"></div>
    <div class="width100 overflow">
    <div class="profile-left-div">
    	<img src="img/big-profile.png" class="profile-profile-img" alt="Profile Picture" title="Profile Picture">
        	<div class="clear"></div>
      
    </div>
    <div class="profile-middle-div">
            <div class="receipt-half-div">
            	<p class="receipt-upper-p">Name<br>
                <b class="receipt-lower-p">Janice Lim</b></p>
            </div>            
            <div class="receipt-half-div second-receipt-half-div">
            	<p class="receipt-upper-p">Position<br>
                <b class="receipt-lower-p">Employer</b></p>
            </div> 
            <div class="clear"></div> 
            <div class="receipt-half-div">
            	<p class="receipt-upper-p">Company<br>
                <a href="company.php"><b class="receipt-lower-p blue-text2 hover-effect">XXX Company</b></a></p>
            </div>            
            <div class="receipt-half-div second-receipt-half-div">
            	<p class="receipt-upper-p">Email<br>
                <b class="receipt-lower-p">janice@gmail.com</b></p>
            </div> 
            <div class="clear"></div>
            <div class="receipt-half-div">
            	<p class="receipt-upper-p">Country<br>
                <b class="receipt-lower-p">Malaysia</b></p>
            </div>            
            <div class="receipt-half-div second-receipt-half-div">
            	<p class="receipt-upper-p">Contact<br>
                <b class="receipt-lower-p">+60 14 533 000</b></p>
            </div> 
            <div class="clear"></div>             
            <div class="receipt-half-div">
            	<p class="receipt-upper-p">Gender<br>
                <b class="receipt-lower-p">Female</b></p>
            </div>            
            <div class="receipt-half-div second-receipt-half-div">
            	<p class="receipt-upper-p">Birthday<br>
                <b class="receipt-lower-p">12/06/1991</b></p>
            </div> 
            <div class="clear"></div>              
            <div class="receipt-half-div">
            	<p class="receipt-upper-p">Bank Name<br>
                <b class="receipt-lower-p">Lim Jia Yi</b></p>
            </div>            
            <div class="receipt-half-div second-receipt-half-div">
            	<p class="receipt-upper-p">Bank<br>
                <b class="receipt-lower-p">Maybank</b></p>
            </div> 
            <div class="clear"></div>      
            <div class="width100 overflow">
            	<p class="receipt-upper-p">Bank Account No.<br>
                <b class="receipt-lower-p">555686557811</b></p>            
            </div>
            <div class="width100 overflow">
            	<p class="receipt-upper-p">About<br>
                <b class="receipt-lower-p">Anything please email to janice@gmail.com</b></p>            
            </div>                                
    </div>
 
    </div>
    <div class="small-divider"></div>
	<div class="clear"></div>
	<h1 class="backend-title-h1">Membership</h1>
    <div class="two-box-container">
        <div class="two-box-div overflow">
            <div class="color-header red-header">
                <img src="img/calendar.png" class="header-icon" alt="My Ongoing Plan" title="My Ongoing Plan"> <p>My Ongoing Plan</p>
                <!--<a href="" class="hover-effect white-text view-a">View All</a>-->
            </div>
            <div class="white-box-content">
            	<a href="receipt.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/seat.png" class="white-icon2 hover-effect" alt="Basic Plan E" title="Basic Plan E"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect">Today    10:00 am - Today 6:00 pm</p>
                            <p class="white-box-content-p hover-effect">Co-Working Space - No.1</p>
                        </div>
                    </div>
                </a>
                <a href="receipt.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/seat.png" class="white-icon2 hover-effect" alt="Basic Plan C" title="Basic Plan C"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect">Tomorrow    10:00 am - Tomorrow    6:00 pm</p>
                            <p class="white-box-content-p hover-effect">Co-Working Space - No.2</p>
                        </div>
                    </div>
                </a>
                <a href="receipt.php" class="hover-effect">               
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/seat.png" class="white-icon2 hover-effect" alt="Basic Plan C" title="Basic Plan C"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect">12/8/2019   10:00 am - 12/8/2019   6:00 pm</p>
                            <p class="white-box-content-p hover-effect">Co-Working Space - No.3</p>
                        </div>
                    </div>   
                </a>                     
            </div>
        </div>
        <div class="two-box-div overflow second-box">
            <div class="color-header orange-header">
                <img src="img/bill.png" class="header-icon" alt="Upcoming Payment" title="Upcoming Payment"> <p>Upcoming Payment</p>
                <!--<a href="booking.php" class="hover-effect white-text view-a">View All</a>-->
            </div>
            <div class="white-box-content">
            	<a href="quotation.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/meeting-room.png" class="white-icon2 hover-effect" alt="Lounge" title="Lounge"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect left-date">Expire on 12/9/2019    10:00 am</p><p class="black-text right-price">RM99.00</p>
                            <p class="white-box-content-p hover-effect clear">Lounge - Monthly Membership</p>
                        </div>
                    </div>
                </a>
                <a href="quotation.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/group.png" class="white-icon2 hover-effect" alt="Private Suit 1 Work Station" title="Private Suit 1 Work Station"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect left-date">Expire on 13/8/2019   10:00 am</p><p class="black-text right-price">RM1000.00</p>
                            <p class="white-box-content-p hover-effect clear">Private Suit 1 Work Station</p>
                        </div>
                    </div>
                </a>
                <a href="quotation.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/group.png" class="white-icon2 hover-effect" alt="Private Suit 2 Work Stations" title="Private Suit 2 Work Stations"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect left-date">Expire on 14/8/2019   10:00 am</p><p class="black-text right-price">RM1600.00</p>
                            <p class="white-box-content-p hover-effect clear">Private Suit 2 Work Stations</p>
                        </div>
                    </div> 
                </a>                       
            </div>            
        </div> 
    </div>  

        <!-- Add class booking for booking day inside the div class="day" inside calendar.js-->
        <div class="two-box-div">
            <div class="color-header blue-header top-radius">
                <img src="img/expired-plan.png" class="header-icon" alt="Expired Plan" title="Expired Plan"> <p>Expired Plan</p>
                <a href="allPlan.php" class="hover-effect white-text view-a">View All</a>
            </div>
            <div class="white-box-content">
            	<a href="receipt.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/meeting-room.png" class="white-icon2 hover-effect" alt="Basic Plan E" title="Basic Plan E"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect left-date">Expire on 12/8/2019    10:00 am <span class="green-status">(PAID)</span></p><p class="black-text right-price">RM99.00</p>
                            <p class="white-box-content-p hover-effect clear">Lounge - Monthly Membership</p>
                        </div>
                    </div>
                </a>
                <a href="receipt.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/meeting-room.png" class="white-icon2 hover-effect" alt="Basic Plan E" title="Basic Plan E"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect left-date">Expire on 13/7/2019   10:00 am <span class="green-status">(PAID)</span></p><p class="black-text right-price">RM99.00</p>
                            <p class="white-box-content-p hover-effect clear">Lounge - Monthly Membership</p>
                        </div>
                    </div>
                </a>
                <a href="receipt.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/meeting-room.png" class="white-icon2 hover-effect" alt="Basic Plan E" title="Basic Plan E"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect left-date">Expire on 14/6/2019   10:00 am <span class="green-status">(PAID)</span></p><p class="black-text right-price">RM99.00</p>
                            <p class="white-box-content-p hover-effect clear">Lounge - Monthly Membership</p>
                        </div>
                    </div> 
                </a>                       
            </div>  
        </div>
        <div class="two-box-div overflow second-box image-container">  
            <!-- Crop image into 16:9, preset the booking details to the plan and discount details. -->
            <a href="addBookingDetails.php" class="hover-effect"><img src="img/promotion.jpg" class="width100" alt="Promotion" title="Promotion"></a>
        </div>  
      	<div class="clear"></div>
        <div class="small-divider"></div>
        <div class="width100 overflow">
            <p class="grey-text input-top-p">Reason of Blacklist</p>
            <input class="three-select clean" placeholder="Type the reason here" type="text">
		</div>
        <div class="small-divider"></div>
        <div class="clear"></div>
        <div class="width100 overflow receipt-two-btn-container">
        	<div class="fillup-2-btn-space"></div>
        	<div class="clean print-btn text-center"    onclick="goBack()">Back</div>
        	<button class="payment-button clean next-btn view-plan-btn red-btn">Blacklist</button>
        	<div class="fillup-2-btn-space"></div>
        </div>  
</div>


<?php include 'js.php'; ?>
</body>
</html>