<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Add Booking Area | Cosiety" />
<title>Add Booking Area | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1">Add Booking Area</h1>
	<div class="three-div">
    	<p class="grey-text input-top-p">Seat Name (1)</p>
		<input type="text" class="three-select clean" placeholder="Seat Name 1">
    </div>
	<div class="three-div middle-three-div second-three-div">
    	<p class="grey-text input-top-p">Seat Name (2)</p>
        <input type="text" class="three-select clean" placeholder="Seat Name 2">
    </div>
	<div class="three-div">
    	<p class="grey-text input-top-p">Seat Name (3)</p>
		<input type="text" class="three-select clean" placeholder="Seat Name 3">
    </div>
    <div class="tempo-three-clear"></div> 
 	<div class="three-div second-three-div">
    	<p class="grey-text input-top-p">Small Meeting Room Name (1)</p>
		<input type="text" class="three-select clean" placeholder="E-1">
    </div>
	<div class="three-div middle-three-div">
    	<p class="grey-text input-top-p">Small Meeting Room Name (2)</p>
		<input type="text" class="three-select clean" placeholder="E-2">
    </div>
 	<div class="three-div second-three-div">
    	<p class="grey-text input-top-p">Small Meeting Room Name (3)</p>
		<input type="text" class="three-select clean" placeholder="E-3">
    </div> 
    <div class="clear"></div>
    <div class="divider"></div>
	<h2 class="backend-title-h2">Upload Floor Plan</h2>  
    <div class="width100 overflow">
            <div class="upload-btn-wrapper">
              <button class="upload-btn">Upload Image</button>
              <input class="hidden-input" type="file" name="myfile" />
            </div>
            <p class="img-preview">Image Preview</p> 
            <div class="left-img-preview ow-width100"><img src="img/floor-plan.jpg" class="uploaded-img" alt="Floor Plan" title="Floor Plan" ></div><span class="right-remove-span hover-effect">Remove</span>           
    </div>       
    <div class="clear"></div>
    <div class="divider"></div>
	<h2 class="backend-title-h2">Upload Area Photo</h2>      
    <div class="width100 overflow">
    	<input type="text" class="three-select clean" placeholder="Photo Name">
    </div>
    <div class="width100 overflow">
            <div class="upload-btn-wrapper">
              <button class="upload-btn">Upload Image</button>
              <input class="hidden-input" type="file" name="myfile" />
            </div>
            <p class="img-preview">Image Preview</p> 
            <div class="left-img-preview"><img src="img/working-space1.jpg" class="uploaded-img" alt="Area Photo" title="Area Photo" ></div><span class="right-remove-span hover-effect">Remove</span>           
    </div>          
 	<div class="hover-effect black-btn-add">Add</div>
	<div class="divider"></div>
    <div class="clear"></div>    
    <div class="fillup-extra-space"></div><a href="bookingArea.php"><button class="blue-btn payment-button clean">Confirm</button></a>
    <div class="clear"></div>
    <div class="fillup-extra-space2"></div><a  onclick="goBack()" class="cancel-a hover-effect">Cancel</a>
</div>


<?php include 'js.php'; ?>
</body>
</html>