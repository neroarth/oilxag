<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Helpdesk | Cosiety" />
<title>Helpdesk | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1 align-select-h1">Helpdesk <a href="addIssue.php" class="hover1"><img src="img/add.png" class="add-icon hover1a" alt="Add New Issue" title="Add New Issue"><img src="img/add2.png" class="add-icon hover1b" alt="Add New Issue" title="Add New Issue"></a> | <a href="faqList.php" class="lightblue-text hover-effect">FAQ</a></h1>
	<select class="clean align-h1-select">
    	<option>Latest</option>
        <option>Oldest</option>
        <option>Solved</option>
        <option>Unsolved</option>
    </select>
	<div class="clear"></div>
    <div class="width100">
    	<div class="overflow-scroll-div">    
            <table class="issue-table">
            	<tr>
                	<thead>
                    	<th>No.</th>
                        <th>Issue No.</th>
                        <th>Issue</th>
                        <th>Submitted Date</th>
                        <th>Status</th>
                    </thead>
                </tr>
                <tr data-url="issue.php" class="link-to-details hover-effect">
                	<td>1.</td>
                    <td>102920</td>
                    <td>Paper and coffee are running out of stock.</td>
                    <td>12/8/2019</td>
                    <td class="orange-status">Submitted</td>
                </tr>
                <tr data-url="issue.php" class="link-to-details hover-effect">
                	<td>2.</td>
                    <td>102921</td>
                    <td>Need extra 1 more extension.</td>
                    <td>12/6/2019</td>
                    <td class="green-status">Solved</td>
                </tr>                
            </table>
		</div>
    </div>
  		<!--
        <div class="clear"></div>
        <div class="fillup-leftspace"></div><a href="addBooking.php"><div class="blue-btn add-new-btn">Add New Booking</div></a>-->
  
     
</div>


<?php include 'js.php'; ?>
</body>
</html>