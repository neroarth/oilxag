<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="FAQ | Cosiety" />
<title>FAQ | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1">FAQ | <a href="helpdesk.php" class="lightblue-text hover-effect">Helpdesk</a> <a href="addIssue.php" class="hover1"><img src="img/add.png" class="add-icon hover1a" alt="Add New Issue" title="Add New Issue"><img src="img/add2.png" class="add-icon hover1b" alt="Add New Issue" title="Add New Issue"></a></h1>
	<div class="clear"></div>
    <div class="faq-width overflow">
 	    <p class="grey-text input-top-p">Question 1 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus posuere, libero nec luctus commodo, sapien eros condimentum lacus, sed dictum sem est id massa? </p>
    	<p class="answer-p">Answer 1 Nam vitae vestibulum purus, nec elementum neque. Aliquam non dapibus risus, id egestas dolor. Quisque porttitor metus a egestas pharetra. Cras ullamcorper magna sem, eget cursus velit pellentesque vitae. Nullam mauris sapien, fermentum a gravida vitae, molestie sed eros. Aenean in neque ut ante lacinia rutrum at ac turpis.</p>

    	<div class="small-divider"></div>

 	    <p class="grey-text input-top-p">Question 2 Aliquam egestas facilisis posuere?</p>
    	<p class="answer-p">Answer 2 Vestibulum ornare tellus nec ullamcorper rhoncus. Cras vehicula metus nec pharetra venenatis. Nunc ac finibus leo, quis imperdiet dolor. Curabitur id massa consectetur risus viverra tincidunt.</p>
    </div>    
  
     
</div>


<?php include 'js.php'; ?>
</body>
</html>