<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Booking Dashboard | Cosiety" />
<title>Booking Dashboard | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1">Booking</h1>
    <div class="clear"></div>
 	<div class="small-divider width100"></div>
    <div class="clear"></div>
	<a href="adminAddBooking.php">
        <div class="four-white-div">
            <div class="color-circle color-circle1">
                <img src="img/add3.png" class="circle-icon" alt="Add New Booking"  title="Add New Booking">
            </div>
            <p class="black-text white-div-title">Add New Booking</p>
        </div>
    </a>
	<a href="cancelPlan.php">
        <div class="four-white-div second-white-div third-two four-two">
            <div class="color-circle color-circle2">
                <img src="img/cancel.png" class="circle-icon" alt="Cancel Booking"  title="Cancel Booking">
            </div>
            <p class="black-text white-div-title">Cancel Booking</p>
        </div>
    </a>   
	<a href="updateBookingDetails.php">
        <div class="four-white-div third-white-div">
            <div class="color-circle color-circle3">
                <img src="img/edit.png" class="circle-icon" alt="Update Details"  title="Update Details">
            </div>
            <p class="black-text white-div-title">Update Details</p>
        </div>
    </a>   
	<a href="viewCalendar.php">
        <div class="four-white-div four-two">
            <div class="color-circle color-circle4">
                <img src="img/helpdesk-issue.png" class="circle-icon" alt="Calendar Settings"  title="Calendar Settings">
            </div>
            <p class="black-text white-div-title">Calendar Settings</p>
        </div>
    </a>  
             
</div>


<?php include 'js.php'; ?>
</body>
</html>