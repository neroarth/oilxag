<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Dashboard | Cosiety" />
<title>Dashboard | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1">Dashboard</h1>
    <div class="two-box-container">
        <div class="two-box-div overflow">
            <div class="color-header red-header">
                <img src="img/notification.png" class="header-icon" alt="Notification/News" title="Notification/News"> <p>Notification/News</p>
                <a href="" class="hover-effect white-text view-a">View All</a>
            </div>
            <div class="white-box-content">
            	<a href="paymentMethod.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div grey-icon hover-effect"><img src="img/bill.png" class="white-icon2 hover-effect" alt="Invoice" title="Invoice"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect">12/8/2019    10:00 am</p>
                            <p class="white-box-content-p hover-effect">Next Invoice: 12/9/2019 (RM 200.00)</p>
                        </div>
                    </div>
                </a>
                <a href="addBooking.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div grey-icon hover-effect"><img src="img/booking.png" class="white-icon2 hover-effect" alt="Booking" title="Booking"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect">12/8/2019    10:00 am</p>
                            <p class="white-box-content-p hover-effect">Booking for 1 month and get a 20% discount for Private Suit!</p>
                        </div>
                    </div>
                </a>
                <a href="addBooking.php" class="hover-effect">               
                    <div class="content-container">
                        <div class="left-icon-div grey-icon hover-effect"><img src="img/calendar.png" class="white-icon2 hover-effect" alt="Calendar" title="Calendar"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect">12/8/2019    10:00 am</p>
                            <p class="white-box-content-p hover-effect">Your booking space will be expired on 12/8/2019 10:00 am</p>
                        </div>
                    </div>   
                </a>                     
            </div>
        </div>
        <div class="two-box-div overflow second-box">
            <div class="color-header orange-header">
                <img src="img/booking.png" class="header-icon" alt="Booking" title="Booking"> <p>All Booking</p>
                <a href="booking.php" class="hover-effect white-text view-a">View All</a>
            </div>
            <div class="white-box-content">
            	<a href="receipt.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/seat.png" class="white-icon2 hover-effect" alt="Co-Working Space - No.1" title="Co-Working Space - No.1"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect">Today    10:00 am - Today 6:00 pm</p>
                            <p class="white-box-content-p hover-effect">Co-Working Space - No.1</p>
                        </div>
                    </div>
                </a>
                <a href="receipt.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/seat.png" class="white-icon2 hover-effect" alt="Co-Working Space - No.1" title="Co-Working Space - No.1"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect">Tomorrow    10:00 am - Tomorrow    6:00 pm</p>
                            <p class="white-box-content-p hover-effect">Co-Working Space - No.1</p>
                        </div>
                    </div>
                </a>
                <a href="receipt.php" class="hover-effect">
                    <div class="content-container">
                        <div class="left-icon-div green-icon hover-effect"><img src="img/seat.png" class="white-icon2 hover-effect" alt="Co-Working Space - No.1" title="Co-Working Space - No.1"></div>
                        <div class="right-icon-div">
                            <p class="light-grey-text small-date hover-effect">12/8/2019   10:00 am - 12/8/2019   6:00 pm</p>
                            <p class="white-box-content-p hover-effect">Co-Working Space - No.1</p>
                        </div>
                    </div> 
                </a>                       
            </div>            
        </div> 
    </div>  

        <!-- Add class booking for booking day inside the div class="day" inside calendar.js-->
        <div class="two-box-div">
             <html ng-app="myApp" ng-controller="AppCtrl" lang="en">
              <head>
                <meta charset="utf-8">
                <title>Circle</title>
              </head>
              <body>
                <div calendar class="calendar" id="calendar"></div>
              </body>
            </html>   
        </div>
        <div class="two-box-div overflow second-box image-container">  
            <!-- Crop image into 16:9, preset the booking details to the plan and discount details. -->
            <a href="addBookingDetails.php" class="hover-effect"><img src="img/promotion.jpg" class="width100" alt="Promotion" title="Promotion"></a>
        </div>  
  
     
</div>


<?php include 'js.php'; ?>
</body>
</html>