<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="View Plan Details | Cosiety" />
<title>View Plan Details | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1">View Plan Details</h1>

	<p class="grey-text input-top-p">Project Title</p>
	<input type="text" class="three-select clean width100" placeholder="Key in Project Title">    
 	<p class="grey-text input-top-p project-p">Project Details</p>
	<textarea class="clean width100 project-textarea" placeholder="Key in Project Details"></textarea>     
    <div class="fillup-extra-space"></div><button class="blue-btn payment-button clean">Save</button>
    <div class="clear"></div>
    <div class="fillup-extra-space3"></div><a href="receipt.php" class="cancel-a hover-effect">View Receipt</a>
</div>


<?php include 'js.php'; ?>
</body>
</html>