<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Issue | Cosiety" />
<title>Issue | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<div class="receipt-half-div issue-half-div">
    	<h1 class="backend-title issue-title-h1">Issue No. 101920 - <b class="green-status">Solved</b></h1>
    </div>
	<div class="receipt-half-div receipt-half-div2">
    	<h2 class="issue-right-h2">Submitted Date</h2>
        <p class="issue-right-p">12/6/2019</p>
        <div class="clear"></div>
        <h2 class="issue-right-h2 issue-right-h2-2">Solved Date</h2>
        <p class="issue-right-p issue-right-p-2">15/6/2019</p>
    </div>   
    <div class="clear"></div>
    <div class="width100 overflow">
        <p class="grey-text input-top-p">Issue</p>
        <p class="three-select-p no-margin-top">Need extra 1 more extension.</p>
            <a href="./img/promotion.jpg"  data-fancybox="images-preview2">
                <img src="img/promotion.jpg" class="comment-pic hover-effect">
            </a>          
    </div> 
    <div class="clear"></div>
    <div class="width"
    <h2 class="backend-title-h2">Progress</h2> 
    <div class="width100 message-border"></div>   
	<div class="width100 overflow message-container">
    	<div class="profile-div">
        	<img src="img/profile2.png" alt="Profile Picture" title="Profile Picture" class="profile-img">
        </div>
        <div class="message-div">
        	<p class="date-details">12/6/2019 &nbsp; &nbsp; &nbsp; 10:00 am  - <span class="orange-status">Submitted</span><br>
            	<b class="extra-b">Need extra 1 more extension.</b>
            </p>
            <a href="./img/promotion.jpg"  data-fancybox="images-preview2">
                <img src="img/promotion.jpg" class="comment-pic hover-effect">
            </a>  
            
        </div>
    </div>
    <div class="width100 message-border"></div> 
	<div class="width100 overflow message-container">
    	<div class="profile-div">
        	<img src="img/admin.png" alt="Profile Picture" title="Profile Picture" class="profile-img">
        </div>
        <div class="message-div">
        	<p class="date-details">12/6/2019 &nbsp; &nbsp; &nbsp; 10:010 am  - <span class="orange-status">In Progress</span><br>
            	<b class="extra-b">Will purchase it in this week.</b>
            </p>
        </div>
    </div>
    <div class="width100 message-border"></div> 
	<div class="width100 overflow message-container">
    	<div class="profile-div">
        	<img src="img/admin.png" alt="Profile Picture" title="Profile Picture" class="profile-img">
        </div>
        <div class="message-div">
        	<p class="date-details">15/6/2019 &nbsp; &nbsp; &nbsp; 10:010 am  - <span class="green-status">Solved</span><br>
            	<b class="extra-b">Please request it from the counter. Thank you.</b>
            </p>
        </div>
    </div>    
    <div class="width100 message-border"></div>   
    <div class="clear"></div>

    <!-- If admin section, remove the review, Review only for Member-->
    <h2 class="backend-title-h2 review-title">Review</h2>  
    <div class="width100 overflow">
    	  <div class="rate">
            <input type="radio" id="star5" name="rate" value="5" />
            <label for="star5" title="Excellent">5 stars</label>
            <input type="radio" id="star4" name="rate" value="4" />
            <label for="star4" title="Good">4 stars</label>
            <input type="radio" id="star3" name="rate" value="3" />
            <label for="star3" title="Neutral">3 stars</label>
            <input type="radio" id="star2" name="rate" value="2" />
            <label for="star2" title="Bad">2 stars</label>
            <input type="radio" id="star1" name="rate" value="1" />
            <label for="star1" title="Extremely Bad">1 star</label>
          </div>
    </div> 
    <!--- End of Review -->
    <h2 class="backend-title-h2 review-title review-title2">Reply</h2>
    	<p class="grey-text input-top-p reply-top-p">Status</p>
        <select class="three-select clean reply-status">
        	<option>Submitted</option>
            <option>In Progress</option>
            <option>Solved</option>
            <option>Abandonment Issue</option>
        </select>         
    <div class="width100 overflow">
			<textarea class="clean width100 project-textarea reply-textarea" placeholder="Type your comment here."></textarea>   
    </div> 
    <div class="width100 overflow">
            <div class="upload-btn-wrapper">
              <button class="upload-btn">Upload Image</button>
              <input class="hidden-input" type="file" name="myfile" />
            </div>
            <!-- Crop the image 16:9 -->
            <p class="img-preview">Image Preview</p> 
            <div class="left-img-preview"><img src="img/promotion.jpg" class="uploaded-img issue-comment-pic"></div><span class="right-remove-span hover-effect">Remove</span>           
    </div>      
    <div class="clear"></div>
    <div class="divider"></div>
    <div class="fillup-extra-space"></div><button class="blue-btn payment-button clean">Submit</button>
    <div class="clear"></div>
    <div class="divider"></div>            
</div>
</div>

<?php include 'js.php'; ?>
</body>
</html>