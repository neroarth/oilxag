<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Member | Cosiety" />
<title>Member | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1 align-select-h1">Member<!--<a href="addMember.php"><img src="img/add.png" class="add-icon hover1a" alt="Add Member" title="Add Member"><img src="img/add2.png" class="add-icon hover1b" alt="Add Member" title="Add Member"></a>-->
     | <a class="lightblue-text hover-effect" href="companyList.php">Company</a> <a href="addMember.php" class="hover1"><img src="img/add.png" class="add-icon hover1a" alt="Add a New Company" title="Add a New Company"><img src="img/add2.png" class="add-icon hover1b" alt="Add a New Company" title="Add a New Company"></a>
    </h1>
	<select class="clean align-h1-select">
    	<option>Latest</option>
        <option>Oldest</option>
    </select>
	<div class="clear"></div>
    <div class="width100 search-div overflow">
    	<div class="three-search-div">
        	<p class="upper-search-p">Member</p>
            <input class="search-input" type="text" placeholder="Member Name">
        </div>
    	<div class="three-search-div middle-three-search second-three-search">
        	<p class="upper-search-p">Company</p>
            <input class="search-input" type="text" placeholder="Company Name">
        </div>
        <div class="three-search-div">
        	<p class="upper-search-p">Contact</p>
            <input class="search-input" type="number" placeholder="Contact">
        </div>
    	<div class="three-search-div second-three-search">
        	<p class="upper-search-p">Email</p>
            <input class="search-input" type="email" placeholder="Email" >
        </div>        
    	<div class="three-search-div middle-three-search">
        	<p class="upper-search-p">Start Date</p>
            <input class="search-input" type="date" >
        </div>
    	<div class="three-search-div second-three-search">
        	<p class="upper-search-p">End Date</p>
            <input class="search-input" type="date" >
        </div>
        <div class="three-search-div"><button class="three-search blue-btn clean search-blue-btn">Search</button></div>                
    </div>
    <div class="clear"></div>    
    <div class="small-divider"></div>
    <div class="width100">
    	<div class="overflow-scroll-div">    
            <table class="issue-table">
            	<tr>
                	<thead>
                    	<th>No.</th>
                        <th>Member</th>
                        <th>Company</th>
                        <th>Position</th>
                        <th>Email</th>
                        <th>Contact</th>
                        <th>Joined Date</th>
                        <th>Status</th>                        
                    </thead>
                </tr>
                <tr data-url="profileDetails.php" class="link-to-details hover-effect">
                	<td>1.</td>
                    <td>Janice Lim</td>
                    <td>XXX Company</td>
                    <td>Employer</td>
                    <td>janice@gmail.com</td>
                    <td>014533000</td>         
                    <td>12/8/2019</td>
                    <td class="green-status">Active</td>                  
                </tr>
                <tr data-url="profileDetails.php" class="link-to-details hover-effect">
                	<td>2.</td>
                    <td>Alicia Lim</td>
                    <td>XXX Company</td>
                    <td>Employee</td>
                    <td>alicia@gmail.com</td>
                    <td>012533000</td>         
                    <td>12/8/2019</td>
                    <td class="red-text">Blacklisted</td>   
                </tr>                
            </table>
		</div>
    </div>
  		<!--
        <div class="clear"></div>
        <div class="fillup-leftspace"></div><a href="addBooking.php"><div class="blue-btn add-new-btn">Add New Booking</div></a>-->
  
     
</div>


<?php include 'js.php'; ?>
</body>
</html>