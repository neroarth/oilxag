<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Booking Details | Cosiety" />
<title>Booking Details | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1">Co-Working Space (Hot Seat)</h1>
	<div class="three-div">
    	<p class="grey-text input-top-p">Duration</p>
        <select class="three-select clean">
        	<option>1 month</option>
            <option>2 months</option>
            <option>3 months</option>
        </select>
    </div>
	<div class="three-div middle-three-div second-three-div">
    	<p class="grey-text input-top-p">Start Date</p>
        <input type="date" class="three-select clean">
    </div>
	<div class="three-div">
    	<p class="grey-text input-top-p">How Many Person?</p>
        <select class="three-select clean">
        	<option>3</option>
            <option>4</option>
            <option>5</option>
            <option>6</option> 
            <option>7</option>
            <option>8</option>
            <option>9</option> 
            <option>10</option>                                                          
        </select>
    </div>
    <div class="tempo-three-clear"></div> 
 	<div class="three-div second-three-div">
    	<p class="grey-text input-top-p">By</p>
        <select class="three-select clean">
        	<option>XXX Company</option>
            <option>Personal</option>
        </select>
    </div>
	<div class="three-div middle-three-div">
    	<p class="grey-text input-top-p">Discount</p>
        <p class="three-select-p">20%</p>
    </div>
 	<div class="three-div second-three-div">
    	<p class="grey-text input-top-p">Total</p>
    	<p class="total-p">RM957.60</p>
    </div> 
    <div class="clear"></div>
	<h2 class="backend-title-h2">Floor Plan</h2>    
    <img src="img/floor-plan.jpg" alt="Floor Plan" title="Floor Plan" class="width100">
    <div class="clear"></div>
    <div class="four-img-container">
        <div class="four-img-div">
            <a href="./img/working-space1.jpg"  data-fancybox="images-preview" title="Co-Working Space (Hot Seat)">
                <img src="img/working-space1.jpg" class="width100 opacity-hover" alt="Co-Working Space (Hot Seat)" title="Co-Working Space (Hot Seat)">
            </a>  
            <p class="four-img-p">Co-Working Space (Hot Seat)</p>  	
        </div>
        <div class="four-img-div middle-four-img-div1">
            <a href="./img/working-space2.jpg"  data-fancybox="images-preview" title="Private Suit">
                <img src="img/working-space2.jpg" class="width100 opacity-hover" alt="Private Suit" title="Private Suit">
            </a>  
            <p class="four-img-p">Private Suit</p>  	
        </div>  
        <div class="four-img-div middle-four-img-div2">
            <a href="./img/working-space3.jpg"  data-fancybox="images-preview" title="Lounge">
                <img src="img/working-space3.jpg" class="width100 opacity-hover" alt="Lounge" title="Lounge">
            </a>  
            <p class="four-img-p">Lounge</p>  	
        </div> 
        <div class="four-img-div">
            <a href="./img/working-space4.jpg"  data-fancybox="images-preview" title="Private Suit 1">
                <img src="img/working-space4.jpg" class="width100 opacity-hover" alt="Private Suit 1" title="Private Suit 1">
            </a>  
            <p class="four-img-p">Private Suit 1</p>  	
        </div>                           
	</div>
	<h2 class="backend-title-h2">Choose your seat (3)</h2>    
    <div class="big-container-for-seat">
    	<div class="eight-checkbox">
            <label class="container1"> 1
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>
    	<div class="eight-checkbox">
            <label class="container1"> 2
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 3
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 4
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>
    	<div class="eight-checkbox">
            <label class="container1"> 5
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 6
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>
    	<div class="eight-checkbox">
            <label class="container1"> 7
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 8
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 9
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div> 
    	<div class="eight-checkbox">
            <label class="container1"> 10
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>  
    	<div class="eight-checkbox">
            <label class="container1"> 11
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 12
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 13
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div> 
    	<div class="eight-checkbox">
            <label class="container1"> 14
              <input type="checkbox" >
              <span class="checkmark1"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 15
              <input type="checkbox" disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 16
              <input type="checkbox" disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div> 
    	<div class="eight-checkbox">
            <label class="container1"> 17
              <input type="checkbox" disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>        
    	<div class="eight-checkbox">
            <label class="container1"> 18
              <input type="checkbox" disabled>
              <span class="checkmark1 booked"></span>
            </label>
        </div>                                                                                 
    </div>
    <div class="clear"></div>
	<p class="grey-text input-top-p">Project Title</p>
	<input type="text" class="three-select clean width100" placeholder="Key in Project Title">    
 	<p class="grey-text input-top-p project-p">Project Details</p>
	<textarea class="clean width100 project-textarea" placeholder="Key in Project Details"></textarea>     
    <div class="fillup-extra-space"></div><a href="paymentMethod.php"><button class="blue-btn payment-button clean">Proceed to Payment</button></a>
    <div class="clear"></div>
    <div class="fillup-extra-space2"></div><a  onclick="goBack()" class="cancel-a hover-effect">Cancel</a>
</div>


<?php include 'js.php'; ?>
</body>
</html>