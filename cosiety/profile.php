<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Profile | Cosiety" />
<title>Profile | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<div class="width100 overflow">
    	<h1 class="backend-title-h1">Profile <a href="editProfile.php"><img src="img/edit-button.png" class="add-icon hover-effect" alt="Edit Profile" title="Edit Profile"></a></h1>
    </div>
	<div class="clear"></div>
    <div class="width100 overflow">
    <div class="profile-left-div">
    	<img src="img/big-profile.png" class="profile-profile-img" alt="Profile Picture" title="Profile Picture">
        	<div class="clear"></div>
            <div class="upload-btn-wrapper profile-update-btn">
              <button class="upload-btn">Update</button>
              <input class="hidden-input" type="file" name="myfile" />
            </div>        
    </div>
    <div class="profile-middle-div">
            <div class="receipt-half-div">
            	<p class="receipt-upper-p">Name<br>
                <b class="receipt-lower-p">Janice Lim</b></p>
            </div>            
            <div class="receipt-half-div second-receipt-half-div">
            	<p class="receipt-upper-p">Position<br>
                <b class="receipt-lower-p">Employer</b></p>
            </div> 
            <div class="clear"></div> 
            <div class="receipt-half-div">
            	<p class="receipt-upper-p">Company<br>
                <a href="company.php"><b class="receipt-lower-p blue-text2 hover-effect">XXX Company</b></a></p>
            </div>            
            <div class="receipt-half-div second-receipt-half-div">
            	<p class="receipt-upper-p">Email<br>
                <b class="receipt-lower-p">janice@gmail.com</b></p>
            </div> 
            <div class="clear"></div>
            <div class="receipt-half-div">
            	<p class="receipt-upper-p">Country<br>
                <b class="receipt-lower-p">Malaysia</b></p>
            </div>            
            <div class="receipt-half-div second-receipt-half-div">
            	<p class="receipt-upper-p">Contact<br>
                <b class="receipt-lower-p">+60 14 533 000</b></p>
            </div> 
            <div class="clear"></div>             
            <div class="receipt-half-div">
            	<p class="receipt-upper-p">Gender<br>
                <b class="receipt-lower-p">Female</b></p>
            </div>            
            <div class="receipt-half-div second-receipt-half-div">
            	<p class="receipt-upper-p">Birthday<br>
                <b class="receipt-lower-p">12/06/1991</b></p>
            </div> 
            <div class="clear"></div>              
            <div class="receipt-half-div">
            	<p class="receipt-upper-p">Bank Name<br>
                <b class="receipt-lower-p">Lim Jia Yi</b></p>
            </div>            
            <div class="receipt-half-div second-receipt-half-div">
            	<p class="receipt-upper-p">Bank<br>
                <b class="receipt-lower-p">Maybank</b></p>
            </div> 
            <div class="clear"></div>      
            <div class="width100 overflow">
            	<p class="receipt-upper-p">Bank Account No.<br>
                <b class="receipt-lower-p">555686557811</b></p>            
            </div>
            <div class="width100 overflow">
            	<p class="receipt-upper-p">About<br>
                <b class="receipt-lower-p">Anything please email to janice@gmail.com</b></p>            
            </div>                                
    </div>
 
    </div>
  
</div>


<?php include 'js.php'; ?>
</body>
</html>