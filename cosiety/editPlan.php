<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Edit Plan | Cosiety" />
<title>Edit Plan | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1">Edit Plan</h1>
	<div class="edit-half-div">
    	<p class="grey-text input-top-p">Plan Name</p>
        <input class="three-select clean" type="text" placeholder="Plan Name">
	</div>
	<div class="edit-half-div second-edit-half-div">
    	<p class="grey-text input-top-p">Category</p>
        <select class="three-select clean">
            	<option>Lounge</option>
                <option>Dedicated Work Desk</option>
                <option>Co-Working Space (Hot Seat)</option>   
                <option>Private Suit</option>           
        </select>
	</div>            
	<div class="clear"></div>
	<div class="edit-half-div">
    	<p class="grey-text input-top-p">Min. Booking Duration Unit</p>
        <select class="three-select clean">
        	<option>Month</option>
            <option>Day</option>
        </select>
	</div>
	<div class="edit-half-div second-edit-half-div">
    	<p class="grey-text input-top-p">Min. Booking Duration</p>
        <input class="three-select clean" type="number" placeholder="1">
	</div>            
	<div class="clear"></div>
    
	<div class="edit-half-div">
		<p class="grey-text input-top-p">Price Per Min. Booking Duration (RM)</p>
        <input class="three-select clean" type="number" placeholder="120.00">
	</div>
	<div class="clear"></div>    
	<div class="edit-half-div">
    	<p class="grey-text input-top-p">Slot Type</p>
        <select class="three-select clean">
            	<option>Hot Seat</option>
                <option>Private Suit</option>
                <option>Lounge</option>    
                <option>Dedicated Work Desk</option>           
        </select>
	</div>
	<div class="edit-half-div second-edit-half-div">
    	<p class="grey-text input-top-p">Can Choose the Slot?</p>
        <select class="three-select clean">
            	<option>Yes</option>
                <option>No</option>            
        </select>
	</div>     
    <div class="clear"></div>   
    <div class="width100 overflow">
    	<p class="grey-text input-top-p project-p">Description</p>
		<textarea class="clean width100 project-textarea" placeholder="Key in description here"></textarea>  
	</div>
    <div class="clear"></div>
    <div class="width100 overflow">
            <div class="upload-btn-wrapper">
              <button class="upload-btn">Upload Icon</button>
              <input class="hidden-input" type="file" name="myfile" />
            </div>
            <!-- Crop the image 16:9 -->
            <p class="img-preview">Image Preview</p> 
            <div class="left-img-preview"><img src="img/big-meeting-room.png" class="uploaded-img"></div><span class="right-remove-span hover-effect">Remove</span>           
    </div> 
    <div class="clear"></div> 
	<div class="divider"></div>
    <div class="width100 overflow">
        <div class="fillup-2-btn-space"></div>
        <a href="customisePlan.php"><button class="clean red-btn text-center payment-button clean next-btn view-plan-btn print-btn open-confirm"   >Delete</button></a>
        <a href="customisePlan.php"><button class="payment-button clean next-btn view-plan-btn blue-btn">Save</button></a>
        <div class="fillup-2-btn-space"></div>
    </div> 
    <div class="clear"></div>
    <div class="fillup-extra-space2"></div><a  onclick="goBack()" class="cancel-a hover-effect">Cancel</a>
</div>


<?php include 'js.php'; ?>
</body>
</html>