<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Receipt | Cosiety" />
<title>Receipt | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
            <h1 class="receipt-title-h1">Receipt</h1> <span class="logo-span"><img src="img/cosiety-logo.png" class="logo-img2" alt="Cosiety" title="Cosiety"></span>
            <div class="clear"></div>
            <div class="address-div">
            	<p class="receipt-address-p"><b class="black-text">Cosiety Sdn. Bhd.</b><br>No, Street,<br>Town, Postcode,<br>City, State,<br>Country</p>
            </div>
            <div class="clear"></div>
            <div class="receipt-half-div">
            	<h2 class="receipt-subtitle-h2 black-text">ISSUED TO</h2>
            </div>
            <div class="receipt-half-div second-receipt-half-div">
            	<h2 class="receipt-subtitle-h2 align-p-h2 black-text">Receipt No.</h2>
                <p class="align-h2-p">112003</p>
            </div>            
            <div class="clear"></div>
            <div class="receipt-half-div">
            	<p class="receipt-address-p no-margin-top"><b class="black-text">Company Name</b><br>No, Street,<br>Town, Postcode,<br>City, State,<br>Country</p>
            </div>            
            <div class="receipt-half-div second-receipt-half-div">
            	<h2 class="receipt-subtitle-h2 align-p-h2 no-margin-top black-text">Issue Date</h2>
                <p class="align-h2-p no-margin-top">12/8/2019</p>
            </div>  
            <div class="clear"></div>            
            <div class="width100 receipt-border"></div>             
            <div class="clear"></div>
            <div class="receipt-half-div">
            	<p class="receipt-upper-p">Plan<br>
                <b class="receipt-lower-p">Co-Working Space (Hot Seat)</b></p>
            </div>            
            <div class="receipt-half-div second-receipt-half-div">
            	<p class="receipt-upper-p">Duration<br>
                <b class="receipt-lower-p">1 Month</b></p>
            </div> 
            <div class="clear"></div>  
            <div class="receipt-half-div">
            	<p class="receipt-upper-p">Start Date<br>
                <b class="receipt-lower-p">1/9/2019</b></p>
            </div>            
            <div class="receipt-half-div second-receipt-half-div">
            	<p class="receipt-upper-p">End Date<br>
                <b class="receipt-lower-p">1/10/2019</b></p>
            </div> 
            <div class="clear"></div> 
            <div class="receipt-half-div">
            	<p class="receipt-upper-p">Reserved Working Space<br>
                <b class="receipt-lower-p">3</b></p>
            </div>            
            <div class="clear"></div>
            <div class="receipt-half-div">
            	<p class="receipt-upper-p">No.1<br>
                <b class="receipt-lower-p">Alice Tang</b></p>
            </div>            
            <div class="receipt-half-div second-receipt-half-div">
            	<p class="receipt-upper-p">No.2<br>
                <b class="receipt-lower-p">Jenny Lim</b></p>
            </div> 
            <div class="clear"></div>    
            <div class="receipt-half-div">
            	<p class="receipt-upper-p">No.3<br>
                <b class="receipt-lower-p">Janice Lim</b></p>
            </div>                     
            <div class="clear"></div> 
            <div class="width100 receipt-border"></div>               
            <div class="overflow width100 total-container">
            	<div class="receipt-left-total">Payment Method</div>
                <div class="receipt-right-total">iPay88</div>
            </div>
            <div class="clear"></div>            
            <div class="overflow width100 total-container">
            	<div class="receipt-left-total">Subtotal</div>
                <div class="receipt-right-total">RM957.60</div>
            </div>
            <div class="clear"></div>             
            <div class="overflow width100 total-container">
            	<div class="receipt-left-total">Discount</div>
                <div class="receipt-right-total slight-left">- </div>
            </div>
            <div class="clear"></div> 
            <div class="overflow width100 total-container padding-bottom-0">
            	<div class="receipt-left-total bigger-font">Total</div>
                <div class="receipt-right-total bigger-font">RM957.60</div>
            </div>
            <div class="clear"></div> 
            <div class="width100 receipt-border"></div>             
            <div class="clear"></div>                                                                                                                    
            <div class="divider"></div>
            <div class="clear"></div>
            <div class="width100 overflow receipt-two-btn-container">
            	<div class="fillup-2-btn-space"></div>
                <button class="clean print-btn"  onclick="printReceipt()">Print</button>
            	<a href="viewPlan.php"><div class="blue-btn payment-button clean next-btn view-plan-btn">View Plan</div></a>
                <div class="fillup-2-btn-space"></div>
            </div>
            <div class="clear"></div>


</div>


<?php include 'js.php'; ?>
</body>
</html>