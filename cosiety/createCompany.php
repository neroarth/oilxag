<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Create Company | Cosiety" />
<title>Create Company | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1">Create Company</h1>
	<div class="edit-half-div">
    	<p class="grey-text input-top-p">Company Name</p>
        <input class="three-select clean" placeholder="Company Name" type="text">
	</div>
	<div class="edit-half-div second-edit-half-div">
    	<p class="grey-text input-top-p">Empoyer</p>
		<p class="black-text answer-p">Janice Lim</p>
	</div>            
	<div class="clear"></div>
	<div class="width100 overflow">
    	<p class="grey-text input-top-p">Add Employee &nbsp;  <span class="red-text hover-effect">(Remove)</span></p>
        <select class="three-select clean">
        	<option>Alice Tang</option>
            <option>Felicia Tang</option>
        </select>
        <div class="hover-effect black-btn-add">Add</div>
	</div>
         
	<div class="clear"></div>
    
    <h2 class="backend-title-h2 review-title">Company Collaboration</h2>  
    <div class="clear"></div>
	<div class="width100 overflow">
    	<p class="grey-text input-top-p">Add Company &nbsp;  <span class="red-text hover-effect">(Remove)</span></p>
        <select class="three-select clean">
        	<option>ABC Company</option>
            <option>XXX Company</option>
        </select>
        <div class="hover-effect black-btn-add">Add</div>
	</div>  
    <div class="clear"></div> 
	<div class="divider"></div>
    <div class="clear"></div> 
    <div class="fillup-extra-space"></div><a href="company.php"><button class="blue-btn payment-button clean next-btn">Confirm</button></a>
    <div class="clear"></div>
    <div class="fillup-extra-space2"></div><a  onclick="goBack()" class="cancel-a hover-effect">Cancel</a>
</div>


<?php include 'js.php'; ?>
</body>
</html>