<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Edit Profile | Cosiety" />
<title>Edit Profile | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1">Edit Profile</h1>
	<div class="edit-half-div">
    	<p class="grey-text input-top-p">Name</p>
        <input class="three-select clean" placeholder="Janice Lim" type="text">
	</div>
	<div class="edit-half-div second-edit-half-div">
    	<p class="grey-text input-top-p">Position</p>
        <select class="three-select clean">
        	<option>Employer</option>
            <option>Employee</option>
        </select>
	</div>            
	<div class="clear"></div>
	<div class="edit-half-div">
    	<p class="grey-text input-top-p">Company (<a href="createCompany.php" class="lightblue-text hover-effect">Create a New Company</a>/<a href="createCompany.php" class="orange-status hover-effect">Edit</a>)</p>
        <select class="three-select clean">
        	<option>Company 1</option>
            <option>Company 2</option>
        </select>
	</div>
	<div class="edit-half-div second-edit-half-div">
    	<p class="grey-text input-top-p">Email</p>
        <input class="three-select clean" placeholder="janice@gmail.com" type="email">
	</div>            
	<div class="clear"></div>
	<div class="edit-half-div">
    	<p class="grey-text input-top-p">Country</p>
        <select class="three-select clean">
        	<option>Malaysia</option>
            <option>Singapore</option>
        </select>
	</div>
	<div class="edit-half-div second-edit-half-div">
    	<p class="grey-text input-top-p">Contact</p>
        <span class="country-code2">+60</span><input class="three-select clean phone-input2" placeholder="14 533 000" type="number">
	</div>     
    <div class="clear"></div>   
    	<div class="edit-half-div">
    	<p class="grey-text input-top-p">Gender</p>
        <select class="three-select clean">
        	<option>Female</option>
            <option>Male</option>
        </select>
	</div>
	<div class="edit-half-div second-edit-half-div">
    	<p class="grey-text input-top-p">Birthday</p>
        <input class="three-select clean" type="date">
	</div>    
    <div class="clear"></div>
	<div class="width100 overflow">
    	<p class="grey-text input-top-p">About</p>
		<textarea class="clean width100 project-textarea edit-margin-btm" placeholder="About you"></textarea> 
    </div>    
    <div class="clear"></div> 

    <div class="fillup-extra-space"></div><a href="profile.php"><button class="blue-btn payment-button clean next-btn">Confirm</button></a>
    <div class="clear"></div>
    <div class="fillup-extra-space2"></div><a  onclick="goBack()" class="cancel-a hover-effect">Cancel</a>
</div>


<?php include 'js.php'; ?>
</body>
</html>