<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Edit Promotion | Cosiety" />
<title>Edit Promotion | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1">Edit Promotion</h1>
	<div class="edit-half-div">
    	<p class="grey-text input-top-p">Promotion Start Date</p>
        <input class="three-select clean" type="date">
	</div>
	<div class="edit-half-div second-edit-half-div">
    	<p class="grey-text input-top-p">Promotion End Date</p>
        <input class="three-select clean" type="date">
	</div>            
	<div class="clear"></div>
	<div class="edit-half-div">
    	<p class="grey-text input-top-p">Effective Booking Start Date </p>
        <input class="three-select clean" type="date">
	</div>
	<div class="edit-half-div second-edit-half-div">
    	<p class="grey-text input-top-p">Effective Booking End Date </p>
        <input class="three-select clean" type="date">
	</div>            
	<div class="clear"></div>
	<div class="edit-half-div">
    	<p class="grey-text input-top-p">Select Plan</p>
        <select class="three-select clean">
            	<option>Basic Plan A</option>
                <option>Premium Plan B</option>
                <option>Basic Plan C</option>
                <option>Premium Plan D</option>
                <option>Basic Plan E</option>
                <option>Premium Plan F</option>                
        </select>
	</div>
	<div class="edit-half-div second-edit-half-div">
    	<p class="grey-text input-top-p">Min. Duration</p>
        <select class="three-select clean">
            	<option>2 Months</option>
                <option>3 Months</option>
                <option>4 Months</option>
                <option>6 Months</option>
                <option>12 Months</option>              
        </select>
	</div>     
    <div class="clear"></div>   
    	<div class="edit-half-div">
    	<p class="grey-text input-top-p">Min. No. of People</p>
        <select class="three-select clean">
        	<option>5</option>
            <option>6</option>
            <option>7</option> 
            <option>8</option> 
            <option>9</option>
            <option>10</option>
            <option>12</option> 
            <option>15</option>             
        </select>
	</div>
	<div class="edit-half-div second-edit-half-div">
    	<p class="grey-text input-top-p">Discount (%)</p>
        <input class="three-select clean" type="number">
	</div>    
    <div class="clear"></div>
    <div class="width100 overflow">
            <div class="upload-btn-wrapper">
              <button class="upload-btn">Upload Image</button>
              <input class="hidden-input" type="file" name="myfile" />
            </div>
            <!-- Crop the image 16:9 -->
            <p class="img-preview">Image Preview</p> 
            <div class="left-img-preview"><img src="img/promotion.jpg" class="uploaded-img"></div><span class="right-remove-span hover-effect">Remove</span>           
    </div> 
    <div class="clear"></div> 
	<div class="divider"></div>
    <div class="width100 overflow">
        <div class="fillup-2-btn-space"></div>
        <button class="clean red-btn text-center payment-button clean next-btn view-plan-btn print-btn open-confirm"   >Delete</button>
        <button class="payment-button clean next-btn view-plan-btn blue-btn">Save</button>
        <div class="fillup-2-btn-space"></div>
    </div> 
    <div class="clear"></div>
    <div class="fillup-extra-space2"></div><a  onclick="goBack()" class="cancel-a hover-effect">Cancel</a>
</div>


<?php include 'js.php'; ?>
</body>
</html>