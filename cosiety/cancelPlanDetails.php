<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Cancel Plan | Cosiety" />
<title>Cancel Plan | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
            <h1 class="receipt-title-h1">Receipt</h1> <span class="logo-span"><img src="img/cosiety-logo.png" class="logo-img2" alt="Cosiety" title="Cosiety"></span>
            <div class="clear"></div>
            <div class="address-div">
            	<p class="receipt-address-p"><b class="black-text">Cosiety Sdn. Bhd.</b><br>No, Street,<br>Town, Postcode,<br>City, State,<br>Country</p>
            </div>
            <div class="clear"></div>
            <div class="receipt-half-div">
            	<h2 class="receipt-subtitle-h2 black-text">ISSUED TO</h2>
            </div>
            <div class="receipt-half-div second-receipt-half-div">
            	<h2 class="receipt-subtitle-h2 align-p-h2 black-text">Receipt No.</h2>
                <p class="align-h2-p">112003</p>
            </div>            
            <div class="clear"></div>
            <div class="receipt-half-div">
            	<p class="receipt-address-p no-margin-top"><b class="black-text">Company Name</b><br>No, Street,<br>Town, Postcode,<br>City, State,<br>Country</p>
            </div>            
            <div class="receipt-half-div second-receipt-half-div">
            	<h2 class="receipt-subtitle-h2 align-p-h2 no-margin-top black-text">Issue Date</h2>
                <p class="align-h2-p no-margin-top">12/8/2019</p>
            </div>  
            <div class="clear"></div>            
            <div class="width100 receipt-border"></div>             
            <div class="clear"></div>
            <div class="receipt-half-div">
            	<p class="receipt-upper-p">Plan<br>
                <b class="receipt-lower-p">Co-Working Space (Hot Seat)</b></p>
            </div>            
            <div class="receipt-half-div second-receipt-half-div">
            	<p class="receipt-upper-p">Duration<br>
                <b class="receipt-lower-p">1 Month</b></p>
            </div> 
            <div class="clear"></div>  
            <div class="receipt-half-div">
            	<p class="receipt-upper-p">Start Date<br>
                <b class="receipt-lower-p">1/9/2019</b></p>
            </div>            
            <div class="receipt-half-div second-receipt-half-div">
            	<p class="receipt-upper-p">End Date<br>
                <b class="receipt-lower-p">1/10/2019</b></p>
            </div> 
            <div class="clear"></div> 
            <div class="receipt-half-div">
            	<p class="receipt-upper-p">Reserved Working Space<br>
                <b class="receipt-lower-p">5</b></p>
            </div>            
            <div class="clear"></div>
            <div class="receipt-half-div">
            	<p class="receipt-upper-p">No.1<br>
                <b class="receipt-lower-p">Alice Tang</b></p>
            </div>            
            <div class="receipt-half-div second-receipt-half-div">
            	<p class="receipt-upper-p">No.2<br>
                <b class="receipt-lower-p">Jenny Lim</b></p>
            </div> 
            <div class="clear"></div>    
            <div class="receipt-half-div">
            	<p class="receipt-upper-p">No.3<br>
                <b class="receipt-lower-p">Janice Lim</b></p>
            </div>            
            <div class="receipt-half-div second-receipt-half-div">
            	<p class="receipt-upper-p">No.4<br>
                <b class="receipt-lower-p">Bonnie Lam</b></p>
            </div> 
            <div class="clear"></div>
            <div class="receipt-half-div">
            	<p class="receipt-upper-p">No.5<br>
                <b class="receipt-lower-p">Jack Sim</b></p>
            </div>             
            <div class="clear"></div> 
            <div class="width100 receipt-border"></div>               
            <div class="overflow width100 total-container">
            	<div class="receipt-left-total">Payment Method</div>
                <div class="receipt-right-total">iPay88</div>
            </div>
            <div class="clear"></div>            
            <div class="overflow width100 total-container">
            	<div class="receipt-left-total">Subtotal</div>
                <div class="receipt-right-total">RM1995.00</div>
            </div>
            <div class="clear"></div>             
            <div class="overflow width100 total-container">
            	<div class="receipt-left-total">Discount</div>
                <div class="receipt-right-total slight-left">-</div>
            </div>
            <div class="clear"></div> 
            <div class="overflow width100 total-container padding-bottom-0">
            	<div class="receipt-left-total bigger-font">Total</div>
                <div class="receipt-right-total bigger-font">RM1995.00</div>
            </div>
            <div class="clear"></div> 
            <div class="width100 receipt-border"></div>             
            <div class="clear"></div>                                                                                                                    
            <div class="small-divider"></div>
            <div class="width100 overflow">
                <p class="grey-text input-top-p">Reason of Cancellation</p>
                <input class="three-select clean" placeholder="Type the reason here" type="text">
            </div>
            <div class="divider"></div>
            <div class="clear"></div>
            <div class="width100 overflow">
            	<div class="fillup-2-btn-space"></div>
                <button class="clean print-btn" >Cancel Plan</button>
            	<a href="refund.php"><button class="blue-btn payment-button clean next-btn view-plan-btn">Refund</button></a>
                <div class="fillup-2-btn-space"></div>
            </div>
            <div class="clear"></div>
            <div class="small-divider"></div>
    		<div class="fillup-extra-space2"></div><a  onclick="goBack()" class="cancel-a hover-effect">Cancel</a>

</div>


<?php include 'js.php'; ?>
</body>
</html>