<div class="landing-first-div">
    <!-- Start Menu -->
    <header id="header" class="header header--fixed same-padding header1 menu-white" role="banner">
        <div class="big-container-size hidden-padding">
            <div class="left-logo-div float-left hidden-logo-padding">
                <a href="index.php" class="hover-effect"><img src="img/cosiety-logo.png" class="logo-img" alt="Cosiety" title="Cosiety"></a>
            </div>
            
            <div class="right-menu-div float-right" id="top-menu">
            	
                <a href="dashboard.php" class="menu-padding">Dashboard</a>
                <a href="booking.php" class="menu-padding">Booking</a>
                <a href="helpdesk.php" class="menu-padding">Helpdesk</a>
                <a href="membership.php" class="menu-padding">Membership</a>
                <div class="dropdown dropdown-1">
                  <span><img src="img/big-profile.png" class="menu-profile" alt="Profile" title="Profile"> <i class="fa fa-caret-down"></i></span>
                  <div class="dropdown-content">
                  	<a href="profile.php">Profile</a>
                    <a href="editPassword.php">Edit Password</a>
                    <a href="index.php">Logout</a>
                  </div>
                </div>
                
                <a class="menu-icon openmenu"><img src="img/menu.png" class="menu-img" alt="Menu" title="menu"></a>

            </div>
        </div>
    
    </header>

</div>


<!-- The Modal -->
<div id="menumodal" class="modal modal-css">

  <!-- Modal content -->
  <p class="close-menu-p"><span class="closemenu close-css">&times;</span></p>
  <div class="clear"></div>
  <div class="modal-content modal-content-css">
  				<h2 class="mobile-menu-h2">Menu</h2>
                <a href="dashboard.php" class="mobile-menu-a closemenu">Dashboard</a>
                <a href="booking.php" class="mobile-menu-a closemenu">Booking</a>
                <a href="membership.php" class="mobile-menu-a closemenu">Membership</a>
                <a href="profile.php"  class="mobile-menu-a">Profile</a>
                <a href="editPassword.php"  class="mobile-menu-a">Edit Password</a>
                <a href="index.php"  class="mobile-menu-a">Logout</a>    
    			<a class="mobile-menu-a closemenu last-menu-a">Close</a>  
  </div>

</div>