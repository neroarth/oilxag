<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Dashboard | Cosiety" />
<title>Dashboard | Cosiety</title>
<meta property="og:description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="description" content="Affordable serviced offices, hot desks, and meeting rooms with scenic sea-view. Vibrant co-working office space located in Penang's first seafront retail marina, Straits Quay." />
<meta name="keywords" content="cosiety, coworking space, penang, malaysia, pulau pinang,  etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'adminHeader.php'; ?>

<div class="grey-bg menu-distance2 same-padding overflow">
	<h1 class="backend-title-h1 align-select-h1">Manage</h1>
    <select class="clean align-h1-select">
    	<option>Today</option>
        <option>Last 7 days</option>
        <option>Last 14 days</option>
        <option>Last 28 days</option>
    </select>
    <div class="clear"></div>
 	<div class="small-divider width100"></div>
    <div class="clear"></div>
	<a href="unsolvedIssue.php">
        <div class="four-white-div">
            <div class="color-circle color-circle1">
                <img src="img/helpdesk-issue.png" class="circle-icon" alt="Unsolved Issues"  title="Unsolved Issues">
            </div>
            <p class="black-text white-div-title">Unsolved Issues</p>
            <p class="white-div-number">2</p>
        </div>
    </a>
	<a href="outstandingPlan.php">
        <div class="four-white-div second-white-div third-two four-two">
            <div class="color-circle color-circle2">
                <img src="img/plan-bill.png" class="circle-icon" alt="Outstanding Plan"  title="Outstanding Plan">
            </div>
            <p class="black-text white-div-title">Outstanding Plan</p>
            <p class="white-div-number">2</p>
        </div>
    </a>   
	<a href="paidPlan.php">
        <div class="four-white-div third-white-div">
            <div class="color-circle color-circle3">
                <img src="img/paid.png" class="circle-icon" alt="New Paid Plan"  title="New Paid Plan">
            </div>
            <p class="black-text white-div-title">New Paid Plan</p>
            <p class="white-div-number">2</p>
        </div>
    </a>   
	<a href="revenue.php">
        <div class="four-white-div four-two">
            <div class="color-circle color-circle4">
                <img src="img/revenue.png" class="circle-icon" alt="Revenue (RM)"  title="Revenue (RM)">
            </div>
            <p class="black-text white-div-title">Revenue (RM)</p>
            <p class="white-div-number">2000.00</p>
        </div>
    </a>  
	<a href="member.php">
        <div class="four-white-div third-two">
            <div class="color-circle color-circle5">
                <img src="img/new-member.png" class="circle-icon" alt="New Member"  title="New Member">
            </div>
            <p class="black-text white-div-title">New Member</p>
            <p class="white-div-number">2</p>
        </div>
    </a>     
	<a href="companyList.php">
        <div class="four-white-div second-white-div four-two">
            <div class="color-circle color-circle6">
                <img src="img/company.png" class="circle-icon" alt="New Company"  title="New Company">
            </div>
            <p class="black-text white-div-title">New Company</p>
            <p class="white-div-number">2</p>
        </div>
    </a>  
	<a href="emptyRoom.php">
        <div class="four-white-div third-white-div">
            <div class="color-circle color-circle7">
                <img src="img/empty-room.png" class="circle-icon" alt="Empty Space/Room (Today)"  title="Empty Space/Room (Today)">
            </div>
            <p class="black-text white-div-title">Empty Space/Room (Today)</p>
            <p class="white-div-number">2</p>
        </div>
    </a>           
</div>


<?php include 'js.php'; ?>
</body>
</html>