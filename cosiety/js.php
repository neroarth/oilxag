<div class="clear"></div>
<div class="footer-div white-text same-padding">
	© 2019 Cosiety, All Rights Reserved.
</div>




<!-- The Modal -->
<div id="menumodal" class="modal modal-css">

    <!-- Modal content -->
    <p class="close-menu-p"><span class="closemenu close-css">&times;</span></p>
    <div class="clear"></div>
    <div class="modal-content modal-content-css">
        <h2 class="mobile-menu-h2">Menu</h2>
        <a href="index.php#about" class="mobile-menu-a closemenu">About</a>
        <a href="index.php#plan" class="mobile-menu-a closemenu">Plan & Price</a>
        <a href="index.php#booking" class="mobile-menu-a closemenu">Booking</a>
        <a class="mobile-menu-a open-signup">Sign Up</a>
        <a class="mobile-menu-a open-login">Login</a>
        <a class="mobile-menu-a closemenu last-menu-a">Close</a>
    </div>

</div>


<!-- The Sign Up Modal -->
<div id="signupmodal" class="modal modal-css">

    <!-- Modal content -->
    <p class="close-menu-p signup-menu-p"><span class="closesignup close-css">&times;</span></p>
    <div class="clear"></div>
    <div class="modal-content modal-content-css login-modal-css signup-modal-css">
        <h2 class="mobile-menu-h2">Sign Up</h2>
        <input class="clean signup-input" type="text" placeholder="Name">
        <input class="clean signup-input" type="email" placeholder="Email">
        <select class="clean signup-select signup-input">
        	<option>Malaysia</option>
            <option>Singapore</option>
        </select>
        <input class="clean signup-input contact-input" type="tel" placeholder="Contact">
        <span class="country-code">+60</span>
        <a href="dashboard.php"><button class="confirm-btn black-btn">Sign Up</button></a>
        <p class="signup-p">Already Have an Account? <a class="open-login loginhere-a">Login Here.</a></p>
    </div>

</div>

<!-- The Login Modal -->
<div id="loginmodal" class="modal modal-css">

    <!-- Modal content -->
    <p class="close-menu-p close-login-p"><span class="closelogin close-css closelogin-css">&times;</span></p>
    <div class="clear"></div>
    <div class="modal-content modal-content-css login-modal-css">
        <h2 class="mobile-menu-h2">Login</h2>
        <input class="clean signup-input" type="email" placeholder="Email">
        <input class="clean signup-input password-input" type="password" placeholder="Password"><span class="visible-icon"><img src="img/visible.png" class="visible-img hover-effect" alt="View Password" title="View Password"></span>
        <label class="container1 remember-me-label">Remember Me
          <input type="checkbox" checked="checked">
          <span class="checkmark1  remember-me-checkmark"></span>
        </label>
        <a href="dashboard.php"><button class="confirm-btn black-btn">Login</button></a>
        <p class="signup-p">Don't have an account yet? <a class="open-signup loginhere-a">Sign up Here.</a></p>
        <p class="signup-p"><a class="open-forgot loginhere-a">Forgot Password?</a></p>
    </div>

</div>


<!-- The Forgot Password Modal -->
<div id="forgotmodal" class="modal modal-css">

    <!-- Modal content -->
    <p class="close-menu-p close-login-p"><span class="closeforgot close-css closelogin-css">&times;</span></p>
    <div class="clear"></div>
    <div class="modal-content modal-content-css login-modal-css">
        <h2 class="mobile-menu-h2">Forgot Password</h2>
        <p class="signup-p">We will send a reset password link to your email.</p>
        <input class="clean signup-input" type="email" placeholder="Email">
        <button class="confirm-btn black-btn">Send</button>
        <p class="signup-p"><a class="open-login loginhere-a">Try login again?</a></p>
        
    </div>

</div>

<!-- The Confirm Modal -->
<div id="confirmmodal" class="modal modal-css">

    <!-- Modal content -->
    <p class="close-menu-p close-login-p"><span class="closeconfirm close-css closelogin-css">&times;</span></p>
    <div class="clear"></div>
    <div class="modal-content modal-content-css login-modal-css">
        <h2 class="mobile-menu-h2">Confirm Delete?</h2>       
        <button class="clean red-btn2">Delete</button>
        <div class="clear"></div>
        <button class="cancel-btn closeconfirm">Cancel</button>  
    </div>

</div>


<script src="js/jquery-3.2.0.min.js" type="text/javascript"></script> 
<script src="js/bootstrap.min.js" type="text/javascript"></script>  
<script src="js/jquery.fancybox.js" type="text/javascript"></script>  
<script src="js/headroom.js"></script>
<script>
function printReceipt() {
  window.print();
}
</script>
<script>
function goBack() {
  window.history.back();
}
</script>
<script>
    $(window).load(function(){
       // PAGE IS FULLY LOADED  
       // FADE OUT YOUR OVERLAYING DIV
       $('#overlay').fadeOut();
    });
</script>
<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})

</script>
<script>
    ;(function(){
      function id(v){return document.getElementById(v); }
      function loadbar() {
        var ovrl = id("overlay"),
            prog = id("progress"),
            stat = id("progstat"),
            img = document.images,
            c = 0;
            tot = img.length;
    
        function imgLoaded(){
          c += 1;
          var perc = ((100/tot*c) << 0) +"%";
          prog.style.width = perc;
          stat.innerHTML = "Loading "+ perc;
          if(c===tot) return doneLoading();
        }
        function doneLoading(){
          ovrl.style.opacity = 0;
          setTimeout(function(){ 
            ovrl.style.display = "none";
          }, 1200);
        }
        for(var i=0; i<tot; i++) {
          var tImg     = new Image();
          tImg.onload  = imgLoaded;
          tImg.onerror = imgLoaded;
          tImg.src     = img[i].src;
        }    
      }
      document.addEventListener('DOMContentLoaded', loadbar, false);
    }());
</script>
<script>
    (function() {
        var header = new Headroom(document.querySelector("#header"), {
            tolerance: 5,
            offset : 205,
            classes: {
              initial: "animated",
              pinned: "slideDown",
              unpinned: "slideUp"
            }
        });
        header.init();
    
    }());
</script>
 
	<script>
    // Cache selectors
    var lastId,
        topMenu = $("#top-menu"),
        topMenuHeight = topMenu.outerHeight(),
        // All list items
        menuItems = topMenu.find("a"),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function(){
          var item = $($(this).attr("href"));
          if (item.length) { return item; }
        });
    
    // Bind click handler to menu items
    // so we can get a fancy scroll animation
    menuItems.click(function(e){
      var href = $(this).attr("href"),
          offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
      $('html, body').stop().animate({ 
          scrollTop: offsetTop
      }, 500);
      e.preventDefault();
    });
    
    // Bind to scroll
    $(window).scroll(function(){
       // Get container scroll position
       var fromTop = $(this).scrollTop()+topMenuHeight;
       
       // Get id of current scroll item
       var cur = scrollItems.map(function(){
         if ($(this).offset().top < fromTop)
           return this;
       });
       // Get the id of the current element
       cur = cur[cur.length-1];
       var id = cur && cur.length ? cur[0].id : "";
                      
    });
    </script>
    
   
<script>
var menumodal = document.getElementById("menumodal");
var signupmodal = document.getElementById("signupmodal");
var loginmodal = document.getElementById("loginmodal");
var forgotmodal = document.getElementById("forgotmodal");
var confirmmodal = document.getElementById("confirmmodal");

var openmenu = document.getElementsByClassName("openmenu")[0];
var opensignup = document.getElementsByClassName("open-signup")[0];
var opensignup1 = document.getElementsByClassName("open-signup")[1];
var opensignup2 = document.getElementsByClassName("open-signup")[2];
var opensignup3 = document.getElementsByClassName("open-signup")[3];
var openlogin = document.getElementsByClassName("open-login")[0];
var openlogin1 = document.getElementsByClassName("open-login")[1];
var openlogin2 = document.getElementsByClassName("open-login")[2];
var openlogin3 = document.getElementsByClassName("open-login")[3];
var openlogin4 = document.getElementsByClassName("open-login")[4];
var openforgot = document.getElementsByClassName("open-forgot")[0];
var openconfirm = document.getElementsByClassName("open-confirm")[0];

var closemenu = document.getElementsByClassName("closemenu")[0];
var closemenu1 = document.getElementsByClassName("closemenu")[1];
var closemenu2 = document.getElementsByClassName("closemenu")[2];
var closemenu3 = document.getElementsByClassName("closemenu")[3];
var closemenu4 = document.getElementsByClassName("closemenu")[4];
var closesignup = document.getElementsByClassName("closesignup")[0];
var closelogin = document.getElementsByClassName("closelogin")[0];
var closeforgot = document.getElementsByClassName("closeforgot")[0];
var closeconfirm = document.getElementsByClassName("closeconfirm")[0];
var closeconfirm1 = document.getElementsByClassName("closeconfirm")[1];

if(openmenu){
openmenu.onclick = function() {
  menumodal.style.display = "block";
}
}
if(opensignup){
opensignup.onclick = function() {
 signupmodal.style.display = "block";
}
}
if(opensignup1){
opensignup1.onclick = function() {
 signupmodal.style.display = "block";
}
}
if(opensignup2){
opensignup2.onclick = function() {
 signupmodal.style.display = "block";
}
}
if(opensignup3){
opensignup3.onclick = function() {
 loginmodal.style.display = "none";	
 signupmodal.style.display = "block";
}
}
if(openlogin){
openlogin.onclick = function() {
 loginmodal.style.display = "block";
}
}
if(openlogin1){
openlogin1.onclick = function() {
 loginmodal.style.display = "block";
}
}
if(openlogin2){
openlogin2.onclick = function() {
 loginmodal.style.display = "block";
}
}
if(openlogin3){
openlogin3.onclick = function() {
 signupmodal.style.display = "none";	
 loginmodal.style.display = "block";
}
}
if(openlogin4){
openlogin4.onclick = function() {
 forgotmodal.style.display = "none";	
 loginmodal.style.display = "block";
}
}
if(openforgot){
openforgot.onclick = function() {
 loginmodal.style.display = "none";	
 forgotmodal.style.display = "block";
}
}
if(openconfirm){
openconfirm.onclick = function() {
 confirmmodal.style.display = "block";	
}
}


if(closemenu){
closemenu.onclick = function() {
  menumodal.style.display = "none";
}
}
if(closemenu1){
closemenu1.onclick = function() {
  menumodal.style.display = "none";
}
}
if(closemenu2){
closemenu2.onclick = function() {
  menumodal.style.display = "none";
}
}
if(closemenu3){
closemenu3.onclick = function() {
  menumodal.style.display = "none";
}
}
if(closemenu4){
closemenu4.onclick = function() {
  menumodal.style.display = "none";
}
}
if(closesignup){
closesignup.onclick = function() {
  signupmodal.style.display = "none";
}
}
if(closelogin){
closelogin.onclick = function() {
  loginmodal.style.display = "none";
}
}
if(closeforgot){
closeforgot.onclick = function() {
  forgotmodal.style.display = "none";
}
}
if(closeconfirm){
closeconfirm.onclick = function() {
  confirmmodal.style.display = "none";
}
}
if(closeconfirm1){
closeconfirm1.onclick = function() {
  confirmmodal.style.display = "none";
}
}

window.onclick = function(event) {
  if (event.target == modal) {
    menumodal.style.display = "none";
  }
  if (event.target == modal) {
    signupmodal.style.display = "none";
  }
  if (event.target == modal) {
    loginmodal.style.display = "none";
  }  
  if (event.target == modal) {
    forgotmodal.style.display = "none";
  }      
  if (event.target == modal) {
    confirmmodal.style.display = "none";
  }     
}
</script>
    <script src="js/jssor.slider.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        jssor_1_slider_init = function() {

            var jssor_1_options = {
              $AutoPlay: 1,
              $SlideDuration: 800,
              $SlideEasing: $Jease$.$OutQuint,
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 3000;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>
    <script type="text/javascript">jssor_1_slider_init();</script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js'></script>
    <script src="js/calendar.js"></script>