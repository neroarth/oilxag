<?php
  

require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php'; 
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    //todo create table for transaction_history with at least a column for quantity(in order table),product_id(in order table), order_id, status (others can refer to btcw's), target_uid, trigger_transaction_id
    //todo create table for order and product_order
    $totalProductCount = count($_POST['product-list-id-input']);
    for($i = 0; $i < $totalProductCount; $i++){
        $productId = $_POST['product-list-id-input'][$i];
        $quantity = $_POST['product-list-quantity-input'][$i];

        echo " this: $productId total: $quantity";
    }
}

$products = getProduct($conn);

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/adminWithdrawal.php" />
    <meta property="og:title" content="Withdrawal Request | DCK Supreme" />
    <title>Withdrawal Request | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/adminWithdrawal.php" />
    <?php include 'css.php'; ?>    
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<!--<form method="POST">-->
    <?php
//    if(isset($_POST['product-list-quantity-input'])){
//        createProductList($products,$_POST['product-list-quantity-input']);
//    }else{
//        createProductList($products);
//    }

    ?>
<!--    <button type="submit" name="addToCartButton" id="addToCartButton" >Add to cart</button>-->
<!--</form>-->
<div class="yellow-body padding-from-menu same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Withdrawal Request | <a href="adminWithdrawalComp.php" class="white-text title-tab-a">Completed</a></h1>
    <!-- This is a filter for the table result -->
    <select class="filter-select clean">
    	<option class="filter-option">Oldest</option>
        <option class="filter-option">Latest</option>
    </select>
    
    <!-- End of Filter -->
    <div class="clear"></div>
	<div class="search-container0">

            <div class="shipping-input clean smaller-text2">
                <p>Withdrawal Number</p>
                <input class="shipping-input2 clean normal-input" type="number" placeholder="Withdrawal Number">
            </div>
            <div class="shipping-input clean smaller-text2 middle-shipping-div second-shipping">
                <p>Username</p>
                <input class="shipping-input2 clean normal-input" type="text" placeholder="Username">
            </div>
            <div class="shipping-input clean smaller-text2">
                <p>Start Date</p>
                <input class="shipping-input2 clean" type="date" placeholder="Start Date">
            </div>

     
            <div class="shipping-input clean smaller-text2 second-shipping">
                <p>End Date</p>
                <input class="shipping-input2 clean" type="date" placeholder="End Date">
            </div>
            <div class="shipping-input clean smaller-text2 middle-shipping-div">
                <p>Amount Range</p>
                <select class="shipping-input2 shipping-select clean">
                    <option class="shipping-option clean">< RM500.00</option>
                    <option class="shipping-option clean">< RM2,000.00 </option>
                    <option class="shipping-option clean">< RM5,000.00</option>
                    <option class="shipping-option clean">> RM5,000.00</option>
                </select>            
            </div>
            <button class="clean black-button shipping-search-btn second-shipping">Search</button>

    </div>    

    <div class="clear"></div>

    <div class="width100 shipping-div2">
    	<div class="overflow-scroll-div">
            <table class="shipping-table">
                <thead>
                    <tr>
                        <th>NO.</th>
                        <th>WITH. NUMBER</th>
                        <th>USERNAME</th>
                        <th>REQUEST DATE</th>
                        <th>CONTACT</th>
                        <th>BANK</th>
                        <th>AMOUNT (RM)</th>
                    </tr>
                </thead>
                <tr data-url="withdrawalReq.php" class="link-to-details">
                    <td>1.</td>
                    <td>#180201</td>
                    <td>Felicia</td>
                    <td>2019-08-15</td>
                    <td>012345678</td>
                    <td>MAYBANK</td>
                    <td>1000.00</td>
                </tr>
                <tr data-url="withdrawalReq.php" class="link-to-details">
                    <td>2.</td>
                    <td>#180202</td>
                    <td>Alicia</td>
                    <td>2019-08-15</td>
                    <td>012345671</td>
                    <td>MAYBANK</td>
                    <td>1000.00</td>
                </tr>            
            </table>
        </div>
    </div>
    <div class="clear"></div>
    <div class="bottom-big-container">
    	<div class="left-btm-page">
        	Page <select class="clean transparent-select"><option>1</option></select> of 1
        </div>
        <div class="middle-btm-page">
        	<a class="round-black-page">1</a>
            <a class="round-white-page">2</a>
        </div>
        <div class="right-btm-page">
        	Total: 2
        </div>
    </div>    
</div>


<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>
<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})

</script>
</body>
</html>