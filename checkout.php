<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/classes/User.php';
//chheckout
require_once dirname(__FILE__) . '/classes/Checkout.php';

require_once dirname(__FILE__) . '/classes/GroupCommission.php';
require_once dirname(__FILE__) . '/classes/TransactionHistory.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    addToCart();
    createOrder($conn,$uid);
    // header('Location: ../oilxag/checkout.php');

    $username = rewrite($_POST["insert_username"]);

    $bankName = rewrite($_POST["insert_bankname"]);
    $bankAccountHolder = rewrite($_POST["insert_bankaccountholder"]);
    $bankAccountNo = rewrite($_POST["insert_bankaccountnumber"]);

    $name = rewrite($_POST["insert_name"]);
    $contactNo = rewrite($_POST["insert_contactNo"]);
    $email = rewrite($_POST["insert_email"]);
    $address_1 = rewrite($_POST["insert_address_1"]);
    $address_2 = rewrite($_POST["insert_address_2"]);
    $city = rewrite($_POST["insert_city"]);
    $zipcode = rewrite($_POST["insert_zipcode"]);
    $state = rewrite($_POST["insert_state"]);
    $country = rewrite($_POST["insert_country"]);
}

if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
    $productListHtml = getShoppingCart($conn,2);
}else
{}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://dcksupreme.asia/information.php" />
<meta property="og:title" content="Information | DCK Supreme" />
<title>Information | DCK Supreme</title>
<meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
<meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
<meta name="keywords" content="DCK®,dck, dck supreme, supreme, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
noisiness and temperature, dry cold start,etc">
<link rel="canonical" href="https://dcksupreme.asia/information.php" />
<?php include 'css.php'; ?>
<?php require_once dirname(__FILE__) . '/header.php'; ?>
</head>

<body class="body">
<!--
<div id="overlay">
<div class="center-food"><img src="img/loading-gif.gif" class="food-gif"></div>
<div id="progstat"></div>
<div id="progress"></div>
</div>-->

<!-- Start Menu -->
<?php include 'header-sherry.php'; ?>
      
<div class="flex-container">    
    <div class="left-status-div">
    	<!-- Start of Check Out Status Div-->
        <div class="check-out-status">
            <div class="status-container">
                <a href="viewCart.php" class="status-a">
                    <div class="black-round-div"><img src="img/tick2.png" class="yellow-tick" alt="Completed" title="Completed"></div>
                    CART
                </a>
            </div>
            <div class="status-div">></div>
            <div class="status-container">        
                <div class="white-round-div">2</div>
                INFORMATION
            </div>
            <div class="status-div">></div>
            <div class="status-container">        
                <div class="white-round-div">3</div>
                SHIPPING
            </div>        
            <div class="status-div">></div>
            <div class="status-container">        
                <div class="white-round-div">4</div>
                PAYMENT
            </div>                
        </div>
        <!-- End of Check Out Status Div-->
        <div class="clear"></div>
    <form method="POST"  action="utilities/orderInformation.php">
        <p class="info-title"><b>CONTACT INFORMATION</b></p>
        <input class="clean white-input two-box-input" type="hidden" id="insert_username" name="insert_username" value="<?php echo $userDetails->getUsername();?>">
        <input class="clean white-input two-box-input" type="hidden" id="insert_bankname" name="insert_bankname" value="<?php echo $userDetails->getBankName();?>">
        <input class="clean white-input two-box-input" type="hidden" id="insert_bankaccountholder" name="insert_bankaccountholder" value="<?php echo $userDetails->getBankAccountHolder();?>">
        <input class="clean white-input two-box-input" type="hidden" id="insert_bankaccountnumber" name="insert_bankaccountnumber" value="<?php echo $userDetails->getBankAccountNo();?>">
        <input class="clean white-input two-box-input" type="text" id="insert_name" name="insert_name" placeholder="Name">
        <input class="clean white-input two-box-input" type="number" id="insert_contactNo" name="insert_contactNo"  placeholder="Contact Number"> 
        <input class="clean white-input two-box-input" type="email" id="insert_email" name="insert_email" placeholder="Email">
 
        <p class="info-title spacing2"><b>SHIPPING ADDRESS</b></p>      
        <!-- <input class="clean white-input half-white-input left-half two-box-input-double" type="text" placeholder="First Name">  
        <input class="clean white-input half-white-input two-box-input-double right-part" type="text" placeholder="Last Name"> -->

        
                
            <input class="clean white-input two-box-input" type="text" id="insert_address_1" name="insert_address_1" placeholder="Address Line 1">
            <input class="clean white-input two-box-input" type="text" id="insert_address_2" name="insert_address_2" placeholder="Address Line 2">
            <input class="clean white-input half-white-input left-half two-box-input-double" type="text" id="insert_city" name="insert_city" placeholder="City">  
            <input class="clean white-input half-white-input two-box-input-double right-part" type="number" id="insert_zipcode" name="insert_zipcode" placeholder="Postcode"> 
            
            <!-- <select class="clean white-input white-select half-white-input left-half two-box-input-double"> -->
            
            <!-- <select class="clean white-input two-box-input">
                <option class="white-option">State</option>
            </select>   -->
            
            <input class="clean white-input two-box-input" type="text" id="insert_state" name="insert_state" placeholder="State">

            <!-- <select class="clean white-input two-box-input">
                <option class="white-option">Country</option>
            </select> -->

            <input class="clean white-input two-box-input" type="text" id="insert_country" name="insert_country" placeholder="Country">

            <!-- <select class="clean white-input white-select half-white-input left-half two-box-input-double"> -->
                    
        <div class="clear"></div>
        <div class="cart-bottom-div spacing2">
            <div class="left-cart-bottom-div">
                <p class="continue-shopping pointer continue2"><a href="viewCart.php" class="black-white-link"><img src="img/back.png" class="back-btn" alt="back" title="back" > Return to Cart</a></p>
                <!-- <p class="continue-shopping pointer continue2" ><a href="cart.php" class="black-white-link"><img src="img/back.png" class="back-btn back-btn2" alt="back" title="back" > Return to Cart</a></p> -->
            </div>
            <div class="right-cart-div">
            <button class="clean black-button add-to-cart-btn checkout-btn">Continue To Shipping</button>
                <!-- <button class="clean black-button add-to-cart-btn checkout-btn continue2 add-to-cart-btn2">NEXT</button> -->
            </div>
        </div>
    </form>
    </div>

    
    
    
    <div class="right-status-div">   
        
    <!-- <table class="cart-table">
        <th class="product-name-td white-text">Product</th>
        <th></th>
        <th class="product-name-td white-text">Quantity</th>
        <th class="product-name-td white-text">Price</th>
    </table> -->

    <?php echo $productListHtml; ?>
    </div>	
</div>    


<?php include 'js.php'; ?>


<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Please Fill Up The Required Details !";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "Fail To Make Order !";
        }
        // if($_GET['type'] == 3)
        // {
        //     $messageType = "Data Update Successfully.";
        // }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>