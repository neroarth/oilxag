<?php
class GroupCommission {
    /* Member variables */
    var $id,$commission,$level,$count,$description,$transactionTypeId,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCommission()
    {
        return $this->commission;
    }

    /**
     * @param mixed $commission
     */
    public function setCommission($commission)
    {
        $this->commission = $commission;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param mixed $level
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param mixed $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getTransactionTypeId()
    {
        return $this->transactionTypeId;
    }

    /**
     * @param mixed $transactionTypeId
     */
    public function setTransactionTypeId($transactionTypeId)
    {
        $this->transactionTypeId = $transactionTypeId;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getGroupCommission($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","commission","level","count","description","transaction_type_id","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"group_commission");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$commission,$level,$count,$description,$transactionTypeId,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new GroupCommission();
            $class->setId($id);
            $class->setCommission($commission);
            $class->setLevel($level);
            $class->setCount($count);
            $class->setDescription($description);
            $class->setTransactionTypeId($transactionTypeId);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}

function splitCommissionToRealAndVoucher($commission){
    $splitCommission = array();

    $splitCommission['real-percent'] = 70;
    $splitCommission['voucher-percent'] = 30;

    $splitCommission['real'] = $commission * $splitCommission['real-percent'] / 100;
    $splitCommission['voucher'] = $commission * $splitCommission['voucher-percent'] / 100;

    return $splitCommission;
}

function rewardDirectUplineCommission($conn,$orderId,$downlineUid){
    //this function only rewards the user's direct upline and is 1 level only
    $referralRows = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($downlineUid),"s");

    if($referralRows){
        $uplineUid = $referralRows[0]->getReferrerId();

        //check if this downline already rewarded the upline, if already rewarded dont do anything
        $checkExistTHRows = getTransactionHistory($conn," WHERE uid = ? AND target_uid = ?  AND transaction_type_id = 1 AND money_type_id = 1 ",array("uid","target_uid"),array($uplineUid,$downlineUid),"ss");

        if($checkExistTHRows){
            //it exists, means that the upline already gotten the reward, so just exit this function with true status
            return true;
        }

        $rewardReceivedCount = getCount($conn,"transaction_history","*"," WHERE uid = ? AND (status = 1 OR status = 2) AND transaction_type_id = 1 AND money_type_id = 1 ",
            array("uid"),array($uplineUid),"s");
        $rewardReceivedCount++;

        $groupCommission = getGroupCommission($conn," WHERE transaction_type_id = 1 AND (count = ? OR count = 0) ORDER BY count DESC ",array("count"),array($rewardReceivedCount),"i");
        if($groupCommission){
            $thisCommission = $groupCommission[0];
            $reward = $thisCommission->getCommission();

            $splitCommission = splitCommissionToRealAndVoucher($reward);
            $realMoneyReward = $splitCommission['real'];
            $voucherReward = $splitCommission['voucher'];

            $hasError = false;
            //status is pending because the requirement says that it will be awarded at the end of the month
            if(!insertIntoTransactionHistory($conn,$realMoneyReward,0,$uplineUid,$downlineUid,$splitCommission['real-percent'],$reward,1,null,$orderId,1,1,null)){
                $hasError = true;
            }
            if(!insertIntoTransactionHistory($conn,$voucherReward,0,$uplineUid,$downlineUid,$splitCommission['voucher-percent'],$reward,1,null,$orderId,1,2,null)){
                $hasError = true;
            }

            extraRewardAgentTiers($conn,$orderId,$uplineUid,$reward,1);

            return !$hasError;

        }else{
            return false;
        }
    }

    return true;
}

function rewardGroupCommission($conn,$orderId,$downlineUid,$finalPrice){
    $all10ReferrerId = getTop10ReferrerOfUser($conn,$downlineUid);

    $level = 0;
    foreach ($all10ReferrerId as $uplineUid){
        $level++;
        $shouldReward = false;

        $uplineUplineReferralHistory = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineUid),"s");
        if($uplineUplineReferralHistory){
            $topReferrerId = $uplineUplineReferralHistory[0]->getTopReferrerId();
            $prevLevel = $uplineUplineReferralHistory[0]->getCurrentLevel();
            $currentLevel = $prevLevel + 1;

            $prevGenCount = getCount($conn,"referral_history","*"," WHERE top_referrer_id = ? AND current_level = ? ",array("top_referrer_id","current_level"),array($topReferrerId,$prevLevel),"si");
            $currentGenCount = getCount($conn,"referral_history","*"," WHERE top_referrer_id = ? AND current_level = ? ",array("top_referrer_id","current_level"),array($topReferrerId,$currentLevel),"si");

            if($currentGenCount >= $prevGenCount){
                $shouldReward = true;
            }else{
                $shouldReward = false;
            }
        }else{
            //if current referrer is already first level, then don't have to check number of people
            $shouldReward = true;
        }

        if($shouldReward){
            $groupCommissionRows = getGroupCommission($conn," WHERE transaction_type_id = 2 AND level = ? ",array("level"),array($level),"i");

            if($groupCommissionRows){
                $commissionPercent = $groupCommissionRows[0]->getCommission();

                $totalCommissionReceived = $finalPrice * $commissionPercent / 100;
                $splitCommission = splitCommissionToRealAndVoucher($totalCommissionReceived);
                $realMoneyReward = $splitCommission['real'];
                $voucherReward = $splitCommission['voucher'];

                //status is pending because the requirement says that it will be awarded at the end of the month
                //real
                insertIntoTransactionHistory($conn,$realMoneyReward,0,$uplineUid,$downlineUid,$splitCommission['real-percent'],$totalCommissionReceived,1,$level,$orderId,2,1,null);
                //voucher
                insertIntoTransactionHistory($conn,$voucherReward,0,$uplineUid,$downlineUid,$splitCommission['voucher-percent'],$totalCommissionReceived,1,$level,$orderId,2,2,null);

                extraRewardAgentTiers($conn,$orderId,$uplineUid,$totalCommissionReceived,1);
            }
        }else{
            //else it will just jump to the next generation, ignoring the current generation
        }
    }
}

function rewardGroupCommissionTEMP($conn,$orderId,$downlineUid,$finalPrice){
    //use this function first, after they obtained mlm function then use the original 1 (rewardGroupCommission())
    $all10ReferrerId = getTop10ReferrerOfUser($conn,$downlineUid);

    $level = 0;
    foreach ($all10ReferrerId as $uplineUid){
        $level++;

        if($level == 4 || $level == "4" || $level >= 4){
            break;
        }

        $shouldReward = false;

        $uplineUplineReferralHistory = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($uplineUid),"s");
        if($uplineUplineReferralHistory){
            $topReferrerId = $uplineUplineReferralHistory[0]->getTopReferrerId();
            $prevLevel = $uplineUplineReferralHistory[0]->getCurrentLevel();
            $currentLevel = $prevLevel + 1;

            $prevGenCount = getCount($conn,"referral_history","*"," WHERE top_referrer_id = ? AND current_level = ? ",array("top_referrer_id","current_level"),array($topReferrerId,$prevLevel),"si");
            $currentGenCount = getCount($conn,"referral_history","*"," WHERE top_referrer_id = ? AND current_level = ? ",array("top_referrer_id","current_level"),array($topReferrerId,$currentLevel),"si");

            if($currentGenCount >= $prevGenCount){
                $shouldReward = true;
            }else{
                $shouldReward = false;
            }
        }else{
            //if current referrer is already first level, then don't have to check number of people
            $shouldReward = true;
        }

        if($shouldReward){
            $groupCommissionRows = getGroupCommission($conn," WHERE transaction_type_id = 2 AND level = ? ",array("level"),array($level),"i");

            if($groupCommissionRows){
                $commissionPercent = $groupCommissionRows[0]->getCommission();

                $totalCommissionReceived = $commissionPercent;
                $splitCommission = splitCommissionToRealAndVoucher($totalCommissionReceived);
                $realMoneyReward = $splitCommission['real'];
                $voucherReward = $splitCommission['voucher'];

                //status is pending because the requirement says that it will be awarded at the end of the month
                //real
                insertIntoTransactionHistory($conn,$realMoneyReward,0,$uplineUid,$downlineUid,$splitCommission['real-percent'],$totalCommissionReceived,1,$level,$orderId,2,1,null);
                //voucher
                insertIntoTransactionHistory($conn,$voucherReward,0,$uplineUid,$downlineUid,$splitCommission['voucher-percent'],$totalCommissionReceived,1,$level,$orderId,2,2,null);

                extraRewardAgentTiers($conn,$orderId,$uplineUid,$totalCommissionReceived,1);
            }
        }else{
            //else it will just jump to the next generation, ignoring the current generation
        }
    }
}

function extraRewardAgentTiers($conn,$orderId,$downlineUid,$totalCommissionDownlineReceived,$tier = 1){
    //the parameter says is downline and the variable used in $referralHistoryRows is called upline but actually is upline and up-upline, since for agent to get this commission, his downline needs to get income from his downline's downline
    //tier 1 = agent
    //tier 2 = district agent
    //tier 3 = master agent

    if($tier <= 0 || $tier >= 4){
        return;
    }

    $referralHistoryRows = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($downlineUid),"s");

    if($referralHistoryRows){
        $uplineUid = $referralHistoryRows[0]->getReferrerId();
        $totalIntroCount = getCount($conn,"referral_history","*"," WHERE referrer_id = ? ",array("referrer_id"),array($uplineUid),"s");

        $totalCommissionToGiveAgent = 0;

        //important because if less than 0 no need mention already
        if($totalCommissionDownlineReceived <= 0){
            $totalCommissionDownlineReceived = 0;
        }

        if($totalIntroCount >= 10){

            switch ($tier){
                case 1:
                    $totalCommissionToGiveAgent = $totalCommissionDownlineReceived * 5 / 100;
                    break;
                case 2:
                    $totalCommissionToGiveAgent = $totalCommissionDownlineReceived * 5 / 100;
                    break;
                case 3:
                    $totalCommissionToGiveAgent = $totalCommissionDownlineReceived * 5 / 100;
                    break;
                default:
                    $totalCommissionToGiveAgent = 0;
                    break;
            }

            insertIntoTransactionHistory($conn,$totalCommissionToGiveAgent,0,$uplineUid,$downlineUid,5,$totalCommissionDownlineReceived,1,$tier,$orderId,6,1,null);
        }

        $tier++;
        extraRewardAgentTiers($conn,$orderId,$uplineUid,$totalCommissionToGiveAgent,$tier);
    }else{
        return;
    }
}

/*
    WenJie_READ - use the below function to calculate all the commissions and store it into database
    (should be correct already because tested and shown to michael)
    WHEN they able to get their mlm license, REMEMBER to change database value of the group_commission table according to the calculations provided by michael
    only the top 3 level's group sales type (transaction_type_id = 2) is affected
    Then the final step is to change the below function rewardGroupCommissionTEMP to rewardGroupCommission instead.
*/
function initiateReward($conn,$orderId,$uid,$totalPrice){
    if($totalPrice >= 30000){
        if(!rewardDirectUplineCommission($conn,$orderId,$uid)){
            promptError("error rewarding direct upline commission with orderId : $orderId");
        }
        rewardGroupCommissionTEMP($conn,$orderId,$uid,$totalPrice);
    }
}