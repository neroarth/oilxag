<?php
class WithdrawalReq {
    /* Member variables */
    var $uid,$withdrawalNumber,$withdrawalStatus,$withdrawAmount,$dateCreated,$dateUpdated,$withdrawalRequestAmount,$withdrawalMethod,$withdrawalAmount,$withdrawalNote,$username,$bankName,$accNumber,$contact,$point,$owner,$withdrawalReceipt;

    /**
     * @return mixed
     */
    public function getWithdrawalReceipt()
    {
        return $this->receipt;
    }

    /**
     * @param mixed $id
     */
    public function setWithdrawalReceipt($withdrawalReceipt)
    {
        $this->receipt = $withdrawalReceipt;
    }

    /**
     * @return mixed
     */
    public function getOwnerUsername()
    {
        return $this->owner;
    }

    /**
     * @param mixed $id
     */
    public function setOwnerUsername($owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return mixed
     */
    public function getAccNumber()
    {
        return $this->acc_number;
    }

    /**
     * @param mixed $id
     */
    public function setAccNumber($accNumber)
    {
        $this->acc_number = $accNumber;
    }

    /**
     * @return mixed
     */
    public function getPoint()
    {
        return $this->point;
    }

    /**
     * @param mixed $id
     */
    public function setPoint($point)
    {
        $this->point = $point;
    }

    /**
     * @return mixed
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param mixed $id
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $id
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getBankName()
    {
        return $this->bank_name;
    }

    /**
     * @param mixed $id
     */
    public function setBankName($bankName)
    {
        $this->bank_name = $bankName;
    }

    /**
     * @return mixed
     */
    public function getWithdrawalMethod()
    {
        return $this->withdrawal_method;
    }

    /**
     * @param mixed $id
     */
    public function setWithdrawalMethod($withdrawalMethod)
    {
        $this->withdrawal_method = $withdrawalMethod;
    }

    /**
     * @return mixed
     */
    public function getWithdrawalAmount()
    {
        return $this->withdrawal_amount;
    }

    /**
     * @param mixed $id
     */
    public function setWithdrawalAmount($withdrawalAmount)
    {
        $this->withdrawal_amount = $withdrawalAmount;
    }

    /**
     * @return mixed
     */
    public function getWithdrawalNote()
    {
        return $this->withdrawal_note;
    }

    /**
     * @param mixed $id
     */
    public function setWithdrawalNote($withdrawalNote)
    {
        $this->withdrawal_note = $withdrawalNote;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $id
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getWithdrawalNumber()
    {
        return $this->withdrawal_number;
    }

    /**
     * @param mixed
     */
    public function setWithdrawalNumber($withdrawalNumber)
    {
        $this->withdrawal_number = $withdrawalNumber;
    }

    /**
     * @return mixed
     */
    public function getWithdrawRequestAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed
     */
    public function setWithdrawalRequestAmount($withdrawalRequestAmount)
    {
        $this->amount = $withdrawalRequestAmount;
    }

    /**
     * @return mixed
     */
    public function getWithdrawalRequestStatus()
    {
        return $this->withdrawal_status;
    }

    /**
     * @param mixed $uid
     */
    public function setWithdrawalStatus($withdrawalStatus)
    {
        $this->withdrawal_status = $withdrawalStatus;
    }

    /**
     * @return mixed
     */
    public function getWithdrawAmount()
    {
        return $this->final_amount;
    }

    /**
     * @param mixed $epin
     */
    public function setWithdrawAmount($withdrawAmount)
    {
        $this->final_amount = $withdrawAmount;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;
    }



}

function getWithdrawReq($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("uid","withdrawal_number", "withdrawal_status", "final_amount","date_created","date_updated", "amount", "withdrawal_method", "withdrawal_amount", "withdrawal_note", "username", "bank_name", "acc_number","contact","point","owner","receipt");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"withdrawal");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($uid,$withdrawalNumber,$withdrawalStatus,$withdrawAmount,$dateCreated,$dateUpdated,$withdrawalRequestAmount,$withdrawalMethod,$withdrawalAmount,$withdrawalNote,$username,$bankName,$accNumber,$contact,$point,$owner,$withdrawalReceipt);
                    // array("id","withdrawal_number", "withdrawal_status", "final_amount","date_create");

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new WithdrawalReq();
            $class->setUid($uid);
            $class->setWithdrawalNumber($withdrawalNumber);
            $class->setWithdrawalStatus($withdrawalStatus);
            $class->setWithdrawAmount($withdrawAmount);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);
            $class->setwithdrawalRequestAmount($withdrawalRequestAmount);
            $class->setWithdrawalMethod($withdrawalMethod);
            $class->setWithdrawalAmount($withdrawalAmount);
            $class->setWithdrawalNote($withdrawalNote);
            $class->setUsername($username);
            $class->setBankName($bankName);
            $class->setAccNumber($accNumber);
            $class->setContact($contact);
            $class->setPoint($point);
            $class->setOwnerUsername($owner);
            $class->setWithdrawalReceipt($withdrawalReceipt);
            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }

}

            //todo this 2 code is AFTER payment successfully done then only execute
//            insertIntoTransactionHistory($conn,$totalPrice,0,$uid,null,null,null,2,null,$orderId,3,null,null);
//
