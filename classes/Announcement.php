<?php
class Announcement{
    var $announce_id, $announce_message, $announce_dateCreated, $announce_lastUpdated ,$announce_showThis;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->announce_id;
    }

    /**
     * @param mixed $announce_id
     */
    public function setId($announce_id)
    {
        $this->announce_id = $announce_id;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->announce_message;
    }

    /**
     * @param mixed $announce_message
     */
    public function setMessage($announce_message)
    {
        $this->announce_message = $announce_message;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->announce_dateCreated;
    }

    /**
     * @param mixed $announce_dateCreated
     */
    public function setDateCreated($announce_dateCreated)
    {
        $this->announce_dateCreated = $announce_dateCreated;
    }

    /**
     * @return mixed
     */
    public function getLastUpdated()
    {
        return $this->announce_lastUpdated;
    }

    /**
     * @param mixed $announce_lastUpdated
     */
    public function setLastUpdated($announce_lastUpdated)
    {
        $this->announce_lastUpdated = $announce_lastUpdated;
    }

    
    /**
     * @return mixed
     */
    public function getShowThis()
    {
        return $this->announce_showThis;
    }

    /**
     * @param mixed $announce_showThis
     */
    public function setShowThis($announce_showThis)
    {
        $this->announce_showThis = $announce_showThis;
    }

}

function getAnnouncement($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("announce_id","announce_message","announce_showThis","announce_dateCreated","announce_lastUpdated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"announcement");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('si',$queryValues[0],$queryValues[1]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($announce_id, $announce_message , $announce_showThis, $announce_dateCreated, $announce_lastUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Announcement();
            $class->setId($announce_id);
            $class->setMessage($announce_message);
            $class->setShowThis($announce_showThis);
            $class->setDateCreated($announce_dateCreated);
            $class->setLastUpdated($announce_lastUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}