<?php
 

require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php'; 
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';


$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    //todo create table for transaction_history with at least a column for quantity(in order table),product_id(in order table), order_id, status (others can refer to btcw's), target_uid, trigger_transaction_id
    //todo create table for order and product_order
    $totalProductCount = count($_POST['product-list-id-input']);
    for($i = 0; $i < $totalProductCount; $i++){
        $productId = $_POST['product-list-id-input'][$i];
        $quantity = $_POST['product-list-quantity-input'][$i];

        echo " this: $productId total: $quantity";
    }
}

$products = getProduct($conn);

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/adminDashboard.php" />
    <meta property="og:title" content="Admin Dashboard | DCK Supreme" />
    <title>Admin Dashboard | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/adminDashboard.php" />
    <?php include 'css.php'; ?>    
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<!--<form method="POST">-->
    <?php
//    if(isset($_POST['product-list-quantity-input'])){
//        createProductList($products,$_POST['product-list-quantity-input']);
//    }else{
//        createProductList($products);
//    }

    ?>
<!--    <button type="submit" name="addToCartButton" id="addToCartButton" >Add to cart</button>-->
<!--</form>-->
<div class="yellow-body padding-from-menu same-padding">
	<h1 class="h1-title h1-before-border">Dashboard</h1>
    <div class="border-top100 four-div-container">
    	<a href="adminProduct.php" class="black-text">
            <div class="four-white-div hover1">
                <img src="img/product1.png" class="four-img hover1a" alt="Products" title="Products">
                <img src="img/product2.png" class="four-img hover1b" alt="Products" title="Products">
                <p class="four-div-p"><b>3 Products Running Low</b></p>
            </div>
        </a>
        <a href="adminShpping.php" class="black-text">
            <div class="four-white-div hover1 four-middle-div1">
                <img src="img/shipping1.png" class="four-img hover1a" alt="Shipping" title="Shipping">
                <img src="img/shipping2.png" class="four-img hover1b" alt="Shipping" title="Shipping">
                <p class="four-div-p"><b>2 Shipping Requests</b></p>
            </div> 
        </a>
        <a href="adminWithdrawal.php" class="black-text">   
            <div class="four-white-div hover1 four-middle-div2">
                <img src="img/withdraw1.png" class="four-img hover1a" alt="Withdrawal" title="Withdrawal">
                <img src="img/withdraw2.png" class="four-img hover1b" alt="Withdrawal" title="Withdrawal">
                <p class="four-div-p"><b>2 Withdrawal Requests</b></p>
            </div>
        </a>
        <a href="adminMember.php" class="black-text">        
            <div class="four-white-div hover1">
                <img src="img/member1.png" class="four-img hover1a" alt="New Joined Members" title="New Joined Members">
                <img src="img/member2.png" class="four-img hover1b" alt="New Joined Members" title="New Joined Members">
                <p class="four-div-p"><b>2 New Joined Members on This Week</b></p>
            </div>
        </a>                     
    </div>
    <div class="clear"></div>
	<h1 class="h1-title extra-mtop2">Sales of This Week</h1>
    <div class="with100">
    	<table class="sales-table">
        	<thead>
            	<tr class="sales-th-tr">
                	<th>NO.</th>
                    <th>PRODUCT</th>
                    <th>QUANTITY</th>
                    <th class="right-cell">TOTAL (RM)</th>
                </tr>
            </thead>
            <tr>
            	<td>1.</td>
                <td>Synthetic Plus Oil - SAE 5W - 30</td>
                <td>1,000</td>
                <td class="right-cell">200,000.00</td>
            </tr>
            <tr>
            	<td>2.</td>
                <td>Synthetic Plus Oil - SAE 10W - 40</td>
                <td>700</td>
                <td class="right-cell">200,000.00</td>
            </tr>            
            <tr>
            	<td>3.</td>
                <td>DCK Fuel Booster</td>
                <td>2,000</td>
                <td class="right-cell">300,000.00</td>
            </tr>
            <tr>
            	<td>4.</td>
                <td>DCK Engine Oil Booster</td>
                <td>3,000</td>
                <td class="right-cell">300,000.00</td>
            </tr>
            <tr class="double-border">
            	<td></td>
                <td></td>
                <td></td>
                <td class="right-cell">1,000,000.00</td>
            </tr>                                    
        </table>
    </div>    
</div>


<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

</body>
</html>