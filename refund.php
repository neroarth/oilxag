<?php
 

require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php'; 
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';


$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    //todo create table for transaction_history with at least a column for quantity(in order table),product_id(in order table), order_id, status (others can refer to btcw's), target_uid, trigger_transaction_id
    //todo create table for order and product_order
    $totalProductCount = count($_POST['product-list-id-input']);
    for($i = 0; $i < $totalProductCount; $i++){
        $productId = $_POST['product-list-id-input'][$i];
        $quantity = $_POST['product-list-quantity-input'][$i];

        echo " this: $productId total: $quantity";
    }
}

$products = getProduct($conn);

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/refund.php" />
    <meta property="og:title" content="Refund | DCK Supreme" />
    <title>Refund | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/refund.php" />
    <?php include 'css.php'; ?>    
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<!--<form method="POST">-->
    <?php
//    if(isset($_POST['product-list-quantity-input'])){
//        createProductList($products,$_POST['product-list-quantity-input']);
//    }else{
//        createProductList($products);
//    }

    ?>
<!--    <button type="submit" name="addToCartButton" id="addToCartButton" >Add to cart</button>-->
<!--</form>-->
<div class="yellow-body padding-from-menu same-padding">
	<h1 class="details-h1" onclick="goBack()">
    	<a class="black-white-link2 hover1">
    		<img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">  
            <img src="img/back2.png" class="back-btn2 hover1b" alt="back" title="back">  
        	Refund Order Number: #180201
        </a>  
    </h1>
    <table class="details-table">
    	<tbody>
            <tr>
            	<td>Bank Name</td>
                <td>:</td>
                <td>Han Lai Meng</td>
            </tr>
            <tr>
            	<td>Bank</td>
                <td>:</td>
                <td>Maybank</td>
            </tr>
             <tr>
            	<td>Acc. No.</td>
                <td>:</td>
                <td>555899606</td>
            </tr>      
    	</tbody>
    </table>    
 	<div class="search-container0">

            <div class="shipping-input clean smaller-text2 fifty-input ow-mbtm">
                <p>Refund Method</p>
                <select class="shipping-input2 clean normal-input same-height-with-date">
                	<option>iPay88</option>
                    <option>Online Banking</option>
                </select>
            </div>
            <div class="shipping-input clean smaller-text2 fifty-input ow-mbtm">
                <p>Refund Amount</p>
                <input class="shipping-input2 clean normal-input same-height-with-date" type="number" placeholder="0.00">
            </div>     
            <div class="shipping-input clean smaller-text2 fifty-input ow-mbtm">
                <p>Note</p>
                <input class="shipping-input2 clean normal-input same-height-with-date" type="text" placeholder="Note">
            </div> 
            <div class="shipping-input clean smaller-text2 fifty-input ow-mbtm  ow-mbtm-reason">
                <p>Choose a Reason</p>
                <select class="shipping-input2 clean normal-input same-height-with-date">
                	<option>Out of our delivery zone</option>
                    <option>Running out of stock</option>
                </select>
            </div>                               
            <div class="shipping-input clean smaller-text2 fifty-input ow-mbtm">
                <textarea class="shipping-input2 clean normal-input same-height-with-date reason-textarea" placeholder="Or write a reason here"></textarea>
            </div>                    
            <div class="clear">
            <div class="upload-btn-wrapper">
              <button class="upload-btn">Upload Shipping Details</button>
              <input class="hidden-input" type="file" name="myfile" />
            </div> 

    </div>    
    <div class="clear"></div>
    <div class="three-btn-container">
        <a href="orderDetails.php" class="shipout-btn-a black-button three-btn-a float-left"><b>REJECT</b></a>
    </div>  
</div>

</div>
<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>
<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})

</script>
<script>
function goBack() {
  window.history.back();
}
</script>
</body>
</html>