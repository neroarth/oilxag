<?php
if (session_id() == ""){
    session_start();
}

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    //todo validation on server side
    $conn = connDB();

    if(isset($_POST['loginButton'])){
        $email = rewrite($_POST['email']);
        $password = $_POST['password'];

        $userRows = getUser($conn," WHERE email = ? ",array("email"),array($email),"s");
        if($userRows){
            $user = $userRows[0];

            $tempPass = hash('sha256',$password);
            $finalPassword = hash('sha256', $user->getSalt() . $tempPass);

            if($finalPassword == $user->getPassword()) {
                $_SESSION['uid'] = $user->getUid();
                echo '<script>window.location.replace("profile.php");</script>';
            }
            else {
                promptError("Incorrect email or password");
            }
        }else{
            promptError("This account does not exist");
        }
    }

    $conn->close();
}

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
</head>
<body>
    <form method="POST">
        <div class="input-container1 ow-input-container1">
            <input type="text" name="email" placeholder="email" value="<?php if(isset($_POST['email'])){echo $_POST['email'];}?>">
        </div>
        <div class="input-container5 ow-input-container5">
            <input type="password"  name="password" placeholder="password">
        </div>
        <button name="loginButton" id="loginButton" >Login</button>
    </form>

    <?php createSimpleNoticeModal(); ?>

    <?php require_once dirname(__FILE__) . '/footer.php'; ?>


</body>
</html>
