<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Images.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/TransactionHistory.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];
$userPic = getImages($conn," WHERE pid = ? ",array("pid"),array($userDetails->getPicture()),"s");
$userProPic = $userPic[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/wallet.php" />
    <meta property="og:title" content="My Wallet | DCK Supreme" />
    <title>My Wallet | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/wallet.php" />
    <?php include 'css.php'; ?>    
</head>
<body class="body">
<?php include 'header-sherry.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">

<!--function to display profile picture-->
<?php include 'profilePictureDispalyPartA.php'; ?>

    <div class="right-profile-div">
    	<div class="profile-tab width100">
        	<a href="profile.php" class="profile-tab-a ">ABOUT</a>
            
            <a href="referee.php" class="profile-tab-a">MY REFEREE</a>
            <a href="#" class="profile-tab-a active-tab-a">MY WALLET</a>
        </div>
        <div class="left-profile-div-user">
            <h1 class="username wallet-username"> <?php echo $userDetails->getUsername();?> </h1>
        </div>
        <div class="right-profile-div-transfer hide">    
        	
            <img src="img/star.png" class="voucher-icon" alt="Transfer Points" title="Transfer Points">
        	<img src="img/coupon.png" class="voucher-icon voucher1" alt="Transfer Voucher" title="Transfer Voucher">
        </div>   
        <div class="clear"></div>
        <div class="width100 oveflow wallet-big-div">
        	<div class="width50 first-50">
            	<div class="white50div">
                    <img src="img/cash2.png" class="cash-icon">
                    <h2>Cash</h2>
                    <p>100</p>   
                </div>
                <div class="open-withdraw withdraw-button">Withdraw</div>
                <div class="open-convert convert-button">Convert to Points</div>         
            </div>
        	<div class="width50 second-width50 wallet-big-div">
				<div class="white50div">
                    <img src="img/points.png" class="cash-icon">
                    <h2>Point</h2>
                    <p>100</p> 
                </div>  
                <div class="open-withdraw withdraw-button">Withdraw</div>
                <div class="open-convert convert-button">Add Referee</div>            
            </div>            
        </div>
        <div class="clear"></div>



            <?php

            /*
                WenJie_READ - basic example of displaying current user's total cash and voucher
                (***need to deduct the withdrew amount and transferred amount accordingly)
            */

            $conn = connDB();

            $top10ReferrerLists = getTop10ReferrerOfUser($conn,$uid);
            $count = 0;

            foreach ($top10ReferrerLists as $uplineUid) {
                $count++;
                $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uplineUid),"s");
                $thisUser = $userRows[0];

                $totalCash = getSum($conn,"transaction_history","money_in"," WHERE uid = ? AND money_type_id = 1 AND (transaction_type_id = 1 OR transaction_type_id = 2 OR transaction_type_id = 6 ) ",array("uid"),array($uplineUid),"s");
                $totalVoucher = getSum($conn,"transaction_history","money_in"," WHERE uid = ? AND money_type_id = 2 AND (transaction_type_id = 1 OR transaction_type_id = 2 OR transaction_type_id = 6 ) ",array("uid"),array($uplineUid),"s");

                echo '
                    <tr class="profile-tr">
                        <td class="profile-td1">'.$count . ') ' .$thisUser->getEmail().' ('.$uplineUid.')</td>
                        <td class="profile-td2">:</td>
                        <td class="profile-td3">'.removeUselessZero($totalCash).' (CASH) --- '.removeUselessZero($totalVoucher).' (VOUCHER)</td>
                    </tr>
                ';
            }


            $conn->close();

            ?>
        	                               
        </table>

        
    </div>



</div>


<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>

</body>
</html>