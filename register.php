<?php

//check phone number with this library from here https://github.com/google/libphonenumber
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/Country.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();
$countries = getCountry($conn);

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    //todo validation on server side

    $uid = md5(uniqid());
    $email = rewrite($_POST['email']);
    $countryCode = rewrite($_POST['country_code']);
    $phoneNo = rewrite($_POST['phone_number']);
    $password = $_POST['password'];
    $confirmPassword = $_POST['confirm_password'];
    $icNo = rewrite($_POST['ic_no']);
    $referrerEmail = rewrite($_POST['referrer_email']);
    $referrerUid = null;
    $countryId = getCountryIdByPhoneCode($conn,$countryCode);

    $password = hash('sha256',$password);
    $salt = substr(sha1(mt_rand()), 0, 100);
    $finalPassword = hash('sha256', $salt.$password);

    $email = filter_var($email, FILTER_SANITIZE_EMAIL);

    if (filter_var($email, FILTER_VALIDATE_EMAIL)) //Validate email using php
    {
        if($referrerEmail){
            $referrerUserRows = getUser($conn," WHERE email = ? ",array("email"),array($referrerEmail),"s");
    
            if($referrerUserRows){
                $referrerUid = $referrerUserRows[0]->getUid();
                $topReferrerUid = $referrerUid;//assign top referrer id to this guy 1st, if he is not the top, will be overwritten
                $currentLevel = 1;
    
                $referralHistoryRows = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($referrerUid),"s");
                if($referralHistoryRows){
                    $topReferrerUid = $referralHistoryRows[0]->getTopReferrerId();
                    $currentLevel = $referralHistoryRows[0]->getCurrentLevel() + 1;
                }
    
                if(registerNewUser($conn,$uid,$email,$countryId,$phoneNo,$finalPassword,$salt,$icNo,$referrerUid,$topReferrerUid,$currentLevel)){
                    promptSuccess("Registration is successful!");
                    goToConfirmationPage();
                }
            }else{
                promptError("The referrer with this email: $referrerEmail does not exist in our database");
            }
        }else{
            if(registerNewUser($conn,$uid,$email,$countryId,$phoneNo,$finalPassword,$salt,$icNo)){
                promptSuccess("Registration is successful!");
                goToConfirmationPage();
            }
        }
    } 
    else 
    {
        promptError("$email is not a valid email address");
    }
}

function registerNewUser($conn,$uid,$email,$countryId,$phoneNo,$finalPassword,$salt,$icNo,$referrerUid = null,$topReferrerUid = null,$currentLevel = null){
    $isReferred = 0;
    if($referrerUid && $topReferrerUid){
        $isReferred = 1;
    }

    if(insertDynamicData($conn,"user",array("uid","email","password","salt","phone_no","ic_no","country_id","is_referred"),
        array($uid,$email,$finalPassword,$salt,$phoneNo,$icNo,$countryId,$isReferred),"ssssssii") === null){
        promptError("error registering new account.The account already exist");
        return false;
    }else{
        if($isReferred === 1){
            if(insertDynamicData($conn,"referral_history",array("referrer_id","referral_id","current_level","top_referrer_id"),
                array($referrerUid,$uid,$currentLevel,$topReferrerUid),"ssis") === null){
                promptError("error assigning referral relationship");
                return false;
            }
        }
    }

    return true;
}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
function goToConfirmationPage()
{
    echo '
        <script>
            window.location.replace("confirmRegister.php");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Registration</title>
</head>
<body>
    <form method="POST" onsubmit="return validateMyForm(errorMsgArrayTranslationForJsValidation);">
        <div class="input-container2 edit-margin">
            <input type="email" class="inputa clean2"  name="email" id="email" placeholder="Your Email" value="<?php if(isset($_POST['email'])){echo $_POST['email'];}?>">
        </div>

        <div class='input-container3'>
            <select class='inputa clean2' onchange="onChangeCountryCode(this.value)" name="country_code" id="country_code" >
                <option disabled selected>country</option>";

                <?php
                foreach ($countries as $country){
                    if(isset($_POST['country_code']) && $_POST['country_code'] == $country->getPhonecode()){
                        echo '<option selected ';
                    }else{
                        echo '<option ';
                    }
                    echo ' value="'.$country->getPhonecode().'">'.$country->getName().'</option>';
                }
                ?>
            </select>
        </div>

        <div class="input-container4 edit-margin">
            <input required type="text" disabled class="country-code-input clean2 white-text" name="country_code_text" id="country_code_text" placeholder="" value="<?php if(isset($_POST['country_code'])){echo $_POST['country_code'];}?>">
            <input type="text" class="mobile-input clean2 white-text" name="phone_number" placeholder="Your Phone Number" id="phone_number" min="0" value="<?php if(isset($_POST['phone_number'])){echo $_POST['phone_number'];}?>">
        </div>

        <div class="input-container5">
            <input type="password" class="inputa clean2 password-input"  name="password" id="password" placeholder="Your Password">

        </div>

        <div class="input-container6">
            <input type="password" class="inputa clean2 password-input"  name="confirm_password" id="confirm_password" placeholder="Confirm Password">
        </div>

        <div class="input-container1">
            <input type="text" class="inputa clean2"  name="ic_no" id="ic_no" placeholder="Your IC Number" value="<?php if(isset($_POST['ic_no'])){echo $_POST['ic_no'];}?>">
        </div>

        <div class="input-container2 edit-margin">
            <input type="email" class="inputa clean2"  name="referrer_email" id="referrer_email" placeholder="Your referrer's email" value="<?php if(isset($_POST['referrer_email'])){echo $_POST['referrer_email'];}?>">
        </div>

        <div class="checkbox-div">
            <div class="checkbox1">
                <input class='form-check-input' type='checkbox' value='' id='check_tnc' name='check_tnc' <?php if(isset($_POST['check_tnc'])){echo "checked";} ?>>

                <label class="form-check-label" for="check_tnc">
                    Check to agree terms and conditions
                </label>
            </div>
        </div>
        <button class="register-button2 clean orange-hover button-width" name="registerButton" id="registerButton" >Register</button>
    </form>

    <?php createSimpleNoticeModal(); ?>

    <?php require_once dirname(__FILE__) . '/footer.php'; ?>
    <script type="text/javascript" src="js/inputValidator.js?version=1.0.0"></script>
    <script type="text/javascript" src="js/validid.js?version=1.0.0"></script>


    <?php echo getJsErrorValidationMsgArray(); ?>

    <script>
        function onChangeCountryCode(value){
            document.getElementById('country_code_text').value = "+" + value;
        }
    </script>

</body>
</html>
