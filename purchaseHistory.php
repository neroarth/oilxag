<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Images.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    // echo $_POST['update_icno'].'<br>';

    $icNo = rewrite($_POST["update_icno"]);
    $username = rewrite($_POST["update_username"]);
    $fullname = rewrite($_POST["update_fullname"]);
    $birthday = rewrite($_POST["update_birthday"]);
    $gender = rewrite($_POST["update_gender"]);
    $phoneNo = rewrite($_POST["update_phoneno"]);
    $address = rewrite($_POST["update_address"]);

    // $email = rewrite($_POST['update_email']);
    $email = rewrite($_POST["update_email"]);
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
    {
        echo "email confic";
        //$emailErr = "Invalid email format"; 
    }
}

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];
$userPic = getImages($conn," WHERE pid = ? ",array("pid"),array($userDetails->getPicture()),"s");
$userProPic = $userPic[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/purchaseHistory.php" />
    <meta property="og:title" content="Purchase History | DCK Supreme" />
    <title>Purchase History | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/purchaseHistory.php" />
    <?php include 'css.php'; ?>    
</head>
<body class="body">
<?php include 'header-sherry.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<!--<form method="POST">-->
    <?php
//    if(isset($_POST['product-list-quantity-input'])){
//        createProductList($products,$_POST['product-list-quantity-input']);
//    }else{
//        createProductList($products);
//    }

    ?>
<!--    <button type="submit" name="addToCartButton" id="addToCartButton" >Add to cart</button>-->

<!-- <form method="POST" onsubmit="return editprofileFunc(name);"> -->
<div class="yellow-body padding-from-menu same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Purchase History</h1>
    <!-- This is a filter for the table result -->
    <select class="filter-select clean">
    	<option class="filter-option">Oldest Order</option>
        <option class="filter-option">Latest Order</option>
        <option class="filter-option">Highest Price</option>
        <option class="filter-option">Lowest Price</option>
    </select>
    
    <!-- End of Filter -->
    <div class="clear"></div>

    <div class="clear"></div>

    <div class="width100 shipping-div2">
    	<div class="overflow-scroll-div">
            <table class="shipping-table">
                <thead>
                    <tr>
                        <th>NO.</th>
                        <th>ORDER NUMBER</th>
                        <th>ORDER DATE</th>
                        <th>PRODUCT</th>
                        <th>PRICE (RM)</th>
                    </tr>
                </thead>
                <tr data-url="memberOrderDetails.php" class="link-to-details">
                    <td>1.</td>
                    <td>#180201</td>
                    <td>2019-08-15</td>
                    <td>DCK Engine Oil Booster X 3</td>
                    <td>1000.00</td>
                </tr>
                <tr data-url="memberOrderDetails.php" class="link-to-details">
                    <td>2.</td>
                    <td>#180201</td>
                    <td>2019-08-15</td>
                    <td>DCK Engine Oil Booster X 3</td>
                    <td>1000.00</td>
                </tr>            
            </table>
        </div>
    </div>
    <div class="clear"></div>
    <div class="bottom-big-container">
    	<div class="left-btm-page">
        	Page <select class="clean transparent-select"><option>1</option></select> of 1
        </div>
        <div class="middle-btm-page">
        	<a class="round-black-page">1</a>
            <a class="round-white-page">2</a>
        </div>
        <div class="right-btm-page">
        	Total: 2
        </div>
    </div>    
</div>
<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>
<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})

</script>
</body>
</html>