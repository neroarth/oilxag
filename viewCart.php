<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

//Change Class (Product to Cart)
require_once dirname(__FILE__) . '/classes/Cart.php';

require_once dirname(__FILE__) . '/classes/GroupCommission.php';
require_once dirname(__FILE__) . '/classes/TransactionHistory.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    addToCart();
    createOrder($conn,$uid);
    header('Location: ./checkout.php');
    //createOrder($conn,$uid);
    // how to add clearcart ?
    // clearCart();
    // header('Location: ../oilxag/product.php');
}

// if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
//     $productListHtml = getShoppingCart($conn,2);
// }else
// {}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>


<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://dcksupreme.asia/product.php" />
<meta property="og:title" content="DCK Engine Oil Booster | DCK Supreme" />
<title>DCK Engine Oil Booster | DCK Supreme</title>
<meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
<meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
<meta name="keywords" content="DCK®,dck, dck supreme, supreme, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
noisiness and temperature, dry cold start,etc">
<link rel="canonical" href="https://dcksupreme.asia/product.php" />
<?php include 'css.php'; ?>
<?php require_once dirname(__FILE__) . '/header.php'; ?>
</head>

<body class="body">
<!--
<div id="overlay">
 <div class="center-food"><img src="img/loading-gif.gif" class="food-gif"></div>
 <div id="progstat"></div>
 <div id="progress"></div>
</div>-->

<!-- Start Menu -->

<div class="yellow-body padding-from-menu same-padding">
	<?php include 'header-sherry.php'; ?>

    <h1 class="cart-h1">Your Cart</h1>

    <form method="POST">
    <!-- <form method="POST"  action="utilities/cartPrice.php"> -->

        <table class="cart-table">
            <th>Product</th>
            <th></th>
            <th class="quantity-td">QUANTITY</th>
            <th>PRICE</th>
            <th>TOTAL</th>
        </table>


        <?php
            $conn = connDB();
            if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart'])
            {
                $productListHtml = getShoppingCart($conn,2);
                echo $productListHtml;
            }
            else
            {
                echo " <h3> YOUR CART IS EMPTY </h3>";
            }
            //echo $productListHtml;
            //  $conn->close();

            if(array_key_exists('xclearCart', $_POST))
            {
                xclearCart();
            }
            else
            {
            // code...
                unset($productListHtml);
            }
            function xclearCart()
            {
                unset ($_SESSION["shoppingCart"]);
            }
            $conn->close();
        ?>

        <!-- <?php //echo $productListHtml; ?> -->
        <!-- <button type="submit" name="checkoutButton" id="checkoutButton" >Checkout</button> -->
   
        <!-- <button type="submit" name="checkoutButton" id="checkoutButton" 
                class="clean black-button add-to-cart-btn">CHECKOUT</button> -->
        
    <div class="cart-bottom-div">
        <div class="left-cart-bottom-div">
            <!-- <p class="continue-shopping pointer"  onclick="goBack()"><img src="img/back.png" class="back-btn" alt="back" title="back" > Continue Shopping</p> -->
            <p class="continue-shopping pointer continue2"><a href="product.php" class="black-white-link"><img src="img/back.png" class="back-btn" alt="back" title="back" > Continue Shopping</a></p>
            <!-- <button img src="img/delete0.png" class="delete-img"></button> -->
            <!-- <td class="no6"><img src="img/delete0.png" alt="delete" title="delete" class="delete-img"></td> -->
        </div>
    </div>

        </form>
</div>

<!-- <script>
function goBack() {
    window.history.back();
}
</script> -->

<?php include 'js.php'; ?>

</body>
</html>