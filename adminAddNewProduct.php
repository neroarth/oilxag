<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    // echo $_POST['update_productname'].'<br>';
    // echo $_POST['update_productprice'].'<br>';
    // echo $_POST['update_producttype'].'<br>';
}

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/adminPayout.php" />
    <meta property="og:title" content="Payout | DCK Supreme" />
    <title>Payout | DCK Supreme</title>
    <meta property="og:description" content="DCK® Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK® Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration,
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/adminPayout.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">

<h1 class="username">Add New Product</h1>



        <form  action="utilities/addNewProductFunction.php" method="POST" enctype="multipart/form-data">
        <table class="edit-profile-table">
            <!-- <tr class="profile-tr">
                <td class="profile-td1">Product Picture</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><input type="file" name="image" id="image" required></td>
            </tr>   -->

            <tr class="profile-tr">
                <td class="profile-td1">Product Name</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><input id="update_productname" class="clean edit-profile-input" type="text" placeholder="Product Name"  name="update_productname"></td>
            </tr>

            <tr class="profile-tr">
                <td class="profile-td1">Product Price</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><input id="update_productprice" class="clean edit-profile-input" type="number" placeholder="Product Price"  name="update_productprice"></td>
            </tr>

            <tr class="profile-tr">
                <td class="profile-td1">Product Type</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><input id="update_producttype" class="clean edit-profile-input" type="number" placeholder="Product Type"  name="update_producttype"></td>
            </tr>
            <tr class="profile-tr">
                <td class="profile-td1">Product Stock</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><input id="update_productstock" class="clean edit-profile-input" type="number" placeholder="Product Stock"  name="update_productstock"></td>
            </tr>

            <tr class="profile-tr">
                <td class="profile-td1">Product Description</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><input id="update_productdescription" class="clean edit-profile-input" type="text" placeholder="Product Description"  name="update_productdescription"></td>
            </tr>
          </table><div class="upload-btn-wrapper">
              <button class="upload-btn">Upload Product Image</button>
              <input class="text" type="file" name="file" />
            </div><br>


              <button input type="submit" name="upload" value="Upload" class="confirm-btn text-center white-text clean black-button">Confirm</button>

        </form>


</div>


<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "New Product Added Successfully";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "There is an error to add the new product";
        }

        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>
