<?php
require_once dirname(__FILE__) . '/adminAccess.php'; 
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BonusReport.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

// $getWho = getBonusReport($conn);
$sponsorBonusReport = getBonusReport($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/admin.php" />
    <meta property="og:title" content="Admin | DCK Supreme" />
    <title>Admin | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/admin.php" />
    <?php include 'css.php'; ?>    
</head>
<body class="body">

<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">

<h1 class="h1-title">TOTAL SPONSOR BONUS REPORT</h1>

            <div class="width100 oveflow">

                <div class="width100 overflow">
                    <div class="table-wrapper no-margin">
                        <div class="table-scroll">
                            <table class="shipping-table">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Name</th>
                                        <th>REGISTERED DOWNLINE NUMBER</th>
                                        <th>Downline Name</th>
                                        <th>JOINED DATE</th>
                                        <th>BONUS</th>
                                    </tr>
                                </thead>
                                <?php
                                    $conn = connDB();
                                    if($sponsorBonusReport) 
                                    {
                                    for($a = 0;$a<count($sponsorBonusReport);$a++)
                                        {?>
                                        <tr class="tr-a">
                                            <td><?php echo ($a+1);?></td>
                                            <td><?php $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($sponsorBonusReport[$a]->getReferrerId()),"s");
                                                    echo $userDetails[0]->getUsername();?>
                                            </td>
                                            <td><?php echo $sponsorBonusReport[$a]->getRegisterDownlineNo();?></td>
                                            <td><?php $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($sponsorBonusReport[$a]->getReferralId()),"s");
                                                    echo $userDetails[0]->getUsername();?>
                                            </td>
                                            <td>
                                                <?php $dateCreated = date("Y-m-d",strtotime($sponsorBonusReport[$a]->getDateCreated()));echo $dateCreated;?>
                                            </td>
                                            <td><?php echo $sponsorBonusReport[$a]->getAmount();?></td>
                                        </tr>
                                <?php
                                        }
                                $conn->close();
                                    }
                                ?>             
                            </table>
                        </div>
                    </div>
                </div>
            </div>
</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Add New Admin";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "Error";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>