<?php
// if (session_id() == ""){
//     session_start();
// }
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

// $id = $_SESSION['order_id'];

$conn = connDB();

$productsOrders =  getProductOrders($conn);

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    if(isset($_POST["payment_verification"])){
        $payment_status = rewrite($_POST["payment_status"]);
        $shipping_status = rewrite($_POST["shipping_status"]);
        $order_id = rewrite($_POST["order_id"]);
    }else{
        $payment_status = "";
        $shipping_status = "";
        $order_id = "";
    }
}

$conn->close();
function promptError($msg)
{
    echo '<script>  alert("'.$msg.'");  </script>';
}

function promptSuccess($msg)
{
    echo '<script>  alert("'.$msg.'");   </script>';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/shippingOut.php" />
    <meta property="og:title" content="Shipping Out | DCK Supreme" />
    <title>Shipping Out | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration,
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/shippingOut.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php include 'header-sherry.php'; ?>



<div class="yellow-body padding-from-menu same-padding">
<form method="POST" action="utilities/updatePaymentVerificationFunction.php">
    <h1 class="details-h1" onclick="goBack()">
        <a class="black-white-link2 hover1">
            <img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">
            <img src="img/back2.png" class="back-btn2 hover1b" alt="back" title="back">
            Order Number : #<?php echo $_POST['order_id'];?>
        </a>
    </h1>


    <div class="width100 shipping-div2">
        <table class="details-table">
            <tbody>
            <?php
            if(isset($_POST['order_id']))
            {
                $conn = connDB();
                //Order
                $orderArray = getOrders($conn,"WHERE id = ? ", array("id") ,array($_POST['order_id']),"i");
                //OrderProduct
                $orderProductArray = getProductOrders($conn,"WHERE order_id = ? ", array("order_id") ,array($_POST['order_id']),"i");
                //$orderDetails = $orderArray[0];

                if($orderArray != null)
                {
                    ?>
                    <tr>
                        <td>Name</td>
                        <td>:</td>
                        <td><?php echo $orderArray[0]->getName()?></td>
                    </tr>
                    <tr>
                        <td>Payment Method</td>
                        <td>:</td>
                        <td><?php echo $orderArray[0]->getPaymentMethod()?></td>
                    </tr>
                    <tr>
                        <td>Total Fees</td>
                        <td>:</td>
                        <td><?php echo $orderArray[0]->getTotal()?></td>
                    </tr>
                    <tr>
                        <td>Payment Amount</td>
                        <td>:</td>
                        <td><?php echo $orderArray[0]->getPaymentAmount()?></td>
                    </tr>
                    <tr>
                        <td>Payment Reference</td>
                        <td>:</td>
                        <td><?php echo $orderArray[0]->getPaymentBankReference()?></td>
                    </tr>
                    <tr>
                        <td>Payment Date and Time</td>
                        <td>:</td>
                        <td><?php echo $orderArray[0]->getPaymentDate()?></td>
                        <td><?php echo $orderArray[0]->getPaymentTime()?></td>
                    </tr>
                    <?php

                }
            }
            else
            {}
            $conn->close();
            ?>
            </tbody>
        </table>
    </div>

    <div class="search-container0">
            <div class="shipping-input clean smaller-text2 three-input">
                <p>PAYMENT VERIFICATION</p>
                <select class="shipping-input2 clean normal-input same-height-with-date" type="text" id="payment_status" name="payment_status">
                    <option value="ACCEPTED" name="ACCEPTED">ACCEPTED</option>
                    <option value="REJECT" name="REJECT">REJECT</option>
                </select>
            </div>

            <div class="shipping-input clean smaller-text2 three-input">
                <p>UPDATE SHIPPING STATUS</p>
                <select class="shipping-input2 clean normal-input same-height-with-date" type="text" id="shipping_status" name="shipping_status">
                    <option value="PENDING" name="PENDING">PENDING</option>
                    <option value="REJECT" name="REJECT">REJECT</option>
                </select>
            </div>

            <div class="clear"></div>

            <input class="shipping-input2 clean normal-input same-height-with-date" type="hidden" id="order_id" name="order_id" value="<?php echo $orderArray[0]->getId()?>">
    </div>

    <div class="clear"></div>

    <div class="three-btn-container">
        <button input type="submit" name="submit" value="ShipOut" class="shipout-btn-a black-button three-btn-a">Submit</button>
    </div>
</form>
</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<script>
function goBack() {
  window.history.back();
}
</script>

</body>
</html>
