<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Images.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    // echo $_POST['update_icno'].'<br>';

    $icNo = rewrite($_POST["update_icno"]);
    $username = rewrite($_POST["update_username"]);
    $fullname = rewrite($_POST["update_fullname"]);
    $birthday = rewrite($_POST["update_birthday"]);
    $gender = rewrite($_POST["update_gender"]);
    $phoneNo = rewrite($_POST["update_phoneno"]);
    $address = rewrite($_POST["update_address"]);

    // $email = rewrite($_POST['update_email']);
    $email = rewrite($_POST["update_email"]);
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
    {
        echo "email confic";
        //$emailErr = "Invalid email format"; 
    }
}

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];
$userPic = getImages($conn," WHERE pid = ? ",array("pid"),array($userDetails->getPicture()),"s");
$userProPic = $userPic[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/memberOrderDetails.php" />
    <meta property="og:title" content="Order Details | DCK Supreme" />
    <title>Order Details | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/memberOrderDetails.php" />
    <?php include 'css.php'; ?>    
</head>
<body class="body">
<?php include 'header-sherry.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<!--<form method="POST">-->
    <?php
//    if(isset($_POST['product-list-quantity-input'])){
//        createProductList($products,$_POST['product-list-quantity-input']);
//    }else{
//        createProductList($products);
//    }

    ?>
<!--    <button type="submit" name="addToCartButton" id="addToCartButton" >Add to cart</button>-->

<!-- <form method="POST" onsubmit="return editprofileFunc(name);"> -->
<div class="yellow-body padding-from-menu same-padding">
	<h1 class="details-h1" onclick="goBack()">
    	<a class="black-white-link2 hover1">
    		<img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">  
            <img src="img/back2.png" class="back-btn2 hover1b" alt="back" title="back">  
        	Order Number: #180201
        </a>  
    </h1>
    <table class="details-table">
    	<tbody>
            <tr>
            	<td>Name</td>
                <td>:</td>
                <td>Han Lai Meng</td>
            </tr>
            <tr>
            	<td>Contact</td>
                <td>:</td>
                <td>012345678</td>
            </tr>
             <tr>
            	<td>Status</td>
                <td>:</td>
                <td>Shipped</td>
            </tr>           
            <tr>
            	<td>Ship to</td>
                <td>:</td>
                <td>No.6, Jalan Putra, Taman H, 07800, Bayan Baru, Penang</td>
            </tr> 
            <tr>
            	<td>Method</td>
                <td>:</td>
                <td>Poslaju Malaysia (Semenanjung)</td>
            </tr>
            <!-- Only appear if shipped out -->
            <tr>
            	<td>Tracking No.</td>
                <td>:</td>
                <td>#177202</td>
            </tr> 
            <tr>
            	<td>Ship Out</td>
                <td>:</td>
                <td>2019-08-15   10:00 am</td>
            </tr>  
            <tr>
            	<td>Photo</td>
                <td>:</td>
                <td><a href="./img/shipping-details.jpg"  data-fancybox="images-preview1"  class="image-popout"><img src="img/shipping-details.jpg" class="details-img receipt-img"></a></td>
            </tr> 
            <!--- End -->                                                                    
        </tbody>
    </table>


    <div class="clear"></div>

    <div class="width100 shipping-div2">
    	<div class="overflow-scroll-div">
            <table class="shipping-table">
                <thead>
                    <tr>
                        <th>NO.</th>
                        <th>PRODUCT</th>
                        <th>QUANTITY</th>
                        <th>PRICE (RM)</th>
                    </tr>
                </thead>
                <tr>
                    <td>1.</td>
                    <td>DCK Engine Oil Booster</td>
                    <td>1</td>
                    <td>60.00</td>
                </tr>
                <tr>
                    <td>2.</td>
                    <td>DCK Fuel Booster</td>
                    <td>1</td>
                    <td>80.00</td>
                </tr>            
            </table>
            <table class="details-table details-table2">
                <tbody>
                    <tr>
                        <td>Points Used</td>
                        <td>:</td>
                        <td>0</td>
                    </tr>
                    <tr>
                        <td>Voucher Used</td>
                        <td>:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>Total</td>
                        <td>:</td>
                        <td>RM140.00</td>
                    </tr> 
                    <tr>
                        <td>Payment Method</td>
                        <td>:</td>
                        <td>ipay88</td>
                    </tr>  
                    <tr>
                        <td>Note</td>
                        <td>:</td>
                        <td>Payment Number #1822892</td>
                    </tr>  
                    <tr>
                        <td>Date</td>
                        <td>:</td>
                        <td>2019-08-15</td>
                    </tr>
                    <tr>
                        <td>Receipt</td>
                        <td>:</td>
                        <td><a href="./img/receipt.jpg"  data-fancybox="images-preview"  class="image-popout"><img src="img/receipt.jpg" class="receipt-img"></a></td>
                    </tr>                                                                                            
                </tbody>
            </table>            
        </div>
    </div>  
    <div class="clear"></div>
    <div class="three-btn-container">
    	
        <a class="refund-btn-a white-button three-btn-a"  onclick="goBack()"><b>BACK</b></a>
    </div>  
</div>
<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>
<script>
function goBack() {
  window.history.back();
}
</script>
</body>
</html>