<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Images.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/GroupCommission.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST'){

}

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$userRows2 = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$getWho = getWholeDownlineTree($conn, $_SESSION['uid'],false);

$userPic = getImages($conn," WHERE pid = ? ",array("pid"),array($userDetails->getPicture()),"s");
$userProPic = $userPic[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/referee.php" />
    <meta property="og:title" content="Referee | DCK Supreme" />
    <title>Referee | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK,DCK Supreme, supreme, DCK®, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration,
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/referee.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">
<!--function to display profile picture-->
<?php include 'profilePictureDispalyPartA.php'; ?>
    <div class="right-profile-div">
    	<div class="profile-tab width100">
        	<a href="profile.php" class="profile-tab-a ">ABOUT</a>
            <a href="#" class="profile-tab-a active-tab-a">MY REFEREE</a>
            <a href="wallet.php" class="profile-tab-a">MY WALLET</a>
        </div>


        <!-- <?php //echo _MAINJS_ENTER_USERNAME ; ?> -->
		<!-- <div class="width100 oveflow">
            <h1 class="username">My Referee</h1>
            <p class="referee-p"><b>Tan Xiao Ming <img src="img/dropdown2.png" class="referee-dropdown" alt="show more" title="show more"  onclick="showMore()"></b></p>
       		<div id="moreReferee" class="moreReferee">
            	<p class="referee-p">Ng Jia Jia <img src="img/dropdown2.png" class="referee-dropdown" alt="show more" title="show more"  onclick="showMore2()"></p>
            	<div id="moreReferee2" class="moreReferee">
            		<p class="referee-p">Ng Jia Xin</p>
            	</div>
            </div>
            <p class="distance-p"></p>
            <p class="referee-p"><b>Lim Jia Yi</b></p>
            <p class="distance-p"></p>
            <p class="referee-p"><b>Lai Jia Yi</b></p>
        </div> -->

        <div class="width100 oveflow">
        <h1 class="username">My Referee</h1>
            <?php
                $conn = connDB();
                    if($getWho){
                        // echo "<br/>";echo "<br/>";echo "<br/>";echo "<br/>";echo "<br/>";echo "<br/>";echo "<br/>";echo "<br/>";


                          echo '<ul>';
                          $lowestLevel = $getWho[0]->getCurrentLevel();
                          foreach($getWho as $thisPerson){
                              $tempUsers = getUser($conn," WHERE uid = ? ",array("uid"),array($thisPerson->getReferralId()),"s");
                              $thisTempUser = $tempUsers[0];

                              if($thisPerson->getCurrentLevel() == $lowestLevel){

                                  // echo '<li id="'.$thisPerson->getReferralId().'">'.$thisPerson->getReferralId().'</li>';
                                  echo '<li id="'.$thisPerson->getReferralId().'">'.$thisTempUser->getUsername().' </li>';

                              }
                            }

                          if (isset($_POST["mybutton"]))
                          {


                        echo '<ul>';
                        $lowestLevel = $getWho[0]->getCurrentLevel();
                        foreach($getWho as $thisPerson){
                            $tempUsers = getUser($conn," WHERE uid = ? ",array("uid"),array($thisPerson->getReferralId()),"s");
                            $thisTempUser = $tempUsers[0];


                                echo '
                                    <script type="text/javascript">
                                        var div = document.getElementById("'.$thisPerson->getReferrerId().'");

                                        div.innerHTML += "<ul><li id=\''.$thisPerson->getReferralId().'\'>'.$thisTempUser->getUsername().' </li></ul>";
                                    </script>
                                ';

                        }
                        echo '</ul>';

                    }elseif (isset($_POST["mybutton2"])) {


                    }
                }
            ?>
       </div>
       <form action="" method="POST">
         <input type="submit" class="confirm-btn text-center white-text clean black-button anc-ow-btn" value="Expand All" name="mybutton">
         <input type="submit" class="confirm-btn text-center white-text clean black-button anc-ow-btn" value="Collapse" name="mybutton2">





	</div>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>
<script>
$("#copy-referral-link").click(function(){
          var textArea = document.createElement("textarea");
          textArea.value = $('#linkCopy').val();
          document.body.appendChild(textArea);
          textArea.select();
          document.execCommand("Copy");
          textArea.remove();
          putNoticeJavascript("Copied!! ","");
      });
      $("#invest-now-referral-link").click(function(){
          var textArea = document.createElement("textarea");
          textArea.value = $('#linkCopy').val();
          document.body.appendChild(textArea);
          textArea.select();
          document.execCommand("Copy");
          textArea.remove();
          putNoticeJavascript("Copied!! ","");
      });
</script>
<script>
function showMore() {
  var x = document.getElementById("moreReferee");
  if (x.style.display === "block") {
    x.style.display = "none";
  } else {
    x.style.display = "block";
  }
}
</script>
<script>
function showMore2() {
  var x = document.getElementById("moreReferee2");
  if (x.style.display === "block") {
    x.style.display = "none";
  } else {
    x.style.display = "block";
  }
}
</script>

<!-- <script type="text/javascript">
    var div = document.getElementById("92b7722e6804cf4b15c563ac5c9004b0");

    div.innerHTML += "<ul><li id="12121212">'.$thisPerson->getReferralId().'</li></ul>";
</script> -->
</body>
</html>
