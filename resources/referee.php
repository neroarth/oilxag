<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Images.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/GroupCommission.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    //todo create table for transaction_history with at least a column for quantity(in order table),product_id(in order table), order_id, status (others can refer to btcw's), target_uid, trigger_transaction_id
    //todo create table for order and product_order
    $totalProductCount = count($_POST['product-list-id-input']);
    for($i = 0; $i < $totalProductCount; $i++){
        $productId = $_POST['product-list-id-input'][$i];
        $quantity = $_POST['product-list-quantity-input'][$i];

        echo " this: $productId total: $quantity";
    }
}

$products = getProduct($conn);

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$userRows2 = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$getWho = getWholeDownlineTree($conn, $_SESSION['uid'],false);

$userPic = getImages($conn," WHERE pid = ? ",array("pid"),array($userDetails->getPicture()),"s");
$userProPic = $userPic[0];
//$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/referee.php" />
    <meta property="og:title" content="Referee | DCK Supreme" />
    <title>Referee | DCK Supreme</title>
    <meta property="og:description" content="DCK® Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK® Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK,DCK Supreme, supreme, DCK®, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/referee.php" />
    <?php include 'css.php'; ?>    
</head>
<body class="body">
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<!--<form method="POST">-->
    <?php
//    if(isset($_POST['product-list-quantity-input'])){
//        createProductList($products,$_POST['product-list-quantity-input']);
//    }else{
//        createProductList($products);
//    }

    ?>
<!--    <button type="submit" name="addToCartButton" id="addToCartButton" >Add to cart</button>-->
<!--</form>-->
<div class="yellow-body padding-from-menu same-padding">
<!--function to display profile picture-->
<?php include 'profilePictureDispalyPartA.php'; ?>
    <div class="right-profile-div">
    	<div class="profile-tab width100">
        	<a href="profile.php" class="profile-tab-a">ABOUT</a>
            <a href="rewards.php" class="profile-tab-a hide">MY REWARDS</a>
            <a href="#" class="profile-tab-a active-tab-a">MY REFEREE</a>
        </div>
        <div class="spacing-div">
            <input type="hidden" id="linkCopy" value="https://dcksupreme.asia/index.php?referrerUID=<?php echo $_SESSION['uid']?>">
        	<h3 class="invite-h3"><b>Invitation Link:</b> <a id="invest-now-referral-link" href="#" class="invitation-link-a black-link-underline">https://dcksupreme.asia/index.php?referrerUID=<?php echo $_SESSION['uid']?></a></h3>
        	<div class="invite-div black-div-btn" id="copy-referral-link">Copy</div>
        </div>    
        <?php
        if($getWho)
        {
            for($a = 1;$a<count($getWho);$a++)
        ?>
		<div class="width100 overflow">
        	<div class="table-wrapper no-margin">
            	<div class="table-scroll">
                    <table class="referee-table">
                        <thead>
                            <tr class="tr-b">
                                <th><input type="checkbox" class="table-checkbox"></th>
                                <th>No.</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Birthday</th>
                                <th class="hide">Action</th>
                            </tr>
                        </thead>

                        <tr class="tr-a">
                        <?php
            if($getWho) 
            {
                for($a = 0;$a<count($getWho);$a++)
                
                    {?>
                        <td class="td-0"><input type="checkbox" class="table-checkbox"></td>  
                        <td class="td-1"><?php echo ($a+1)?></td>
                        <td class="td-2"><?php $userRows11 = getUser($conn," WHERE uid = ? ",array("uid"),array($getWho[$a]->getReferralId()),"s");
                                echo $userRows11[0]->getUsername();?>
                        </td>
                        <td class="td-3"><?php 
                                echo $userRows11[0]->getEmail();?>
                        </td>
                        <td class="td-4"><?php 
                                echo $userRows11[0]->getPhoneNo();?>
                        </td>
                        <td class="td-4"><?php
                                echo $userRows11[0]->getBirthday();?>
                        </td>
                    </tr>
                <?php
                    }
            }else { } 
                ?>
                        </tr>                           
                    </table>
                </div>
            </div>
            
                              
        </div>
        <?php
        }?>
    </div>



</div>


<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>
<script>
$("#copy-referral-link").click(function(){
          var textArea = document.createElement("textarea");
          textArea.value = $('#linkCopy').val();
          document.body.appendChild(textArea);
          textArea.select();
          document.execCommand("Copy");
          textArea.remove();
          putNoticeJavascript("Copied!! ","");
      });
      $("#invest-now-referral-link").click(function(){
          var textArea = document.createElement("textarea");
          textArea.value = $('#linkCopy').val();
          document.body.appendChild(textArea);
          textArea.select();
          document.execCommand("Copy");
          textArea.remove();
          putNoticeJavascript("Copied!! ","");
      });
</script>
</body>
</html>