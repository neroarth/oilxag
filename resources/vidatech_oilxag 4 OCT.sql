-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 04, 2019 at 05:02 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_oilxag`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `announce_id` int(255) NOT NULL,
  `announce_message` varchar(25000) NOT NULL,
  `announce_showThis` int(2) NOT NULL,
  `announce_dateCreated` timestamp NOT NULL DEFAULT current_timestamp(),
  `announce_lastUpdated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `announcement`
--

INSERT INTO `announcement` (`announce_id`, `announce_message`, `announce_showThis`, `announce_dateCreated`, `announce_lastUpdated`) VALUES
(1, 'Thanks for joining us, We welcome u to our journey', 1, '2019-08-14 04:17:07', '2019-08-14 06:58:37'),
(2, 'Have A nice day ! Fizo', 1, '2019-08-14 04:29:46', '2019-08-14 06:58:21'),
(3, 'Welcome To Oilxag People', 1, '2019-08-14 06:24:43', '2019-08-15 02:01:14'),
(4, 'Mun cun  eeedsafafasdfasd', 0, '2019-08-15 02:01:49', '2019-08-15 02:24:18'),
(5, 'asdsdaasdccc vvvvvvvv', 0, '2019-08-15 02:26:06', '2019-08-15 02:26:17'),
(6, 'lyon is noob team', 1, '2019-08-15 02:26:24', '2019-08-28 06:27:42'),
(7, 'GuangZhou Evergrande BIG BIG!!', 1, '2019-08-28 06:27:16', '2019-09-11 14:57:28'),
(8, 'lalala', 0, '2019-09-10 09:23:39', '2019-09-10 09:23:47'),
(9, 'dadada', 0, '2019-09-10 09:24:04', '2019-09-10 09:24:10');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `sortname` varchar(3) NOT NULL,
  `name` varchar(150) NOT NULL,
  `phonecode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `sortname`, `name`, `phonecode`) VALUES
(1, 'AF', 'Afghanistan', 93),
(2, 'AL', 'Albania', 355),
(3, 'DZ', 'Algeria', 213),
(4, 'AS', 'American Samoa', 1684),
(5, 'AD', 'Andorra', 376),
(6, 'AO', 'Angola', 244),
(7, 'AI', 'Anguilla', 1264),
(8, 'AQ', 'Antarctica', 672),
(9, 'AG', 'Antigua And Barbuda', 1268),
(10, 'AR', 'Argentina', 54),
(11, 'AM', 'Armenia', 374),
(12, 'AW', 'Aruba', 297),
(13, 'AU', 'Australia', 61),
(14, 'AT', 'Austria', 43),
(15, 'AZ', 'Azerbaijan', 994),
(16, 'BS', 'Bahamas The', 1242),
(17, 'BH', 'Bahrain', 973),
(18, 'BD', 'Bangladesh', 880),
(19, 'BB', 'Barbados', 1246),
(20, 'BY', 'Belarus', 375),
(21, 'BE', 'Belgium', 32),
(22, 'BZ', 'Belize', 501),
(23, 'BJ', 'Benin', 229),
(24, 'BM', 'Bermuda', 1441),
(25, 'BT', 'Bhutan', 975),
(26, 'BO', 'Bolivia', 591),
(27, 'BA', 'Bosnia and Herzegovina', 387),
(28, 'BW', 'Botswana', 267),
(29, 'BV', 'Bouvet Island', 0),
(30, 'BR', 'Brazil', 55),
(31, 'IO', 'British Indian Ocean Territory', 246),
(32, 'BN', 'Brunei', 673),
(33, 'BG', 'Bulgaria', 359),
(34, 'BF', 'Burkina Faso', 226),
(35, 'BI', 'Burundi', 257),
(36, 'KH', 'Cambodia', 855),
(37, 'CM', 'Cameroon', 237),
(38, 'CA', 'Canada', 1),
(39, 'CV', 'Cape Verde', 238),
(40, 'KY', 'Cayman Islands', 1345),
(41, 'CF', 'Central African Republic', 236),
(42, 'TD', 'Chad', 235),
(43, 'CL', 'Chile', 56),
(44, 'CN', 'China', 86),
(45, 'CX', 'Christmas Island', 61),
(46, 'CC', 'Cocos (Keeling) Islands', 672),
(47, 'CO', 'Colombia', 57),
(48, 'KM', 'Comoros', 269),
(49, 'CG', 'Republic Of The Congo', 242),
(50, 'CD', 'Democratic Republic Of The Congo', 242),
(51, 'CK', 'Cook Islands', 682),
(52, 'CR', 'Costa Rica', 506),
(53, 'CI', 'Cote D\'Ivoire (Ivory Coast)', 225),
(54, 'HR', 'Croatia (Hrvatska)', 385),
(55, 'CU', 'Cuba', 53),
(56, 'CY', 'Cyprus', 357),
(57, 'CZ', 'Czech Republic', 420),
(58, 'DK', 'Denmark', 45),
(59, 'DJ', 'Djibouti', 253),
(60, 'DM', 'Dominica', 1767),
(61, 'DO', 'Dominican Republic', 1809),
(62, 'TP', 'East Timor', 670),
(63, 'EC', 'Ecuador', 593),
(64, 'EG', 'Egypt', 20),
(65, 'SV', 'El Salvador', 503),
(66, 'GQ', 'Equatorial Guinea', 240),
(67, 'ER', 'Eritrea', 291),
(68, 'EE', 'Estonia', 372),
(69, 'ET', 'Ethiopia', 251),
(70, 'XA', 'External Territories of Australia', 61),
(71, 'FK', 'Falkland Islands', 500),
(72, 'FO', 'Faroe Islands', 298),
(73, 'FJ', 'Fiji Islands', 679),
(74, 'FI', 'Finland', 358),
(75, 'FR', 'France', 33),
(76, 'GF', 'French Guiana', 594),
(77, 'PF', 'French Polynesia', 689),
(78, 'TF', 'French Southern Territories', 0),
(79, 'GA', 'Gabon', 241),
(80, 'GM', 'Gambia The', 220),
(81, 'GE', 'Georgia', 995),
(82, 'DE', 'Germany', 49),
(83, 'GH', 'Ghana', 233),
(84, 'GI', 'Gibraltar', 350),
(85, 'GR', 'Greece', 30),
(86, 'GL', 'Greenland', 299),
(87, 'GD', 'Grenada', 1473),
(88, 'GP', 'Guadeloupe', 590),
(89, 'GU', 'Guam', 1671),
(90, 'GT', 'Guatemala', 502),
(91, 'XU', 'Guernsey and Alderney', 44),
(92, 'GN', 'Guinea', 224),
(93, 'GW', 'Guinea-Bissau', 245),
(94, 'GY', 'Guyana', 592),
(95, 'HT', 'Haiti', 509),
(96, 'HM', 'Heard and McDonald Islands', 0),
(97, 'HN', 'Honduras', 504),
(98, 'HK', 'Hong Kong S.A.R.', 852),
(99, 'HU', 'Hungary', 36),
(100, 'IS', 'Iceland', 354),
(101, 'IN', 'India', 91),
(102, 'ID', 'Indonesia', 62),
(103, 'IR', 'Iran', 98),
(104, 'IQ', 'Iraq', 964),
(105, 'IE', 'Ireland', 353),
(106, 'IL', 'Israel', 972),
(107, 'IT', 'Italy', 39),
(108, 'JM', 'Jamaica', 1876),
(109, 'JP', 'Japan', 81),
(110, 'XJ', 'Jersey', 44),
(111, 'JO', 'Jordan', 962),
(112, 'KZ', 'Kazakhstan', 7),
(113, 'KE', 'Kenya', 254),
(114, 'KI', 'Kiribati', 686),
(115, 'KP', 'Korea North', 850),
(116, 'KR', 'Korea South', 82),
(117, 'KW', 'Kuwait', 965),
(118, 'KG', 'Kyrgyzstan', 996),
(119, 'LA', 'Laos', 856),
(120, 'LV', 'Latvia', 371),
(121, 'LB', 'Lebanon', 961),
(122, 'LS', 'Lesotho', 266),
(123, 'LR', 'Liberia', 231),
(124, 'LY', 'Libya', 218),
(125, 'LI', 'Liechtenstein', 423),
(126, 'LT', 'Lithuania', 370),
(127, 'LU', 'Luxembourg', 352),
(128, 'MO', 'Macau S.A.R.', 853),
(129, 'MK', 'Macedonia', 389),
(130, 'MG', 'Madagascar', 261),
(131, 'MW', 'Malawi', 265),
(132, 'MY', 'Malaysia', 60),
(133, 'MV', 'Maldives', 960),
(134, 'ML', 'Mali', 223),
(135, 'MT', 'Malta', 356),
(136, 'XM', 'Man (Isle of)', 44),
(137, 'MH', 'Marshall Islands', 692),
(138, 'MQ', 'Martinique', 596),
(139, 'MR', 'Mauritania', 222),
(140, 'MU', 'Mauritius', 230),
(141, 'YT', 'Mayotte', 269),
(142, 'MX', 'Mexico', 52),
(143, 'FM', 'Micronesia', 691),
(144, 'MD', 'Moldova', 373),
(145, 'MC', 'Monaco', 377),
(146, 'MN', 'Mongolia', 976),
(147, 'MS', 'Montserrat', 1664),
(148, 'MA', 'Morocco', 212),
(149, 'MZ', 'Mozambique', 258),
(150, 'MM', 'Myanmar', 95),
(151, 'NA', 'Namibia', 264),
(152, 'NR', 'Nauru', 674),
(153, 'NP', 'Nepal', 977),
(154, 'AN', 'Netherlands Antilles', 599),
(155, 'NL', 'Netherlands The', 31),
(156, 'NC', 'New Caledonia', 687),
(157, 'NZ', 'New Zealand', 64),
(158, 'NI', 'Nicaragua', 505),
(159, 'NE', 'Niger', 227),
(160, 'NG', 'Nigeria', 234),
(161, 'NU', 'Niue', 683),
(162, 'NF', 'Norfolk Island', 672),
(163, 'MP', 'Northern Mariana Islands', 1670),
(164, 'NO', 'Norway', 47),
(165, 'OM', 'Oman', 968),
(166, 'PK', 'Pakistan', 92),
(167, 'PW', 'Palau', 680),
(168, 'PS', 'Palestinian Territory Occupied', 970),
(169, 'PA', 'Panama', 507),
(170, 'PG', 'Papua new Guinea', 675),
(171, 'PY', 'Paraguay', 595),
(172, 'PE', 'Peru', 51),
(173, 'PH', 'Philippines', 63),
(174, 'PN', 'Pitcairn Island', 0),
(175, 'PL', 'Poland', 48),
(176, 'PT', 'Portugal', 351),
(177, 'PR', 'Puerto Rico', 1787),
(178, 'QA', 'Qatar', 974),
(179, 'RE', 'Reunion', 262),
(180, 'RO', 'Romania', 40),
(181, 'RU', 'Russia', 70),
(182, 'RW', 'Rwanda', 250),
(183, 'SH', 'Saint Helena', 290),
(184, 'KN', 'Saint Kitts And Nevis', 1869),
(185, 'LC', 'Saint Lucia', 1758),
(186, 'PM', 'Saint Pierre and Miquelon', 508),
(187, 'VC', 'Saint Vincent And The Grenadines', 1784),
(188, 'WS', 'Samoa', 684),
(189, 'SM', 'San Marino', 378),
(190, 'ST', 'Sao Tome and Principe', 239),
(191, 'SA', 'Saudi Arabia', 966),
(192, 'SN', 'Senegal', 221),
(193, 'RS', 'Serbia', 381),
(194, 'SC', 'Seychelles', 248),
(195, 'SL', 'Sierra Leone', 232),
(196, 'SG', 'Singapore', 65),
(197, 'SK', 'Slovakia', 421),
(198, 'SI', 'Slovenia', 386),
(199, 'XG', 'Smaller Territories of the UK', 44),
(200, 'SB', 'Solomon Islands', 677),
(201, 'SO', 'Somalia', 252),
(202, 'ZA', 'South Africa', 27),
(203, 'GS', 'South Georgia', 0),
(204, 'SS', 'South Sudan', 211),
(205, 'ES', 'Spain', 34),
(206, 'LK', 'Sri Lanka', 94),
(207, 'SD', 'Sudan', 249),
(208, 'SR', 'Suriname', 597),
(209, 'SJ', 'Svalbard And Jan Mayen Islands', 47),
(210, 'SZ', 'Swaziland', 268),
(211, 'SE', 'Sweden', 46),
(212, 'CH', 'Switzerland', 41),
(213, 'SY', 'Syria', 963),
(214, 'TW', 'Taiwan', 886),
(215, 'TJ', 'Tajikistan', 992),
(216, 'TZ', 'Tanzania', 255),
(217, 'TH', 'Thailand', 66),
(218, 'TG', 'Togo', 228),
(219, 'TK', 'Tokelau', 690),
(220, 'TO', 'Tonga', 676),
(221, 'TT', 'Trinidad And Tobago', 1868),
(222, 'TN', 'Tunisia', 216),
(223, 'TR', 'Turkey', 90),
(224, 'TM', 'Turkmenistan', 7370),
(225, 'TC', 'Turks And Caicos Islands', 1649),
(226, 'TV', 'Tuvalu', 688),
(227, 'UG', 'Uganda', 256),
(228, 'UA', 'Ukraine', 380),
(229, 'AE', 'United Arab Emirates', 971),
(230, 'GB', 'United Kingdom', 44),
(231, 'US', 'United States', 1),
(232, 'UM', 'United States Minor Outlying Islands', 1),
(233, 'UY', 'Uruguay', 598),
(234, 'UZ', 'Uzbekistan', 998),
(235, 'VU', 'Vanuatu', 678),
(236, 'VA', 'Vatican City State (Holy See)', 39),
(237, 'VE', 'Venezuela', 58),
(238, 'VN', 'Vietnam', 84),
(239, 'VG', 'Virgin Islands (British)', 1284),
(240, 'VI', 'Virgin Islands (US)', 1340),
(241, 'WF', 'Wallis And Futuna Islands', 681),
(242, 'EH', 'Western Sahara', 212),
(243, 'YE', 'Yemen', 967),
(244, 'YU', 'Yugoslavia', 38),
(245, 'ZM', 'Zambia', 260),
(246, 'ZW', 'Zimbabwe', 263);

-- --------------------------------------------------------

--
-- Table structure for table `group_commission`
--

CREATE TABLE `group_commission` (
  `id` int(255) NOT NULL,
  `commission` decimal(30,2) NOT NULL,
  `level` int(3) DEFAULT NULL,
  `count` int(3) DEFAULT NULL COMMENT 'This is for number of referrals that the user has referred and has purchased a product',
  `description` varchar(5000) DEFAULT NULL,
  `transaction_type_id` int(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `group_commission`
--

INSERT INTO `group_commission` (`id`, `commission`, `level`, `count`, `description`, `transaction_type_id`, `date_created`, `date_updated`) VALUES
(1, '8.00', 1, NULL, 'in percent', 2, '2019-07-25 09:17:37', '2019-07-25 09:25:28'),
(2, '8.00', 2, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-08-08 06:07:13'),
(3, '6.00', 3, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-08-08 06:07:15'),
(4, '6.00', 4, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-08-08 06:07:17'),
(5, '4.00', 5, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-08-08 06:07:19'),
(6, '4.00', 6, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-08-08 06:07:20'),
(7, '2.00', 7, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-08-08 06:07:31'),
(8, '2.00', 8, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-08-08 06:07:33'),
(9, '1.00', 9, NULL, 'in percent', 2, '2019-07-25 09:18:43', '2019-07-25 09:25:36'),
(10, '5000.00', NULL, 1, 'in points (RM1 = 100).first referral.', 1, '2019-07-25 09:50:48', '2019-07-25 09:50:58'),
(11, '10000.00', NULL, 2, 'in points (RM1 = 100).second referral.', 1, '2019-07-25 09:53:00', '2019-07-25 09:53:36'),
(12, '15000.00', NULL, 3, 'in points (RM1 = 100).third referral.', 1, '2019-07-25 09:53:00', '2019-07-25 09:53:39'),
(13, '5000.00', NULL, 0, 'in points (RM1 = 100).subsequent referral.', 1, '2019-07-25 09:53:00', '2019-07-25 09:53:43'),
(14, '1.00', 10, NULL, 'in percent', 2, '2019-08-08 06:06:46', '2019-08-08 06:06:46');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `pid` int(255) NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `uploaded` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`pid`, `filename`, `uploaded`, `status`) VALUES
(124, 'messi1568963506.jpg', '2019-09-20 07:11:46', '1'),
(125, 'Martial1569385433.jpg', '2019-09-25 04:23:53', '1');

-- --------------------------------------------------------

--
-- Table structure for table `money_type`
--

CREATE TABLE `money_type` (
  `id` int(255) NOT NULL,
  `name` varchar(2000) NOT NULL,
  `description` varchar(10000) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `money_type`
--

INSERT INTO `money_type` (`id`, `name`, `description`, `date_created`, `date_updated`) VALUES
(1, 'user_real', 'this is user\'s real money. can withdraw', '2019-07-25 06:09:00', '2019-07-25 06:09:00'),
(2, 'voucher', 'this money is gotten from certain commissions. cannot withdraw', '2019-07-25 06:09:00', '2019-07-25 06:09:00');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `username` varchar(200) DEFAULT NULL COMMENT 'account that login by user to make order',
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_holder` varchar(255) DEFAULT NULL,
  `bank_account_no` varchar(255) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL COMMENT 'person to receive the product for delivery',
  `contactNo` varchar(20) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `address_line_1` varchar(2500) DEFAULT NULL,
  `address_line_2` varchar(2500) DEFAULT NULL,
  `address_line_3` varchar(2500) DEFAULT NULL,
  `city` varchar(500) DEFAULT NULL,
  `zipcode` varchar(100) DEFAULT NULL,
  `state` varchar(500) DEFAULT NULL,
  `country` varchar(500) DEFAULT NULL,
  `subtotal` decimal(50,0) DEFAULT NULL,
  `total` decimal(50,0) DEFAULT NULL COMMENT 'include postage fees',
  `payment_status` varchar(200) DEFAULT NULL COMMENT 'pending, accepted/completed, rejected, NULL = nothing',
  `shipping_status` varchar(200) DEFAULT NULL COMMENT 'pending, shipped, refunded',
  `shipping_method` varchar(200) DEFAULT NULL,
  `shipping_date` varchar(20) DEFAULT NULL,
  `tracking_number` varchar(200) DEFAULT NULL,
  `reject_reason` varchar(255) DEFAULT NULL,
  `refund_method` varchar(255) DEFAULT NULL,
  `refund_amount` int(20) DEFAULT NULL,
  `refund_note` varchar(255) DEFAULT NULL,
  `refund_reason` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `uid`, `username`, `bank_name`, `bank_account_holder`, `bank_account_no`, `name`, `contactNo`, `email`, `address_line_1`, `address_line_2`, `address_line_3`, `city`, `zipcode`, `state`, `country`, `subtotal`, `total`, `payment_status`, `shipping_status`, `shipping_method`, `shipping_date`, `tracking_number`, `reject_reason`, `refund_method`, `refund_amount`, `refund_note`, `refund_reason`, `date_created`, `date_updated`) VALUES
(99, 'b5b01c5215ba0928ddd52498cd7f412a', 'Martial', '-', NULL, NULL, 'asd', '123123', 'asd@gmail.com', 'asd', 'asd', NULL, 'asd', '11111', 'asd', 'asd', '1800', '1830', 'accepted', 'SHIPPED', 'POSLAJU', '2019-10-22', 'AA112S3GH6NBB', NULL, NULL, NULL, NULL, NULL, '2019-09-25 08:29:47', '2019-10-03 04:42:42'),
(100, '29ba05a1b287154031c3418dda40e78e', 'Jones', NULL, NULL, NULL, 'KC Ng', '044077230', 'kcng0731@gmail.com', '66 Main Road', 'Jln Merbau', NULL, 'Serdang', '09800', 'Kedah', 'Malaysia', '900', '930', 'accepted', 'SHIPPED', 'POSLAJU', '2019-07-14', '9267KCNG', NULL, NULL, NULL, NULL, NULL, '2019-09-25 08:34:00', '2019-10-01 04:00:56'),
(101, '2d5079e0beb3b491381ffbb7981e8949', 'messi', NULL, NULL, NULL, 'leo messi', '10101010', 'messi10@gmail.com', 'Camp Nou', 'C. d&#039;Arístides Maillol', NULL, '12', '08028', 'Barca', 'Spain', '3000', '3030', 'accepted', 'PENDING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-25 08:35:48', '2019-10-01 04:47:41'),
(102, 'f515487f84db8587f4753e4604fd771a', 'test', NULL, NULL, NULL, 'Neymar', '10101010', 'neymar10.psg@gmail.com', '24', 'Rue du Commandant', NULL, 'Guilbaud', '75016', 'Paris', 'France', '2400', '2430', 'accepted', 'REJECT', '-', '2019-10-16', '-', 'Suspicious User', NULL, NULL, NULL, NULL, '2019-09-25 08:37:39', '2019-10-01 04:57:00'),
(103, 'c5d5c2a57107b6b59cd1a52a7ae3236f', 'ddddd', NULL, NULL, NULL, 'Florentino Perez', '07017111', 'perez.rm@gmail.com', 'Santiago Bernabéu Stadium', 'Av. de Concha Espina', NULL, '1', '28036', 'Madrid', 'Spain', '4500', '4530', 'accepted', 'PENDING', 'SKYNET', '2019-09-30', 'CR7LM10KM07', NULL, NULL, NULL, NULL, NULL, '2019-09-25 08:43:08', '2019-10-01 04:01:10'),
(104, 'c5d5c2a57107b6b59cd1a52a7ae3236f', 'ddddd', NULL, NULL, NULL, 'Florentino Perez', '07017111', 'perez.rm@gmail.com', 'Santiago Bernabéu Stadium', 'Av. de Concha Espina', NULL, '1', '28036', 'Madrid', 'Spain', '4500', '4530', 'accepted', 'PENDING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-25 08:47:02', '2019-10-01 04:01:13'),
(105, '03540dfb2c6ebd27df970032eed9dfcf', 'qwert', NULL, NULL, NULL, 'asd', '12345678', 'asd123@gmail.com', 'asd123', 'asd456', NULL, 'asdaa', '123456', 'asd bb', 'asdasd cc', '1800', '1830', 'accepted', 'PENDING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-25 08:53:05', '2019-10-02 07:25:20'),
(108, '3c73247267883b0f3c0866edd6f3621d', 'Henderson', 'HONG LEONG BANK BERHAD', 'Jor Henderson', '14140808', 'hhhhhh', '14141414', 'hhhhhh@gmail.com', 'hhhhhh', 'hhhhhh', NULL, 'hhhhhh', '141414', 'hhhhhh', 'hhhhhh', '1200', '1230', 'accepted', 'PENDING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-10-02 04:23:57', '2019-10-02 07:42:20');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL,
  `name` varchar(5000) NOT NULL,
  `price` decimal(50,0) NOT NULL COMMENT 'With MYR as base and is in point system (RM1 = 100 point)',
  `type` int(255) NOT NULL DEFAULT 1 COMMENT '1 = normal product',
  `description` varchar(10000) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `price`, `type`, `description`, `date_created`, `date_updated`) VALUES
(1, 'oilxag', '300', 1, 'Main product', '2019-07-24 09:17:04', '2019-09-20 09:31:42'),
(2, '2nd product', '300', 1, 'very good product', '2019-07-25 04:11:21', '2019-09-20 09:31:47');

-- --------------------------------------------------------

--
-- Table structure for table `product_orders`
--

CREATE TABLE `product_orders` (
  `id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `order_id` bigint(20) NOT NULL,
  `quantity` int(10) NOT NULL,
  `final_price` decimal(50,0) NOT NULL COMMENT 'In points',
  `original_price` decimal(50,0) NOT NULL COMMENT 'In points',
  `discount_given` decimal(50,0) NOT NULL COMMENT 'in points',
  `totalProductPrice` decimal(50,0) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_orders`
--

INSERT INTO `product_orders` (`id`, `product_id`, `order_id`, `quantity`, `final_price`, `original_price`, `discount_given`, `totalProductPrice`, `date_created`, `date_updated`) VALUES
(1, 1, 99, 2, '300', '300', '0', '600', '2019-09-25 08:29:47', '2019-09-25 08:29:47'),
(2, 2, 99, 4, '300', '300', '0', '1200', '2019-09-25 08:29:47', '2019-09-25 08:29:47'),
(3, 1, 100, 1, '300', '300', '0', '300', '2019-09-25 08:34:00', '2019-09-25 08:34:00'),
(4, 2, 100, 2, '300', '300', '0', '600', '2019-09-25 08:34:00', '2019-09-25 08:34:00'),
(5, 1, 101, 10, '300', '300', '0', '3000', '2019-09-25 08:35:48', '2019-09-25 08:35:48'),
(6, 2, 102, 8, '300', '300', '0', '2400', '2019-09-25 08:37:39', '2019-09-25 08:37:39'),
(7, 1, 103, 2, '300', '300', '0', '600', '2019-09-25 08:43:08', '2019-09-25 08:43:08'),
(8, 2, 103, 5, '300', '300', '0', '1500', '2019-09-25 08:43:08', '2019-09-25 08:43:08'),
(9, 1, 104, 5, '300', '300', '0', '1500', '2019-09-25 08:47:02', '2019-09-25 08:47:02'),
(10, 2, 104, 10, '300', '300', '0', '3000', '2019-09-25 08:47:02', '2019-09-25 08:47:02'),
(11, 1, 105, 2, '300', '300', '0', '600', '2019-09-25 08:53:05', '2019-09-25 08:53:05'),
(12, 2, 105, 4, '300', '300', '0', '1200', '2019-09-25 08:53:05', '2019-09-25 08:53:05'),
(17, 1, 108, 2, '300', '300', '0', '600', '2019-10-02 04:23:57', '2019-10-02 04:23:57'),
(18, 2, 108, 2, '300', '300', '0', '600', '2019-10-02 04:23:57', '2019-10-02 04:23:57');

-- --------------------------------------------------------

--
-- Table structure for table `referral_history`
--

CREATE TABLE `referral_history` (
  `id` bigint(20) NOT NULL,
  `referrer_id` varchar(255) NOT NULL COMMENT 'the uid of the person that intro this user',
  `referral_id` varchar(255) NOT NULL COMMENT 'the uid of the person that gets invited to join this platform',
  `current_level` int(100) NOT NULL,
  `top_referrer_id` varchar(100) NOT NULL COMMENT 'the topmost person''s uid',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `referral_history`
--

INSERT INTO `referral_history` (`id`, `referrer_id`, `referral_id`, `current_level`, `top_referrer_id`, `date_created`, `date_updated`) VALUES
(2, '065ae1f4ef8a2802608988aa1597fc0e', '03540dfb2c6ebd27df970032eed9dfcf', 1, '065ae1f4ef8a2802608988aa1597fc0e', '2019-08-01 07:42:35', '2019-08-01 07:42:35'),
(3, '03540dfb2c6ebd27df970032eed9dfcf', '542194c89dbd7893fb519164e3db0646', 2, '065ae1f4ef8a2802608988aa1597fc0e', '2019-08-01 07:45:09', '2019-08-01 07:45:09'),
(4, 'b5b01c5215ba0928ddd52498cd7f412a', '29ba05a1b287154031c3418dda40e78e', 1, 'b5b01c5215ba0928ddd52498cd7f412a', '2019-08-02 01:51:03', '2019-08-02 01:51:03'),
(5, '29ba05a1b287154031c3418dda40e78e', '2d5079e0beb3b491381ffbb7981e8949', 2, 'b5b01c5215ba0928ddd52498cd7f412a', '2019-08-02 01:51:46', '2019-08-02 01:51:46'),
(6, '2d5079e0beb3b491381ffbb7981e8949', 'c5d5c2a57107b6b59cd1a52a7ae3236f', 3, 'b5b01c5215ba0928ddd52498cd7f412a', '2019-08-02 03:29:00', '2019-08-02 03:29:00'),
(7, '0b29538dc8b70629e7fa4780d2380b3e', '659d7d893bbdf14766a2d7092727957f', 1, '0b29538dc8b70629e7fa4780d2380b3e', '2019-08-07 06:13:21', '2019-08-07 06:13:21'),
(8, '659d7d893bbdf14766a2d7092727957f', 'f515487f84db8587f4753e4604fd771a', 2, '0b29538dc8b70629e7fa4780d2380b3e', '2019-08-07 06:43:01', '2019-08-07 06:43:01'),
(9, '659d7d893bbdf14766a2d7092727957f', '01d2a6635300bddde2aebfa667d1e9ac', 2, '0b29538dc8b70629e7fa4780d2380b3e', '2019-08-07 06:46:46', '2019-08-07 06:46:46'),
(10, '01d2a6635300bddde2aebfa667d1e9ac', '60b43f3f888096e7b7d78b5660b717bc', 3, '0b29538dc8b70629e7fa4780d2380b3e', '2019-08-07 07:15:26', '2019-08-07 07:15:26'),
(11, '3d5acf2423441ad6a4be7741a37932aa', 'fd18baa3163ea94767c1b8216d511c32', 1, '3d5acf2423441ad6a4be7741a37932aa', '2019-08-07 09:21:07', '2019-08-07 09:21:07'),
(12, '9bc6adce5b0738754f1d3bb8f234a7ae', 'd6ae09e2d7ddba5c4f3407296061d2d6', 1, '9bc6adce5b0738754f1d3bb8f234a7ae', '2019-08-08 09:49:11', '2019-08-08 09:49:11'),
(13, '3d5acf2423441ad6a4be7741a37932aa', '807ddd8f7aef7e48568cf1dde27e2adb', 1, '3d5acf2423441ad6a4be7741a37932aa', '2019-08-08 10:09:22', '2019-08-08 10:09:22'),
(14, 'd6ae09e2d7ddba5c4f3407296061d2d6', '5d8adfd03fb6994f3ed819d75fcecdc7', 2, '9bc6adce5b0738754f1d3bb8f234a7ae', '2019-08-09 12:06:41', '2019-08-09 12:06:41');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_history`
--

CREATE TABLE `transaction_history` (
  `id` bigint(20) NOT NULL,
  `money_in` decimal(50,0) NOT NULL DEFAULT 0 COMMENT 'in points',
  `money_out` decimal(50,0) NOT NULL DEFAULT 0 COMMENT 'in points',
  `uid` varchar(255) NOT NULL,
  `target_uid` varchar(255) DEFAULT NULL COMMENT 'this is the uid for the targeted user. it can be a downline''s uid because upline can get commissions from his downline. therefore this target_uid shall be downline''s uid while the uid field above will be upline''s uid',
  `percentage` decimal(5,2) DEFAULT NULL COMMENT 'The percentage assigned to this transaction. Usually for commissions use only',
  `original_value` decimal(50,0) DEFAULT NULL COMMENT '(in point)the original value that has been given before calculating the percentage. Usually for commissions use only',
  `status` int(3) DEFAULT NULL COMMENT '1 = pending, 2 = accepted/completed, 3 = rejected, NULL = nothing',
  `level` int(10) DEFAULT NULL COMMENT 'can be group sales commission given by a downline at a level',
  `order_id` bigint(20) DEFAULT NULL COMMENT 'refers to a particular order in order table and it refers to another table that lists out all the products and quantity purchased in that table with order_id as foreign key',
  `transaction_type_id` int(255) NOT NULL,
  `money_type_id` int(255) DEFAULT NULL,
  `source_transaction_id` bigint(20) DEFAULT NULL COMMENT 'this is referring to this table''s own id. Might be needed when some previous transaction is triggering new transactions',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_type`
--

CREATE TABLE `transaction_type` (
  `id` int(255) NOT NULL,
  `name` varchar(2000) NOT NULL,
  `description` varchar(10000) DEFAULT NULL,
  `percentage` decimal(5,2) DEFAULT NULL COMMENT 'Percentage of the transaction, for example if transaction is withdrawal type, user need give 0.5% of total',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transaction_type`
--

INSERT INTO `transaction_type` (`id`, `name`, `description`, `percentage`, `date_created`, `date_updated`) VALUES
(1, 'referral', 'user can get commission by referring people and the referred person must at least buy a product for the referral to get this commission', NULL, '2019-07-25 08:46:07', '2019-07-25 08:46:07'),
(2, 'group_sales', 'This is when the user\'s downline bought a product, he will get a percentage from it. up to 9 levels with each level having different percentage. please refer to group_commission table', NULL, '2019-07-25 09:15:08', '2019-07-25 09:15:08'),
(3, 'buy_product', 'This is when user purchase a product', NULL, '2019-07-26 01:35:16', '2019-07-26 01:35:16'),
(4, 'transfer', 'point transfer between members', '0.25', '2019-07-31 02:46:22', '2019-07-31 02:46:22'),
(5, 'withdraw', 'withdraw money and transfer to bank', '0.50', '2019-07-31 02:46:22', '2019-07-31 02:46:22');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `uid` varchar(255) NOT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) NOT NULL COMMENT 'Can login with email too',
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `ic_no` varchar(200) DEFAULT NULL,
  `country_id` int(10) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `epin` char(64) DEFAULT NULL,
  `salt_epin` char(64) DEFAULT NULL,
  `email_verification_code` varchar(10) DEFAULT NULL,
  `is_email_verified` tinyint(1) NOT NULL DEFAULT 1,
  `is_phone_verified` tinyint(1) NOT NULL DEFAULT 0,
  `login_type` int(2) NOT NULL DEFAULT 1 COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `downline_accumulated_points` decimal(50,0) NOT NULL DEFAULT 0 COMMENT 'RM1 = 100 point',
  `can_send_newsletter` tinyint(1) NOT NULL DEFAULT 0,
  `is_referred` tinyint(1) NOT NULL DEFAULT 0,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `address` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_holder` varchar(255) DEFAULT NULL,
  `bank_account_no` int(255) DEFAULT NULL,
  `car_model` varchar(200) DEFAULT NULL,
  `car_year` int(20) DEFAULT NULL,
  `picture_id` int(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`uid`, `username`, `email`, `password`, `salt`, `phone_no`, `ic_no`, `country_id`, `full_name`, `epin`, `salt_epin`, `email_verification_code`, `is_email_verified`, `is_phone_verified`, `login_type`, `user_type`, `downline_accumulated_points`, `can_send_newsletter`, `is_referred`, `date_created`, `date_updated`, `address`, `birthday`, `gender`, `bank_name`, `bank_account_holder`, `bank_account_no`, `car_model`, `car_year`, `picture_id`) VALUES
('01d2a6635300bddde2aebfa667d1e9ac', 'sss', 'nerosssarth@gmail.com', '043beb6f3b028d1e46d5b0710693e7ecf2de3130a1d1f851386d1d3cfeecef73', 'eb28d91c39090c5e0fa8c45337aba5332e9fd5e7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 1, '0', 0, 1, '2019-08-07 06:46:46', '2019-08-07 06:47:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('03540dfb2c6ebd27df970032eed9dfcf', 'qwert', 'qwert@gmail.com', '388f46620cbf75b3234e1141e90decc43f2cd8ef63b3a119133acba93eb15451', '31cb550939c623d4bf91dbf1ad0604059b4a5748', NULL, '941113075645', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-08-01 07:42:35', '2019-08-01 08:54:17', '1-12-7, Suntech@Penang Cybercity, Lintang Mayang Pasir 3,\r\n11950 Bayan Lepas, Penang', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('0641aa179e3fbae9d3605badf6c5e1e8', 'Maguire', 'wenjie195.vidatech@gmail.com', '6ca92a325fffe550111192c83969a6eebc187500dc533905bdfca3f404465e52', 'd983b2f0d47ab2a834740df8658710cf866c2369', '01159118132', '930305-05-1505', NULL, 'Jacob Harry Maguire', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 0, '2019-08-07 07:27:06', '2019-08-09 06:15:43', 'Carrington, Manchester, UK', '1993-03-05', 'Male', NULL, NULL, NULL, NULL, NULL, NULL),
('065ae1f4ef8a2802608988aa1597fc0e', 'test1', 'asd', '61005aef478d5b2ec3d6c8c13f5f98fcdfff5dc1a898217bb2681295d06c654a', '1a9206809b70a163b8e5ce3e427c9de00d01aed2', '123', '331', 118, 'test1 lalala', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 0, '2019-07-24 06:52:52', '2019-08-07 06:07:10', 'Suntech@Penang Cybercity, Lintang Mayang Pasir 3,\r\n11950 Bayan Lepas, Penang', '1999-06-04', 'Female', NULL, NULL, NULL, NULL, NULL, NULL),
('0b29538dc8b70629e7fa4780d2380b3e', 'hafeezdzeko374', 'hafeezdzekos374@gmail.com', '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 0, '2019-08-07 01:28:31', '2019-08-07 06:50:16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('182471b43d599fa093bc0030e410628b', '3', 'hello@a.com', 'd29ee8a321edc530bf33c540a239d57d45df59ef62e04dbed352ac687ff8ffc3', 'e89fd06c4050ac8b0770236e31e9c641e46cae79', '123', '941113075645', 60, NULL, NULL, NULL, NULL, 0, 0, 1, 1, '0', 0, 1, '2019-07-24 04:41:06', '2019-07-26 08:10:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('1bc0efc264f9c75a61482961c2dc782e', 'sherry', 'sherry.vidatech2@gmail.com', '3d5fde2375817adbb3593a2826358ad3c30091d159748413d94fac7ed8dbdbb6', '92f0b8711fed03dec16c92f5392b391dfb3fa563', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 1, '0', 0, 0, '2019-08-07 09:13:49', '2019-08-07 09:13:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('1dd28d5e5f05e3d3db5d9eda9a58090a', 'asdzxc', 'asdasd@gmail.com', '2d75d14b4b801716d0b3861dd37c0fa0ea2e1c13d81cc661b75144d7ebeca5ba', '18574f11ffae890461273d587141ee02909df0be', '242132323', NULL, NULL, 'asdzxc', NULL, NULL, NULL, 1, 0, 1, 0, '0', 0, 0, '2019-09-30 08:45:09', '2019-09-30 08:45:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('29ba05a1b287154031c3418dda40e78e', 'Jones', 'jones04.mu@gmail.com', '6ed91d989ded48a9edbe8314c6574eb2decc4dc308dc09b6de0015d28d2c2539', 'ffcd5302b0417ede7e8f757c911273ed10a30ab8', '04041616', '04041212', NULL, 'Phil Jones', 'af3fa4eb9c634e5dd72b3325ac3bbd1425d3e486335e757a1c1a801b357ed54c', 'd323c821940125cd59b72d4983cd7ae68dacc9db', NULL, 1, 0, 1, 1, '0', 0, 1, '2019-08-02 01:51:03', '2019-10-04 02:25:20', 'Sir Matt Busby Way Old Trafford Greater Manchester England', '1992-02-21', 'Male', NULL, NULL, NULL, 'Nissan Almera', 2015, NULL),
('2d5079e0beb3b491381ffbb7981e8949', 'messi', 'messi10.barca@gmail.com', '2b562421990e4f77241f429477bbe1d0645a311a789744222ecacfe9d4dad284', 'f36b7264eae94728ca766745ef1dbd7b8b527972', '10101010', '690430075441', NULL, 'Lionel Andres Messi', 'f9af4a00c15e60e9d0fb3e516364fd6d600d51ca87298104f92dd2c7172bad0b', 'daa53ec862ef20cdd5c66494b80f0ef3d8d12051', NULL, 1, 0, 1, 1, '0', 0, 1, '2019-08-02 01:51:46', '2019-10-04 02:44:46', 'Catalonia Esp, Jln Rosario, Argentina.', '1987-06-27', 'Male', NULL, NULL, NULL, 'Audi A8 D5', 2017, 124),
('30ae37a1b18ec80d1ea411b55ebe663f', '4', 'fhusajs@asddsa.com', 'b2f1a8802fd2924559cca515d991a96f3a7275a185183a28ef59fc1c3576b7ba', 'dd6ab7e6c4585411c4e99cbed4bf657bfd2dc4b3', '164763391', '941113075645', 60, NULL, NULL, NULL, NULL, 0, 0, 1, 1, '0', 0, 0, '2019-07-24 06:42:12', '2019-07-26 08:10:19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('34009ee113eece40072ffbf0b204c279', '5', '1212@sad.com', '9cb035dd0ee1215911ad1d39638453a0fb2b8ede05a1c56663b5bcaf4fedae97', '16d12fb6fcf98eb524e4716f6f6265f03cac8fcb', '164763391', '941113075645', 114, NULL, NULL, NULL, NULL, 0, 0, 1, 1, '0', 0, 0, '2019-07-24 06:50:31', '2019-07-26 08:10:20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('3c73247267883b0f3c0866edd6f3621d', 'Henderson', 'henderson.liv14@gmail.com', '12f271c7ce75ce7e27e22944326c216f9c49fd33cf6ae79c9b1a70f1f1382049', 'bdd0b4c1218f9aafef4efaa9b76e8c539c700054', '14140808', NULL, NULL, 'Jordan Henderson', NULL, NULL, NULL, 1, 0, 1, 0, '0', 0, 0, '2019-09-30 04:24:48', '2019-10-02 04:23:18', NULL, NULL, 'Female', 'HONG LEONG BANK BERHAD', 'Jor Henderson', 14140808, NULL, NULL, NULL),
('3d5acf2423441ad6a4be7741a37932aa', 'Michaelwong', 'michaelwong.vidatech@gmail.com', '353df3f801fb217c2bbaa80100f1192d772557f783300e3d35772ea2b01af6c1', '57a8e59099d91c613f8b2a53569398ce3c16aca8', '0104767878', '910506055353', NULL, 'Michael wong', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 0, '2019-08-07 08:56:48', '2019-08-13 06:31:45', NULL, '1991-05-06', 'Male', NULL, NULL, NULL, NULL, NULL, NULL),
('53992b0ee7b4fec44b87578baef6f3c5', '1', '1212@sad.com12', 'bed8413fc564c1c62d92febca9c8a72631c5005aaa57a5dc11c4094711af1978', '9ba6d5b45051d7b15d25ec465a29fcd2d372153e', '164763391', '941113075645', 132, NULL, NULL, NULL, NULL, 0, 0, 1, 1, '0', 0, 0, '2019-07-24 06:51:40', '2019-07-26 08:10:16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('542194c89dbd7893fb519164e3db0646', 'asd', 'asd@gmail.com', 'a240ae4fc83ca89dc6b4304e45d6006607e22fc2574028c8463d946b3dec6501', '90bf6df43d09131ddde43b55d46a378fa2f23d33', NULL, '941113075645', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-08-01 07:45:09', '2019-08-15 09:31:42', '1-15-12B, Suntech@Penang Cybercity, Lintang Mayang Pasir 3,\r\n11950 Bayan Lepas, Penang', '1994-08-15', 'Female', NULL, NULL, NULL, NULL, NULL, NULL),
('5d8adfd03fb6994f3ed819d75fcecdc7', 'cklim', 'cklim5155@gmail.com', '7aa88f175ce78413631cd2dd0fca2319ae3c0f6ba94132ceee6154043cef2e20', '81e85205b49a30c2944d12f23ec2b74c746a5cc8', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 1, '0', 0, 1, '2019-08-09 12:06:41', '2019-08-09 12:06:41', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('60b43f3f888096e7b7d78b5660b717bc', 'fuckLevel3', 'neroarth@hotmail.com', '5e5e14189b532793ac473842829c3c6cbbfec0fda8287ccd10f90ee04156f7ba', '65f3d2bb8cb9c2bee4f17b0367fbf71a80cb8608', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-08-07 07:15:26', '2019-08-07 07:22:41', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('659d7d893bbdf14766a2d7092727957f', '555', '123@sasda.com', '737e636a7d1a0ef194324b31614a8d6cb28d986a3b5016507244803b81072daf', '4a30463bd1c098868dd2d3dc4e8f0c45b9195704', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 1, '0', 0, 1, '2019-08-07 06:13:21', '2019-08-07 06:42:56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('807ddd8f7aef7e48568cf1dde27e2adb', 'mikewong', 'likkit1990@hotmail.com', '5860c994c8123790d7cdaf39dffae1d7b06b22e5534ff24801910a4195092943', 'ab9af7b364e5309164c7cf71ba3bdcc0b4547f12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-08-08 10:09:22', '2019-08-08 10:10:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('9bc6adce5b0738754f1d3bb8f234a7ae', 'kevin168', 'kevinyam.vidatech@gmail.com', 'f636642a8cb24cbc488b52736200bd02b419d917c4a97a88784572160e96c4a1', '935437934aeeb5e5ab70b99944c501717ec3a1b3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 0, '2019-08-07 11:51:38', '2019-08-08 09:52:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('a92e1ecc53a0d93e0976d957e20ec638', 'hafeezsaaulanwar', 'hafeezdzekdo374@gmail.com', '4b0d02ed55a11332947fb8e30987afbed918ef7469ac63bdaecbad4065e9d760', '299342bebad61e448e9d737661c7732064d40047', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 0, '2019-08-07 06:50:34', '2019-08-07 07:11:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('b33b5059d229373855d81021121e96b1', '2', '111@ss.com12', 'dd8b79ed97dac0d460746435f74ce0fa1ad795d3c7763c85631fabf136b9acc5', '1281094f26e98ba86e789c0766a9abb121b8b8dc', '333', '941113075645', 60, NULL, NULL, NULL, NULL, 0, 0, 1, 1, '0', 0, 1, '2019-07-24 04:27:08', '2019-07-26 08:10:17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('b5b01c5215ba0928ddd52498cd7f412a', 'Martial', 'martial09.mu@gmail.com', '5d32ad785e5b164be79e4c5a6185fbe098100354c69df53a61e9bd4fc8872398', '551ef0d7d7c951969cdbd4d4bb72ddcdfbc3af1f', '09091111', '09091111', NULL, 'Anthony Martial', NULL, NULL, NULL, 1, 0, 1, 0, '0', 0, 0, '2019-08-02 01:50:18', '2019-10-02 03:40:08', 'Sir Matt Busby Way Old Trafford Greater Manchester England', '1995-12-05', 'Male', '-', NULL, NULL, 'Audi A8 D5', 2016, 125),
('bb7a401ae602b078eb2a45bd423b8261', 'sherry2', 'sherry2.vidatech@gmail.com', 'cedd934ccea58248ee9209e227bebc9d66298bfb86ad4b40a9740ea7a546a24d', 'bead5793e783dba74b61de8970631326faf94631', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 0, '2019-08-08 10:09:19', '2019-08-08 10:09:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('bd3fc856974714748b8cb9713028f102', 'hafeezulanwar', 'hafeezdzeko374@gmail.com', '4c30be6adf76ffe5ffc7fa9f93362d9d66f02f32686847d8470bf45ec2b872e1', 'e3c48babed39baa97ba7c64e368d6932075faf6e', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 0, '2019-08-07 07:12:05', '2019-08-08 08:04:57', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('c1970a119d85f4a37553a952a7468ece', '6', '1212@sad.com1', 'e0224de870a936949e4ccf467cd3775ecc4bd729ec1cb7d4ccca538e6a13faaa', '22dbed079213dccec3ca32834aee40c827ce3cbe', '164763391', '941113075645', 3, NULL, NULL, NULL, NULL, 0, 0, 1, 1, '0', 0, 0, '2019-07-24 06:51:03', '2019-07-26 08:10:21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('c5d5c2a57107b6b59cd1a52a7ae3236f', 'ddddd', 'ddddd@gmail.com', '8e48b9f24951a51887603b4eb1cb75ffa17cd1a18ba7ce4cbc79ba938a9c565f', 'ebce501efdb9e2a14a719c072b0868069a0556bf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-08-02 03:29:00', '2019-10-02 03:48:15', NULL, NULL, 'Female', 'CIMB BANK BERHAD', 'ddd xxx ddd', 2147483647, NULL, NULL, NULL),
('d38a48ff9066f63836f43b468589177f', 'hikari', 'sherry.vidatech@gmail.com', '19978063f65278da14070adc26f0db37bda67f556628cab7d43f376dbcf4344e', 'f63b5d60dacabe624b935e18122569ce133e109e', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 0, '2019-08-09 03:17:11', '2019-08-09 03:17:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('d6ae09e2d7ddba5c4f3407296061d2d6', 'David Chua', 'chuacts@gmail.com', '04d895fab4c13d74b408a072b84dc691507af4735644015c0976fdc6b2942e74', '89de8f68626ec754b3dbb6cccf4c693b63afe4ef', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 1, '0', 0, 1, '2019-08-08 09:49:11', '2019-08-08 09:49:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('de7234d1e7e603315448caf956796c78', 'qqq', 'neroarth@gmail.com', 'b9184abb916851e16ba412e487ea9a98f270e7fe524130d1c4d4913bb18e856e', '2864d0a0a963c9d1067ce90c5386fa4db0484964', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 0, '2019-08-07 06:47:59', '2019-08-07 06:51:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('f515487f84db8587f4753e4604fd771a', 'test', 'nerosarth@gmail.com', 'b393322cbbd8735153a54e4243535e1014c12ca6665a1ca2786e9a023c2cca4b', '1e13621afcee13ac593fea52f684c55c315fcf68', NULL, NULL, NULL, 'test', NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-08-07 06:43:01', '2019-09-25 04:10:16', NULL, NULL, 'Male', NULL, NULL, NULL, NULL, NULL, NULL),
('fd18baa3163ea94767c1b8216d511c32', 'likkitwong', 'likkit1990@gmail.com', '68c8cc9a4bf75050851bf1361a4622bd0bea99e82f5707c5402cecf0e86bbe25', '4fb5c24efd49e1a1a3463bffc583a06330e93160', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 1, '0', 0, 1, '2019-08-07 09:21:07', '2019-08-07 09:21:41', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`announce_id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_commission`
--
ALTER TABLE `group_commission`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactionTypeIdGroupCommission_relateTo_transactionTypeId` (`transaction_type_id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `money_type`
--
ALTER TABLE `money_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_orders`
--
ALTER TABLE `product_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productIdTransactionHistory_relateTo_productId` (`product_id`),
  ADD KEY `orderIdProductOrders_relateTo_orderId` (`order_id`);

--
-- Indexes for table `referral_history`
--
ALTER TABLE `referral_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `referralIdReferralHistory_relateTo_userId` (`referral_id`),
  ADD KEY `referrerIdReferralHistory_relateTo_userId` (`referrer_id`),
  ADD KEY `topReferrerIdReferralHistory_relateTo_userId` (`top_referrer_id`);

--
-- Indexes for table `transaction_history`
--
ALTER TABLE `transaction_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uidTransactionHistory_relateTo_userId` (`uid`),
  ADD KEY `targetUidTransactionHistory_relateTo_userId` (`target_uid`),
  ADD KEY `transactionTypeIdTransactionHistory_relateTo_transactionTypeId` (`transaction_type_id`),
  ADD KEY `moneyTypeIdTransactionHistory_relateTo_moneyTypeId` (`money_type_id`),
  ADD KEY `sourceTransactionIdTransactionHistory_relateTo_self` (`source_transaction_id`),
  ADD KEY `orderIdTransactionHistory_relateTo_orderId` (`order_id`);

--
-- Indexes for table `transaction_type`
--
ALTER TABLE `transaction_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`uid`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `countryIdUser_relateTo_countryId` (`country_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `announce_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;

--
-- AUTO_INCREMENT for table `group_commission`
--
ALTER TABLE `group_commission`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `pid` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;

--
-- AUTO_INCREMENT for table `money_type`
--
ALTER TABLE `money_type`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9223372036854775807;

--
-- AUTO_INCREMENT for table `product_orders`
--
ALTER TABLE `product_orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `referral_history`
--
ALTER TABLE `referral_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `transaction_history`
--
ALTER TABLE `transaction_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transaction_type`
--
ALTER TABLE `transaction_type`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `group_commission`
--
ALTER TABLE `group_commission`
  ADD CONSTRAINT `transactionTypeIdGroupCommission_relateTo_transactionTypeId` FOREIGN KEY (`transaction_type_id`) REFERENCES `transaction_type` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `product_orders`
--
ALTER TABLE `product_orders`
  ADD CONSTRAINT `orderIdProductOrders_relateTo_orderId` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `productIdTransactionHistory_relateTo_productId` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `referral_history`
--
ALTER TABLE `referral_history`
  ADD CONSTRAINT `referralIdReferralHistory_relateTo_userId` FOREIGN KEY (`referral_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `referrerIdReferralHistory_relateTo_userId` FOREIGN KEY (`referrer_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `topReferrerIdReferralHistory_relateTo_userId` FOREIGN KEY (`top_referrer_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE;

--
-- Constraints for table `transaction_history`
--
ALTER TABLE `transaction_history`
  ADD CONSTRAINT `moneyTypeIdTransactionHistory_relateTo_moneyTypeId` FOREIGN KEY (`money_type_id`) REFERENCES `money_type` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `orderIdTransactionHistory_relateTo_orderId` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `sourceTransactionIdTransactionHistory_relateTo_self` FOREIGN KEY (`source_transaction_id`) REFERENCES `transaction_history` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `targetUidTransactionHistory_relateTo_userId` FOREIGN KEY (`target_uid`) REFERENCES `user` (`uid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `transactionTypeIdTransactionHistory_relateTo_transactionTypeId` FOREIGN KEY (`transaction_type_id`) REFERENCES `transaction_type` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `uidTransactionHistory_relateTo_userId` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`) ON UPDATE CASCADE;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `countryIdUser_relateTo_countryId` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
