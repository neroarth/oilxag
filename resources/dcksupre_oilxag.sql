-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 07, 2019 at 05:05 PM
-- Server version: 5.7.27-cll-lve
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dcksupre_oilxag`
--

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `sortname` varchar(3) NOT NULL,
  `name` varchar(150) NOT NULL,
  `phonecode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `sortname`, `name`, `phonecode`) VALUES
(1, 'AF', 'Afghanistan', 93),
(2, 'AL', 'Albania', 355),
(3, 'DZ', 'Algeria', 213),
(4, 'AS', 'American Samoa', 1684),
(5, 'AD', 'Andorra', 376),
(6, 'AO', 'Angola', 244),
(7, 'AI', 'Anguilla', 1264),
(8, 'AQ', 'Antarctica', 672),
(9, 'AG', 'Antigua And Barbuda', 1268),
(10, 'AR', 'Argentina', 54),
(11, 'AM', 'Armenia', 374),
(12, 'AW', 'Aruba', 297),
(13, 'AU', 'Australia', 61),
(14, 'AT', 'Austria', 43),
(15, 'AZ', 'Azerbaijan', 994),
(16, 'BS', 'Bahamas The', 1242),
(17, 'BH', 'Bahrain', 973),
(18, 'BD', 'Bangladesh', 880),
(19, 'BB', 'Barbados', 1246),
(20, 'BY', 'Belarus', 375),
(21, 'BE', 'Belgium', 32),
(22, 'BZ', 'Belize', 501),
(23, 'BJ', 'Benin', 229),
(24, 'BM', 'Bermuda', 1441),
(25, 'BT', 'Bhutan', 975),
(26, 'BO', 'Bolivia', 591),
(27, 'BA', 'Bosnia and Herzegovina', 387),
(28, 'BW', 'Botswana', 267),
(29, 'BV', 'Bouvet Island', 0),
(30, 'BR', 'Brazil', 55),
(31, 'IO', 'British Indian Ocean Territory', 246),
(32, 'BN', 'Brunei', 673),
(33, 'BG', 'Bulgaria', 359),
(34, 'BF', 'Burkina Faso', 226),
(35, 'BI', 'Burundi', 257),
(36, 'KH', 'Cambodia', 855),
(37, 'CM', 'Cameroon', 237),
(38, 'CA', 'Canada', 1),
(39, 'CV', 'Cape Verde', 238),
(40, 'KY', 'Cayman Islands', 1345),
(41, 'CF', 'Central African Republic', 236),
(42, 'TD', 'Chad', 235),
(43, 'CL', 'Chile', 56),
(44, 'CN', 'China', 86),
(45, 'CX', 'Christmas Island', 61),
(46, 'CC', 'Cocos (Keeling) Islands', 672),
(47, 'CO', 'Colombia', 57),
(48, 'KM', 'Comoros', 269),
(49, 'CG', 'Republic Of The Congo', 242),
(50, 'CD', 'Democratic Republic Of The Congo', 242),
(51, 'CK', 'Cook Islands', 682),
(52, 'CR', 'Costa Rica', 506),
(53, 'CI', 'Cote D\'Ivoire (Ivory Coast)', 225),
(54, 'HR', 'Croatia (Hrvatska)', 385),
(55, 'CU', 'Cuba', 53),
(56, 'CY', 'Cyprus', 357),
(57, 'CZ', 'Czech Republic', 420),
(58, 'DK', 'Denmark', 45),
(59, 'DJ', 'Djibouti', 253),
(60, 'DM', 'Dominica', 1767),
(61, 'DO', 'Dominican Republic', 1809),
(62, 'TP', 'East Timor', 670),
(63, 'EC', 'Ecuador', 593),
(64, 'EG', 'Egypt', 20),
(65, 'SV', 'El Salvador', 503),
(66, 'GQ', 'Equatorial Guinea', 240),
(67, 'ER', 'Eritrea', 291),
(68, 'EE', 'Estonia', 372),
(69, 'ET', 'Ethiopia', 251),
(70, 'XA', 'External Territories of Australia', 61),
(71, 'FK', 'Falkland Islands', 500),
(72, 'FO', 'Faroe Islands', 298),
(73, 'FJ', 'Fiji Islands', 679),
(74, 'FI', 'Finland', 358),
(75, 'FR', 'France', 33),
(76, 'GF', 'French Guiana', 594),
(77, 'PF', 'French Polynesia', 689),
(78, 'TF', 'French Southern Territories', 0),
(79, 'GA', 'Gabon', 241),
(80, 'GM', 'Gambia The', 220),
(81, 'GE', 'Georgia', 995),
(82, 'DE', 'Germany', 49),
(83, 'GH', 'Ghana', 233),
(84, 'GI', 'Gibraltar', 350),
(85, 'GR', 'Greece', 30),
(86, 'GL', 'Greenland', 299),
(87, 'GD', 'Grenada', 1473),
(88, 'GP', 'Guadeloupe', 590),
(89, 'GU', 'Guam', 1671),
(90, 'GT', 'Guatemala', 502),
(91, 'XU', 'Guernsey and Alderney', 44),
(92, 'GN', 'Guinea', 224),
(93, 'GW', 'Guinea-Bissau', 245),
(94, 'GY', 'Guyana', 592),
(95, 'HT', 'Haiti', 509),
(96, 'HM', 'Heard and McDonald Islands', 0),
(97, 'HN', 'Honduras', 504),
(98, 'HK', 'Hong Kong S.A.R.', 852),
(99, 'HU', 'Hungary', 36),
(100, 'IS', 'Iceland', 354),
(101, 'IN', 'India', 91),
(102, 'ID', 'Indonesia', 62),
(103, 'IR', 'Iran', 98),
(104, 'IQ', 'Iraq', 964),
(105, 'IE', 'Ireland', 353),
(106, 'IL', 'Israel', 972),
(107, 'IT', 'Italy', 39),
(108, 'JM', 'Jamaica', 1876),
(109, 'JP', 'Japan', 81),
(110, 'XJ', 'Jersey', 44),
(111, 'JO', 'Jordan', 962),
(112, 'KZ', 'Kazakhstan', 7),
(113, 'KE', 'Kenya', 254),
(114, 'KI', 'Kiribati', 686),
(115, 'KP', 'Korea North', 850),
(116, 'KR', 'Korea South', 82),
(117, 'KW', 'Kuwait', 965),
(118, 'KG', 'Kyrgyzstan', 996),
(119, 'LA', 'Laos', 856),
(120, 'LV', 'Latvia', 371),
(121, 'LB', 'Lebanon', 961),
(122, 'LS', 'Lesotho', 266),
(123, 'LR', 'Liberia', 231),
(124, 'LY', 'Libya', 218),
(125, 'LI', 'Liechtenstein', 423),
(126, 'LT', 'Lithuania', 370),
(127, 'LU', 'Luxembourg', 352),
(128, 'MO', 'Macau S.A.R.', 853),
(129, 'MK', 'Macedonia', 389),
(130, 'MG', 'Madagascar', 261),
(131, 'MW', 'Malawi', 265),
(132, 'MY', 'Malaysia', 60),
(133, 'MV', 'Maldives', 960),
(134, 'ML', 'Mali', 223),
(135, 'MT', 'Malta', 356),
(136, 'XM', 'Man (Isle of)', 44),
(137, 'MH', 'Marshall Islands', 692),
(138, 'MQ', 'Martinique', 596),
(139, 'MR', 'Mauritania', 222),
(140, 'MU', 'Mauritius', 230),
(141, 'YT', 'Mayotte', 269),
(142, 'MX', 'Mexico', 52),
(143, 'FM', 'Micronesia', 691),
(144, 'MD', 'Moldova', 373),
(145, 'MC', 'Monaco', 377),
(146, 'MN', 'Mongolia', 976),
(147, 'MS', 'Montserrat', 1664),
(148, 'MA', 'Morocco', 212),
(149, 'MZ', 'Mozambique', 258),
(150, 'MM', 'Myanmar', 95),
(151, 'NA', 'Namibia', 264),
(152, 'NR', 'Nauru', 674),
(153, 'NP', 'Nepal', 977),
(154, 'AN', 'Netherlands Antilles', 599),
(155, 'NL', 'Netherlands The', 31),
(156, 'NC', 'New Caledonia', 687),
(157, 'NZ', 'New Zealand', 64),
(158, 'NI', 'Nicaragua', 505),
(159, 'NE', 'Niger', 227),
(160, 'NG', 'Nigeria', 234),
(161, 'NU', 'Niue', 683),
(162, 'NF', 'Norfolk Island', 672),
(163, 'MP', 'Northern Mariana Islands', 1670),
(164, 'NO', 'Norway', 47),
(165, 'OM', 'Oman', 968),
(166, 'PK', 'Pakistan', 92),
(167, 'PW', 'Palau', 680),
(168, 'PS', 'Palestinian Territory Occupied', 970),
(169, 'PA', 'Panama', 507),
(170, 'PG', 'Papua new Guinea', 675),
(171, 'PY', 'Paraguay', 595),
(172, 'PE', 'Peru', 51),
(173, 'PH', 'Philippines', 63),
(174, 'PN', 'Pitcairn Island', 0),
(175, 'PL', 'Poland', 48),
(176, 'PT', 'Portugal', 351),
(177, 'PR', 'Puerto Rico', 1787),
(178, 'QA', 'Qatar', 974),
(179, 'RE', 'Reunion', 262),
(180, 'RO', 'Romania', 40),
(181, 'RU', 'Russia', 70),
(182, 'RW', 'Rwanda', 250),
(183, 'SH', 'Saint Helena', 290),
(184, 'KN', 'Saint Kitts And Nevis', 1869),
(185, 'LC', 'Saint Lucia', 1758),
(186, 'PM', 'Saint Pierre and Miquelon', 508),
(187, 'VC', 'Saint Vincent And The Grenadines', 1784),
(188, 'WS', 'Samoa', 684),
(189, 'SM', 'San Marino', 378),
(190, 'ST', 'Sao Tome and Principe', 239),
(191, 'SA', 'Saudi Arabia', 966),
(192, 'SN', 'Senegal', 221),
(193, 'RS', 'Serbia', 381),
(194, 'SC', 'Seychelles', 248),
(195, 'SL', 'Sierra Leone', 232),
(196, 'SG', 'Singapore', 65),
(197, 'SK', 'Slovakia', 421),
(198, 'SI', 'Slovenia', 386),
(199, 'XG', 'Smaller Territories of the UK', 44),
(200, 'SB', 'Solomon Islands', 677),
(201, 'SO', 'Somalia', 252),
(202, 'ZA', 'South Africa', 27),
(203, 'GS', 'South Georgia', 0),
(204, 'SS', 'South Sudan', 211),
(205, 'ES', 'Spain', 34),
(206, 'LK', 'Sri Lanka', 94),
(207, 'SD', 'Sudan', 249),
(208, 'SR', 'Suriname', 597),
(209, 'SJ', 'Svalbard And Jan Mayen Islands', 47),
(210, 'SZ', 'Swaziland', 268),
(211, 'SE', 'Sweden', 46),
(212, 'CH', 'Switzerland', 41),
(213, 'SY', 'Syria', 963),
(214, 'TW', 'Taiwan', 886),
(215, 'TJ', 'Tajikistan', 992),
(216, 'TZ', 'Tanzania', 255),
(217, 'TH', 'Thailand', 66),
(218, 'TG', 'Togo', 228),
(219, 'TK', 'Tokelau', 690),
(220, 'TO', 'Tonga', 676),
(221, 'TT', 'Trinidad And Tobago', 1868),
(222, 'TN', 'Tunisia', 216),
(223, 'TR', 'Turkey', 90),
(224, 'TM', 'Turkmenistan', 7370),
(225, 'TC', 'Turks And Caicos Islands', 1649),
(226, 'TV', 'Tuvalu', 688),
(227, 'UG', 'Uganda', 256),
(228, 'UA', 'Ukraine', 380),
(229, 'AE', 'United Arab Emirates', 971),
(230, 'GB', 'United Kingdom', 44),
(231, 'US', 'United States', 1),
(232, 'UM', 'United States Minor Outlying Islands', 1),
(233, 'UY', 'Uruguay', 598),
(234, 'UZ', 'Uzbekistan', 998),
(235, 'VU', 'Vanuatu', 678),
(236, 'VA', 'Vatican City State (Holy See)', 39),
(237, 'VE', 'Venezuela', 58),
(238, 'VN', 'Vietnam', 84),
(239, 'VG', 'Virgin Islands (British)', 1284),
(240, 'VI', 'Virgin Islands (US)', 1340),
(241, 'WF', 'Wallis And Futuna Islands', 681),
(242, 'EH', 'Western Sahara', 212),
(243, 'YE', 'Yemen', 967),
(244, 'YU', 'Yugoslavia', 38),
(245, 'ZM', 'Zambia', 260),
(246, 'ZW', 'Zimbabwe', 263);

-- --------------------------------------------------------

--
-- Table structure for table `group_commission`
--

CREATE TABLE `group_commission` (
  `id` int(255) NOT NULL,
  `commission` decimal(30,2) NOT NULL,
  `level` int(3) DEFAULT NULL,
  `count` int(3) DEFAULT NULL COMMENT 'This is for number of referrals that the user has referred and has purchased a product',
  `description` varchar(5000) DEFAULT NULL,
  `transaction_type_id` int(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `group_commission`
--

INSERT INTO `group_commission` (`id`, `commission`, `level`, `count`, `description`, `transaction_type_id`, `date_created`, `date_updated`) VALUES
(1, 8.00, 1, NULL, 'in percent', 2, '2019-07-25 09:17:37', '2019-07-25 09:25:28'),
(2, 6.00, 2, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-07-25 09:25:32'),
(3, 5.00, 3, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-07-25 09:25:34'),
(4, 4.00, 4, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-07-25 09:25:42'),
(5, 3.00, 5, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-07-25 09:25:40'),
(6, 2.00, 6, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-07-25 09:25:39'),
(7, 1.00, 7, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-07-25 09:25:38'),
(8, 1.00, 8, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-07-25 09:25:36'),
(9, 1.00, 9, NULL, 'in percent', 2, '2019-07-25 09:18:43', '2019-07-25 09:25:36'),
(10, 5000.00, NULL, 1, 'in points (RM1 = 100).first referral.', 1, '2019-07-25 09:50:48', '2019-07-25 09:50:58'),
(11, 10000.00, NULL, 2, 'in points (RM1 = 100).second referral.', 1, '2019-07-25 09:53:00', '2019-07-25 09:53:36'),
(12, 15000.00, NULL, 3, 'in points (RM1 = 100).third referral.', 1, '2019-07-25 09:53:00', '2019-07-25 09:53:39'),
(13, 5000.00, NULL, 0, 'in points (RM1 = 100).subsequent referral.', 1, '2019-07-25 09:53:00', '2019-07-25 09:53:43');

-- --------------------------------------------------------

--
-- Table structure for table `money_type`
--

CREATE TABLE `money_type` (
  `id` int(255) NOT NULL,
  `name` varchar(2000) NOT NULL,
  `description` varchar(10000) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `money_type`
--

INSERT INTO `money_type` (`id`, `name`, `description`, `date_created`, `date_updated`) VALUES
(1, 'user_real', 'this is user\'s real money. can withdraw', '2019-07-25 06:09:00', '2019-07-25 06:09:00'),
(2, 'voucher', 'this money is gotten from certain commissions. cannot withdraw', '2019-07-25 06:09:00', '2019-07-25 06:09:00');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `address_line_1` varchar(2500) DEFAULT NULL,
  `address_line_2` varchar(2500) DEFAULT NULL,
  `address_line_3` varchar(2500) DEFAULT NULL,
  `city` varchar(500) DEFAULT NULL,
  `zipcode` varchar(100) DEFAULT NULL,
  `state` varchar(500) DEFAULT NULL,
  `country` varchar(500) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL,
  `name` varchar(5000) NOT NULL,
  `price` decimal(50,0) NOT NULL COMMENT 'With MYR as base and is in point system (RM1 = 100 point)',
  `type` int(255) NOT NULL DEFAULT '1' COMMENT '1 = normal product',
  `description` varchar(10000) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `price`, `type`, `description`, `date_created`, `date_updated`) VALUES
(1, 'oilxag', 30000, 1, 'Main product', '2019-07-24 09:17:04', '2019-07-24 09:17:04'),
(2, '2nd product', 100, 1, 'very good product', '2019-07-25 04:11:21', '2019-07-25 04:11:21');

-- --------------------------------------------------------

--
-- Table structure for table `product_orders`
--

CREATE TABLE `product_orders` (
  `id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `order_id` bigint(20) NOT NULL,
  `quantity` int(10) NOT NULL,
  `final_price` decimal(50,0) NOT NULL COMMENT 'In points',
  `original_price` decimal(50,0) NOT NULL COMMENT 'In points',
  `discount_given` decimal(50,0) NOT NULL COMMENT 'in points',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `referral_history`
--

CREATE TABLE `referral_history` (
  `id` bigint(20) NOT NULL,
  `referrer_id` varchar(255) NOT NULL COMMENT 'the uid of the person that intro this user',
  `referral_id` varchar(255) NOT NULL COMMENT 'the uid of the person that gets invited to join this platform',
  `current_level` int(100) NOT NULL,
  `top_referrer_id` varchar(100) NOT NULL COMMENT 'the topmost person''s uid',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `referral_history`
--

INSERT INTO `referral_history` (`id`, `referrer_id`, `referral_id`, `current_level`, `top_referrer_id`, `date_created`, `date_updated`) VALUES
(2, '065ae1f4ef8a2802608988aa1597fc0e', '03540dfb2c6ebd27df970032eed9dfcf', 1, '065ae1f4ef8a2802608988aa1597fc0e', '2019-08-01 07:42:35', '2019-08-01 07:42:35'),
(3, '03540dfb2c6ebd27df970032eed9dfcf', '542194c89dbd7893fb519164e3db0646', 2, '065ae1f4ef8a2802608988aa1597fc0e', '2019-08-01 07:45:09', '2019-08-01 07:45:09'),
(4, 'b5b01c5215ba0928ddd52498cd7f412a', '29ba05a1b287154031c3418dda40e78e', 1, 'b5b01c5215ba0928ddd52498cd7f412a', '2019-08-02 01:51:03', '2019-08-02 01:51:03'),
(5, '29ba05a1b287154031c3418dda40e78e', '2d5079e0beb3b491381ffbb7981e8949', 2, 'b5b01c5215ba0928ddd52498cd7f412a', '2019-08-02 01:51:46', '2019-08-02 01:51:46'),
(6, '2d5079e0beb3b491381ffbb7981e8949', 'c5d5c2a57107b6b59cd1a52a7ae3236f', 3, 'b5b01c5215ba0928ddd52498cd7f412a', '2019-08-02 03:29:00', '2019-08-02 03:29:00'),
(7, '0b29538dc8b70629e7fa4780d2380b3e', '659d7d893bbdf14766a2d7092727957f', 1, '0b29538dc8b70629e7fa4780d2380b3e', '2019-08-07 06:13:21', '2019-08-07 06:13:21'),
(8, '659d7d893bbdf14766a2d7092727957f', 'f515487f84db8587f4753e4604fd771a', 2, '0b29538dc8b70629e7fa4780d2380b3e', '2019-08-07 06:43:01', '2019-08-07 06:43:01'),
(9, '659d7d893bbdf14766a2d7092727957f', '01d2a6635300bddde2aebfa667d1e9ac', 2, '0b29538dc8b70629e7fa4780d2380b3e', '2019-08-07 06:46:46', '2019-08-07 06:46:46'),
(10, '01d2a6635300bddde2aebfa667d1e9ac', '60b43f3f888096e7b7d78b5660b717bc', 3, '0b29538dc8b70629e7fa4780d2380b3e', '2019-08-07 07:15:26', '2019-08-07 07:15:26');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_history`
--

CREATE TABLE `transaction_history` (
  `id` bigint(20) NOT NULL,
  `money_in` decimal(50,0) NOT NULL DEFAULT '0' COMMENT 'in points',
  `money_out` decimal(50,0) NOT NULL DEFAULT '0' COMMENT 'in points',
  `uid` varchar(255) NOT NULL,
  `target_uid` varchar(255) DEFAULT NULL COMMENT 'this is the uid for the targeted user. it can be a downline''s uid because upline can get commissions from his downline. therefore this target_uid shall be downline''s uid while the uid field above will be upline''s uid',
  `percentage` decimal(5,2) DEFAULT NULL COMMENT 'The percentage assigned to this transaction. Usually for commissions use only',
  `original_value` decimal(50,0) DEFAULT NULL COMMENT '(in point)the original value that has been given before calculating the percentage. Usually for commissions use only',
  `status` int(3) DEFAULT NULL COMMENT '1 = pending, 2 = accepted/completed, 3 = rejected, NULL = nothing',
  `level` int(10) DEFAULT NULL COMMENT 'can be group sales commission given by a downline at a level',
  `order_id` bigint(20) DEFAULT NULL COMMENT 'refers to a particular order in order table and it refers to another table that lists out all the products and quantity purchased in that table with order_id as foreign key',
  `transaction_type_id` int(255) NOT NULL,
  `money_type_id` int(255) DEFAULT NULL,
  `source_transaction_id` bigint(20) DEFAULT NULL COMMENT 'this is referring to this table''s own id. Might be needed when some previous transaction is triggering new transactions',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_type`
--

CREATE TABLE `transaction_type` (
  `id` int(255) NOT NULL,
  `name` varchar(2000) NOT NULL,
  `description` varchar(10000) DEFAULT NULL,
  `percentage` decimal(5,2) DEFAULT NULL COMMENT 'Percentage of the transaction, for example if transaction is withdrawal type, user need give 0.5% of total',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transaction_type`
--

INSERT INTO `transaction_type` (`id`, `name`, `description`, `percentage`, `date_created`, `date_updated`) VALUES
(1, 'referral', 'user can get commission by referring people and the referred person must at least buy a product for the referral to get this commission', NULL, '2019-07-25 08:46:07', '2019-07-25 08:46:07'),
(2, 'group_sales', 'This is when the user\'s downline bought a product, he will get a percentage from it. up to 9 levels with each level having different percentage. please refer to group_commission table', NULL, '2019-07-25 09:15:08', '2019-07-25 09:15:08'),
(3, 'buy_product', 'This is when user purchase a product', NULL, '2019-07-26 01:35:16', '2019-07-26 01:35:16'),
(4, 'transfer', 'point transfer between members', 0.25, '2019-07-31 02:46:22', '2019-07-31 02:46:22'),
(5, 'withdraw', 'withdraw money and transfer to bank', 0.50, '2019-07-31 02:46:22', '2019-07-31 02:46:22');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `uid` varchar(255) NOT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) NOT NULL COMMENT 'Can login with email too',
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `ic_no` varchar(200) DEFAULT NULL,
  `country_id` int(10) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `email_verification_code` varchar(10) DEFAULT NULL,
  `is_email_verified` tinyint(1) NOT NULL DEFAULT '0',
  `is_phone_verified` tinyint(1) NOT NULL DEFAULT '0',
  `login_type` int(2) NOT NULL DEFAULT '1' COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT '1' COMMENT '0 = admin, 1 = normal user',
  `downline_accumulated_points` decimal(50,0) NOT NULL DEFAULT '0' COMMENT 'RM1 = 100 point',
  `can_send_newsletter` tinyint(1) NOT NULL DEFAULT '0',
  `is_referred` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `address` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`uid`, `username`, `email`, `password`, `salt`, `phone_no`, `ic_no`, `country_id`, `full_name`, `email_verification_code`, `is_email_verified`, `is_phone_verified`, `login_type`, `user_type`, `downline_accumulated_points`, `can_send_newsletter`, `is_referred`, `date_created`, `date_updated`, `address`, `birthday`, `gender`) VALUES
('01d2a6635300bddde2aebfa667d1e9ac', 'sss', 'nerosssarth@gmail.com', '043beb6f3b028d1e46d5b0710693e7ecf2de3130a1d1f851386d1d3cfeecef73', 'eb28d91c39090c5e0fa8c45337aba5332e9fd5e7', NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 1, 0, 0, 1, '2019-08-07 06:46:46', '2019-08-07 06:47:36', NULL, NULL, NULL),
('03540dfb2c6ebd27df970032eed9dfcf', 'qwert', 'qwert@gmail.com', '388f46620cbf75b3234e1141e90decc43f2cd8ef63b3a119133acba93eb15451', '31cb550939c623d4bf91dbf1ad0604059b4a5748', NULL, '941113075645', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-08-01 07:42:35', '2019-08-01 08:54:17', '1-12-7, Suntech@Penang Cybercity, Lintang Mayang Pasir 3,\r\n11950 Bayan Lepas, Penang', NULL, NULL),
('0641aa179e3fbae9d3605badf6c5e1e8', 'tj', 'wenjie195.vidatech@gmail.com', '20cf14822338c39966dfe2c5198c0838ba4fb45d8dcef34054581e1c75496fa5', '6c1e12c5e8c185151cfe9e722ae3ac3885f0db1a', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 0, '2019-08-07 07:27:06', '2019-08-07 07:29:02', NULL, NULL, NULL),
('065ae1f4ef8a2802608988aa1597fc0e', 'test1', 'asd', '61005aef478d5b2ec3d6c8c13f5f98fcdfff5dc1a898217bb2681295d06c654a', '1a9206809b70a163b8e5ce3e427c9de00d01aed2', '123', '331', 118, 'test1 lalala', NULL, 1, 0, 1, 1, 0, 0, 0, '2019-07-24 06:52:52', '2019-08-07 06:07:10', 'Suntech@Penang Cybercity, Lintang Mayang Pasir 3,\r\n11950 Bayan Lepas, Penang', '1999-06-04', 'Female'),
('0b29538dc8b70629e7fa4780d2380b3e', 'hafeezdzeko374', 'hafeezdzekos374@gmail.com', '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 0, '2019-08-07 01:28:31', '2019-08-07 06:50:16', NULL, NULL, NULL),
('182471b43d599fa093bc0030e410628b', '3', 'hello@a.com', 'd29ee8a321edc530bf33c540a239d57d45df59ef62e04dbed352ac687ff8ffc3', 'e89fd06c4050ac8b0770236e31e9c641e46cae79', '123', '941113075645', 60, NULL, NULL, 0, 0, 1, 1, 0, 0, 1, '2019-07-24 04:41:06', '2019-07-26 08:10:18', NULL, NULL, NULL),
('29ba05a1b287154031c3418dda40e78e', 'bbbbb', 'bbbbb@gmail.com', 'f0e39d61ea6fb7d312cb262b8ce5e411edb35b5624edd6f2bca1a9b4d561679e', 'b7dd5af3322ff39b6dc88458849ef6933429c447', '22222', '222222', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-08-02 01:51:03', '2019-08-02 01:53:57', NULL, NULL, NULL),
('2d5079e0beb3b491381ffbb7981e8949', 'ccccc', 'ccccc@gmail.com', '2b562421990e4f77241f429477bbe1d0645a311a789744222ecacfe9d4dad284', 'f36b7264eae94728ca766745ef1dbd7b8b527972', '33333', '333333', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-08-02 01:51:46', '2019-08-02 01:54:05', NULL, NULL, NULL),
('30ae37a1b18ec80d1ea411b55ebe663f', '4', 'fhusajs@asddsa.com', 'b2f1a8802fd2924559cca515d991a96f3a7275a185183a28ef59fc1c3576b7ba', 'dd6ab7e6c4585411c4e99cbed4bf657bfd2dc4b3', '164763391', '941113075645', 60, NULL, NULL, 0, 0, 1, 1, 0, 0, 0, '2019-07-24 06:42:12', '2019-07-26 08:10:19', NULL, NULL, NULL),
('34009ee113eece40072ffbf0b204c279', '5', '1212@sad.com', '9cb035dd0ee1215911ad1d39638453a0fb2b8ede05a1c56663b5bcaf4fedae97', '16d12fb6fcf98eb524e4716f6f6265f03cac8fcb', '164763391', '941113075645', 114, NULL, NULL, 0, 0, 1, 1, 0, 0, 0, '2019-07-24 06:50:31', '2019-07-26 08:10:20', NULL, NULL, NULL),
('3d5acf2423441ad6a4be7741a37932aa', 'Michaelwong', 'michaelwong.vidatech@gmail.com', '35126f1557384eaf55edc963690b22814335dc882340b2997f70ebe68a01dd05', 'e04a014f869c13b947460904cbdcd321c4df432b', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 0, '2019-08-07 08:56:48', '2019-08-07 09:02:09', NULL, NULL, NULL),
('53992b0ee7b4fec44b87578baef6f3c5', '1', '1212@sad.com12', 'bed8413fc564c1c62d92febca9c8a72631c5005aaa57a5dc11c4094711af1978', '9ba6d5b45051d7b15d25ec465a29fcd2d372153e', '164763391', '941113075645', 132, NULL, NULL, 0, 0, 1, 1, 0, 0, 0, '2019-07-24 06:51:40', '2019-07-26 08:10:16', NULL, NULL, NULL),
('542194c89dbd7893fb519164e3db0646', 'asd', 'asd@gmail.com', 'a240ae4fc83ca89dc6b4304e45d6006607e22fc2574028c8463d946b3dec6501', '90bf6df43d09131ddde43b55d46a378fa2f23d33', NULL, '941113075645', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-08-01 07:45:09', '2019-08-06 10:01:06', '1-15-12B, Suntech@Penang Cybercity, Lintang Mayang Pasir 3,\r\n11950 Bayan Lepas, Penang', '1994-08-15', 'Unknown\r\n'),
('60b43f3f888096e7b7d78b5660b717bc', 'fuckLevel3', 'neroarth@hotmail.com', '5e5e14189b532793ac473842829c3c6cbbfec0fda8287ccd10f90ee04156f7ba', '65f3d2bb8cb9c2bee4f17b0367fbf71a80cb8608', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-08-07 07:15:26', '2019-08-07 07:22:41', NULL, NULL, NULL),
('659d7d893bbdf14766a2d7092727957f', '555', '123@sasda.com', '737e636a7d1a0ef194324b31614a8d6cb28d986a3b5016507244803b81072daf', '4a30463bd1c098868dd2d3dc4e8f0c45b9195704', NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 1, 0, 0, 1, '2019-08-07 06:13:21', '2019-08-07 06:42:56', NULL, NULL, NULL),
('a92e1ecc53a0d93e0976d957e20ec638', 'hafeezsaaulanwar', 'hafeezdzekdo374@gmail.com', '4b0d02ed55a11332947fb8e30987afbed918ef7469ac63bdaecbad4065e9d760', '299342bebad61e448e9d737661c7732064d40047', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 0, '2019-08-07 06:50:34', '2019-08-07 07:11:44', NULL, NULL, NULL),
('b33b5059d229373855d81021121e96b1', '2', '111@ss.com12', 'dd8b79ed97dac0d460746435f74ce0fa1ad795d3c7763c85631fabf136b9acc5', '1281094f26e98ba86e789c0766a9abb121b8b8dc', '333', '941113075645', 60, NULL, NULL, 0, 0, 1, 1, 0, 0, 1, '2019-07-24 04:27:08', '2019-07-26 08:10:17', NULL, NULL, NULL),
('b5b01c5215ba0928ddd52498cd7f412a', 'aaaaa', 'aaaaa@gmail.com', '202b48df3f14e93bc75bbdc2bf7706c03fb09dcebfa12a89425c21e2b09c9a32', 'd6ad6735d32d1b1aa0b502e8db23d1cc07635c17', '111111', '111111', NULL, 'aaaaa lalala', NULL, 1, 0, 1, 1, 0, 0, 0, '2019-08-02 01:50:18', '2019-08-06 09:40:38', 'Lintang Mayang Pasir 3,\r\n11950 Bayan Lepas, Penang', '2004-08-19', 'Male'),
('bd3fc856974714748b8cb9713028f102', 'hafeezulanwar', 'hafeezdzeko374@gmail.com', '11b05b61bd22e3c161198b4541bfa57d47247470736e4bce5831b69962bc0b0d', 'c69b4d3e914b66f3c22dd415acf511c61106142b', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 0, '2019-08-07 07:12:05', '2019-08-07 07:23:51', NULL, NULL, NULL),
('c1970a119d85f4a37553a952a7468ece', '6', '1212@sad.com1', 'e0224de870a936949e4ccf467cd3775ecc4bd729ec1cb7d4ccca538e6a13faaa', '22dbed079213dccec3ca32834aee40c827ce3cbe', '164763391', '941113075645', 3, NULL, NULL, 0, 0, 1, 1, 0, 0, 0, '2019-07-24 06:51:03', '2019-07-26 08:10:21', NULL, NULL, NULL),
('c5d5c2a57107b6b59cd1a52a7ae3236f', 'ddddd', 'ddddd@gmail.com', '8e48b9f24951a51887603b4eb1cb75ffa17cd1a18ba7ce4cbc79ba938a9c565f', 'ebce501efdb9e2a14a719c072b0868069a0556bf', NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 1, 0, 0, 1, '2019-08-02 03:29:00', '2019-08-02 03:29:00', NULL, NULL, NULL),
('de7234d1e7e603315448caf956796c78', 'qqq', 'neroarth@gmail.com', 'b9184abb916851e16ba412e487ea9a98f270e7fe524130d1c4d4913bb18e856e', '2864d0a0a963c9d1067ce90c5386fa4db0484964', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 0, '2019-08-07 06:47:59', '2019-08-07 06:51:42', NULL, NULL, NULL),
('f515487f84db8587f4753e4604fd771a', 'test', 'nerosarth@gmail.coma', 'dcccb157878eff146140d89c1fe6f4573e0823dbb66869c1af5d94012cb14eec', '3de89bbb0937423df259e9c232824a7d858941f6', NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 1, 0, 0, 1, '2019-08-07 06:43:01', '2019-08-07 06:53:03', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_commission`
--
ALTER TABLE `group_commission`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactionTypeIdGroupCommission_relateTo_transactionTypeId` (`transaction_type_id`);

--
-- Indexes for table `money_type`
--
ALTER TABLE `money_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_orders`
--
ALTER TABLE `product_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productIdTransactionHistory_relateTo_productId` (`product_id`),
  ADD KEY `orderIdProductOrders_relateTo_orderId` (`order_id`);

--
-- Indexes for table `referral_history`
--
ALTER TABLE `referral_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `referralIdReferralHistory_relateTo_userId` (`referral_id`),
  ADD KEY `referrerIdReferralHistory_relateTo_userId` (`referrer_id`),
  ADD KEY `topReferrerIdReferralHistory_relateTo_userId` (`top_referrer_id`);

--
-- Indexes for table `transaction_history`
--
ALTER TABLE `transaction_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uidTransactionHistory_relateTo_userId` (`uid`),
  ADD KEY `targetUidTransactionHistory_relateTo_userId` (`target_uid`),
  ADD KEY `transactionTypeIdTransactionHistory_relateTo_transactionTypeId` (`transaction_type_id`),
  ADD KEY `moneyTypeIdTransactionHistory_relateTo_moneyTypeId` (`money_type_id`),
  ADD KEY `sourceTransactionIdTransactionHistory_relateTo_self` (`source_transaction_id`),
  ADD KEY `orderIdTransactionHistory_relateTo_orderId` (`order_id`);

--
-- Indexes for table `transaction_type`
--
ALTER TABLE `transaction_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`uid`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `countryIdUser_relateTo_countryId` (`country_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;

--
-- AUTO_INCREMENT for table `group_commission`
--
ALTER TABLE `group_commission`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `money_type`
--
ALTER TABLE `money_type`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product_orders`
--
ALTER TABLE `product_orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `referral_history`
--
ALTER TABLE `referral_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `transaction_history`
--
ALTER TABLE `transaction_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transaction_type`
--
ALTER TABLE `transaction_type`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `group_commission`
--
ALTER TABLE `group_commission`
  ADD CONSTRAINT `transactionTypeIdGroupCommission_relateTo_transactionTypeId` FOREIGN KEY (`transaction_type_id`) REFERENCES `transaction_type` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `product_orders`
--
ALTER TABLE `product_orders`
  ADD CONSTRAINT `orderIdProductOrders_relateTo_orderId` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `productIdTransactionHistory_relateTo_productId` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `referral_history`
--
ALTER TABLE `referral_history`
  ADD CONSTRAINT `referralIdReferralHistory_relateTo_userId` FOREIGN KEY (`referral_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `referrerIdReferralHistory_relateTo_userId` FOREIGN KEY (`referrer_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `topReferrerIdReferralHistory_relateTo_userId` FOREIGN KEY (`top_referrer_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE;

--
-- Constraints for table `transaction_history`
--
ALTER TABLE `transaction_history`
  ADD CONSTRAINT `moneyTypeIdTransactionHistory_relateTo_moneyTypeId` FOREIGN KEY (`money_type_id`) REFERENCES `money_type` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `orderIdTransactionHistory_relateTo_orderId` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `sourceTransactionIdTransactionHistory_relateTo_self` FOREIGN KEY (`source_transaction_id`) REFERENCES `transaction_history` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `targetUidTransactionHistory_relateTo_userId` FOREIGN KEY (`target_uid`) REFERENCES `user` (`uid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `transactionTypeIdTransactionHistory_relateTo_transactionTypeId` FOREIGN KEY (`transaction_type_id`) REFERENCES `transaction_type` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `uidTransactionHistory_relateTo_userId` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`) ON UPDATE CASCADE;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `countryIdUser_relateTo_countryId` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
