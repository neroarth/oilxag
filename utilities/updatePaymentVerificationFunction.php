<?php
if (session_id() == ""){
    session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Product.php';
//Product Order
require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/ProductOrders.php';

require_once dirname(__FILE__) . './databaseFunction.php';
require_once dirname(__FILE__) . './generalFunction.php';
require_once dirname(__FILE__) . './languageFunction.php';
require_once dirname(__FILE__) . './allNoticeModals.php';

// echo 'aaa';
// echo $_POST['shipping_method'];
// echo '<br/>';
// echo 'bbb';
// echo $_POST['order_id'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        $conn = connDB();

        $payment_status = rewrite($_POST["payment_status"]);
        $shipping_status = rewrite($_POST["shipping_status"]);
        $order_id = rewrite($_POST["order_id"]);

        //for debugging
        // echo "<br>";
        // echo $_POST['order_id']."<br>";
        // echo $payment_status."<br>";
        // echo $shipping_status."<br>";

        if(isset($_POST['order_id']))
        {   
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
            if($payment_status)
            {
                array_push($tableName,"payment_status");
                array_push($tableValue,$payment_status);
                $stringType .=  "s";
            }     
            if($shipping_status)
            {
                array_push($tableName,"shipping_status");
                array_push($tableValue,$shipping_status);
                $stringType .=  "s";
            }     

            array_push($tableValue,$order_id);
            $stringType .=  "s";
            $orderUpdated = updateDynamicData($conn,"orders"," WHERE id = ? ",$tableName,$tableValue,$stringType);
            
            if($orderUpdated)
            {
                // echo "<br>";
                // echo $_POST['order_id']."<br>";
                // echo $payment_status."<br>";
                // echo $shipping_status."<br>";
                // echo $shipping_date."<br>";
                // echo $shipping_status."<br>";
                // echo "success";
                // $_SESSION['messageType'] = 1;
                header('Location: ../adminShipping.php');
            }
            else
            {
                echo "fail";
                // $_SESSION['messageType'] = 1;
                // header('Location: ../adminShipping.php?type=2');
            }
        }
        else
        {
            echo "dunno";
            // $_SESSION['messageType'] = 1;
            // header('Location: ../adminShipping.php?type=3');
        }

    }
else 
{
    header('Location: ../adminShipping.php');
}

?>