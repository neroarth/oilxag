<?php
if (session_id() == ""){
     session_start();
 }
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';
require_once dirname(__FILE__) . '/allNoticeModals.php';

function registerNewUser($conn,$username,$uid,$email,$countryId,$phoneNo,$finalPassword,$salt,$icNo,$referrerUid = null,$topReferrerUid = null,$currentLevel = null){
     $isReferred = 0;
     if($referrerUid && $topReferrerUid){
         $isReferred = 1;
     }
 
     if(insertDynamicData($conn,"user",array("uid","username","email","password","salt","phone_no","ic_no","country_id","is_referred"),
         array($uid,$username,$email,$finalPassword,$salt,$phoneNo,$icNo,$countryId,$isReferred),"sssssssii") === null){
          header('Location: ../index.php?promptError=1');
     //     promptError("error registering new account.The account already exist");
     //     return false;
     }else{
         if($isReferred === 1){
             if(insertDynamicData($conn,"referral_history",array("referrer_id","referral_id","current_level","top_referrer_id"),
                 array($referrerUid,$uid,$currentLevel,$topReferrerUid),"ssis") === null){
                    header('Location: ../index.php?promptError=2');
               //   promptError("error assigning referral relationship");
               //   return false;
             }
         }
     }
 
     return true;
 }
 function sendEmailForVerification($uid) 
 {
     $conn = connDB();
     $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");

     $verifyUser_debugMode = 2;
     $verifyUser_host = "mail.dcksupreme.asia";
     $verifyUser_usernameThatSendEmail = "noreply@dcksupreme.asia";                   // Sender Acc Username
     $verifyUser_password = "~sh~1z+kL=;C";                                                      // Sender Acc Password
     $verifyUser_smtpSecure = "ssl";                                                           // SMTP type
     $verifyUser_port = 465;                                                                   // SMTP port no
     $verifyUser_sentFromThisEmailName = "noreply@dcksupreme.asia";                                       // Sender Username
     $verifyUser_sentFromThisEmail = "noreply@dcksupreme.asia";                       // Sender Email
     $verifyUser_sendToThisEmailName = $userRows[0]->getUsername();                                      // Recipient Username
     $verifyUser_sendToThisEmail = $userRows[0]->getEmail();                                   // Recipient Email
     $verifyUser_isHtml = true;                                                                // Set To Html
     $verifyUser_subject = "Confirm Your Registration! ";                                      // Title
     
          $verifyUser_body = "<p>CONGRATULATIONS !! your registration on DCK Supreme was successfully.</p>"; // Body
          // $verifyUser_body = "<p>Username : ".$$userRows->getUsername()."</p>"; // Body

          // $verifyUser_body = "<p>Please confirm your registration of DCK Supreme by clicking the "; // Body
          // $verifyUser_body .="<a href='http://www.dcksupreme.asia/email-verified.php?getVerified=".$uid."'>link</a>";
          // $verifyUser_body .=" below. </p>";
          // $verifyUser_altBody = "Test Email 2";                                                    // Body
          
     sendMailTo(
          null,
          $verifyUser_host,
          $verifyUser_usernameThatSendEmail,
          $verifyUser_password,
          $verifyUser_smtpSecure,
          $verifyUser_port, 
          $verifyUser_sentFromThisEmailName,
          $verifyUser_sentFromThisEmail,
          $verifyUser_sendToThisEmailName,
          $verifyUser_sendToThisEmail,
          $verifyUser_isHtml,
          $verifyUser_subject,
          $verifyUser_body,
          null
     );
 }

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $register_uid = md5(uniqid());
     $register_username = rewrite($_POST['register_username']);
     $register_email_user = rewrite($_POST['register_email_user']);
     $register_email_user = filter_var($register_email_user, FILTER_SANITIZE_EMAIL);
     $register_password = $_POST['register_password'];
     $register_password_validation = strlen($register_password);
     $register_retype_password = $_POST['register_retype_password'];
     $register_email_referrer = null;

     if(isset($_POST['register_email_referrer']))
     {
          $register_email_referrer = rewrite($_POST['register_email_referrer']);
          $register_email_referrer = filter_var($register_email_referrer, FILTER_SANITIZE_EMAIL);
     }          


     $password = hash('sha256',$register_password);
     $salt = substr(sha1(mt_rand()), 0, 100);
     $finalPassword = hash('sha256', $salt.$password);

//   FOR DEBUGGING 
//     echo $register_uid;
//     echo $register_username;
//     echo $register_email_user;
//     echo $register_password;
//     echo $register_retype_password;
//     echo $register_email_referrer;

     if(filter_var($register_email_user, FILTER_VALIDATE_EMAIL))
     {
          if($register_password == $register_retype_password)
          {
               if($register_password_validation >= 6)
               {
                    if($register_email_referrer)
                    {
                         $referrerUserRows = getUser($conn," WHERE email = ? ",array("email"),array($register_email_referrer),"s");

                         if($referrerUserRows)
                         {
                              $referrerUid = $referrerUserRows[0]->getUid();
                              $topReferrerUid = $referrerUid;//assign top referrer id to this guy 1st, if he is not the top, will be overwritten
                              $currentLevel = 1;
                  
                              $referralHistoryRows = getReferralHistory($conn," WHERE referral_id = ? ",array("referral_id"),array($referrerUid),"s");
                              if($referralHistoryRows)
                              {
                                  $topReferrerUid = $referralHistoryRows[0]->getTopReferrerId();
                                  $currentLevel = $referralHistoryRows[0]->getCurrentLevel() + 1;
                              }
                  
                              if(registerNewUser($conn,$register_username,$register_uid,$register_email_user,null,null,$finalPassword,$salt,null,$referrerUid,$topReferrerUid,$currentLevel))
                              {
                                   sendEmailForVerification($register_uid);
                                   // $_SESSION['messageType'] = 1;
                                   // header('Location: ../index.php?type=1');
                                   // // echo "// register success with referral ";
                                   $_SESSION['uid'] = $register_uid;
                                   $_SESSION['usertype_level'] = 1;
                                   header('Location: ../profile.php');
                              }
                         }
                         else
                         {
                              $_SESSION['messageType'] = 1;
                              header('Location: ../index.php?type=2');
                              //echo "// register error with referral ";
                         }
                    }
                    else 
                    {
                         if(registerNewUser($conn,$register_username,$register_uid,$register_email_user,null,null,$finalPassword,$salt,null))
                         {
                              sendEmailForVerification($register_uid);
                              // $_SESSION['messageType'] = 1;
                              // header('Location: ../index.php?type=3');
                              //echo "// register success without referral";
                              $_SESSION['uid'] = $register_uid;
                              $_SESSION['usertype_level'] = 1;
                              header('Location: ../profile.php');
                         }
                    }
               }
               else 
               {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../index.php?type=4');
                    //echo "// password must be more than 6 ";
               }
          }
          else 
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../index.php?type=5');
               //echo "// password does not match ";
          }   
     }
     else 
     {
          $_SESSION['messageType'] = 1;
          header('Location: ../index.php?type=6');
          //echo "// wrong email format ";
     }



    
}
else 
{
     header('Location: ../index.php');
}
?>