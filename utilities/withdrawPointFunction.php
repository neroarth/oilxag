<?php
if (session_id() == "")
{
     session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Withdrawal.php';
require_once dirname(__FILE__) . './databaseFunction.php';
require_once dirname(__FILE__) . './generalFunction.php';
require_once dirname(__FILE__) . './mailerFunction.php';
require_once dirname(__FILE__) . './allNoticeModals.php';

$uid = $_SESSION['uid'];

    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        $conn = connDB();

        $withdrawalRows = getWithdrawReq($conn," WHERE uid = ? AND withdrawal_status = 'PENDING' ",array("uid"),array($uid),"s");
        $withdrawalDetails = $withdrawalRows[0];
        // $priority = $withdrawalDetails -> getWithdrawalRequestStatus();





        $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
        $userDetails = $userRows[0];
        $update_money = $userDetails -> getWithdrawAmount();
        $currentPoint = $userDetails -> getUserPoint();
        $name = $userDetails->getUsername();
        $update_point = rewrite($_POST['update_point']);

        $currentEnterEpin = $_POST["withdraw_epin_convert"];
        $dbEpin =  $userDetails->getEpin();
        $dbSaltEpin =  $userDetails->getSaltEpin();
        $newEpin_hashed = hash('sha256',$currentEnterEpin);
        $status = 'COMPLETED';
        $newEpin_hashed_salt = hash('sha256', $dbSaltEpin .   $newEpin_hashed);

        if ($update_point <= $update_money &&  $update_money > 0) {
      //  if ($update_point < $update_money &&  $update_money >= 10) {
          if ($dbEpin == $newEpin_hashed_salt) {
            if ($withdrawalDetails) {
              $_SESSION['messageType'] = 1;
              header('Location: ../wallet.php?type=4');
            }else {
              $finalMoney = $update_money - $update_point;
              $finalPoint = $currentPoint + $update_point;
              if (NewWithdraw($conn,$uid,$name,$update_point,$status)) {
                        $_SESSION['messageType'] = 1;
                        header('Location: ../wallet.php?type=1');
                    }
                    else
                    {
                        $_SESSION['messageType'] = 2;
                        header('Location: ../wallet.php?type=1');
                    }
              $user = getUser($conn," username = ?   ",array("username"),array($username),"s");
              if(!$user)
              {
                  $tableName = array();
                  $tableValue =  array();
                  $stringType =  "";
                  //echo "save to database";
                  if(!$finalMoney)
                  {
                    $money = 0;
                      array_push($tableName,"final_amount");
                      array_push($tableValue,$money);
                      $stringType .=  "i";
                  }

                  array_push($tableValue,$uid);
                  $stringType .=  "s";
                  $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                  if($passwordUpdated)
                  {
                      // echo "success";
                      // $_SESSION['messageType'] = 1;
                      // header('Location: ../wallet.php?type=1');
                      // header('Location: ../editprofile.php?type=3');
                  }
                  else
                  {
                      // echo "fail";
                      // $_SESSION['messageType'] = 3;
                      // header('Location: ../wallet.php?type=3');
                  }
              }
              else
              {
                  //echo "";
                  // $_SESSION['messageType'] = 2;
                  // header('Location: ../wallet.php?type=2');
              }
              $_SESSION['messageType'] = 1;
              header('Location: ../wallet.php?type=5');

            }

          }else {
            $_SESSION['messageType'] = 1;
            header('Location: ../wallet.php?type=3');
          }

        }else {
          $_SESSION['messageType'] = 1;
          header('Location: ../wallet.php?type=2');
        }




        $user = getUser($conn," username = ?   ",array("username"),array($username),"s");
        if(!$user)
        {
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
            if($finalPoint)
            {
                array_push($tableName,"point");
                array_push($tableValue,$finalPoint);
                $stringType .=  "i";
            }
            if($finalMoney)
            {
                array_push($tableName,"final_amount");
                array_push($tableValue,$finalMoney);
                $stringType .=  "i";
            }
            // if(!$finalMoney)
            // {
            //   $money = 0;
            //     array_push($tableName,"final_amount");
            //     array_push($tableValue,$money);
            //     $stringType .=  "i";
            // }

            array_push($tableValue,$uid);
            $stringType .=  "s";
            $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
            if($passwordUpdated)
            {
                // echo "success";
                // $_SESSION['messageType'] = 1;
                // header('Location: ../wallet.php?type=1');
                // header('Location: ../editprofile.php?type=3');
            }
            else
            {
                // echo "fail";
                // $_SESSION['messageType'] = 3;
                // header('Location: ../wallet.php?type=3');
            }
        }
        else
        {
            //echo "";
            // $_SESSION['messageType'] = 2;
            // header('Location: ../wallet.php?type=2');
        }

        // if ($finalMoney = 0) {
        //   $money = 0;
        //   $tableName = array();
        //   $tableValue =  array();
        //   $stringType =  "";
        //   //echo "save to database";
        //   if($money)
        //   {
        //       array_push($tableName,"final_amount");
        //       array_push($tableValue,$money);
        //       $stringType .=  "i";
        //   }
        //   array_push($tableValue,$uid);
        //   $stringType .=  "s";
        //   $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        //   if($passwordUpdated)
        //   {
        //       // echo "success";
        //       // $_SESSION['messageType'] = 1;
        //       // header('Location: ../wallet.php?type=1');
        //       // header('Location: ../editprofile.php?type=3');
        //   }
        //   else
        //   {
        //       // echo "fail";
        //       // $_SESSION['messageType'] = 3;
        //       // header('Location: ../wallet.php?type=3');
        //   }
        //}

    }
else
{
    //header('Location: ../editprofile.php');
    header('Location: ../index.php');
}

function NewWithdraw($conn,$uid,$name,$update_point,$status){

     if(insertDynamicData($conn,"cash_to_point",array("uid","name","point","status"),
         array($uid,$name,$update_point,$status),"ssis") === null){

          return false;
     }else{
     }

    return true;
 }
?>
