<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Withdrawal.php';
require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';
require_once dirname(__FILE__) . '/allNoticeModals.php';

$conn = connDB();

$uid = $_SESSION['uid'];
$getWho = getWholeDownlineTree($conn, $_SESSION['uid'],false);
    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        $conn = connDB();



        $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
        $userDetails = $userRows[0];
        $sendName = $userDetails->getUsername();



        $currentUsernameSearch = $_POST["username"];
        $existingIC = $userDetails -> getIcNo();
        $currentPoint = $userDetails -> getUserPoint();
        $transferAmount = $_POST["transferAmount"];

        $userReceiveRows = getUser($conn," WHERE username = ? ",array("username"),array($currentUsernameSearch),"s");
        $userReceiveDetails = $userReceiveRows[0];
        $uidReceiver = $userReceiveDetails->getUid();


        if ($getWho) {
          for($a = 0;$a<count($getWho);$a++){

            $userRows11 = getUser($conn," WHERE uid = ? ",array("uid"),array($getWho[$a]->getReferralId()),"s");
            $usernameSearch = $userRows11[0]->getUsername();
            $currentLevel = $getWho[$a]->getCurrentLevel();
            $icPoint = $userRows11[0]->getUserPoint();


        $currentEnterEpin = $_POST["withdraw_epin_convert"];
        $dbEpin =  $userDetails->getEpin();
        $dbSaltEpin =  $userDetails->getSaltEpin();
        $newEpin_hashed = hash('sha256',$currentEnterEpin);
        $status = 'RECEIVED';
        $newEpin_hashed_salt = hash('sha256', $dbSaltEpin .   $newEpin_hashed);

        if ($transferAmount <= $currentPoint &&  $currentPoint > 0) {
          if ($dbEpin == $newEpin_hashed_salt) {
              if ($usernameSearch == $currentUsernameSearch) {
                $pointNow = $currentPoint - $transferAmount;
                $pointBeingTransfer = $icPoint + $transferAmount;
                if (NewWithdraw($conn,$uid,$sendName,$transferAmount,$currentUsernameSearch,$uidReceiver,$status)) {
                          $_SESSION['messageType'] = 1;
                          header('Location: ../wallet.php?type=1');
                      }
                      else
                      {
                          $_SESSION['messageType'] = 2;
                          header('Location: ../wallet.php?type=1');
                      }
                $user = getUser($conn," username = ?   ",array("username"),array($username),"s");
                if(!$user)
                {
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    //echo "save to database";
                    if($pointNow)
                    {
                        array_push($tableName,"point");
                        array_push($tableValue,$pointNow);
                        $stringType .=  "i";
                    }
                    if(!$pointNow)
                    {   $point = 0;
                        array_push($tableName,"point");
                        array_push($tableValue,$point);
                        $stringType .=  "i";
                    }
                    if($finalMoney)
                    {
                        array_push($tableName,"final_amount");
                        array_push($tableValue,$finalMoney);
                        $stringType .=  "i";
                    }

                    array_push($tableValue,$uid);
                    $stringType .=  "s";
                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                    if($passwordUpdated)
                    {}
                    else
                    {}
                }
                else
                {}
                $_SESSION['messageType'] = 1;
                header('Location: ../wallet.php?type=8');

              }else {
                header('Location: ../wallet.php');

              }

          }else {
            $_SESSION['messageType'] = 1;
            header('Location: ../wallet.php?type=3');
          }

        }else {
          $_SESSION['messageType'] = 1;
          header('Location: ../wallet.php?type=9');
        }




        $user = getUser($conn," username = ?   ",array("username"),array($username),"s");
        if(!$user)
        {
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
            if($pointNow)
            {
                array_push($tableName,"point");
                array_push($tableValue,$pointNow);
                $stringType .=  "i";
            }

            if($finalMoney)
            {
                array_push($tableName,"final_amount");
                array_push($tableValue,$finalMoney);
                $stringType .=  "i";
            }

            array_push($tableValue,$uid);
            $stringType .=  "s";
            $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
            if($passwordUpdated)
            {}
            else
            {}
        }
        else
        {}

        $user = getUser($conn," username = ?   ",array("username"),array($username),"s");
        if(!$user)
        {
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
            if($pointBeingTransfer)
            {
                array_push($tableName,"point");
                array_push($tableValue,$pointBeingTransfer);
                $stringType .=  "s";
            }


            array_push($tableValue,$currentUsernameSearch);
            $stringType .=  "s";
            $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
            if($passwordUpdated)
            {}
            else
            {}
        }
        else
        {}

      }
      }
    }
else
{
    header('Location: ../index.php');
}

function NewWithdraw($conn,$uid,$sendName,$transferAmount,$currentUsernameSearch,$uidReceiver,$status){

     if(insertDynamicData($conn,"transfer_point",array("send_uid","send_name","amount","receive_name","receive_uid","status"),
         array($uid,$sendName,$transferAmount,$currentUsernameSearch,$uidReceiver,$status),"ssisss") === null){

          return false;
     }else{
     }

    return true;
 }
?>