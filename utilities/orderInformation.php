<?php
if (session_id() == "")
{
     session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/ProductOrders.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';
require_once dirname(__FILE__) . '/allNoticeModals.php';

    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        $conn = connDB();

        $uid = $_SESSION['uid'];

        $username = rewrite($_POST["insert_username"]);

        $bankName = rewrite($_POST["insert_bankname"]);
        $bankAccountHolder = rewrite($_POST["insert_bankaccountholder"]);
        $bankAccountNo = rewrite($_POST["insert_bankaccountnumber"]);
        
        $name = rewrite($_POST["insert_name"]);
        $contactNo = rewrite($_POST["insert_contactNo"]);
        $email = rewrite($_POST["insert_email"]);
        $address_1 = rewrite($_POST["insert_address_1"]);
        $address_2 = rewrite($_POST["insert_address_2"]);
        $city = rewrite($_POST["insert_city"]);
        $zipcode = rewrite($_POST["insert_zipcode"]);
        // $postcode = rewrite($_POST["insert_postcode"]);
        $state = rewrite($_POST["insert_state"]);
        $country = rewrite($_POST["insert_country"]);

        // $user = getUser($conn," username = ?   ",array("username"),array($username),"s");    
        $orderUid = getOrders($conn," uid = ?   ",array("uid"),array($uid),"s");  

        if(!$orderUid)
        {   
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
            if($username)
            {
                array_push($tableName,"username");
                array_push($tableValue,$username);
                $stringType .=  "s";
            }

            if($bankName)
            {
                array_push($tableName,"bank_name");
                array_push($tableValue,$bankName);
                $stringType .=  "s";
            }
            if($bankAccountHolder)
            {
                array_push($tableName,"bank_account_holder");
                array_push($tableValue,$bankAccountHolder);
                $stringType .=  "s";
            }
            if($bankAccountNo)
            {
                array_push($tableName,"bank_account_no");
                array_push($tableValue,$bankAccountNo);
                $stringType .=  "s";
            }

            if($name)
            {
                array_push($tableName,"name");
                array_push($tableValue,$name);
                $stringType .=  "s";
            }
            if($contactNo)
            {
                array_push($tableName,"contactNo");
                array_push($tableValue,$contactNo);
                $stringType .=  "s";
            }
            if($email)
            {
                array_push($tableName,"email");
                array_push($tableValue,$email);
                $stringType .=  "s";
            }
            if($address_1)
            {
                array_push($tableName,"address_line_1");
                array_push($tableValue,$address_1);
                $stringType .=  "s";
            }
            if($address_2)
            {
                array_push($tableName,"address_line_2");
                array_push($tableValue,$address_2);
                $stringType .=  "s";
            }
            if($city)
            {
                array_push($tableName,"city");
                array_push($tableValue,$city);
                $stringType .=  "s";
            }
            if($zipcode)
            {
                array_push($tableName,"zipcode");
                array_push($tableValue,$zipcode);
                $stringType .=  "s";
            }
            if($state)
            {
                array_push($tableName,"state");
                array_push($tableValue,$state);
                $stringType .=  "s";
            }
            if($country)
            {
                array_push($tableName,"country");
                array_push($tableValue,$country);
                $stringType .=  "s";
            }

            array_push($tableValue,$uid);
            $stringType .=  "s";
            $orderUpdated = updateDynamicData($conn,"orders"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
            if($orderUpdated)
            {
                // echo "success";
                // echo "<br>";
                // echo $bankName."<br>";
                // echo $bankAccountHolder."<br>";
                // echo $bankAccountNo."<br>";
                // echo $name."<br>";
                // echo $contactNo."<br>";
                // echo $email."<br>";
                // echo $address_1."<br>";
                // echo $address_2."<br>";
                // echo $city."<br>";
                // echo $zipcode."<br>";
                // echo $state."<br>";
                // echo $country."<br>";
                header('Location: ../shipping.php');
            }
            else
            {
                //echo "fail";
                $_SESSION['messageType'] = 1;
                header('Location: ../checkout.php?type=1');
            }
        }
        else
        {
            //echo "dunno";
            $_SESSION['messageType'] = 1;
            header('Location: ../checkout.php?type=2');
        }

    }
else 
{
    header('Location: ../product.php');
    // header('Location: ../index.php');
}
?>
