<?php
if (session_id() == ""){
     session_start();
 }
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../classes/Announcement.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     if(isset($_POST['announcement_id']))
     {
          if($_POST['confirm_new_announcement'] && $_POST['confirm_new_announcement'] == 1)
          {
               if(isset($_POST['announcement_message']) && $_POST['announcement_message'] != "")
               {
                    $announcement_message = rewrite($_POST['announcement_message']);

                    if(updateDynamicData(
                         $conn,
                         "announcement",
                         " WHERE announce_id = ? ",
                         array("announce_message","announce_showThis"),
                         array($announcement_message,1,$_POST['announcement_id']),
                         "sii"))
                    {
                         unset($_POST);
                         $_SESSION['messageType'] = 1;
                         header('Location: ../announcement.php?type=2');
                         // echo "// announcement successfully added ";
                    }
                    else 
                    {
                         unset($_POST);
                         $_SESSION['messageType'] = 1;
                         header('Location: ../announcement_crud.php?type=3');
                    }
               }
               else
               {
                    unset($_POST);
                    $_SESSION['messageType'] = 1;
                    header('Location: ../announcement_crud.php?type=2');
                    //echo "//there are no announcement/message ";
               }
               
          }
          else
          {
               if(updateDynamicData(
                    $conn,
                    "announcement",
                    " WHERE announce_id = ? ",
                    array("announce_showThis"),
                    array(0,$_POST['announcement_id']),
                    "ii"))
               {
                    unset($_POST);
                    $_SESSION['messageType'] = 1;
                    header('Location: ../announcement.php?type=3');
                    // echo "// announcement successfully deleted ";
               }
          }    
     }
     else
     {
          if($_POST['confirm_new_announcement'] && $_POST['confirm_new_announcement'] == 1)
          {
               if(isset($_POST['announcement_message']) && $_POST['announcement_message'] != "")
               {
                    $announcement_message = rewrite($_POST['announcement_message']);

                    if(insertDynamicData(
                         $conn,
                         "announcement",
                         array("announce_message","announce_showThis"),
                         array($announcement_message,1),
                         "si") === null)
                         {
                              unset($_POST);
                              $_SESSION['messageType'] = 1;
                              header('Location: ../announcement_crud.php?type=1');
                              // echo "// cannot add into db ";
                         }
                         else
                         {
                              unset($_POST);
                              $_SESSION['messageType'] = 1;
                              header('Location: ../announcement.php?type=1');
                              // echo "// announcement successfully added ";
                         }
               }
               else
               {
                    unset($_POST);
                    $_SESSION['messageType'] = 1;
                    header('Location: ../announcement_crud.php?type=2');
                    //echo "//there are no announcement/message ";
               }
          }
          else
          {
               header('Location: ../announcement.php');
          }
     }
}
else
{

}
?>