<?php
if (session_id() == "")
{
     session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Withdrawal.php';
require_once dirname(__FILE__) . '/../classes/Rate.php';

require_once dirname(__FILE__) . './databaseFunction.php';
require_once dirname(__FILE__) . './generalFunction.php';
require_once dirname(__FILE__) . './mailerFunction.php';
require_once dirname(__FILE__) . './allNoticeModals.php';

    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        $conn = connDB();
        $uid = $_SESSION['uid'];


        $bankName = $_POST['bank_name'];
        $accNumber = $_POST['acc_number'];
        $usernameAccount = $_POST['usernameaccount'];

        $withdrawalRows = getWithdrawReq($conn," WHERE uid = ? AND withdrawal_status = 'PENDING' ",array("uid"),array($uid),"s");
        $withdrawalDetails = $withdrawalRows[0];

        $withdrawRate = getRate($conn," WHERE id = ? ",array("id"),array(1),"i");
        $rateDetails = $withdrawRate[0];

        $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
        $userDetails = $userRows[0];
        $contact = $userDetails->getPhoneNo();
        $username = $userDetails->getUsername();

        $withdrawalStatus = "PENDING";
        $withdraw = $_POST["withdraw_amount"]; //money user entered
        $withdrawAmount = $userDetails -> getWithdrawAmount();
        $charges = $rateDetails -> getChargesWithdraw();
        $withdraw -= $charges;

        $currentEnterEpin = $_POST["withdraw_epin"];
        $dbEpin =  $userDetails->getEpin();
        $dbSaltEpin =  $userDetails->getSaltEpin();
        $newEpin_hashed = hash('sha256',$currentEnterEpin);
        $newEpin_hashed_salt = hash('sha256', $dbSaltEpin .   $newEpin_hashed);

        $currentEnterEpinConvert = $_POST["withdraw_epin_convert"];
        $newEpin_hashed_convert = hash('sha256',$currentEnterEpinConvert);
        $newEpin_hashed_salt_convert = hash('sha256', $dbSaltEpin .   $newEpin_hashed_convert);

        $charges = $rateDetails -> getChargesWithdraw();
        //$MoneyAfterCharge = $withdraw + $charges;

        if ($withdraw <= $withdrawAmount && $withdraw > 10) {
      //  if ($MoneyAfterCharge < $withdrawAmount && $withdraw >= 10) {
          if ($dbEpin == $newEpin_hashed_salt) {
            if ($withdrawalDetails) {
              $_SESSION['messageType'] = 1;
              header('Location: ../wallet.php?type=6');
            }else {
              if (NewWithdraw($conn,$uid,$withdrawalStatus,$withdraw,$withdrawAmount,$usernameAccount,$bankName,$accNumber,$contact,$username)) {
                        $_SESSION['messageType'] = 1;
                        header('Location: ../wallet.php?type=1');
                    }
                    else
                    {
                        $_SESSION['messageType'] = 2;
                        header('Location: ../wallet.php?type=1');
                    }
            }

          }else {
            $_SESSION['messageType'] = 1;
            header('Location: ../wallet.php?type=3');
          }


}  else
  {
      // echo "fail";
      $_SESSION['messageType'] = 1;
      header('Location: ../wallet.php?type=2');
  }




}
else
{
    header('Location: ../index.php');
}


function NewWithdraw($conn,$uid,$withdrawalStatus,$withdraw,$withdrawAmount,$usernameAccount,$bankName,$accNumber,$contact,$username){

     if(insertDynamicData($conn,"withdrawal",array("uid","withdrawal_status","amount","final_amount","username","bank_name","acc_number","contact","owner"),
         array($uid,$withdrawalStatus,$withdraw,$withdrawAmount,$usernameAccount,$bankName,$accNumber,$contact,$username),"sssissiis") === null){

          return false;
     }else{
     }

    return true;
 }



?>
