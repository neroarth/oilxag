<?php
if (session_id() == "")
{
     session_start();
}
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/ProductOrders.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';
require_once dirname(__FILE__) . '/allNoticeModals.php';

    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        $conn = connDB();

        $uid = $_SESSION['uid'];

        $subtotal = rewrite($_POST["insert_subtotal"]);
        $total = rewrite($_POST["insert_total"]);
        $payment_status = rewrite($_POST["insert_paymentstatus"]);
  
        $orderUid = getOrders($conn," uid = ?   ",array("uid"),array($uid),"s");  

        if(!$orderUid)
        {   
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
            if($subtotal)
            {
                array_push($tableName,"subtotal");
                array_push($tableValue,$subtotal);
                $stringType .=  "d";
            }
            if($total)
            {
                array_push($tableName,"total");
                array_push($tableValue,$total);
                $stringType .=  "d";
            }
            if($payment_status)
            {
                array_push($tableName,"payment_status");
                array_push($tableValue,$payment_status);
                $stringType .=  "s";
            }
       

            array_push($tableValue,$uid);
            $stringType .=  "s";
            $orderUpdated = updateDynamicData($conn,"orders"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
            if($orderUpdated)
            {
                // echo "success";
                header('Location: ../payment.php');
                
                // echo "<br>";
                // echo $subtotal."<br>";
                // echo $total."<br>";
                // echo $payment_status."<br>";

            }
            else
            {
                echo "fail";
                // $_SESSION['messageType'] = 1;
                // header('Location: ../checkout.php?type=1');
            }
        }
        else
        {
            echo "dunno";
            // $_SESSION['messageType'] = 1;
            // header('Location: ../checkout.php?type=2');
        }

    }
else 
{
    header('Location: ../product.php');
}
?>
