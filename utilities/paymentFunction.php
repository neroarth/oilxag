<?php
if (session_id() == "")
{
     session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/ProductOrders.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';
require_once dirname(__FILE__) . '/allNoticeModals.php';

    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        $conn = connDB();

        $uid = $_SESSION['uid'];

        $payment_method = rewrite($_POST["payment_method"]);
        $payment_amount = rewrite($_POST["payment_amount"]);
        $payment_bankreference = rewrite($_POST["payment_bankreference"]);
        $payment_date = rewrite($_POST["payment_date"]);
        $payment_time = rewrite($_POST["payment_time"]);
        $payment_status = rewrite($_POST["payment_status"]);
        
        // echo "<br>";
        // echo $payment_method."<br>";
        // echo $payment_amount."<br>";
        // echo $payment_date."<br>";
        // echo $payment_time."<br>";

        // $user = getUser($conn," username = ?   ",array("username"),array($username),"s");    
        $paymentUid = getOrders($conn," uid = ?   ",array("uid"),array($uid),"s");  

        if(!$paymentUid)
        {   
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
            if($payment_method)
            {
                array_push($tableName,"payment_method");
                array_push($tableValue,$payment_method);
                $stringType .=  "s";
            }
            if($payment_amount)
            {
                array_push($tableName,"payment_amount");
                array_push($tableValue,$payment_amount);
                $stringType .=  "i";
            }
            if($payment_bankreference)
            {
                array_push($tableName,"payment_bankreference");
                array_push($tableValue,$payment_bankreference);
                $stringType .=  "s";
            }
            if($payment_date)
            {
                array_push($tableName,"payment_date");
                array_push($tableValue,$payment_date);
                $stringType .=  "s";
            }
            if($payment_time)
            {
                array_push($tableName,"payment_time");
                array_push($tableValue,$payment_time);
                $stringType .=  "s";
            }
            if($payment_status)
            {
                array_push($tableName,"payment_status");
                array_push($tableValue,$payment_status);
                $stringType .=  "s";
            }
            

            array_push($tableValue,$uid);
            $stringType .=  "s";
            $orderUpdated = updateDynamicData($conn,"orders"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
            if($orderUpdated)
            {
                // echo "<br>";
                // echo "success";
                // echo $bankName."<br>";
                $_SESSION['messageType'] = 1;
                header('Location: ../purchaseHistory.php?type=1');
                
            }
            else
            {
                echo "fail";
                // $_SESSION['messageType'] = 1;
                // header('Location: ../payment.php?type=2');
            }
        }
        else
        {
            echo "dunno";
            // $_SESSION['messageType'] = 1;
            // header('Location: ../payment.php?type=3');
        }

    }
else 
{
    header('Location: ../product.php');
    // header('Location: ../index.php');
}
?>
