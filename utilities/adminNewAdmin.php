<?php
if (session_id() == ""){
    session_start();
}
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
// require_once dirname(__FILE__) . '/adminAccess.php'; 
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . './databaseFunction.php';
require_once dirname(__FILE__) . './generalFunction.php';
require_once dirname(__FILE__) . './languageFunction.php';
require_once dirname(__FILE__) . './allNoticeModals.php';

function addNewAdmin($conn,$fullName,$username,$uid,$register_email_user,$phoneNo,$finalPassword,$salt,$userType){
 
    if(insertDynamicData($conn,"user",array("full_name","username","uid","email","phone_no","password","salt","user_type"),
         array($fullName,$username,$uid,$register_email_user,$phoneNo,$finalPassword,$salt,$userType),"sssssssi") === null)
         {}
    else{}
 
     return true;
 }

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = md5(uniqid());
    $fullName = rewrite($_POST['fullName']);
    $username = rewrite($_POST['username']);
    $phoneNo = rewrite($_POST['phoneNo']);
    $userType = rewrite($_POST['userType']);
    $register_email_user = rewrite($_POST['email']);
    $register_email_user = filter_var($register_email_user, FILTER_SANITIZE_EMAIL);
    $register_password = $_POST['register_password'];
    $password = hash('sha256',$register_password);
    $salt = substr(sha1(mt_rand()), 0, 100);
    $finalPassword = hash('sha256', $salt.$password);
                    
                    if(addNewAdmin($conn,$fullName,$username,$uid,$register_email_user,$phoneNo,$finalPassword,$salt,$userType))
                    {
                        $_SESSION['messageType'] = 1;
                        header('Location: ../adminAdmin.php?type=1');
                        // echo "<br>";
                        // echo "register success";
                        // echo $uid."<br>";
                        // echo $fullName."<br>";
                        // echo $username."<br>";
                        // echo $phoneNo."<br>";
                        // echo $register_email_user."<br>";
                    }
}
else 
{
    $_SESSION['messageType'] = 1;
    header('Location: ../adminAdmin.php?type=2');
}
?>