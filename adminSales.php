<?php
  

require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php'; 
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    //todo create table for transaction_history with at least a column for quantity(in order table),product_id(in order table), order_id, status (others can refer to btcw's), target_uid, trigger_transaction_id
    //todo create table for order and product_order
    $totalProductCount = count($_POST['product-list-id-input']);
    for($i = 0; $i < $totalProductCount; $i++){
        $productId = $_POST['product-list-id-input'][$i];
        $quantity = $_POST['product-list-quantity-input'][$i];

        echo " this: $productId total: $quantity";
    }
}

$products = getProduct($conn);

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/adminSales.php" />
    <meta property="og:title" content="Sales | DCK Supreme" />
    <title>Sales | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/adminSales.php" />
    <?php include 'css.php'; ?>    
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<!--<form method="POST">-->
    <?php
//    if(isset($_POST['product-list-quantity-input'])){
//        createProductList($products,$_POST['product-list-quantity-input']);
//    }else{
//        createProductList($products);
//    }

    ?>
<!--    <button type="submit" name="addToCartButton" id="addToCartButton" >Add to cart</button>-->
<!--</form>-->
<div class="yellow-body padding-from-menu same-padding">
	<h1 class="h1-title h1-before-border shipping-h1 h1-with-select">Daily Sales(Product): RM1,360,000.00</h1>
    <select class="select-with-h1 clean transparent-select">
    	<option>Name (Descending)</option>
        <option>Name (Ascending)</option>
        <option>Highest Sales</option>
        <option>Lowest Sales</option>
    </select>
    <div class="clear"></div>
	<div class="search-container0 sales-search">

            <div class="shipping-input clean smaller-text2">
                <p>Start Date</p>
                <input class="shipping-input2 clean normal-input" type="date">
            </div>
            <div class="shipping-input clean smaller-text2 middle-shipping-div second-shipping">
                <p>End Date</p>
                <input class="shipping-input2 clean normal-input" type="date">
            </div>
            <div class="shipping-input clean smaller-text2">
                <p>Product</p>
                <select class="shipping-input2 shipping-select clean same-height-with-date3">
                    <option class="shipping-option clean">All Product</option>
                    <option class="shipping-option clean">Synthetic Plus Oil - SAE 5W - 30</option>
                    <option class="shipping-option clean">Synthetic Plus Oil - SAE 10W - 40</option>
                    <option class="shipping-option clean">DCK Fuel Booster</option>
                    <option class="shipping-option clean">DCK Engine Oil Booster</option>
                </select>
            </div>
            <div class="shipping-input clean smaller-text2 second-shipping">
                <p>Type of Report</p>
                <select class="shipping-input2 shipping-select clean same-height-with-date3">
                    <option class="shipping-option clean">Overall (Product)</option>
                    <option class="shipping-option clean">Daily (Product)</option>
                    <option class="shipping-option clean">Overall (Member)</option>
                    <option class="shipping-option clean">Daily (Member)</option>                    
                </select>
            </div>
     
            <button class="clean black-button shipping-search-btn second-shipping same-height-with-date2">Search</button>

    </div>    

    <div class="clear"></div>

    <div class="width100 shipping-div2">
    	<div class="overflow-scroll-div">
        	<p class="table-subtitle">Synthetic Plus Oil - SAE 5W - 30</p>
            <table class="shipping-table payout-table">
                <thead>
                    <tr class="payout-th-tr">
                        <th>NO.</th>
                        <th>Date</th>
                        <th>QUANTITY</th>
                        <th>TOTAL (RM)</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1.</td>
                        <td>2019-08-15</td>
                        <td>1,000</td>
                        <td>200,000.00</td>
                    </tr>
                    <tr>
                        <td>2.</td>
                        <td>2019-08-15</td>
                        <td>700</td>
                        <td>140,000.00</td>
                    </tr>   
                    <tr class="total-tr">
                        <td></td>
                        <td><b>Total</b></td>
                        <td><b>1,700</b></td>
                        <td><b>340,000.00</b></td>
                    </tr>
                  </tbody>                                                       
            </table>
        </div>
    </div>

    <div class="clear"></div>

    <div class="width100 shipping-div2">
    	<div class="overflow-scroll-div">
        	<p class="table-subtitle">Synthetic Plus Oil - SAE 10W - 40</p>
            <table class="shipping-table payout-table">
                <thead>
                    <tr class="payout-th-tr">
                        <th>NO.</th>
                        <th>Date</th>
                        <th>QUANTITY</th>
                        <th>TOTAL (RM)</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1.</td>
                        <td>2019-08-15</td>
                        <td>1,000</td>
                        <td>200,000.00</td>
                    </tr>
                    <tr>
                        <td>2.</td>
                        <td>2019-08-15</td>
                        <td>700</td>
                        <td>140,000.00</td>
                    </tr>   
                    <tr class="total-tr">
                        <td></td>
                        <td><b>Total</b></td>
                        <td><b>1,700</b></td>
                        <td><b>340,000.00</b></td>
                    </tr>
                  </tbody>                                                       
            </table>
        </div>
    </div>
    <div class="clear"></div>

    <div class="width100 shipping-div2">
    	<div class="overflow-scroll-div">
        	<p class="table-subtitle">DCK Fuel Booster</p>
            <table class="shipping-table payout-table">
                <thead>
                    <tr class="payout-th-tr">
                        <th>NO.</th>
                        <th>Date</th>
                        <th>QUANTITY</th>
                        <th>TOTAL (RM)</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1.</td>
                        <td>2019-08-15</td>
                        <td>1,000</td>
                        <td>200,000.00</td>
                    </tr>
                    <tr>
                        <td>2.</td>
                        <td>2019-08-15</td>
                        <td>700</td>
                        <td>140,000.00</td>
                    </tr>   
                    <tr class="total-tr">
                        <td></td>
                        <td><b>Total</b></td>
                        <td><b>1,700</b></td>
                        <td><b>340,000.00</b></td>
                    </tr>
                  </tbody>                                                       
            </table>
        </div>
    </div>
    <div class="clear"></div>    

    <div class="width100 shipping-div2">
    	<div class="overflow-scroll-div">
        	<p class="table-subtitle">DCK Engine Oil Booster</p>
            <table class="shipping-table payout-table">
                <thead>
                    <tr class="payout-th-tr">
                        <th>NO.</th>
                        <th>Date</th>
                        <th>QUANTITY</th>
                        <th>TOTAL (RM)</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1.</td>
                        <td>2019-08-15</td>
                        <td>1,000</td>
                        <td>200,000.00</td>
                    </tr>
                    <tr>
                        <td>2.</td>
                        <td>2019-08-15</td>
                        <td>700</td>
                        <td>140,000.00</td>
                    </tr>   
                    <tr class="total-tr">
                        <td></td>
                        <td><b>Total</b></td>
                        <td><b>1,700</b></td>
                        <td><b>340,000.00</b></td>
                    </tr>
                  </tbody>                                                       
            </table>
        </div>
    </div>
    <div class="clear"></div>     
    
        
</div>


<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

</body>
</html>