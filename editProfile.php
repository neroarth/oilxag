<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Images.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    // echo $_POST['update_icno'].'<br>';

    $icNo = rewrite($_POST["update_icno"]);
    $username = rewrite($_POST["update_username"]);
    $fullname = rewrite($_POST["update_fullname"]);
    $birthday = rewrite($_POST["update_birthday"]);
    $gender = rewrite($_POST["update_gender"]);
    $phoneNo = rewrite($_POST["update_phoneno"]);
    $address = rewrite($_POST["update_address"]);

    $bankName = rewrite($_POST["update_bankname"]);
    $bankAccountHolder = rewrite($_POST["update_bankaccountholder"]);
    $bankAccountNo = rewrite($_POST["update_bankaccountnumber"]);

    $carModel = rewrite($_POST["update_carmodel"]);
    $carYear = rewrite($_POST["update_caryear"]);

    // $email = rewrite($_POST['update_email']);
    $email = rewrite($_POST["update_email"]);
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
    {
        echo "email confic";
        //$emailErr = "Invalid email format"; 
    }
}

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];
$userPic = getImages($conn," WHERE pid = ? ",array("pid"),array($userDetails->getPicture()),"s");
$userProPic = $userPic[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/editProfile.php" />
    <meta property="og:title" content="Edit Profile | DCK Supreme" />
    <title>Edit Profile | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/editProfile.php" />
    <?php include 'css.php'; ?>    
</head>
<body class="body">
<?php include 'header-sherry.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<!--<form method="POST">-->
    <?php
//    if(isset($_POST['product-list-quantity-input'])){
//        createProductList($products,$_POST['product-list-quantity-input']);
//    }else{
//        createProductList($products);
//    }

    ?>
<!--    <button type="submit" name="addToCartButton" id="addToCartButton" >Add to cart</button>-->

<!-- <form method="POST" onsubmit="return editprofileFunc(name);"> -->
<div class="yellow-body padding-from-menu same-padding">


<form method="POST" onsubmit="return editprofileFunc(name);" action="utilities/editProfileFunction.php">
    <div class="edit-profile-div2">
        <h1 class="username"> <?php echo $userDetails->getUsername();?> </h1>
        <h2 class="profile-title">BASIC INFORMATION</h2>
        <table class="edit-profile-table">
        	<tr class="profile-tr">
            	<td class="profile-td1">IC Number</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><input id="update_icno"class="clean edit-profile-input" type="text" value="<?php echo $userDetails->getIcNo();?>" name="update_icno"></td>
                <!-- <td class="profile-td3"><input class="clean edit-profile-input" type="text" placeholder="880609-02-9870"></td> -->
            </tr>
        	<tr class="profile-tr">
                <!-- <td class="profile-td1">First Name</td> -->
                <td class="profile-td1">Username</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><input id="update_username" class="clean edit-profile-input" type="text" placeholder="" value="<?php echo $userDetails->getUsername();?>" name="update_username" required></td>
            </tr>            
        	<tr class="profile-tr">
            	<td class="profile-td1">Full Name</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><input id="update_fullname" class="clean edit-profile-input" type="text" placeholder="" value="<?php echo $userDetails->getFullname();?>" name="update_fullname"></td>
            </tr>
        	<tr class="profile-tr">
            	<td class="profile-td1">Birthday</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><input id="update_birthday" class="clean edit-profile-input" type="text" placeholder="YYYY-MM-DD" value="<?php echo $userDetails->getBirthday();?>" name="update_birthday"></td>
            </tr>

        	<tr class="profile-tr">
            	<td class="profile-td1">Gender </td>
                <td class="profile-td2">:</td>
                <td class="profile-td3">
                	<select class="edit-profile-input edit-profile-select clean" id="update_gender" value="<?php echo $userDetails->getGender();?>" name="update_gender">
                                  
                                    <?php 
                                    if($userDetails->getGender() == 'Male')
                                    {
                                        ?>
                                        <option value="Female"  name='Female'>Female</option>
                                        <option selected value="Male"  name='male'>Male</option>
                                        <?php 
                                    }
                                    else
                                    {
                                        ?>
                                        <option selected value="Female"  name='Female'>Female</option>
                                        <option value="Male"  name='male'>Male</option>
                                        <?php 
                                    }
                                    ?>
                              
                    </select><img src="img/dropdown2.png" class="dropdown-png">
                </td>
            </tr>                                    
        </table>

        <h2 class="profile-title">BANK INFORMATION</h2>
        <table class="edit-profile-table">
        <tr class="profile-tr">
                <td class="profile-td1">Bank</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3">
 
                    <select class="edit-profile-input edit-profile-select clean" type="text" id="update_bankname" name="update_bankname">
                        <option value=" - " name=" - ">PLEASE SELECT A BANK</option>
                        <option value="AMBANK (M) BERHAD" name="AMBANK (M) BERHAD">AMBANK (M) BERHAD</option>
                        <option value="CIMB BANK BERHAD" name="CIMB BANK BERHAD">CIMB BANK BERHAD</option>
                        <option value="HONG LEONG BANK BERHAD" name="HONG LEONG BANK BERHAD">HONG LEONG BANK BERHAD</option>
                        <option value="MAYBANK ISLAMIC BERHAD" name="MAYBANK ISLAMIC BERHAD">MAYBANK ISLAMIC BERHAD</option>
                        <option value="PUBLIC BANK BERHAD" name="PUBLIC BANK BERHAD">PUBLIC BANK BERHAD</option>
                    </select>
                </td>
            </tr> 
        	<tr class="profile-tr">
                <td class="profile-td1">Acc. Holder Name</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><input id="update_bankaccountholder" class="clean edit-profile-input" type="text" value="<?php echo $userDetails->getBankAccountHolder();?>" name="update_bankaccountholder"></td>
            </tr>       
            <tr class="profile-tr">
                <td class="profile-td1">Acc. Number</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><input id="update_bankaccountnumber" class="clean edit-profile-input" type="text" value="<?php echo $userDetails->getBankAccountNo();?>" name="update_bankaccountnumber"></td>
            </tr>                     
        </table>

        <h2 class="profile-title">VEHICLES INFORMATION</h2>
        <table class="edit-profile-table">
        <tr class="profile-tr">
            	<td class="profile-td1">Car Model</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><input id="update_carmodel" class="clean edit-profile-input" type="text" value="<?php echo $userDetails->getCarModel();?>" name="update_carmodel"></td>
            </tr> 
        	<tr class="profile-tr">
            	<td class="profile-td1">Year</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><input id="update_caryear" class="clean edit-profile-input" type="text" value="<?php echo $userDetails->getCarYear();?>" name="update_caryear"></td>
            </tr>                           
        </table>

        <h2 class="profile-title">CONTACT INFORMATION</h2>
        <table class="edit-profile-table">
        	<tr class="profile-tr">
            	<td class="profile-td1">Email</td>
                <td class="profile-td2">:</td>
                <!-- <td class="profile-td3"><input id="update_email" class="clean edit-profile-input" type="email" placeholder="" value="<?php echo $userDetails->getEmail();?>" name="update_email"></td> -->
                <td class="profile-td3"><input id="update_email" class="clean edit-profile-input" type="email" placeholder="" 
                    value="<?php if(isset($userDetails)){echo $userDetails->getEmail();}?>" name="update_email">
                </td>                
            </tr>
        	<tr class="profile-tr">
            	<td class="profile-td1">Phone</td>
                <td class="profile-td2">:</td>
                <!-- <td class="profile-td3"><input class="clean edit-profile-input" type="number" placeholder="" value="<?php echo $userDetails->getPhoneNo();?>" name="update_phoneno"></td> -->
                <td class="profile-td3"><input id="update_phoneno" class="clean edit-profile-input" type="text" value="<?php echo $userDetails->getPhoneNo();?>" name="update_phoneno"></td>
            </tr>            
        	<tr class="profile-tr">
            	<td class="profile-td1">Address</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><input id="update_address" class="clean edit-profile-input" type="text" placeholder="" value="<?php echo $userDetails->getAddress();?>" name="update_address"></td>
            </tr>                   
        </table>
        

        <h2 class="profile-title">BANK INFORMATION</h2>
        <table class="edit-profile-table">
        	<tr class="profile-tr">
            	<td class="profile-td1">Acc. No.</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><input  class="clean edit-profile-input" type="text" placeholder="Bank Account Number" >
                </td>

                
            </tr>
        	<tr class="profile-tr">
            	<td class="profile-td1">Acc. Name</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><input class="clean edit-profile-input" type="text"   placeholder="Bank Account Name e.g. Tan Xiao Ming"></td>
            </tr>            
        	<tr class="profile-tr">
            	<td class="profile-td1">Bank</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><input id="update_address" class="clean edit-profile-input" type="text" placeholder="Bank" placeholder="Bank Name e.g. CIMB, Maybank"></td>
            </tr>
        	                               
        </table>        
        <button input type="submit" name="submit" value="Submit" class="confirm-btn text-center white-text clean black-button">Confirm</button>
		<!-- <button class="confirm-btn text-center white-text clean black-button">Confirm</button> -->
        <p class="change-password-p"><a href="editPassword.php" class="edit-password-a black-link">Edit Password</a></p>
    </div>
</div>
</form>


<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>
<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Data Update Successfully.";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "Fail To Update Data.";
        }
        if($_GET['type'] == 3)
        {
            $messageType = "Error";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<script>
function checkIfVariableIsNullOrEmptyString(field,isValidate)
{
if(field == null || field == "" || field.length == 0)
{
    isValidate += 1;
    return isValidate;
}
else
{
    return isValidate;
}
}

function editprofileFunc()
{
    // First Level Validation
    let isValidatedAndCanProceedToNextLevel = 0;

    let update_gender = $('#update_gender').val();
    let update_email = $('#update_email').val();
    
    // console.log('Initial Counter = '+isValidatedAndCanProceedToNextLevel);

    isValidatedAndCanProceedToNextLevel = checkIfVariableIsNullOrEmptyString(update_gender,isValidatedAndCanProceedToNextLevel);
    // console.log('counter = '+isValidatedAndCanProceedToNextLevel);
  
    isValidatedAndCanProceedToNextLevel = checkIfVariableIsNullOrEmptyString(update_email,isValidatedAndCanProceedToNextLevel);
    // console.log('counter = '+isValidatedAndCanProceedToNextLevel);
    
    //alert(isValidatedAndCanProceedToNextLevel);
    if(isValidatedAndCanProceedToNextLevel == 0)
    {
        // if(update_icno == null || update_icno == "" || update_icno.length == 0)
        if($("#update_gender").val() == "zero")
        {
            alert('Please Select Your Gender');
              event.preventDefault();
        }
        else{}
    }
    //email verified
    else
    {
      alert('Please Enter Email ! ');
      event.preventDefault();
    }    
}

// function updateUserDetails($conn,$icNo,$firstName,$lastName,$birthday,$gender,$phoneNo,$address,$email){
//     $isReferred = 0;
    
//     if(insertDynamicData($conn,"user",array("uid","ic_no","firstname","lastname","birthday","gender","phone_no","address","email"),
//         array($uid,$ic_no,$firstname,$lastname,$birthday,$gender,$phone_no,$address,$email),"sssssssss") === null){
//             promptSuccess("User Details Updated !");
//             goToConfirmationPage();
//     }else{}

//     return true;
// }
</script>

</body>
</html>