<?php
if (session_id() == ""){
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$uid = null;
$userRows = null;
$conn = connDB();


?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://dcksupreme.asia/adminPassword.php" />
<meta property="og:title" content="Set Password | DCK Supreme" />
<title>Set Password | DCK Supreme</title>
<meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
<meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
<meta name="keywords" content="DCK®, dck supreme,supreme,dck,  engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
noisiness and temperature, dry cold start,etc">
<link rel="canonical" href="https://dcksupreme.asia/adminPassword.php" />
<?php include 'css.php'; ?>
<?php require_once dirname(__FILE__) . '/header.php'; ?>
</head>

<body class="body">

<!-- Start Menu -->
<?php include 'header-sherry.php'; ?>
<div class="yellow-body padding-from-menu same-padding">
	<h1 class="success-h1 text-center">
    	Set Password
    </h1>
    <div class="reset-password-div">
        <form class="login-form" method="POST" action="utilities/resetPasswordFunction.php">
            <input type="hidden" name="checkThat" value="<?php if(isset($_GET['uid'])){echo $_GET['uid'] ;}?>">
            <div class="input-grey-div">
                <span class="input-span"><img src="img/lock.png" class="login-input-icon" alt="New Password" title="New Password"></span>
                <input name="verify_Pass" id="verify_Pass" required class="login-input password-input clean" type="password" placeholder="New Password">
                 <span class="visible-span"><img src="img/visible.png" class="login-input-icon" alt="View Password" title="View Password" id="verify_Pass_img"></span>
            </div>        
            <div class="input-grey-div">
                <span class="input-span"><img src="img/lock.png" class="login-input-icon" alt="Retype New Password" title="Retype New Password"></span>
                <input name="verify_Reenter" id="verify_Reenter" required class="login-input password-input clean" type="password" placeholder="Retype New Password">
                <span class="visible-span"><img src="img/visible.png" class="login-input-icon" alt="View Password" title="View Password" id="verify_Reenter_img"></span>
            </div>
               
            <div class="clear"></div>
            <button class="clean submit-black">SUBMIT</button>   
            
        </form>
     </div>


</div>
<?php include 'js.php'; ?>
<script>
  viewPassword( document.getElementById('verify_Code_img'), document.getElementById('verify_Code'));
  viewPassword( document.getElementById('verify_Pass_img'), document.getElementById('verify_Pass'));
  viewPassword( document.getElementById('verify_Reenter_img'), document.getElementById('verify_Reenter'));
</script>
<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Wrong Code Verification. <br>Please Try Again.";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Password must be more than 5. <br>Please Try Again";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Password Does Not Match. <br>Please Try Again";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Server Failure ! <br>Please Try Again Later In A Few Minutes.";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>
</body>
</html>