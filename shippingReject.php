<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php'; 
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

// $id = $_SESSION['order_id'];

$conn = connDB();

$productsOrders =  getProductOrders($conn);

date_default_timezone_set("Asia/Kuala_Lumpur");
$date = date("Y-m-d H:i:s"); 

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    if(isset($_POST["shipping_method"])){
        // $reject_reason_one = rewrite($_POST["reject_reason_one"]);
        $reject_reason = rewrite($_POST["reject_reason"]);
        $shipping_date = $date;
        $shipping_status = rewrite($_POST["shipping_status"]);
        $shipping_method = rewrite($_POST["shipping_method"]);
        $tracking_number = rewrite($_POST["tracking_number"]);
        $order_id = rewrite($_POST["order_id"]);
    }else{
        // $reject_reason_one = "";
        $reject_reason = "";
        $shipping_date = "";
        $shipping_status = "";
        $shipping_method = "";
        $tracking_number = "";
        $order_id = "";
    }
}

$conn->close();
function promptError($msg)
{
    echo '<script>  alert("'.$msg.'");  </script>';
}

function promptSuccess($msg)
{
    echo '<script>  alert("'.$msg.'");   </script>';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/shippingOut.php" />
    <meta property="og:title" content="Shipping Out | DCK Supreme" />
    <title>Shipping Out | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/shippingOut.php" />
    <?php include 'css.php'; ?>    
</head>
<body class="body">

<?php include 'header-sherry.php'; ?>



<div class="yellow-body padding-from-menu same-padding">
<form method="POST" action="utilities/rejectShippingFunction.php">

    <h1 class="details-h1" onclick="goBack()">
        <a class="black-white-link2 hover1">
            <img src="img/back.png" class="back-btn2 hover1a" alt="back" title="back">  
            <img src="img/back2.png" class="back-btn2 hover1b" alt="back" title="back">  
            Shipping Out Order Number : #<?php echo $_POST['order_id'];?>
        </a>  
    </h1>      


    <div class="width100 shipping-div2">
        <table class="details-table">
            <tbody>
            <?php 
            if(isset($_POST['order_id']))
            {
                $conn = connDB();
                //Order
                $orderArray = getOrders($conn,"WHERE id = ? ", array("id") ,array($_POST['order_id']),"i");
                //OrderProduct
                $orderProductArray = getProductOrders($conn,"WHERE order_id = ? ", array("order_id") ,array($_POST['order_id']),"i");
                //$orderDetails = $orderArray[0];
                
                if($orderArray != null)
                {
                    ?>
                    <tr>
                        <td>Name</td>
                        <td>:</td>
                        <td><?php echo $orderArray[0]->getName()?></td>
                        <!-- <td><?php //echo $orderArray[0]->getPrpduvtID->getName()?></td> -->
                    </tr>
                    <tr>
                        <td>Ship To</td>
                        <td>:</td>
                        <td>
                            <?php echo $orderArray[0]->getAddressLine1()?>
                            <?php echo $orderArray[0]->getAddressLine2()?>
                            <?php echo $orderArray[0]->getCity()?>
                            <?php echo $orderArray[0]->getZipcode()?>
                            <?php echo $orderArray[0]->getState()?>
                            <?php echo $orderArray[0]->getCountry()?>
                        </td>
                    </tr>
                    <?php
                    
                } 
            }       
            else
            {}
            $conn->close();
            ?>
            </tbody>
        </table>
    </div>

    <div class="search-container0">
        <div class="shipping-input clean smaller-text2 fifty-input">
            <p>Reason</p>
            <!-- <select class="shipping-input2 clean normal-input same-height-with-date" type="text" id="reject_reason_one" name="reject_reason_one"> -->
            <select class="shipping-input2 clean normal-input same-height-with-date" type="text" id="reject_reason" name="reject_reason">
                <option value="Out of delivery zone" name="Out of delivery zone" >Out of our delivery zone</option>
                <option value="Suspicious User" name="Suspicious User">Suspicious User</option>
            </select>
        </div>

        <!-- <div class="shipping-input clean smaller-text2 fifty-input">
            <textarea class="shipping-input2 clean normal-input same-height-with-date reason-textarea" placeholder="Or write a reason here"></textarea>
        </div>    -->

        <!-- <div class="shipping-input clean smaller-text2 fifty-input">
            <p>Default Date</p>
            <input class="shipping-input2 clean normal-input same-height-with-date" type="date" id="shipping_date" name="shipping_date">
        </div>   -->

        <input class="shipping-input2 clean normal-input same-height-with-date" type="hidden" id="order_id" name="order_id" value="<?php echo $orderArray[0]->getId()?>">
        <input class="shipping-input2 clean normal-input same-height-with-date" type="hidden" id="shipping_status" name="shipping_status" value="REJECT">
        <input class="shipping-input2 clean normal-input same-height-with-date" type="hidden" id="shipping_method" name="shipping_method" value="-">
        <input class="shipping-input2 clean normal-input same-height-with-date" type="hidden" id="tracking_number" name="tracking_number" value="-">
    </div> 

    <div class="clear"></div>

    <div class="three-btn-container">
        <!-- <a href="orderDetails.php" class="shipout-btn-a black-button three-btn-a float-left"><b>REJECT</b></a> -->
        <button input type="submit" name="submit" value="ShipOut" class="shipout-btn-a black-button three-btn-a float-left">REJECT</button>
    </div>  

</form>
</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<script>
function goBack() {
  window.history.back();
}
</script>

</body>
</html>