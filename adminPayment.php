<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php'; 
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';

//Product Order
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();

// $productsOrders =  getProductOrders($conn," WHERE quantity > 2 ");
$productsOrders =  getProductOrders($conn);

$conn->close();

function promptError($msg){
    echo ' 
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/adminShipping.php" />
    <meta property="og:title" content="Shipping | DCK Supreme" />
    <title>Shipping | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/adminShipping.php" />
    <?php include 'css.php'; ?>    
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">
    <!-- <h1 class="h1-title h1-before-border shipping-h1">Payment Verification | <a href="adminShippingComp.php" class="white-text title-tab-a">Completed</a></h1> -->
    <h1 class="h1-title h1-before-border shipping-h1">Payment Verification </h1>
  
    <div class="clear"></div>

    <div class="width100 shipping-div2">
    <?php $conn = connDB();?>
    <table class="shipping-table">
    <!-- <form action="shippingOut.php" method="POST"> -->
    <!-- <form action="shippingReject.php" method="POST"> -->
    
        <thead>
            <tr>
                <th>NO.</th>
                <th>ORDER NUMBER</th>
                <th>USERNAME</th>
                <th>ORDER DATE</th>
                <th>PRICE (RM)</th>
                <th>VERIFY</th>
            </tr>
        </thead>
        <tbody>
        <?php
        for($cnt = 0;$cnt < count($productsOrders) ;$cnt++)
        {   
            $orderDetails = getOrders($conn," WHERE id = ? AND payment_status = 'PENDING' ",array("id"),array($productsOrders[$cnt]->getOrderId()),"s");
            if($orderDetails != null)
            {
                for($cntAA = 0;$cntAA < count($orderDetails) ;$cntAA++)
                {?>
                <tr>
                    <td><?php echo ($cnt+1)?></td>
                    <td>#<?php echo $productsOrders[$cnt]->getOrderId();?></td>
                    <td><?php echo $orderDetails[$cntAA]->getUsername();?></td>
                    <td><?php $dateCreated = date("Y-m-d",strtotime($orderDetails[$cntAA]->getDateCreated()));
                            echo $dateCreated;?></td>
                    <td><?php echo $productsOrders[$cnt]->getTotalProductPrice();?></td>
                    <td>
                        <!-- <form action="shippingOut.php" method="POST"> -->
                        <form action="paymentverification.php" method="POST">
                            <button class="clean edit-anc-btn hover1" type="submit" name="order_id" value="<?php echo $productsOrders[$cnt]->getOrderId();?>">
                                <img src="img/verify-payment.png" class="edit-announcement-img hover1a" alt="Verify Payment" title="Verify Payment">
                                <img src="img/verify-payment2.png" class="edit-announcement-img hover1b" alt="Verify Payment" title="Verify Payment">
                            </button>
                        </form>
                    </td>
                </tr>
                <?php
                }
            } 
           
        }
        ?>
        </tbody>
    <!-- </form> -->
    </table>
    <?php $conn->close();?>
    </div>  
     
</div>


<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Shipping Details Update Successfully.";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "Fail To Update Data.";
        }
        if($_GET['type'] == 3)
        {
            $messageType = "Error";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<script>
$(function () {
    $('.link-to-details').click(function () {
        window.location.href = $(this).data('url');
    });
})
</script>

</body>
</html>