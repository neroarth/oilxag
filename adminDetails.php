<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php'; 
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $uid = md5(uniqid());
    $fullName = rewrite($_POST['fullName']);
    $username = rewrite($_POST['username']);
    $phoneNo = rewrite($_POST['phoneNo']);
    $userType = rewrite($_POST['userType']);

    $register_email_user = rewrite($_POST['email']);
    $register_email_user = filter_var($register_email_user, FILTER_SANITIZE_EMAIL);

    $register_password = $_POST['register_password'];
    $password = hash('sha256',$register_password);
    $salt = substr(sha1(mt_rand()), 0, 100);
    $finalPassword = hash('sha256', $salt.$password);
}


$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/adminDetails.php" />
    <meta property="og:title" content="Admin  Details | DCK Supreme" />
    <title>Admin  Details | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/adminDetails.php" />
    <?php include 'css.php'; ?>    
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding product-det admin-big-div">

<form method="POST" action="utilities/adminNewAdmin.php">
<h1 class="details-h1">Add Admin</h1>

    <div class="left50">
    	<p>Name</p>
        <input  class="clean product-input" type="text" placeholder="Name" id="fullName" name="fullName">
    </div> 
    <div class="left50 right50">
    	<p>Username</p>
        <input class="clean product-input" type="text" placeholder="Username" id="username" name="username">
    </div>
    <div class="left50">
    	<p>Contact</p>
        <input class="clean product-input" type="number" placeholder="Contact Number" id="phoneNo" name="phoneNo">
    </div>      
    <div class="left50 right50">
    	<p>Email</p>
        <input class="clean product-input" type="email" placeholder="Email" id="email" name="email">
    </div>     
    
    <div class="left50">
    	<!-- <p>Status</p>
    	<select class="clean product-input select-input2">
        	<option>Active</option>
            <option>Inactive</option>
        </select> -->
    </div>     

    <div class="left50 right50">
        <input class="clean product-input" type="hidden" value="123321" id="register_password" name="register_password">
        <input class="clean product-input" type="hidden" value="0" id="userType" name="userType">
    </div> 
    
    <div class="clear"></div> 
    <p class="table-subtitle access-level">Access Level:</p>
    <!-- add check or uncheck all function for this box, can see the reference inside the resources folder--> 
	<div class="width100 same-size">
    	<input type="checkbox" name="" value=""> Check or Uncheck All
    </div>
    
    <div class="clear"></div>
    <div class="distance-bit"></div>
    
	<div class="left50">
    	<table>
        	<tr>
        		<td><input type="checkbox" name="" value=""></td><td> Add/Edit Admin</td>
            </tr>
        </table>
    </div>
    <div class="left50 right50">
    	<table>
        	<tr>    
    			<td><input type="checkbox" name="" value=""></td><td> Add/Edit Product Details</td>
            </tr>
        </table>
    </div>    
	<div class="left50">
    	<table>
        	<tr>    
    			<td><input type="checkbox" name="" value=""></td><td>View Sales</td>
            </tr>
        </table>
    </div>
    <div class="left50 right50">
    	<table>
        	<tr>    
    			<td><input type="checkbox" name="" value=""></td><td>View Product Details</td>
            </tr>
        </table>
    </div>      
	<div class="left50">
    	<table>
        	<tr>    
    			<td><input type="checkbox" name="" value=""></td><td>View Payout</td>
            </tr>
        </table>
    </div>
    <div class="left50 right50">
    	<table>
        	<tr>    
    			<td><input type="checkbox" name="" value=""></td><td>Add Announcement</td>
            </tr>
        </table>
    </div>  
	<div class="left50">
    	<table>
        	<tr>    
    			<td><input type="checkbox" name="" value=""></td><td>Handle Shipping Request</td>
            </tr>
        </table>
    </div>
    <div class="left50 right50">
    	<table>
        	<tr>    
    			<td><input type="checkbox" name="" value=""></td><td>Handle Withdrawal Request</td>
            </tr>
        </table>
    </div>   
	<div class="left50">
    	<table>
        	<tr>    
    			<td><input type="checkbox" name="" value=""></td><td>Preset Reason</td>
            </tr>
        </table>
    </div>
    <div class="left50 right50">
    	<table>
        	<tr>    
    			<td><input type="checkbox" name="" value=""></td><td>Adjust Commission Rate, etc</td>
            </tr>
        </table>
    </div>         
    <div class="clear"></div>
    <div class="three-btn-container">
        <!-- <a href="adminAdmin.php" class="shipout-btn-a black-button three-btn-a"><b>Confirm</b></a> -->
        <button input type="submit" name="submit" value="Submit" class="shipout-btn-a black-button three-btn-a">CONFIRM</button>
    </div>  
</form>
</div>

</div>
<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

</body>
</html>