<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php'; 
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();

$adminList = getUser($conn," WHERE user_type = ? ORDER BY date_created asc ",array("user_type"),array(0),"i");
//for memberlist
// $adminList = getUser($conn," WHERE user_type = ? ",array("user_type"),array(1),"i");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/admin.php" />
    <meta property="og:title" content="Admin | DCK Supreme" />
    <title>Admin | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/admin.php" />
    <?php include 'css.php'; ?>    
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">
    <h1 class="h1-title h1-before-border shipping-h1">Admin</h1>
    
    <!-- This is a filter for the table result -->
    <!-- <select class="filter-select clean">
    	<option class="filter-option">Latest</option>
    	<option class="filter-option">Oldest</option>    
    </select> -->

    <button name="add_new_admin" class="confirm-btn text-center white-text clean black-button right-add-btn"><a href="adminDetails.php" class="add-a">Add</a></button>
    <!-- End of Filter -->

    <!-- <div class="clear"></div> -->

	<!-- <div class="search-container0">

            <div class="shipping-input clean smaller-text2">
                <p>Name</p>
                <input class="shipping-input2 clean normal-input" type="text" placeholder="Name">
            </div>
            <div class="shipping-input clean smaller-text2 middle-shipping-div second-shipping">
                <p>Contact</p>
                <input class="shipping-input2 clean normal-input" type="number" placeholder="Contact">
            </div>
            <div class="shipping-input clean smaller-text2">
                <p>Email</p>
                <input class="shipping-input2 clean normal-input" type="email" placeholder="Email">
            </div>            
            <div class="shipping-input clean smaller-text2 second-shipping">
                <p>Start Date</p>
                <input class="shipping-input2 clean" type="date" placeholder="Start Date">
            </div>

     
            <div class="shipping-input clean middle-shipping-div smaller-text2">
                <p>End Date</p>
                <input class="shipping-input2 clean" type="date" placeholder="End Date">
            </div>

            <button class="clean black-button shipping-search-btn second-shipping">Search</button>

    </div>     -->

    <div class="clear"></div>

    <div class="width100 shipping-div2">
    	<div class="overflow-scroll-div">
            <table class="shipping-table">
                <thead>
                    <tr>
                        <th>NO.</th>
                        <th>NAME</th>
                        <th>JOINED DATE</th>
                        <th>CONTACT</th>
                        <th>EMAIL</th>
                        <th>STATUS</th>
                    </tr>
                </thead>
                <tbody>

                <?php
                if($adminList)
                {
                    for($cnt = 0;$cnt < count($adminList) ;$cnt++)
                    {?>
                        <tr class="link-to-details">
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $adminList[$cnt]->getFullname();?></td>
                            <td>
                                <?php $dateCreated = date("Y-m-d",strtotime($adminList[$cnt]->getDateCreated()));echo $dateCreated;?>
                            </td>
                            <td><?php echo $adminList[$cnt]->getPhoneNo();?></td>
                            <td><?php echo $adminList[$cnt]->getEmail();?></td>
                            <td><?php echo $adminList[$cnt]->getisEmailVerified();?></td>
                        </tr>
                        <?php
                    }
                }
                ?>
                </tbody>           
            </table>
        </div>
    </div>

    <div class="clear"></div>

    <!-- <div class="bottom-big-container">
    	<div class="left-btm-page">
        	Page <select class="clean transparent-select"><option>1</option></select> of 1
        </div>
        <div class="middle-btm-page">
        	<a class="round-black-page">1</a>
            <a class="round-white-page">2</a>
        </div>
        <div class="right-btm-page">
        	Total: 2
        </div>
    </div>  -->

</div>


<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Add New Admin";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "Error";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>