<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Announcement.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$announcementMessagesArray = getAnnouncement($conn,"WHERE announce_showThis = ? ORDER BY announce_dateCreated DESC ",array("announce_showThis"),array(1),"i");

$conn->close();
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://dcksupreme.asia/announcement.php" />
<meta property="og:title" content="Announcement | DCK Supreme" />
<title>Announcement | DCK Supreme</title>
<meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
<meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
<meta name="keywords" content="DCK®,dck, dck supreme, supreme, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
noisiness and temperature, dry cold start,etc">
<link rel="canonical" href="https://dcksupreme.asia/announcement.php" />
<?php include 'css.php'; ?>
<?php require_once dirname(__FILE__) . '/header.php'; ?>
</head>

<body class="body">
<!-- Start Menu -->

<div class="yellow-body padding-from-menu same-padding">
	<?php include 'header-sherry.php'; ?>
    <h1 class="cart-h1 m-btm-0 announcement-h1">
        <img src="img/announcement.png" alt="Announcement" title="Announcement" class="announcement-icon">
        Announcement
    </h1>
    <?php 
    if(isset($_SESSION['usertype_level']) && $_SESSION['usertype_level'] == 0)
    {
    ?> 
    <button name="add_new_announcement" class="confirm-btn text-center white-text clean black-button right-add-btn"><a href="announcement_crud.php" class="add-a">Add</a></button>
    <?php 
    }
    ?>
    <div class="clear"></div>
    <div class="extra-mtop3">
        <table class="announcement-table">
            <thead>
                <tr class="thicker-border">
                    <th class="announcement-p no-padding-left">Date</th>
                    <th class="announcement-p">Content</th>
                    <?php 
                    if(isset($_SESSION['usertype_level']) &&$_SESSION['usertype_level'] == 0)
                    {
                    ?> 
                    <th></th>
                    </td>
                        <?php 
                    }
                    ?>
                </tr>
            </thead>
            <?php
            for($counter = 0; $counter < count($announcementMessagesArray); $counter++)
            {
                $announcementMessages = $announcementMessagesArray[$counter];
                ?>
                <tr>
                    <td class="announcement-p no-padding-left">
                        <?php
                            $dateCreated = date("Y-m-d",strtotime($announcementMessages->getDateCreated()));
                            echo $dateCreated;
                        ?>
                    </td>
                    <td class="announcement-p">
                        <?php
                            echo $announcementMessages->getMessage();
                        ?>
                    </td>
                    <?php 
                    if(isset($_SESSION['usertype_level']) && $_SESSION['usertype_level'] == 0)
                    {
                        ?> 
                        <td class="right-cell">
                            <form action="announcement_crud.php" method="POST">
                                <button class="clean edit-anc-btn hover1" type="submit" name="announcement_id" value="<?php echo $announcementMessages->getId();?>">
                                    <img src="img/edit.png" class="edit-announcement-img hover1a" alt="Edit Announcement" title="Edit Announcement">
                                    <img src="img/edit2.png" class="edit-announcement-img hover1b" alt="Edit Announcement" title="Edit Announcement">
                                </button>
                            </form>
                        </td>
                        <?php 
                    }
                    ?>
                   
                </tr>
                <?php
            }

            ?>
        </table>    	
    </div>
</div>
<?php include 'js.php'; ?>
<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully created announcement.";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Successfully updated announcement.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Successfully deleted announcement.";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>
</body>
</html>