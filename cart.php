<?php
if (session_id() == ""){
    session_start();
}

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST'){

}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://dcksupreme.asia/cart.php" />
<meta property="og:title" content="Cart | DCK Supreme" />
<title>Cart | DCK Supreme</title>
<meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
<meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
<meta name="keywords" content="DCK®,dck, dck supreme, supreme, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
noisiness and temperature, dry cold start,etc">
<link rel="canonical" href="https://dcksupreme.asia/cart.php" />
<?php include 'css.php'; ?>
<?php require_once dirname(__FILE__) . '/header.php'; ?>
</head>

<body class="body">
<!--
<div id="overlay">
 <div class="center-food"><img src="img/loading-gif.gif" class="food-gif"></div>
 <div id="progstat"></div>
 <div id="progress"></div>
</div>-->

<!-- Start Menu -->

<div class="yellow-body padding-from-menu same-padding">
	<?php include 'header-sherry.php'; ?>
    <h1 class="cart-h1">Your Cart</h1>
    <div class="border-div-bk table-wrapper">
    	<div class="table-scroll">
            <table class="cart-table">
                <thead>
                    <tr>
                    	<th>NO.</th>
                        <th>PRODUCT</th>
                        <th></th>
                        <th class="quantity-td">QUANTITY</th>
                        <th>PRICE</th>
                        <th>TOTAL</th>
                        <th class="no6"></th>
                    </tr>
                </thead>
                <tr>
                	<td>1</td>
                    <td class="product-td"><img src="img/product-potrait.png" class="product-pic2" alt="DCK Engine Oil Booster" title="DCK Engine Oil Booster"></td>
                    <td>DCK Engine Oil Booster</td>
                    <td class="quantity-td">
                        <div class="input-group transparent-group">
                          <input type="button" value="-" class="button-minus math-button clean" data-field="quantity">
                          <input type="number" step="1" max="" value="1" name="quantity" class="quantity-field math-input clean">
                          <input type="button" value="+" class="button-plus math-button clean" data-field="quantity">
                        </div> 
                    </td>
                    <td>RM80.00</td>
                    <td>RM160.00</td>
                    <td class="no6"><img src="img/delete0.png" alt="delete" title="delete" class="delete-img"></td>
                </tr>
                <tr>
                	<td>2</td>
                    <td class="product-td"><img src="img/product-potrait.png" class="product-pic2" alt="DCK Engine Oil Booster" title="DCK Engine Oil Booster"></td>
                    <td>DCK® Engine Fuel Booster</td>
                    <td class="quantity-td">
                        <div class="input-group transparent-group">
                          <input type="button" value="-" class="button-minus math-button clean" data-field="quantity">
                          <input type="number" step="1" max="" value="1" name="quantity" class="quantity-field math-input clean">
                          <input type="button" value="+" class="button-plus math-button clean" data-field="quantity">
                        </div> 
                    </td>
                    <td>RM90.00</td>
                    <td>RM180.00</td>
                    <td class="no6"><img src="img/delete0.png" alt="delete" title="delete" class="delete-img"></td>
                </tr>           
            </table>
        </div>
    </div>
    <div class="cart-bottom-div">
    	<div class="left-cart-bottom-div">
        	<p class="continue-shopping pointer"  onclick="goBack()"><img src="img/back.png" class="back-btn" alt="back" title="back" > Continue Shopping</p>
        </div>
        <div class="right-cart-div">
        	<h1 class="product-name-h1 sub-total">SUBTOTAL:  RM 340.00</h1>
            <button class="clean black-button add-to-cart-btn checkout-btn">CHECKOUT</button>
        </div>
    
    </div>
    
	
    	
    	
        
        

</div>
<script>
function goBack() {
  window.history.back();
}
</script>
<script>
function incrementValue(e) {
  e.preventDefault();
  var fieldName = $(e.target).data('field');
  var parent = $(e.target).closest('div');
  var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

  if (!isNaN(currentVal)) {
    parent.find('input[name=' + fieldName + ']').val(currentVal + 1);
  } else {
    parent.find('input[name=' + fieldName + ']').val(0);
  }
}

function decrementValue(e) {
  e.preventDefault();
  var fieldName = $(e.target).data('field');
  var parent = $(e.target).closest('div');
  var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

  if (!isNaN(currentVal) && currentVal > 0) {
    parent.find('input[name=' + fieldName + ']').val(currentVal - 1);
  } else {
    parent.find('input[name=' + fieldName + ']').val(0);
  }
}

$('.input-group').on('click', '.button-plus', function(e) {
  incrementValue(e);
});

$('.input-group').on('click', '.button-minus', function(e) {
  decrementValue(e);
});

</script>

<?php include 'js.php'; ?>
<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully register your profile! Please confirm your registration inside of your email.";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "There are no referrer with this email ! Please register again";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Successfully register your profile! Please confirm your registration inside of your email.";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "User password must be more than 5 !";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "User password does not match";
        }
        else if($_GET['type'] == 6)
        {
            $messageType = "Wrong email format.";
        }
        else if($_GET['type'] == 7)
        {
            $messageType = "There are no user with this email ! Please try again.";
        }
        else if($_GET['type'] == 8)
        {
            $messageType = "Successfully reset your password! Please check your email.";
        }
        else if($_GET['type'] == 9)
        {
            $messageType = "Successfully reset your password! ";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
if(isset($_GET['promptError']))
{
    $messageType = null;

    if($_GET['promptError'] == 1)
    {
        $messageType = "Error registering new account.The account already exist";
    }
    else if($_GET['promptError'] == 2)
    {
        $messageType = "Error assigning referral relationship. Please register again.";
    }
    echo '
    <script>
        putNoticeJavascript("Notice !! ","'.$messageType.'");
    </script>
    ';   
}
?>
</body>
</html>