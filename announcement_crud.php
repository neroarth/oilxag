<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php'; 
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Announcement.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';


// unset($_POST);
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://dcksupreme.asia/announcement_crud.php" />
<meta property="og:title" content="Edit Announcement | DCK Supreme" />
<title>Edit Announcement | DCK Supreme</title>
<meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
<meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
<meta name="keywords" content="DCK®,dck, dck supreme, supreme, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
noisiness and temperature, dry cold start,etc">
<link rel="canonical" href="https://dcksupreme.asia/announcement_crud.php" />
<?php include 'css.php'; ?>
<?php require_once dirname(__FILE__) . '/header.php'; ?>
</head>

<body class="body">
<!-- Start Menu -->

<?php 
if(isset($_POST['announcement_id']))
{

    $conn = connDB();
    $thisAnnouncementArray = getAnnouncement($conn,"WHERE announce_id = ? ",array("announce_id"),array($_POST['announcement_id']),"i");
    $thisAnnouncement = $thisAnnouncementArray[0];
    ?>
    <form class="yellow-body padding-from-menu same-padding" method="POST" action="utilities/announcementFunction.php">
        <?php include 'header-sherry.php'; ?>
        <h1 class="cart-h1 m-btm-0 announcement-h1">
            <img src="img/announcement.png" alt="Announcement" title="Announcement" class="announcement-icon">
            Edit Announcement
        </h1>
		<div class="clear"></div>
        <div class="border-announcement">
        <!-- resize: none; TO REMOVE resize grabber at textarea -->
            <textarea name="announcement_message" placeholder="Type your content here" class="anc-textarea"><?php echo $thisAnnouncement->getMessage();?></textarea>
        </div>

        <input type="hidden" name="announcement_id" value="<?php echo $_POST['announcement_id'];?>"> 

        <button type="submit" name="delete_new_announcement" class="confirm-btn text-center white-text clean red-button anc-ow-btn" value="0">
            Delete
        </button>

        <button type="submit" name="confirm_new_announcement" class="confirm-btn text-center white-text clean black-button anc-ow-btn" value="1">
            Confirm
        </button>
        <a class="back-a2"  onclick="goBack()">Back</a>
    </form>
    <?php 
}
else
{
    ?>
    <form class="yellow-body padding-from-menu same-padding" method="POST" action="utilities/announcementFunction.php">
        <?php include 'header-sherry.php'; ?>
        <h1 class="cart-h1 m-btm-0 announcement-h1">
            <img src="img/announcement.png" alt="Announcement" title="Announcement" class="announcement-icon">
            Add Announcement
        </h1>
		<div class="clear"></div>
        <div class="border-announcement">
        <!-- resize: none; TO REMOVE resize grabber at textarea -->
            <textarea name="announcement_message" placeholder="Type your content here"  class="anc-textarea"></textarea>
        </div>



        <button type="submit" name="confirm_new_announcement" class="confirm-btn text-center white-text clean black-button" value="1">
            Confirm
        </button>
        <button type="submit" name="cancel_new_announcement" class="confirm-btn text-center white-text clean no-background margin-right20" value="0">
            Cancel
        </button>        
    </form>
    <?php 
}
?>
<script>
function goBack() {
  window.history.back();
}
</script>
<?php include 'js.php'; ?>
<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Server currently fail. Please try again later.";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "There is no announcement message. Please try again.";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Server currently fail. Please try again later";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>
</body>
</html>