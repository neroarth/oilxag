<?php
if (session_id() == ""){
    session_start();
}

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$uid = null;
$userRows = null;
$conn = connDB();

if(isset($_GET['getVerified']))
{
    $uid = rewrite($_GET['getVerified']);
    echo $uid;
    if(updateDynamicData($conn,"user"," WHERE uid = ? ",array("is_email_verified"),array(1,$uid),"is"))
    {
        header( "refresh:3;url=index.php" );
    }
    else 
    {
        header('Location:index.php');
    }

    // echo $uid;
}
// header( "refresh:5;url=wherever.php" );

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://dcksupreme.asia/email-verified.php" />
<meta property="og:title" content="Engine Oil Booster | DCK Supreme" />
<title>Email Verified | DCK Supreme</title>
<meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
<meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
<meta name="keywords" content="DCK®, dck supreme,supreme,dck,  engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
noisiness and temperature, dry cold start,etc">
<link rel="canonical" href="https://dcksupreme.asia/email-verified.php" />
<?php include 'css.php'; ?>
<?php require_once dirname(__FILE__) . '/header.php'; ?>
</head>

<body class="body">

<!-- Start Menu -->
<?php include 'header-sherry.php'; ?>
<div class="yellow-body padding-from-menu same-padding">
	<h1 class="success-h1 text-center">
    	Success!
    </h1>
    <p class="success-p text-center">
    	Your email has been successfully verified!
    </p>


</div>
<?php include 'js.php'; ?>

</body>
</html>