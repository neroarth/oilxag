<?php
if (session_id() == ""){
    session_start();
}

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    //todo validation on server side
    //TODO change login with email to use username instead
    //TODO add username field to register's backend
    $conn = connDB();

    if(isset($_POST['loginButton'])){
        $email = rewrite($_POST['email']);
        $password = $_POST['password'];

        $userRows = getUser($conn," WHERE username = ? ",array("username"),array($email),"s");
        if($userRows)
        {
            $user = $userRows[0];

            if($user->getisEmailVerified() == 1)
            {
                $tempPass = hash('sha256',$password);
                $finalPassword = hash('sha256', $user->getSalt() . $tempPass);
    
                if($finalPassword == $user->getPassword()) 
                {
                    if(isset($_POST['remember-me'])) 
                    {
                        
                        setcookie('email-oilxag', $email, time() + (86400 * 30), "/");
                        setcookie('password-oilxag', $password, time() + (86400 * 30), "/");
                        setcookie('remember-oilxag', 1, time() + (86400 * 30), "/");
                        // echo 'remember me';
                    }
                    else 
                    {
                        setcookie('email-oilxag', '', time() + (86400 * 30), "/");
                        setcookie('password-oilxag', '', time() + (86400 * 30), "/");
                        setcookie('remember-oilxag', 0, time() + (86400 * 30), "/");
                        // echo 'null';
                    }

                    $_SESSION['uid'] = $user->getUid();
                    echo '<script>window.location.replace("profile.php");</script>';
                }
                else 
                {
                    promptError("Incorrect email or password");
                }
            }
            else 
            {
                promptError("Please confirm your registration inside your email");
            }

        }
        else
        {
            promptError("This account does not exist");
        }
    }

    $conn->close();
}

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://dcksupreme.asia/information.php" />
<meta property="og:title" content="Information | DCK Supreme" />
<title>Information | DCK Supreme</title>
<meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
<meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
<meta name="keywords" content="DCK®,dck, dck supreme, supreme, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
noisiness and temperature, dry cold start,etc">
<link rel="canonical" href="https://dcksupreme.asia/information.php" />
<?php include 'css.php'; ?>
<?php require_once dirname(__FILE__) . '/header.php'; ?>
</head>

<body class="body">
<!--
<div id="overlay">
 <div class="center-food"><img src="img/loading-gif.gif" class="food-gif"></div>
 <div id="progstat"></div>
 <div id="progress"></div>
</div>-->

<!-- Start Menu -->
	<?php include 'header-sherry.php'; ?>
<div class="flex-container">    
    <div class="left-status-div">
    	<!-- Start of Check Out Status Div-->
        <div class="check-out-status">
            <div class="status-container">
                <a href="cart.php" class="status-a">
                    <div class="black-round-div"><img src="img/tick2.png" class="yellow-tick" alt="Completed" title="Completed"></div>
                    CART
                </a>
            </div>
            <div class="status-div">></div>
            <div class="status-container">        
                <div class="white-round-div">2</div>
                INFORMATION
            </div>
            <div class="status-div">></div>
            <div class="status-container">        
                <div class="white-round-div">3</div>
                SHIPPING
            </div>        
            <div class="status-div">></div>
            <div class="status-container">        
                <div class="white-round-div">4</div>
                PAYMENT
            </div>                
        </div>
        <!-- End of Check Out Status Div-->
        <div class="clear"></div>
        <p class="info-small-title">Already have an account? <a class="open-login fake-link">Log In.</a></p>
        <p class="info-title"><b>CONTACT INFORMATION</b></p>
        <input class="clean white-input two-box-input" type="email" placeholder="Email">
 
        <p class="info-title spacing2"><b>SHIPPING ADDRESS</b></p>      
        <input class="clean white-input half-white-input left-half two-box-input-double" type="text" placeholder="First Name">  
        <input class="clean white-input half-white-input two-box-input-double right-part" type="text" placeholder="Last Name">
        <input class="clean white-input two-box-input" type="text" placeholder="Address">
        <input class="clean white-input half-white-input left-half two-box-input-double" type="text" placeholder="City">  
    	<select class="clean white-input white-select half-white-input two-box-input-double right-part">
        	<option class="white-option">Country</option>
        </select>
    	<select class="clean white-input white-select half-white-input left-half two-box-input-double">
        	<option class="white-option">State</option>
        </select>  
        <input class="clean white-input half-white-input two-box-input-double right-part" type="number" placeholder="Postcode">  
        <input class="clean white-input two-box-input" type="number" placeholder="Contact Number">                      
        <div class="clear"></div>
        <div class="cart-bottom-div spacing2">
            <div class="left-cart-bottom-div">
                <p class="continue-shopping pointer continue2" ><a href="cart.php" class="black-white-link"><img src="img/back.png" class="back-btn back-btn2" alt="back" title="back" > Return to Cart</a></p>
            </div>
            <div class="right-cart-div">
    
                <button class="clean black-button add-to-cart-btn checkout-btn continue2 add-to-cart-btn2">NEXT</button>
            </div>
        </div>
    </div>

    
    
    
    <div class="right-status-div">    
    	<table class="info-table">
			<tr class="product-tr">
            	<td><img src="img/small-product-pic.png" class="info-product-pic" alt="DCK Engine Oil  Booster" title="DCK Engine Oil  Booster"><div class="product-amount-dot"><p class="info-amount-p">2</p></div></td>
                <td class="product-name-td white-text">DCK Engine Oil  Booster</td>
                <td class="product-name-td white-text product-price-td">RM160.00</td>
            </tr>
			<tr class="product-tr">
            	<td><img src="img/small-product-pic.png" class="info-product-pic" alt="DCK Fuel Booster" title="DCK Fuel Booster"><div class="product-amount-dot"><p class="info-amount-p">2</p></div></td>
                <td class="product-name-td white-text">DCK Fuel Booster</td>
                <td class="product-name-td white-text product-price-td">RM180.00</td>
            </tr> 
        </table>
        <table class="info-table">
            <tr class="calc-tr white-border-top">
            	<td class="product-name-td white-text">Subtotal</td>
                <td class="product-name-td white-text right-cell">RM340.00</td>
            </tr>  
            <tr class="calc-tr">
            	<td class="product-name-td white-text">Shipping</td>
                <td class="product-name-td white-text smaller-text right-cell">Calculated at next step</td>
            </tr>            
            <tr class="calc-tr">
            	<td class="product-name-td white-text">Use Points <input type="number" class="clean short-input" placeholder="0"></td>
                <td class="product-name-td white-text right-cell">-RM40.00</td>
            </tr>  
            <tr class="calc-tr">
            	<td class="product-name-td white-text">Voucher</td>
                <td class="product-name-td white-text right-cell">
                	<select class="voucher-select">
                    	<option>15%</option>
                        <option>30%</option>
                    </select>
                </td>
            </tr>
            <tr class="calc-tr white-border-top two-white-border">
            	<td class="product-name-td white-text">Total</td>
                <td class="product-name-td white-text right-cell">RM255.00</td>
            </tr>                                           
        </table>
    </div>	
</div>    	
        
        


<script>
function goBack() {
  window.history.back();
}
</script>
<script>
function incrementValue(e) {
  e.preventDefault();
  var fieldName = $(e.target).data('field');
  var parent = $(e.target).closest('div');
  var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

  if (!isNaN(currentVal)) {
    parent.find('input[name=' + fieldName + ']').val(currentVal + 1);
  } else {
    parent.find('input[name=' + fieldName + ']').val(0);
  }
}

function decrementValue(e) {
  e.preventDefault();
  var fieldName = $(e.target).data('field');
  var parent = $(e.target).closest('div');
  var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);

  if (!isNaN(currentVal) && currentVal > 0) {
    parent.find('input[name=' + fieldName + ']').val(currentVal - 1);
  } else {
    parent.find('input[name=' + fieldName + ']').val(0);
  }
}

$('.input-group').on('click', '.button-plus', function(e) {
  incrementValue(e);
});

$('.input-group').on('click', '.button-minus', function(e) {
  decrementValue(e);
});

</script>

<?php include 'js.php'; ?>
<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully register your profile! Please confirm your registration inside of your email.";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "There are no referrer with this email ! Please register again";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Successfully register your profile! Please confirm your registration inside of your email.";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "User password must be more than 5 !";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "User password does not match";
        }
        else if($_GET['type'] == 6)
        {
            $messageType = "Wrong email format.";
        }
        else if($_GET['type'] == 7)
        {
            $messageType = "There are no user with this email ! Please try again.";
        }
        else if($_GET['type'] == 8)
        {
            $messageType = "Successfully reset your password! Please check your email.";
        }
        else if($_GET['type'] == 9)
        {
            $messageType = "Successfully reset your password! ";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
if(isset($_GET['promptError']))
{
    $messageType = null;

    if($_GET['promptError'] == 1)
    {
        $messageType = "Error registering new account.The account already exist";
    }
    else if($_GET['promptError'] == 2)
    {
        $messageType = "Error assigning referral relationship. Please register again.";
    }
    echo '
    <script>
        putNoticeJavascript("Notice !! ","'.$messageType.'");
    </script>
    ';   
}
?>
</body>
</html>